// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module generic.set;

import std.range;
import std.traits;
import std.format;



struct Set(T) {
	private void[0][T] data;
	
	public this(T[] data) pure {
		this ~= data;
	}
	
	public this(Range)(Range data)
	if(isInputRange!Range && isImplicitlyConvertible!(ElementType!Range, T) && !is(Range == T[])) {
		this ~= data;
	}
	
	public void insert(inout T x) pure {
		data[cast(T)x] = (void[0]).init;
	}
	
	public void remove(inout T x) pure {
		data.remove(cast(T)x);
	}
	
	public void clear() pure {
		data.clear();
	}
	
	@property public bool contains(inout T x) const {
		return (cast(T)x in data) !is null;
	}
	
	@property public bool empty() const {
		return length == 0;
	}
	
	@property public size_t length() const {
		return data.length;
	}
	
	@property public Set!T dup() const {
		Set!T ret;
		ret.data = cast(void[0][T])data.dup;
		return ret;
	}
	
	public void toString(
		scope void delegate(const(char)[]) sink,
		FormatSpec!char fmt
	) const {
		formatValue(sink, data.keys, fmt);
	}
	
	public inout(T[]) opSlice() inout const {
		return data.keys;
	}
	
	public void rehash() pure {
		data.rehash();
	}
	
	public T removeAny() pure {
		auto result = data.keys[0];
		data.remove(result);
		return result;
	}
	
	public bool is_disjoint_from(inout Set!T other) inout const {
		if(other.length < this.length) { return other.is_disjoint_from(this); }
		foreach(x; data.keys) {
			if(other.contains(x)) { return false; }
		}
		return true;
	}
	
	public bool is_subset_of(inout Set!T other) inout const {
		if( ! (this.length <= other.length)) { return false; }
		foreach(x; data.keys) {
			if( ! other.contains(x)) { return false; }
		}
		return true;
	}
	
	public bool is_propper_subset_of(inout Set!T other) inout const {
		if( ! (this.length < other.length)) { return false; }
		foreach(x; data.keys) {
			if( ! other.contains(x)) { return false; }
		}
		return true;
	}
	
	public bool is_superset_of(inout Set!T other) inout const {
		return other.is_subset_of(this);
	}
	
	public bool is_propper_superset_of(inout Set!T other) inout const {
		return other.is_propper_subset_of(this);
	}
	
	public bool opEquals()(auto ref Set!T other) const {
		if(this.length != other.length) { return false; }
		foreach(x; data.keys) {
			if( ! other.contains(x)) { return false; }
		}
		return true;
	}
	
	// Intersection
	public Set!T opBinary(string op)(auto ref Set!T other) if (op == "&") {
		if(other.length < this.length) { return other & this; }
		
		Set!T ret;
		foreach(x; data.keys) {
			if(other.contains(x)) { ret ~= x; }
		}
		return ret;
	}
	
	public void opOpAssign(string op)(auto ref Set!T other) pure if (op == "&") {
		foreach(x; data.keys) {
			if( ! other.contains(x)) { remove(x); }
		}
	}
	
	// Union
	public Set!T opBinary(string op)(auto ref Set!T other) if (op == "|") {
		if(other.length < this.length) { return other | this; }
		
		return this.dup ~ other[];
	}
	
	public void opOpAssign(string op)(auto ref Set!T other) pure if (op == "|") {
		this ~= other[];
	}
	
	// Difference
	public Set!T opBinary(string op)(auto ref Set!T other) if (op == "-") {
		Set!T ret;
		foreach(x; data.keys) {
			if( ! other.contains(x)) { ret ~= x; }
		}
		return ret;
	}
	
	public void opOpAssign(string op)(auto ref Set!T other) pure if (op == "-") {
		foreach(x; other) { remove(x); }
	}
	
	// Symetric difference
	public Set!T opBinary(string op)(auto ref Set!T other) if (op == "^") {
		Set!T ret;
		foreach(x; this.data.keys) {
			if( ! other.contains(x)) { ret ~= x; }
		}
		foreach(x; other.data.keys) {
			if( ! this.contains(x)) { ret ~= x; }
		}
		return ret;
	}
	
	public void opOpAssign(string op)(auto ref Set!T other) pure if (op == "^") {
		foreach(x; other[]) {
			if(contains(x)) {
				remove(x);
			}
			else {
				insert(x);
			}
		}
	}
	
	// Append
	public Set!T opBinary(string op, U)(auto ref U stuff) if (op == "~") {
		Set!T ret = this.dup;
		ret ~= stuff;
		return ret;
	}
	
	public void opOpAssign(string op, U)(auto ref U stuff) if (
		op == "~" &&
		isInputRange!U || isArray!U
	) {
		foreach(x; stuff) { insert(x); }
	}
	
	public void opOpAssign(string op, U)(auto ref U stuff) if (
		op == "~" &&
		isImplicitlyConvertible!(U, T)
	) {
		insert(stuff);
	}
}



version(unittest) {
private:
	import fluent.asserts;
	import std.algorithm.sorting : sort;
	import std.functional : binaryFun;
	import std.conv;
	
	void test_asym(alias fun)(char[] a, char[] b, bool result) {
		Set!char(a).binaryFun!("a."~fun~"(b)")(Set!char(b)).should.equal(result).because("[%s] %s [%s]".format(a, fun, b));
	}
	
	void test_sym(alias fun)(char[] a, char[] b, bool result) {
		test_asym!fun(a, b, result);
		test_asym!fun(b, a, result);
	}
	
	void op_asym(alias fun)(char[] a, char[] b, char[] result) {
		binaryFun!("a"~fun~"b")(Set!char(a), Set!char(b))[].to!(dchar[]).sort.should.equal(result).because("Op: [%s] %s [%s]".format(a, fun, b));
		
		auto s_a = Set!char(a);
		auto s_b = Set!char(b);
		mixin("s_a "~fun~"= s_b;");
		s_a[].to!(dchar[]).sort.should.equal(result).because("Assign: [%s] %s [%s]".format(a, fun, b));
		s_b[].to!(dchar[]).sort.should.equal(b).because("Assign: [%s] %s [%s]".format(a, fun, b));
	}
	
	void op_sym(alias fun)(char[] a, char[] b, char[] result) {
		op_asym!fun(a, b, result);
		op_asym!fun(b, a, result);
	}
}

@("Constructor")
unittest {
	import std.range : only;
	
	Set!int()[].sort.should.equal([]);
	Set!int([])[].sort.should.equal([]);
	Set!int(takeNone!(int[])())[].sort.should.equal([]);
	Set!int([55, 33, 44, 33])[].sort.should.equal([33, 44, 55]);
	Set!int(only(55, 33, 44, 33))[].sort.should.equal([33, 44, 55]);
}

@("Insert/Remove")
unittest {
	auto s = Set!int(); s.empty.should.equal(true);  s.length.should.equal(0); s.contains(33).should.equal(false); s[].sort.should.equal([]);
	s.insert(55);       s.empty.should.equal(false); s.length.should.equal(1); s.contains(33).should.equal(false); s[].sort.should.equal([55]);
	s.insert(33);       s.empty.should.equal(false); s.length.should.equal(2); s.contains(33).should.equal(true);  s[].sort.should.equal([33, 55]);
	s.insert(55);       s.empty.should.equal(false); s.length.should.equal(2); s.contains(33).should.equal(true);  s[].sort.should.equal([33, 55]);
	s.insert(44);       s.empty.should.equal(false); s.length.should.equal(3); s.contains(33).should.equal(true);  s[].sort.should.equal([33, 44, 55]);
	s.remove(33);       s.empty.should.equal(false); s.length.should.equal(2); s.contains(33).should.equal(false); s[].sort.should.equal([44, 55]);
	s.clear();          s.empty.should.equal(true);  s.length.should.equal(0); s.contains(33).should.equal(false); s[].sort.should.equal([]);
}

@("Copy")
unittest {
	auto a = Set!int([55]);
	auto b = a;
	auto c = a.dup;
	
	a[].sort.should.equal([55]);
	b[].sort.should.equal([55]);
	c[].sort.should.equal([55]);
	
	a.insert(11);
	b.insert(22);
	c.insert(33);
	
	a[].sort.should.equal([11, 22, 55]);
	b[].sort.should.equal([11, 22, 55]);
	c[].sort.should.equal([33, 55]);
}

@("Format")
unittest {
	import std.format : format;
	auto s = Set!int([55, 33, 44]);
	format("%s", s).should.equal("[55, 33, 44]");
	format("(%(<%s>%)>)", s).should.equal("(<55><33><44>)");
}

@("Rehash")
unittest {
	auto s = Set!int([55, 33, 44]);
	s[].sort.should.equal([33, 44, 55]);
	({ s.rehash; }).should.not.throwAnyException();
	s[].sort.should.equal([33, 44, 55]);
}

@("removeAny")
unittest {
	auto s = Set!int([10, 20, 30, 20]);
	bool[3] removed;
	
	foreach(i; 0..3) {
		auto x = s.removeAny();
		[10, 20, 30].should.contain(x);
		removed[x/10-1].should.equal(false);
		removed[x/10-1] = true;
	}
}

@("Tests")
unittest {
	test_sym!"is_disjoint_from"(       [],              [],              true);  // Both null
	test_sym!"is_disjoint_from"(       ['a'],           [],              true);  // One null
	test_sym!"is_disjoint_from"(       ['a'],           ['A'],           true);  // Disjoint, same length
	test_sym!"is_disjoint_from"(       ['a', 'b'],      ['A'],           true);  // Disjoint, different length
	test_sym!"is_disjoint_from"(       ['a', '!'],      ['A', '!'],      false); // Not disjoint, same length
	test_sym!"is_disjoint_from"(       ['a', '!'],      ['!'],           false); // Not disjoint, different length
	test_sym!"is_disjoint_from"(       ['a', 'b'],      ['a', 'b'],      false); // Same sets
	
	test_asym!"is_subset_of"(          [],              [],              true);  // Both null
	test_asym!"is_subset_of"(          ['a'],           [],              false); // One null
	test_asym!"is_subset_of"(          [],              ['a'],           true);
	test_asym!"is_subset_of"(          ['a'],           ['a'],           true);  // Equivalent sets
	test_asym!"is_subset_of"(          ['a', 'b'],      ['b', 'a'],      true);
	test_asym!"is_subset_of"(          ['a'],           ['A'],           false); // Disjoint sets
	test_asym!"is_subset_of"(          ['a', 'b'],      ['B', 'A'],      false);
	test_asym!"is_subset_of"(          ['a', 'b'],      ['a', 'c'],      false); // Partly disjoined, same length
	test_asym!"is_subset_of"(          ['a', 'b', '!'], ['a', 'c'],      false); // Partly disjoined, different length
	test_asym!"is_subset_of"(          ['a', 'b'],      ['a', 'c', '!'], false); // Partly disjoined, different length
	test_asym!"is_subset_of"(          ['a', 'b'],      ['a', 'b', 'c'], true);  // Subsets
	test_asym!"is_subset_of"(          ['a', 'b', 'c'], ['a', 'b'],      false);
	
	test_asym!"is_propper_subset_of"(  [],              [],              false); // Both null
	test_asym!"is_propper_subset_of"(  ['a'],           [],              false); // One null
	test_asym!"is_propper_subset_of"(  [],              ['a'],           true);
	test_asym!"is_propper_subset_of"(  ['a'],           ['a'],           false); // Equivalent sets
	test_asym!"is_propper_subset_of"(  ['a', 'b'],      ['b', 'a'],      false);
	test_asym!"is_propper_subset_of"(  ['a'],           ['A'],           false); // Disjoint sets
	test_asym!"is_propper_subset_of"(  ['a', 'b'],      ['B', 'A'],      false);
	test_asym!"is_propper_subset_of"(  ['a', 'b'],      ['a', 'c'],      false); // Partly disjoined, same length
	test_asym!"is_propper_subset_of"(  ['a', 'b', '!'], ['a', 'c'],      false); // Partly disjoined, different length
	test_asym!"is_propper_subset_of"(  ['a', 'b'],      ['a', 'c', '!'], false); // Partly disjoined, different length
	test_asym!"is_propper_subset_of"(  ['a', 'b'],      ['a', 'b', 'c'], true);  // Subsets
	test_asym!"is_propper_subset_of"(  ['a', 'b', 'c'], ['a', 'b'],      false);
	
	test_asym!"is_superset_of"(        [],              [],              true);  // Both null
	test_asym!"is_superset_of"(        ['a'],           [],              true);  // One null
	test_asym!"is_superset_of"(        [],              ['a'],           false);
	test_asym!"is_superset_of"(        ['a'],           ['a'],           true);  // Equivalent sets
	test_asym!"is_superset_of"(        ['a', 'b'],      ['b', 'a'],      true);
	test_asym!"is_superset_of"(        ['a'],           ['A'],           false); // Disjoint sets
	test_asym!"is_superset_of"(        ['a', 'b'],      ['B', 'A'],      false);
	test_asym!"is_superset_of"(        ['a', 'b'],      ['a', 'c'],      false); // Partly disjoined, same length
	test_asym!"is_superset_of"(        ['a', 'b', '!'], ['a', 'c'],      false); // Partly disjoined, different length
	test_asym!"is_superset_of"(        ['a', 'b'],      ['a', 'c', '!'], false); // Partly disjoined, different length
	test_asym!"is_superset_of"(        ['a', 'b'],      ['a', 'b', 'c'], false); // Subsets
	test_asym!"is_superset_of"(        ['a', 'b', 'c'], ['a', 'b'],      true);
	
	test_asym!"is_propper_superset_of"([],              [],              false); // Both null
	test_asym!"is_propper_superset_of"(['a'],           [],              true);  // One null
	test_asym!"is_propper_superset_of"([],              ['a'],           false);
	test_asym!"is_propper_superset_of"(['a'],           ['a'],           false); // Equivalent sets
	test_asym!"is_propper_superset_of"(['a', 'b'],      ['b', 'a'],      false);
	test_asym!"is_propper_superset_of"(['a'],           ['A'],           false); // Disjoint sets
	test_asym!"is_propper_superset_of"(['a', 'b'],      ['B', 'A'],      false);
	test_asym!"is_propper_superset_of"(['a', 'b'],      ['a', 'c'],      false); // Partly disjoined, same length
	test_asym!"is_propper_superset_of"(['a', 'b', '!'], ['a', 'c'],      false); // Partly disjoined, different length
	test_asym!"is_propper_superset_of"(['a', 'b'],      ['a', 'c', '!'], false); // Partly disjoined, different length
	test_asym!"is_propper_superset_of"(['a', 'b'],      ['a', 'b', 'c'], false); // Subsets
	test_asym!"is_propper_superset_of"(['a', 'b', 'c'], ['a', 'b'],      true);
}

@("Ops")
unittest {
	op_sym!"&"([],         [],         []);
	op_sym!"&"(['a'],      [],         []);
	op_sym!"&"(['a'],      ['b'],      []);
	op_sym!"&"(['a', 'b'], ['a'],      ['a']);
	op_sym!"&"(['a', 'b'], ['a', 'c'], ['a']);
	op_sym!"&"(['a', 'b'], ['a', 'b'], ['a', 'b']);
	
	op_sym!"|"([],         [],         []);
	op_sym!"|"(['a'],      [],         ['a']);
	op_sym!"|"(['a'],      ['b'],      ['a', 'b']);
	op_sym!"|"(['a', 'b'], ['a'],      ['a', 'b']);
	op_sym!"|"(['a', 'b'], ['a', 'c'], ['a', 'b', 'c']);
	op_sym!"|"(['a', 'b'], ['a', 'b'], ['a', 'b']);
	
	op_asym!"-"([],         [],         []);
	op_asym!"-"(['a'],      [],         ['a']);
	op_asym!"-"([],         ['a'],      []);
	op_asym!"-"(['a'],      ['b'],      ['a']);
	op_asym!"-"(['a', 'b'], ['a'],      ['b']);
	op_asym!"-"(['a'],      ['a', 'b'], []);
	op_asym!"-"(['a', 'b'], ['a', 'c'], ['b']);
	op_asym!"-"(['a', 'b'], ['a', 'b'], []);
	
	op_sym!"^"([],         [],         []);
	op_sym!"^"(['a'],      [],         ['a']);
	op_sym!"^"(['a'],      ['b'],      ['a', 'b']);
	op_sym!"^"(['a', 'b'], ['a'],      ['b']);
	op_sym!"^"(['a', 'b'], ['a', 'c'], ['b', 'c']);
	op_sym!"^"(['a', 'b'], ['a', 'b'], []);
}

@("Equals")
unittest {
	(Set!char([]) == Set!char([])).should.equal(true);
	(Set!char(['a']) == Set!char([])).should.equal(false);
	(Set!char([]) == Set!char(['a'])).should.equal(false);
	(Set!char(['a']) == Set!char(['a', 'b'])).should.equal(false);
	(Set!char(['a', 'b']) == Set!char(['a'])).should.equal(false);
	(Set!char(['a', 'b']) == Set!char(['a', 'b'])).should.equal(true);
}

@("Appending")
unittest {
	auto s = Set!int([11, 22]);
	s[].sort.should.equal([11, 22]);
	
	(s ~ 33)[].sort.should.equal([11, 22, 33]);
	s[].sort.should.equal([11, 22]);
	
	(s ~ [33, 44])[].sort.should.equal([11, 22, 33, 44]);
	s[].sort.should.equal([11, 22]);
	
	s ~= 33; s[].sort.should.equal([11, 22, 33]);
	
	s ~= [44, 55]; s[].sort.should.equal([11, 22, 33, 44, 55]);
}
