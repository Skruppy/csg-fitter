// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module generic.parser.slr1.core;

import generic.parser.symbols;
import std.algorithm.iteration : map;
import std.algorithm.sorting : sort;
import std.container.slist : SList;
import std.conv : to;
import std.format : format;
import std.range : array;
import std.range : isInputRange;
import std.traits : Unqual;
import generic.util;



alias SLR1State = size_t;


//#### ACTIONS
abstract class SLR1Action(C) {}

final class SLR1ReduceAction(C) : SLR1Action!C {
	SLR1Production!C production;
	this(SLR1Production!C production) { this.production = production; }
}

final class SLR1ShiftAction(C) : SLR1Action!C {
	immutable SLR1State newState;
	this(SLR1State newState) { this.newState = newState; }
}

final class SLR1AcceptAction(C) : SLR1Action!C {}


//#### PRODUCTION
abstract class SLR1Production(C) {
	abstract string name() const pure;
	abstract size_t length() const pure;
	abstract NonTerminal produce(C context, Symbol[] os) const;
	override string toString() const { return name; }
}


//#### Parser
private struct SLR1StackElement {
	SLR1State state;
	Symbol symbol;
}


struct SLR1Parser(C) {
	SLR1Action!C[TypeInfo_Class /* Terminal */][/* State */] actionsTable; // Terminals LUT
	SLR1State[TypeInfo_Class /* NonTerminals */][/* State */] gotoTable; // Non Terminals LUT
	
	
	this(SLR1Action!C[TypeInfo_Class][] actionsTable, SLR1State[TypeInfo_Class][] gotoTable) {
		assert(actionsTable.length == gotoTable.length);
		this.actionsTable = actionsTable.dup;
		this.gotoTable = gotoTable.dup;
	}
	
	
	void parse(R)(scope R tokens_, scope C context) if(isInputRange!(Unqual!R)) {
		auto tokens = tokens_.terminate;
		auto stack = SList!SLR1StackElement(SLR1StackElement());
		
		while( ! stack.empty) {
			auto action = typeid(tokens.front) in actionsTable[stack.front.state];
			if(action is null) {
				throw new ParserException(
					"Unexpected token %s. Expected one of %-(%s, %)"
					.format(tokens.front, actionsTable[stack.front.state].keys.map!"a.toString".array.sort()),
					tokens.front
				);
			}
			// Reduce ...
			else if(auto reduce = cast(SLR1ReduceAction!C)*action) {
				// ... n symbols from the stack ...
				scope auto oldSymbols = new Symbol[reduce.production.length];
				foreach_reverse(i; 0..reduce.production.length) {
					oldSymbols[i] = stack.pop.symbol;
				}
				
				// ... to a new non terminal symbol and the derived new state.
				auto nt = reduce.production.produce(context, oldSymbols);
				auto newState = typeid(nt) in gotoTable[stack.front.state];
				stack.insertFront(SLR1StackElement(*newState, nt));
			}
			// Shift ...
			else if(auto shift = cast(SLR1ShiftAction!C)*action) {
				assert( ! tokens.atEnd, "A shift action was applied to the END token.");
				// ... token from input onto stack and change state accordingly.
				stack.insertFront(SLR1StackElement(shift.newState, tokens.pop));
			}
			// Accept
			else if(auto accept = cast(SLR1AcceptAction!C)*action) {
				break;
			}
			else {
				assert(false);
			}
		}
		
		assert(tokens.atEnd, "Did not expect any more input, but got %s".format(tokens.front));
	}
}



version(unittest) {
private:
	import core.exception : AssertError;
	import fluent.asserts;
	import generic.parser.slr1.helper;
	import std.conv : to;
	
	class Ctx { bool visited; }
	class A : Terminal {}
	class B : Terminal {}
	class S : NonTerminal {
		@slr1production("0") this(Ctx c) { c.visited = true; }
		@slr1production("1") this(Ctx c, A a, S s, B b) { c.visited = true; }
	}
}

@("Parser")
unittest {
	Ctx c;
	auto productions = collectSLR1Productions!(Ctx, S);
	auto sa = function(SLR1State x)  { return new SLR1ShiftAction!Ctx(x); };
	auto ra = delegate(string x) { return new SLR1ReduceAction!Ctx(productions[x]); };
	SLR1Action!Ctx[TypeInfo_Class][5] actionsTable = [
		[typeid(A): sa(2), typeid(B): ra("0"), typeid(End): ra("0")                   ],
		[                                      typeid(End): new SLR1AcceptAction!Ctx()],
		[typeid(A): sa(2), typeid(B): ra("0"), typeid(End): ra("0")                   ],
		[                  typeid(B): sa(4)                                           ],
		[                  typeid(B): ra("1"), typeid(End): ra("1")                   ],
	];
	SLR1State[TypeInfo_Class][5] gotoTable = [0: [typeid(S): 1], 2: [typeid(S): 3]];
	
	auto parser = new SLR1Parser!Ctx(actionsTable, gotoTable);
	
	parser.parse([new A(), new A(), new B(), new B()], c = new Ctx());
	c.visited.should.equal(true);
	
	// End too early
	parser.parse([new A(), new A(), new B()], c = new Ctx())
	.should.throwException!ParserException.withMessage.equal("Unexpected token <End Of File>. Expected one of generic.parser.slr1.core.B");
	
	// Unexpected token
	parser.parse([new A(), new B(), new A()], c = new Ctx())
	.should.throwException!ParserException.withMessage.equal("Unexpected token generic.parser.slr1.core.A. Expected one of generic.parser.slr1.core.B, generic.parser.symbols.End");
	
	// ???
	parser.parse([new B()], c = new Ctx())
	.should.throwException!ParserException.withMessage.equal("Unexpected token generic.parser.slr1.core.B. Expected one of generic.parser.symbols.End");
	
	// Empty
	parser.parse(cast(Terminal[])[], c = new Ctx());
	c.visited.should.equal(true);
}

@("Production name")
unittest {
	auto production = new class SLR1Production!Ctx {
		override string name() const pure { return "bar"; }
		override size_t length() const pure { return 0; }
		override NonTerminal produce(Ctx context, Symbol[] os) const { return null; }
	};
	
	production.name.should.equal("bar");
	production.to!string.should.equal("bar");
}

@("Invalid parser: Alien action")
unittest {
	SLR1Action!Ctx[TypeInfo_Class][1] actionsTable = [
		[typeid(A): new class SLR1Action!Ctx {}],
	];
	SLR1State[TypeInfo_Class][1] gotoTable;
	auto parser = new SLR1Parser!Ctx(actionsTable, gotoTable);
	parser.parse([new A()], new Ctx()).should.throwException!AssertError;
}

@("Invalid parser: End too early")
unittest {
	SLR1Action!Ctx[TypeInfo_Class][1] actionsTable = [
		[typeid(A): new SLR1AcceptAction!Ctx()],
	];
	SLR1State[TypeInfo_Class][1] gotoTable;
	auto parser = new SLR1Parser!Ctx(actionsTable, gotoTable);
	parser.parse([new A()], new Ctx()).should.throwException!AssertError;
}
