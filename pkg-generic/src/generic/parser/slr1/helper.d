// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module generic.parser.slr1.helper;

import generic.parser.slr1;
import std.array : join;
import std.format : format;
import std.traits : Parameters;
import std.traits : ReturnType;
import std.traits : getSymbolsByUDA;
import std.traits : getUDAs;
import std.traits : staticMap;
import std.typecons : Tuple;



struct slr1production {
	string name;
}


class SLR1ConstructorProduction(C, alias nt_constructor) : SLR1Production!C {
	private alias NT = ReturnType!nt_constructor;
	private alias SYMs = Parameters!nt_constructor[1..$];
	
	@property override string name() const pure {
		enum string TypeName(TL...) = TL[0].stringof;
		return [NT.stringof, "::=", staticMap!(TypeName, SYMs)].join("  ");
	}
	
	override size_t length() const pure { return SYMs.length; }
	
	override NonTerminal produce(C ctx, Symbol[] os) const {
		assert(os.length == SYMs.length);
		
		Tuple!SYMs args;
		foreach(i, S; SYMs) {
			args[i] = cast(S)os[i];
			if(args[i] is null) throw new Exception(
				"Production %s expected %sth Symbol to be %s, not %s."
				.format(name, i+1, S.stringof, typeid((os[i])))
			);
		}
		
		return new NT(ctx, args[]);
	}
}


SLR1Production!C[string] collectSLR1Productions(C, NTs...)() {
	SLR1Production!C[string] productions;
	
	foreach(NT; NTs) {
		foreach(fn; getSymbolsByUDA!(NT, slr1production)) {
			auto meta = getUDAs!(fn, slr1production)[0];
			assert(meta.name !in productions, "Duplicate production name \""~meta.name~"\"");
			productions[meta.name] = new SLR1ConstructorProduction!(C, fn);
		}
	}
	productions.rehash;
	
	return productions;
}



version(unittest) {
private:
	import core.exception : AssertError;
	import fluent.asserts;
	
	class Ctx {}
	class A : Terminal {}
	class B : NonTerminal {}
	class S1 : NonTerminal {
		@slr1production("A") this(Ctx ctx, A a, B b) { }
		@slr1production("B") this(Ctx ctx) { }
	}
}

@("Duplicate name/production")
unittest {
	collectSLR1Productions!(Ctx, S1).should.not.throwAnyException;
	collectSLR1Productions!(Ctx, S1, S1).should.throwException!AssertError;
}

@("Production")
unittest {
	auto ctx = new Ctx();
	auto productions = collectSLR1Productions!(Ctx, S1);
	productions.keys.should.equal(["A", "B"]);
	
	auto production_a = productions["A"];
	production_a.length.should.equal(2);
	production_a.name.should.equal("S1  ::=  A  B");
	typeid(production_a.produce(ctx, [new A(), new B()])).should.equal(typeid(S1));
	production_a.produce(ctx, []).should.throwException!AssertError;
	production_a.produce(ctx, [new A()]).should.throwException!AssertError;
	production_a.produce(ctx, [new A(), new A()]).should.throwException!Exception.withMessage.equal("Production S1  ::=  A  B expected 2th Symbol to be B, not generic.parser.slr1.helper.A.");
	production_a.produce(ctx, [new B(), new B()]).should.throwException!Exception.withMessage.equal("Production S1  ::=  A  B expected 1th Symbol to be A, not generic.parser.slr1.helper.B.");
	production_a.produce(ctx, [new A(), new B(), new B()]).should.throwException!AssertError;
	
	auto production_b = productions["B"];
	production_b.length.should.equal(0);
	production_b.name.should.equal("S1  ::=");
	typeid(production_b.produce(ctx, [])).should.equal(typeid(S1));
	production_b.produce(ctx, [new A()]).should.throwException!AssertError;
}
