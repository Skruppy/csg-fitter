// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module generic.parser.symbols;

import std.range : isInputRange;
import std.range.primitives : empty;
import std.range.primitives : front;
import std.range.primitives : popFront;



abstract class Symbol {}
abstract class Terminal : Symbol {}
abstract class NonTerminal : Symbol {}
final class End : Terminal {
	override string toString() const {
		return "<End Of File>";
	}
}


template terminate(R) if(isInputRange!R) {
private:
	enum TerminatorState {
		NORMAL,
		END,
		DONE,
	}
	
	struct Terminator {
		End end;
		TerminatorState state;
		R r;
		
		this(scope R r) {
			this.end = new End();
			this.r = r;
			this.state = r.empty ? TerminatorState.END : TerminatorState.NORMAL;
		}
		
		@property bool empty() const pure {
			final switch(state) {
				case TerminatorState.NORMAL: return false;
				case TerminatorState.END:    return false;
				case TerminatorState.DONE:   return true;
			}
		}
		
		@property Terminal front() {
			final switch(state) {
				case TerminatorState.NORMAL: return r.front;
				case TerminatorState.DONE:   assert(false, "Attempting to fetch the front of an empty token stream.");
				case TerminatorState.END:    return end;
			}
		}
		
		void popFront() {
			final switch(state) {
				case TerminatorState.NORMAL:
					r.popFront();
					if(r.empty) { state = TerminatorState.END; }
					break;
				
				case TerminatorState.END:
					state = TerminatorState.DONE;
					break;
				
				case TerminatorState.DONE:
					assert(false, "Attempting to popFront of an empty token stream.");
			}
		}
		
		@property bool atEnd() const pure {
			return state == TerminatorState.END;
		}
	}
	
public:
	Terminator terminate(R r) {
		return Terminator(r);
	}
}


class ParserException : Exception {
	Terminal token;
	this(string msg, Terminal token, string file=__FILE__, ulong line=__LINE__) pure nothrow @nogc @safe {
		super(msg, file, line);
		this.token = token;
	}
	override string toString() {
		import std.format : format;
		return "Failed parsing input %s: %s".format(token, msg);
	}
}



version(unittest) {
private:
	import fluent.asserts;
	
	class A : Terminal {}
	class B : Terminal {}
}

@("terminate")
unittest {
	import core.exception : AssertError;
	import std.range : only;
	
	auto a = new A();
	auto b = new B();
	auto r = only(a, b).terminate;
	
	r.empty.should.equal(false);
	r.atEnd.should.equal(false);
	r.front.should.equal(a);
	
	r.popFront();
	
	r.empty.should.equal(false);
	r.atEnd.should.equal(false);
	r.front.should.equal(b);
	
	r.popFront();
	
	r.empty.should.equal(false);
	r.atEnd.should.equal(true);
	typeid(r.front).should.equal(typeid(End));
	
	r.popFront();
	
	r.empty.should.equal(true);
	r.atEnd.should.equal(false);
	
	r.front.should.throwException!AssertError;
	r.popFront.should.throwException!AssertError;
}

@("ParserException")
unittest {
	import std.conv : to;
	
	auto token = new End();
	token.to!string.should.equal("<End Of File>");
	
	auto ex = new ParserException("fuu", token);
	ex.line.should.equal(__LINE__ - 1);
	ex.file.should.equal(__FILE__);
	ex.msg.should.equal("fuu");
	ex.token.should.equal(token);
	ex.to!string.should.equal("Failed parsing input <End Of File>: fuu");
}
