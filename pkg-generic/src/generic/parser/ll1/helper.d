// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module generic.parser.ll1.helper;

import generic.parser.ll1;
import std.array : join;
import std.format : format;
import std.traits : Parameters;
import std.traits : ReturnType;
import std.traits : TemplateArgsOf;
import std.traits : getSymbolsByUDA;
import std.traits : getUDAs;
import std.traits : staticMap;
import std.typecons : Tuple;



struct ll1production(Ts...) {}


class ConstructorProduction(C, alias nt_constructor) : LL1Production!C {
	private alias NT = ReturnType!nt_constructor;
	private alias SYMs = Parameters!nt_constructor[1..$];
	
	@property override string name() const pure {
		enum string TypeName(TL...) = TL[0].stringof;
		return [NT.stringof, "::=", staticMap!(TypeName, SYMs)].join("  ");
	}
	
	@property override ExpectSymbol[] expectedSymbols() const pure {
		auto ret = new ExpectSymbol[SYMs.length];
		foreach(i, S; SYMs) {
			static if(is(S : Terminal))         { ret[i] = new ExpectT(typeid(S)); }
			else static if(is(S : NonTerminal)) { ret[i] = new ExpectNT!C(typeid(S)); }
			else                                static assert(false, "A parameter of "~(typeof(nt_constructor).stringof)~" is neither a Terminal, nor a NonTerminal.");
		}
		return ret;
	}
	
	override NonTerminal produce(C ctx, Symbol[] os) const {
		assert(os.length == SYMs.length);
		
		Tuple!SYMs args;
		foreach(i, S; SYMs) {
			args[i] = cast(S)os[i];
			if(args[i] is null) throw new Exception(
				"Production %s expected %sth Symbol to be %s, not %s."
				.format(name, i+1, S.stringof, typeid((os[i])))
			);
		}
		
		return new NT(ctx, args[]);
	}
}


auto collectLL1Productions(C, NTs...)() {
	LL1Production!C[TypeInfo_Class][TypeInfo_Class] productions;
	
	// For each non terminal provided as an template argument ...
	foreach(NT; NTs) {
		LL1Production!C[TypeInfo_Class] nt_productions;
		
		// ... find each annotated constructor and ...
		foreach(nt_constructor; getSymbolsByUDA!(NT, ll1production)) {
			auto production = new ConstructorProduction!(C, nt_constructor);
			
			// ... add the production (based on the annotated constructor) for
			// each terminal from the annotation(s)
			foreach(annotation; getUDAs!(nt_constructor, ll1production)) {
				foreach(T; TemplateArgsOf!annotation) {
					if(typeid(T) in nt_productions) {
						assert(false, "Duplicate production for "~T.stringof~" in "~NT.stringof);
					}
					nt_productions[typeid(T)] = production;
				}
			}
		}
		
		if(typeid(NT) in productions) {
			assert(false, "Duplicate non terminal "~NT.stringof);
		}
		nt_productions.rehash;
		productions[typeid(NT)] = nt_productions;
	}
	productions.rehash;
	
	return productions;
}



version(unittest) {
private:
	import core.exception : AssertError;
	import fluent.asserts;
	
	class Ctx {}
	class A : Terminal {}
	class B : NonTerminal {}
	class S1 : NonTerminal {
		@ll1production!(A) this(Ctx ctx, A a, B b) { }
		@ll1production!(B) this(Ctx ctx) { }
	}
	class S2 : NonTerminal {
		@ll1production!(A) this(Ctx ctx)      { }
		@ll1production!(A) this(Ctx ctx, A a) { }
	}
}

@("Duplicate non terminal")
unittest {
	collectLL1Productions!(Ctx, S1).should.not.throwAnyException;
	collectLL1Productions!(Ctx, S1, S1).should.throwException!AssertError;
}

@("Duplicate production")
unittest {
	collectLL1Productions!(Ctx, S2).should.throwException!AssertError;
}

@("Production")
unittest {
	auto ctx = new Ctx();
	auto productions = collectLL1Productions!(Ctx, S1);
	productions.keys.should.equal([typeid(S1)]);
	
	auto nt_productions = productions[typeid(S1)];
	nt_productions.keys.should.equal([typeid(A), typeid(B)]);
	
	auto production_a = nt_productions[typeid(A)];
	production_a.name.should.equal("S1  ::=  A  B");
	auto symbols = production_a.expectedSymbols;
	typeid(symbols[0]).should.equal(typeid(ExpectT));
	symbols[0].type.should.equal(typeid(A));
	typeid(symbols[1]).should.equal(typeid(ExpectNT!Ctx));
	symbols[1].type.should.equal(typeid(B));
	typeid(production_a.produce(ctx, [new A(), new B()])).should.equal(typeid(S1));
	production_a.produce(ctx, []).should.throwException!AssertError;
	production_a.produce(ctx, [new A()]).should.throwException!AssertError;
	production_a.produce(ctx, [new A(), new A()]).should.throwException!Exception.withMessage.equal("Production S1  ::=  A  B expected 2th Symbol to be B, not generic.parser.ll1.helper.A.");
	production_a.produce(ctx, [new B(), new B()]).should.throwException!Exception.withMessage.equal("Production S1  ::=  A  B expected 1th Symbol to be A, not generic.parser.ll1.helper.B.");
	production_a.produce(ctx, [new A(), new B(), new B()]).should.throwException!AssertError;
	
	auto production_b = nt_productions[typeid(B)];
	production_b.name.should.equal("S1  ::=");
	production_b.expectedSymbols.should.equal([]);
	typeid(production_b.produce(ctx, [])).should.equal(typeid(S1));
	production_b.produce(ctx, [new A()]).should.throwException!AssertError;
}
