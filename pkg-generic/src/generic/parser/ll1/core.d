// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module generic.parser.ll1.core;

import generic.parser.symbols;
import std.algorithm.iteration : map;
import std.container.slist : SList;
import std.format : format;
import std.range : array;
import std.range : isInputRange;
import generic.util;



abstract class LL1Production(C) {
	@property abstract string name() const pure;
	@property abstract ExpectSymbol[] expectedSymbols() const pure;
	abstract NonTerminal produce(C ctx, Symbol[] os) const;
	override string toString() const { return name; }
}


abstract class Expection {}

final class ExpectProduction(C) : Expection {
	ExpectSymbol[] symbols;
	const LL1Production!C production;
	NonTerminal content;
	this(LL1Production!C production) pure {
		this.production = production;
		this.symbols = production.expectedSymbols;
	}
}

abstract class ExpectSymbol : Expection {
	const TypeInfo_Class type;
	this(in TypeInfo_Class type) pure { this.type = type; }
	@property abstract Symbol symbol() pure;
}

final class ExpectNT(C) : ExpectSymbol {
	ExpectProduction!C content;
	this(in TypeInfo_Class type) pure { super(type); }
	@property override Symbol symbol() pure { return content.content; }
}

final class ExpectT : ExpectSymbol {
	Terminal content;
	this(in TypeInfo_Class type) pure { super(type); }
	@property override Symbol symbol() pure { return content; }
}

struct LL1Parser(C, S) if(is(S : Symbol)) {
	import std.algorithm.sorting : sort;
	import std.conv : to;
	import std.range : empty;
	import std.range : front;
	
	LL1Production!C[const TypeInfo_Class][const TypeInfo_Class] productions;
	
	this(scope LL1Production!C[const TypeInfo_Class][const TypeInfo_Class] productions) {
		this.productions = productions;
	}
	
	auto parse(R)(scope R tokens_, scope C ctx = C.init) if(isInputRange!R) {
		auto tokens = tokens_.terminate;
		auto stack = SList!Expection(new ExpectNT!C(typeid(S)));
		NonTerminal lastNt;
		
		while( ! stack.empty) {
			auto expection = stack.pop;
			
			if(auto expectNT = cast(ExpectNT!C)expection) {
				auto nt_productions = expectNT.type in productions;
				assert(nt_productions !is null, "There are no productions defined for %s".format(expectNT.type));
				
				auto production = typeid(tokens.front) in *nt_productions;
				if(production is null) {
					throw new ParserException("Production %s expected one of %-(%s, %)".format(expectNT.type, (*nt_productions).keys.map!"a.toString".array.sort()), tokens.front);
				}
				
				auto expectProd = new ExpectProduction!C(*production);
				stack.insertFront(expectProd);
				foreach_reverse(symbol; expectProd.symbols) {
					stack.insertFront(symbol);
				}
				
				expectNT.content = expectProd;
			}
			else if(auto expectT = cast(ExpectT)expection) {
				if(typeid(tokens.front) != expectT.type) {
					throw new ParserException("Expected %s".format(expectT.type), tokens.front);
				}
				
				expectT.content = tokens.pop;
			}
			else if(auto expectProduction = cast(ExpectProduction!C)expection) {
				auto symbols = expectProduction.symbols.map!"a.symbol".array;
				lastNt = expectProduction.production.produce(ctx, symbols);
				
				expectProduction.content = lastNt;
			}
			else assert(false);
		}
		
		assert( ! tokens.empty);
		if( ! tokens.atEnd) {
			throw new ParserException("Did not expect any more input", tokens.front);
		}
		
		return cast(S)lastNt;
	}
}



version(unittest) {
private:
	import core.exception : AssertError;
	import fluent.asserts;
	import generic.parser.ll1.helper;
	import std.conv : to;
	import std.string : toLower;
	
	class DummyProduction : LL1Production!Ctx {
		@property override string name() const pure { return "bar"; }
		@property override ExpectSymbol[] expectedSymbols() const pure { return [new ExpectT(typeid(A)), new ExpectT(typeid(S_G1))]; }
		override NonTerminal produce(Ctx ctx, Symbol[] os) const { return null; }
	}
	
	class BrokenProduction : DummyProduction {
		@property override ExpectSymbol[] expectedSymbols() const pure {
			return [new class ExpectSymbol {
				this() { super(null); }
				@property override Symbol symbol() pure { return null; }
			}];
		}
	}
	
	class Token : Terminal {
		override string toString() const { return typeid(this).className.toLower; }
	}
	class A : Token {}
	class B : Token {}
	class X : Token {}
	class Y : Token {}
	class Z : Token {}
	
	class Ctx {}
	
	class NT : NonTerminal {
		string name;
		this(string name) { this.name = name; }
		override string toString() const { return name; }
	}
	
	/*
	S -> A M b .
	
	A -> a A .
	A -> .
	
	M -> x M z .
	M -> y .
	
	   | b | a          | x          | z | y          | $ |
	===+===+============+============+===+============+===+
	 S |   | S -> A M b | S -> A M b |   | S -> A M b |   |
	 A |   | A -> a A   | A -> ε     |   | A -> ε     |   |
	 M |   |            | M -> x M z |   | M -> y     |   |
	*/
	
	class S_G1 : NT {
		@ll1production!(A, X, Y) this(Ctx ctx, A_G1 a_nt, M_G1 m_nt, B b) { super("S(%s %s %s)".format(a_nt, m_nt, b)); }
	}
	
	class A_G1 : NT {
		@ll1production!(A)       this(Ctx ctx, A a, A_G1 a_nt)            { super("A(%s %s)".format(a, a_nt)); }
		@ll1production!(X, Y)    this(Ctx ctx)                            { super("A()"); }
	}
	
	class M_G1 : NT {
		@ll1production!(X)       this(Ctx ctx, X x, M_G1 m_nt, Z z)       { super("M(%s %s %s)".format(x, m_nt, z)); }
		@ll1production!(Y)       this(Ctx ctx, Y y)                       { super("M(%s)".format(y)); }
	}
	
	/*
	S -> a S .
	S -> .
	
	   | a       | $
	===+=========+========
	 S | S → a S | S → ε 
	*/
	
	class S_G2 : NT {
		@ll1production!(A)   this(Ctx ctx, A a, S_G2 s) { super("S(%s %s)".format(a, s)); }
		@ll1production!(End) this(Ctx ctx)              { super("S()"); }
	}
}


@("Production name")
unittest {
	auto production = new DummyProduction();
	production.name.should.equal("bar");
	production.to!string.should.equal("bar");
}

@("Expected production")
unittest {
	auto production = new DummyProduction();
	auto expect = new ExpectProduction!Ctx(production);
	expect.production.should.equal(production);
	expect.symbols.length.should.equal(2);
}

@("Expected NT")
unittest {
	auto nt = new A_G1(null);
	auto production = new DummyProduction();
	
	auto expectProd = new ExpectProduction!Ctx(production);
	expectProd.content = nt;
	
	auto expect = new ExpectNT!Ctx(typeid(A_G1));
	expect.content = expectProd;
	
	expect.type.should.equal(typeid(A_G1));
	expect.symbol.should.equal(nt);
}

@("Complex")
unittest {
	auto g1parser = LL1Parser!(Ctx, S_G1)(collectLL1Productions!(Ctx, S_G1, A_G1, M_G1));
	auto top_nt = g1parser.parse([new A(), new A(), new X(), new X(), new X(), new Y(), new Z(), new Z(), new Z(), new B()]);
	top_nt.name.should.equal("S(A(a A(a A())) M(x M(x M(x M(y) z) z) z) b)");
}

@("End too early")
unittest {
	auto g1parser = LL1Parser!(Ctx, S_G1)(collectLL1Productions!(Ctx, S_G1, A_G1, M_G1));
	g1parser.parse([new A(), new A(), new X(), new X(), new X(), new Y(), new Z(), new Z(), new Z()])
	.should.throwException!ParserException.original.to!string.should.equal("Failed parsing input <End Of File>: Expected generic.parser.ll1.core.B");
}

@("Too much input")
unittest {
	auto g1parser = LL1Parser!(Ctx, S_G1)(collectLL1Productions!(Ctx, S_G1, A_G1, M_G1));
	g1parser.parse([new A(), new A(), new X(), new X(), new X(), new Y(), new Z(), new Z(), new Z(), new B(), new B()])
	.should.throwException!ParserException.original.to!string.should.equal("Failed parsing input b: Did not expect any more input");
}

@("Unexpected token")
unittest {
	auto g2parser = LL1Parser!(Ctx, S_G2)(collectLL1Productions!(Ctx, S_G2));
	g2parser.parse([new A(), new A(), new B()])
	.should.throwException!ParserException.original.to!string.should.equal("Failed parsing input b: Production generic.parser.ll1.core.S_G2 expected one of generic.parser.ll1.core.A, generic.parser.symbols.End");
}

@("Unknown NT")
unittest {
	auto parser = LL1Parser!(Ctx, S_G1)(collectLL1Productions!(Ctx, S_G2));
	parser.parse([new A()]).should.throwException!AssertError;
}

@("Unknown Expection")
unittest {
	auto parser = LL1Parser!(Ctx, S_G1)([typeid(S_G1): [typeid(A): new BrokenProduction()]]);
	parser.parse([new A()]).should.throwException!AssertError;
}

@("Empty")
unittest {
	Token[] tokens;
	auto g1parser = LL1Parser!(Ctx, S_G1)(collectLL1Productions!(Ctx, S_G1, A_G1, M_G1));
	auto g2parser = LL1Parser!(Ctx, S_G2)(collectLL1Productions!(Ctx, S_G2));
	g1parser.parse(tokens).should.throwException!ParserException.original.to!string.should.equal("Failed parsing input <End Of File>: Production generic.parser.ll1.core.S_G1 expected one of generic.parser.ll1.core.A, generic.parser.ll1.core.X, generic.parser.ll1.core.Y");
	g2parser.parse(tokens).name.should.equal("S()");
}
