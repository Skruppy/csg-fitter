// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module generic.graph;

import generic.set;
import std.format : FormatSpec;
import std.format : formattedWrite;
import std.format : formatValue;



struct UndirectedGraph(T) {
	private Set!(T)[T] edges;
	
	public this(T[] nodes) {
		foreach(node; nodes) { add_node(node); }
	}
	
	// Nodes
	public @property T[] nodes() const {
		return edges.keys;
	}
	
	public void add_node(T node) pure {
		assert(node !in edges);
		edges[node] = Set!(T).init;
	}
	
	public void remove_node(T node) pure {
		auto neighbours = node in edges;
		assert(neighbours !is null);
		foreach(neighbour ; (*neighbours)[]) {
			_remove_one_way(neighbour, node);
		}
		edges.remove(node);
	}
	
	public bool contains_node(inout T node) const {
		return (cast(T)node in edges) !is null;
	}
	
	public @property size_t node_count() const {
		return edges.length;
	}
	
	// Edges
	public void add_edge(T a, T b) {
		_add_one_way(a, b);
		if(a != b) { _add_one_way(b, a); }
	}
	
	public void remove_edge(T a, T b) {
		_remove_one_way(a, b);
		if(a != b) { _remove_one_way(b, a); }
	}
	
	public void remove_all_edges(T node) pure {
		auto neighbours = node in edges;
		assert(neighbours !is null);
		foreach(neighbour ; (*neighbours)[]) {
			_remove_one_way(neighbour, node);
		}
		edges[node] = Set!(T).init;
	}
	
	public bool is_connected(T a, T b) pure {
		auto neighbours = a in edges;
		if(neighbours is null) { return false; }
		return (*neighbours).contains(b);
	}
	
	public @property size_t edge_count() const {
		size_t cnt;
		Set!T visited;
		
		foreach(from, neighbours; edges) {
			foreach(to; neighbours[]) {
				if(visited.contains(to)) { continue; }
				cnt++;
			}
			visited.insert(from);
		}
		
		return cnt;
	}
	
	private void _add_one_way(T a, T b) pure {
		auto neighbours = a in edges;
		assert(neighbours !is null);
		assert( ! neighbours.contains(b));
		neighbours.insert(b);
	}
	
	private void _remove_one_way(T a, T b) pure {
		auto neighbours = a in edges;
		assert(neighbours !is null);
		assert(neighbours.contains(b));
		neighbours.remove(b);
	}
	
	// Misc
	public void clear() pure {
		edges.clear();
	}
	
	public UndirectedGraph!T dup() const {
		UndirectedGraph!T ret;
		foreach(node, neighbours ; edges) {
			ret.edges[node] = neighbours.dup();
		}
		return ret;
	}
	
	public @property T[] neighbours(T node) const {
		auto neighbours = node in edges;
		assert(neighbours !is null);
		return cast(T[])(*neighbours)[];
	}
	
	public void toString(
		scope void delegate(const(char)[]) sink,
		FormatSpec!char fmt
	) const {
		if(fmt.spec == 's') {
			sink.formattedWrite("{ %((%s)%(->%s%| %)%| | %) }", edges);
		}
		else {
			sink.formatValue(edges, fmt);
		}
	}
	
	public void rehash(bool full=true) pure {
		edges.rehash();
		
		if(full) {
			foreach(neighbours; edges.values) {
				neighbours.rehash();
			}
		}
	}
}



version(unittest) {
private:
	import fluent.asserts;
	import std.algorithm.sorting : sort;
	import std.array : array;
	import std.format : format;
	
	public string det_rep(UndirectedGraph!int g) {
		string ret;
		auto nodes = g.nodes[].array;
		nodes.sort();
		foreach(i, node; nodes) {
			auto neighbours = g.neighbours(node).array;
			neighbours.sort();
			if(i > 0) { ret ~= ", "; }
			ret ~= "%s ↦ %s".format(node, neighbours);
		}
		return ret;
	}
}

@("Graph")
unittest {
	auto g = UndirectedGraph!int([1, 2, 3, 4]);
	g.nodes.sort.should.equal([1, 2, 3, 4]);
	g.add_edge(1, 2);
	g.add_edge(1, 3);
	g.neighbours(1).sort.should.equal([2, 3]);
	g.neighbours(2).sort.should.equal([1]);
	g.neighbours(3).sort.should.equal([1]);
	g.neighbours(4).sort.should.equal([]);
}


@("Rehash")
unittest {
	auto g = UndirectedGraph!int([1, 2, 3]);
	g.add_edge(1, 2);
	({ g.rehash; }).should.not.throwAnyException();
	({ g.rehash(false); }).should.not.throwAnyException();
	g.nodes.sort.should.equal([1, 2, 3]);
	g.neighbours(1).sort.should.equal([2]);
	g.neighbours(2).sort.should.equal([1]);
	g.neighbours(3).sort.should.equal([]);
}

@("Constructor")
unittest {
	auto g1 = UndirectedGraph!int();
	g1.nodes.sort.should.equal([]);
	
	auto g2 = UndirectedGraph!int([1, 2, 3]);
	g2.nodes.sort.should.equal([1, 2, 3]);
}

@("dup")
unittest {
	auto g1 = UndirectedGraph!int([1, 2, 3]);
	g1.add_edge(1, 2);
	auto g2 = g1;
	auto g3 = g1.dup;
	
	g2.det_rep.should.equal("1 ↦ [2], 2 ↦ [1], 3 ↦ []");
	g3.det_rep.should.equal("1 ↦ [2], 2 ↦ [1], 3 ↦ []");
	
	g1.add_node(4);
	g1.add_edge(1, 3);
	
	g2.det_rep.should.equal("1 ↦ [2, 3], 2 ↦ [1], 3 ↦ [1], 4 ↦ []");
	g3.det_rep.should.equal("1 ↦ [2], 2 ↦ [1], 3 ↦ []");
}

@("toString")
unittest {
	auto g = UndirectedGraph!int([1, 2, 3, 4]);
	g.add_edge(1, 2); g.add_edge(1, 3);
	"%s".format(g).should.equal("{ (4) | (3)->1 | (2)->1 | (1)->3 ->2 }");
	"%(%s ↦ %s%|, %)".format(g).should.equal("4 ↦ [], 3 ↦ [1], 2 ↦ [1], 1 ↦ [3, 2]");
}

@("clear")
unittest {
	auto g = UndirectedGraph!int([1, 2, 3, 4]);
	g.add_edge(1, 2); g.add_edge(1, 3);
	g.det_rep.should.equal("1 ↦ [2, 3], 2 ↦ [1], 3 ↦ [1], 4 ↦ []");
	({ g.clear; }).should.not.throwAnyException();
	g.det_rep.should.equal("");
}

@("is_connected")
unittest {
	auto g = UndirectedGraph!int([1, 2, 3, 4]);
	g.add_edge(1, 2); g.add_edge(1, 3); g.add_edge(2, 2);
	g.is_connected(1, 1).should.equal(false);
	g.is_connected(1, 2).should.equal(true); // First edge "->"
	g.is_connected(1, 3).should.equal(true); // Second edge "->"
	g.is_connected(1, 4).should.equal(false);
	g.is_connected(2, 1).should.equal(true); // First edge "<-"
	g.is_connected(2, 2).should.equal(true); // Thirt edge "<->"
	g.is_connected(2, 3).should.equal(false);
	g.is_connected(2, 4).should.equal(false);
	g.is_connected(3, 1).should.equal(true); // Second edge "<-"
	g.is_connected(3, 2).should.equal(false);
	g.is_connected(3, 3).should.equal(false);
	g.is_connected(3, 4).should.equal(false);
	g.is_connected(4, 1).should.equal(false);
	g.is_connected(4, 2).should.equal(false);
	g.is_connected(4, 3).should.equal(false);
	g.is_connected(4, 4).should.equal(false);
}

@("edge_count")
unittest {
	auto g = UndirectedGraph!int([1, 2, 3, 4]);
	g.add_edge(1, 2); g.add_edge(1, 3); g.add_edge(2, 2);
	g.edge_count.should.equal(3);
}

@("node_count")
unittest {
	auto g = UndirectedGraph!int([1, 2, 3, 4]);
	g.add_edge(1, 2); g.add_edge(1, 3); g.add_edge(2, 2);
	g.node_count.should.equal(4);
}

@("remove_edge")
unittest {
	auto g = UndirectedGraph!int([1, 2, 3, 4]);
	g.add_edge(1, 2); g.add_edge(1, 3); g.add_edge(2, 2);
	g.det_rep.should.equal("1 ↦ [2, 3], 2 ↦ [1, 2], 3 ↦ [1], 4 ↦ []");
	g.remove_edge(1, 2); g.det_rep.should.equal("1 ↦ [3], 2 ↦ [2], 3 ↦ [1], 4 ↦ []");
	g.remove_edge(3, 1); g.det_rep.should.equal("1 ↦ [], 2 ↦ [2], 3 ↦ [], 4 ↦ []");
	g.remove_edge(2, 2); g.det_rep.should.equal("1 ↦ [], 2 ↦ [], 3 ↦ [], 4 ↦ []");
}

@("remove_edge")
unittest {
	auto g = UndirectedGraph!int([1, 2, 3, 4]);
	g.add_edge(1, 2); g.add_edge(1, 3); g.add_edge(2, 2);
	g.remove_all_edges(2); g.det_rep.should.equal("1 ↦ [3], 2 ↦ [], 3 ↦ [1], 4 ↦ []");
	g.remove_all_edges(1); g.det_rep.should.equal("1 ↦ [], 2 ↦ [], 3 ↦ [], 4 ↦ []");
}
