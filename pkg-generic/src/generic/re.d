// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module generic.re;

// Inspired by https://issues.dlang.org/show_bug.cgi?id=18378
import std.regex;


Regex regex(string pattern, const char[] flags=null) {
	return Regex(regexImpl(pattern,flags));
}

struct Regex {
	alias Type = typeof(regexImpl(""));
	Type re;
}

Captures matchFirst(string input, inout Regex re) {
	return Captures(std.regex.matchFirst(input, re.re));
}

struct Captures {
	std.regex.Captures!string cre;
	string opIndex(size_t i) const { return cre[i]; }
	auto empty() { return cre.empty; }
	auto hit() { return cre.hit; }
	auto post() { return cre.post; }
	auto pre() { return cre.pre; }
}
