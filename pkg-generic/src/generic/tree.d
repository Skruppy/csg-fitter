// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module generic.tree;

import generic.util : pop;
import std.algorithm.mutation : reverse;
import std.algorithm.searching : maxElement;
import std.container.dlist : DList;
import std.container.slist : SList;
import std.conv : to;
import std.functional : unaryFun;
import std.range : isForwardRange;
import std.range : join;



struct SiblingsRange(T, alias I = "a.next_sibiling") {
	private T cur;
	
	public bool empty() {
		return cur is null;
	}
	
	public T front() {
		assert(!empty, "tree.SiblingsRange.front: Range is empty");
		return cur;
	}
	
	public void popFront() {
		assert(!empty, "tree.SiblingsRange.popFront: Range is empty");
		cur = cur.unaryFun!I;
	}
	
	auto save() {
		return this;
	}
}


struct ParentsRange(T) {
	private T cur;
	
	public bool empty() {
		return cur is null;
	}
	
	public T front() {
		assert(!empty, "tree.ParentsRange.front: Range is empty");
		return cur;
	}
	
	public void popFront() {
		assert(!empty, "tree.ParentsRange.popFront: Range is empty");
		cur = cur.parent;
	}
	
	auto save() {
		return this;
	}
}


struct DescendantsRange(T) {
	private DList!T fifo;
	
	private this(R)(R children) if(isForwardRange!R) {
		fifo = DList!T(children);
	}
	
	public bool empty() {
		return fifo.empty;
	}
	
	public T front() {
		assert(!empty, "tree.DescendantsRange.front: Range is empty");
		return fifo.front;
	}
	
	public void popFront() {
		assert(!empty, "tree.DescendantsRange.popFront: Range is empty");
		fifo ~= fifo.front.children;
		fifo.removeFront();
	}
}

// static assert(isInputRange!(ChildRange!(Node!void)));


/// @todo split into Leaf and Branch(Leaf)
public class Node(this T, V) {
	protected T _parent;        // Up
	protected T _first_child;   // Down
	protected T _last_child;    // Down
	protected T _prev_sibiling; // Left
	protected T _next_sibiling; // Right
	protected long _position;
	
	
	// #### Constructors
	public this(T[] children) {
		this(null, children);
	}
	
	
	public this(T parent=null, T[] children=[]) {
		_position = -1;
		
		this.parent = parent;
		
		foreach(child; children) {
			child.parent = cast(T)this;
		}
	}
	
	
	// #### Neighboring elements
	@property
	public T first_child() {
		return _first_child;
	}
	
	@property
	public T last_child() {
		return _last_child;
	}
	
	@property
	public T prev_sibiling() {
		return _prev_sibiling;
	}
	
	@property
	public T next_sibiling() {
		return _next_sibiling;
	}
	
	@property
	public T parent() {
		return _parent;
	}
	
	@property
	public void parent(T new_parent) {
		if(new_parent == _parent) return;
		
		if(_parent !is null) {
			_parent._removeChild(cast(T)this);
			_position = -1;
		}
		_parent = new_parent;
		if(new_parent !is null) {
			_position = new_parent._appendChild(cast(T)this);
		}
	}
	
	// #### Lists of neighboring elements
	// https://www.researchgate.net/profile/Pierre_Geneves/publication/23421077/figure/fig2/AS:669548386713602@1536644183341/XPath-axes-partition-of-tree-nodes.png
	public auto children() {
		return SiblingsRange!T(_first_child);
	}
	
	public auto children_rev() {
		return SiblingsRange!(T, "a.prev_sibiling")(_last_child);
	}
	
	public auto following_siblings() {
		return SiblingsRange!T(_next_sibiling);
	}
	
	public auto preceding_siblings_rev() {
		return SiblingsRange!(T, "a.prev_sibiling")(_prev_sibiling);
	}
	
	public auto ancestors() {
		return ParentsRange!(T)(_parent);
	}
	
	public auto descendants() {
		return DescendantsRange!(T)(children);
	}
	
	// #### Derived properties
	@property
	public T root() {
		T cur = cast(T)this;
		while(cur._parent) cur = cur._parent;
		return cur;
	}
	
	@property
	public size_t depth() {
		size_t lvl = 0;
		for(T cur = cast(T)this; cur._parent; cur = cur._parent) {
			lvl++;
		}
		return lvl;
	}
	
	@property
	public size_t level() {
		return depth + 1;
	}
	
	@property
	public size_t size() {
		size_t size = 0;
		auto fifo = DList!T(cast(T)this);
		while( ! fifo.empty) {
			size++;
			for(auto child=fifo.pop.first_child; child !is null; child=child.next_sibiling) {
				fifo ~= child;
			}
		}
		return size;
	}
	
	@property
	public size_t height() {
		struct Job {size_t depth; T node;}
		
		size_t height = 0;
		auto jobs = SList!Job(Job(0, cast(T)this));
		while(!jobs.empty) {
			auto job = jobs.pop();
			
			if(job.node.hasChildren) {
				foreach(ref child; job.node.children) {
					jobs.insert(Job(job.depth+1, child));
				}
			}
			else {
				if(job.depth > height) { height = job.depth; }
			}
		}
		
		return height;
	}
	
	@property
	public bool isRoot() {
		return _parent is null;
	}
	
	@property
	public bool hasChildren() const {
		return _first_child !is null;
	}
	
	@property
	public bool isFirstChild() const {
		return _prev_sibiling is null;
	}
	
	@property
	public bool isLastChild() const {
		return _next_sibiling is null;
	}
	
	
	@property
	public long position() const {
		return _position;
	}
	
	
	public override string toString() {
		return _parent is null ? "root" : "["~to!string(_position)~"]";
	}
	
	
	public string path(string seperator=" / ") const {
		T node = cast(T)this;
		string[] names;
		while(node !is null) {
			names ~= to!string(node);
			node = node._parent;
		}
		
		return names.reverse.join(seperator);
	}
	
	
	public auto appendChild(T child) {
		if(child._parent !is null) throw new Exception("Node already a child.");
		child.parent = cast(T)this;
		return this;
	}
	
	
	public auto removeChild(T child) {
		if(child._parent !is this) throw new Exception("Node is not our child.");
		child.parent = null;
		return this;
	}
	
	public auto remove() {
		return _parent is null ? null : _parent.removeChild(cast(T)this);
	}
	
	public void clear() {
		T next_child;
		for(auto child = _first_child; child !is null; child = next_child) {
			next_child = child._next_sibiling;
			
			child._prev_sibiling = null;
			child._next_sibiling = null;
			child._parent = null;
			child._position = -1;
		}
		
		_first_child = _last_child = null;
	}
	
	
	protected final void integrate(T prev_sibiling, T next_sibiling, T parent) {
		_prev_sibiling = prev_sibiling;
		_next_sibiling = next_sibiling;
		_parent = parent;
		
		if(prev_sibiling !is null) prev_sibiling._next_sibiling = cast(T)this;
		else if(parent !is null) parent._first_child = cast(T)this;
		
		if(next_sibiling !is null) next_sibiling._prev_sibiling = cast(T)this;
		else if(parent !is null) parent._last_child = cast(T)this;
	}
	
	
	public T replaceWith(T new_child) {
		new_child.parent = null; // Deregister from old parent
		
		if(_parent is null) {
			new_child.integrate(null, null, null);
			new_child._position = -1;
		}
		else {
			new_child.integrate(_prev_sibiling, _next_sibiling, _parent);
			new_child._position = _position;
		}
		
		return new_child;
	}
	
	
	public void swapWith(T other) {
		auto other_prev_sibiling = other._prev_sibiling;
		auto other_next_sibiling = other._next_sibiling;
		auto other_parent        = other._parent;
		auto other_position      = other._position;
		
		other.integrate(_prev_sibiling, _next_sibiling, _parent);
		other._position = _position;
		
		integrate(other_prev_sibiling, other_next_sibiling, other_parent);
		_position = other_position;
	}
	
	
	protected long _appendChild(T child) {
		if(_last_child !is null) {
			child._position = _last_child._position + 1;
			child.integrate(_last_child, null, cast(T)this);
		}
		else {
			child._position = 0;
			child.integrate(null, null, cast(T)this);
		}
		
		return child._position;
	}
	
	
	protected void _removeChild(T child) {
		assert(child.parent is this);
		
		if(child._prev_sibiling !is null) {
			child._prev_sibiling._next_sibiling = child._next_sibiling;
		}
		else {
			_first_child = child._next_sibiling;
		}
		
		if(child._next_sibiling !is null) {
			child._next_sibiling._prev_sibiling = child._prev_sibiling;
		}
		else {
			_last_child = child._prev_sibiling;
		}
		
		long i = child._position;
		foreach(node; child.following_siblings) {
			node._position = i++;
		}
	}
	
	
	public abstract void acceptPre(V);
	public abstract void acceptPost(V);
}



mixin template NodeConstructors(T) {
	public this(T[] children)                   { super(null,   children); }
	public this(T parent=null, T[] children=[]) { super(parent, children); }
}



mixin template TreeVisit(T) {
	override public void acceptPre(T visitor) {
		visitor.visitPre(this);
	}
	
	override public void acceptPost(T visitor) {
		visitor.visitPost(this);
	}
}



public void walk(T, U)(T visitor, U root) {
	enum Occasion {PRE, POST}
	struct Job {Occasion occasion; U node;}
	
	auto jobs = SList!Job(Job(Occasion.PRE, root));
	Job job;
	while(!jobs.empty) {
		job = jobs.front;
		jobs.removeFront();
		
		if(job.occasion == Occasion.PRE) {
			job.node.acceptPre(visitor);
			
			jobs.insert(Job(Occasion.POST, job.node));
			foreach(ref child; job.node.children_rev) {
				jobs.insert(Job(Occasion.PRE, child));
			}
		}
		else {
			job.node.acceptPost(visitor);
		}
	}
}


template pprinter(N : Node!(T, V), T, V) {
private:
	class PPrinter : V {
		private N _root;
		private void delegate(string) write;
		
		public this(N root) {
			_root = root;
		}
		
		public void print() {
			import std.stdio : std_write = write;
			write = line => std_write(line);
			walk(this, _root);
		}
		
		public override string toString() {
			import std.outbuffer : OutBuffer;
			auto buffer = new OutBuffer();
			write = line => buffer.write(line);
			walk(this, _root);
			return buffer.toString();
		}
		
		override public void visitPreAll(N node) {
			if(node != _root) {
				write(" ");
				scope string[] sperators;
				foreach(ancestor; node.ancestors) {
					if(ancestor == _root) { break; }
					sperators ~= ancestor.isLastChild ? "   " : "│  ";
				}
				foreach_reverse(sperator; sperators) {
					write(sperator);
				}
				write(node.isLastChild ? "└" : "├");
				write("─");
			}
			write(node.hasChildren ? "[#] " : "──> ");
			write(to!string(node));
			write("\n");
		}
	}
	
public:
	auto pprinter(N node) {
		return new PPrinter(node);
	}
}



version(unittest) {
private:
	import fluent.asserts;
	import std.algorithm.iteration : map;
	import std.array : array;
	import generic.util;
	
	class V {
		void visitPre(N node)     { visitPreAll(node); }
		void visitPost(N node)    { visitPostAll(node); }
		void visitPreAll(N node)  { }
		void visitPostAll(N node) { }
	}
	
	class N : Node!(N, V) {
		string id;
		this(string id) { this.id = id; }
		mixin NodeConstructors!N;
		mixin TreeVisit!V;
		override string toString() { return this.id; }
	}
	
	N[string] buildTree(string prefix="") {
		// [#] root
		//  ├───> 1
		//  ├─[#] 2
		//  │  ├─[#] 2.1
		//  │  │  └─[#] 2.1.1
		//  │  │     ├─[#] 2.1.1.1
		//  │  │     │  └───> 2.1.1.1.1
		//  │  │     ├─[#] 2.1.1.2
		//  │  │     │  └───> 2.1.1.2.1
		//  │  │     ├─[#] 2.1.1.3
		//  │  │     │  └───> 2.1.1.3.1
		//  │  │     ├─[#] 2.1.1.4
		//  │  │     │  ├─[#] 2.1.1.4.1
		//  │  │     │  │  ├───> 2.1.1.4.1.1
		//  │  │     │  │  └───> 2.1.1.4.1.2
		//  │  │     │  └─[#] 2.1.1.4.2
		//  │  │     │     ├───> 2.1.1.4.2.1
		//  │  │     │     └───> 2.1.1.4.2.2
		//  │  │     ├─[#] 2.1.1.5
		//  │  │     │  └───> 2.1.1.5.1
		//  │  │     ├─[#] 2.1.1.6
		//  │  │     │  └───> 2.1.1.6.1
		//  │  │     └─[#] 2.1.1.7
		//  │  │        └───> 2.1.1.7.1
		//  │  └───> 2.2
		//  └───> 3
		
		N[string] n;
		
		foreach(id; [
			"root",
			"1", "2", "3",
			"2.1", "2.2",
			"2.1.1",
			"2.1.1.1", "2.1.1.2", "2.1.1.3", "2.1.1.4", "2.1.1.5", "2.1.1.6", "2.1.1.7",
			"2.1.1.1.1", "2.1.1.2.1", "2.1.1.3.1", "2.1.1.4.1", "2.1.1.4.2", "2.1.1.5.1", "2.1.1.6.1", "2.1.1.7.1",
			"2.1.1.4.1.1", "2.1.1.4.1.2", "2.1.1.4.2.1", "2.1.1.4.2.2",
		]) {
			n[id] = new N(prefix~id);
		}
		
		n["root"].appendChild(n["1"]).appendChild(n["2"]).appendChild(n["3"]);
		n["2"].appendChild(n["2.1"]).appendChild(n["2.2"]);
		n["2.1"].appendChild(n["2.1.1"]);
		n["2.1.1"].appendChild(n["2.1.1.1"]).appendChild(n["2.1.1.2"]).appendChild(n["2.1.1.3"]).appendChild(n["2.1.1.4"]).appendChild(n["2.1.1.5"]).appendChild(n["2.1.1.6"]).appendChild(n["2.1.1.7"]);
		n["2.1.1.1"].appendChild(n["2.1.1.1.1"]);
		n["2.1.1.2"].appendChild(n["2.1.1.2.1"]);
		n["2.1.1.3"].appendChild(n["2.1.1.3.1"]);
		n["2.1.1.4"].appendChild(n["2.1.1.4.1"]).appendChild(n["2.1.1.4.2"]);
		n["2.1.1.5"].appendChild(n["2.1.1.5.1"]);
		n["2.1.1.6"].appendChild(n["2.1.1.6.1"]);
		n["2.1.1.7"].appendChild(n["2.1.1.7.1"]);
		n["2.1.1.4.1"].appendChild(n["2.1.1.4.1.1"]).appendChild(n["2.1.1.4.1.2"]);
		n["2.1.1.4.2"].appendChild(n["2.1.1.4.2.1"]).appendChild(n["2.1.1.4.2.2"]);
		
		return n;
	}
}

@("basic props")
unittest {
	auto nodes = buildTree();
	nodes["2.1.1.4"].children.map!"a.id".should.equal(["2.1.1.4.1", "2.1.1.4.2"]);
	nodes["2.1.1.4"].children_rev.map!"a.id".should.equal(["2.1.1.4.2", "2.1.1.4.1"]);
	nodes["2.1.1.4"].following_siblings.map!"a.id".should.equal(["2.1.1.5", "2.1.1.6", "2.1.1.7"]);
	nodes["2.1.1.4"].preceding_siblings_rev.map!"a.id".should.equal(["2.1.1.3", "2.1.1.2", "2.1.1.1"]);
	nodes["2.1.1.4"].ancestors.map!"a.id".should.equal(["2.1.1", "2.1", "2", "root"]);
	nodes["2.1.1.4"].descendants.map!"a.id".array.should.equal(["2.1.1.4.1", "2.1.1.4.2", "2.1.1.4.1.1", "2.1.1.4.1.2", "2.1.1.4.2.1", "2.1.1.4.2.2"]);
	
	auto empty = new N("empty");
	auto someNodes = [empty, nodes["root"], nodes["1"], nodes["2"], nodes["3"], nodes["2.1"], nodes["2.2"], nodes["2.1.1"]];
	someNodes.map!"a.first_child".should.equal([null, nodes["1"], null, nodes["2.1"], null, nodes["2.1.1"], null, nodes["2.1.1.1"]]);
	someNodes.map!"a.last_child".should.equal([null, nodes["3"], null, nodes["2.2"], null, nodes["2.1.1"], null, nodes["2.1.1.7"]]);
	someNodes.map!"a.prev_sibiling".should.equal([null, null, null, nodes["1"], nodes["2"], null, nodes["2.1"], null]);
	someNodes.map!"a.next_sibiling".should.equal([null, null, nodes["2"], nodes["3"], null, nodes["2.2"], null, null]);
	someNodes.map!"a.parent".should.equal([null, null, nodes["root"], nodes["root"], nodes["root"], nodes["2"], nodes["2"], nodes["2.1"]]);
	
	someNodes.map!"a.root.id".should.equal(["empty", "root", "root", "root", "root", "root", "root", "root"]);
	someNodes.map!"a.depth".should.equal(cast(size_t[])[0, 0, 1, 1, 1, 2, 2, 3]);
	someNodes.map!"a.level".should.equal(cast(size_t[])[1, 1, 2, 2, 2, 3, 3, 4]);
	someNodes.map!"a.height".should.equal(cast(size_t[])[0, 6, 0, 5, 0, 4, 0, 3]);
	someNodes.map!"a.size".should.equal(cast(size_t[])[1, 26, 1, 23, 1, 21, 1, 20]);
	someNodes.map!"a.isRoot".should.equal([true, true, false, false, false, false, false, false]);
	someNodes.map!"a.hasChildren".should.equal([false, true, false, true, false, true, false, true]);
	someNodes.map!"a.isFirstChild".should.equal([true, true, true, false, false, true, false, true]);
	someNodes.map!"a.isLastChild".should.equal([true, true, false, false, true, false, true, true]);
	someNodes.map!"a.position".should.equal(cast(long[])[-1, -1, 0, 1, 2, 0, 1, 0]);
	
	someNodes.map!"a.toString".should.equal(["empty", "root", "1", "2", "3", "2.1", "2.2", "2.1.1"]);
	someNodes.map!"a.path".should.equal(["empty", "root", "root / 1", "root / 2", "root / 3", "root / 2 / 2.1", "root / 2 / 2.2", "root / 2 / 2.1 / 2.1.1"]);
}

@("parent")
unittest {
	auto t = new N("top");
	auto b = new N("bottom");
	
	b.parent.should.equal(null);
	t.first_child.should.equal(null);
	
	b.parent = t;
	b.parent.should.equal(t);
	t.first_child.should.equal(b);
	
	b.parent = null;
	b.parent.should.equal(null);
	t.first_child.should.equal(null);
}

@("clear")
unittest {
	auto nodes = buildTree();
	({nodes["3"].clear();}).should.not.throwAnyException;
	nodes["3"].parent.should.equal(nodes["root"]);
	nodes["3"].children.map!"a.id".should.equal([]);
	nodes["root"].children.map!"a.id".should.equal(["1", "2", "3"]);
	
	({nodes["2"].clear();}).should.not.throwAnyException;
	
	nodes["2"].parent.should.equal(nodes["root"]);
	
	nodes["root"].descendants.map!"a.id".array.should.equal(["1", "2", "3"]);
	
	nodes["2.1"].parent.should.equal(null);
	nodes["2.1"].children.map!"a.id".should.equal(["2.1.1"]);
	
	nodes["2.2"].parent.should.equal(null);
	nodes["2.2"].children.map!"a.id".should.equal([]);
}

@("remove")
unittest {
	auto nodes = buildTree();
	nodes["root"].children.map!"a.id".should.equal(["1", "2", "3"]);
	
	nodes["root"].removeChild(nodes["2"]).should.equal(nodes["root"]);
	nodes["root"].children.map!"a.id".should.equal(["1", "3"]);
	
	nodes["3"].remove().should.equal(nodes["root"]);
	nodes["root"].children.map!"a.id".should.equal(["1"]);
	
	({nodes["root"].removeChild(nodes["2.1"]);}).should.throwException!Exception.withMessage.equal("Node is not our child.");
	nodes["root"].children.map!"a.id".should.equal(["1"]);
	
	nodes["root"].remove().should.equal(null);
	nodes["root"].children.map!"a.id".should.equal(["1"]);
}

@("replace")
unittest {
	auto tree_a = buildTree("a:");
	auto tree_b = buildTree("b:");
	
	tree_a["2"].replaceWith(tree_b["2.1.1.4"]);
	tree_a["root"].descendants.map!"a.id".array.should.equal([
		"a:1", "b:2.1.1.4", "a:3",
		"b:2.1.1.4.1", "b:2.1.1.4.2",
		"b:2.1.1.4.1.1", "b:2.1.1.4.1.2", "b:2.1.1.4.2.1", "b:2.1.1.4.2.2",
	]);
	tree_b["root"].descendants.map!"a.id".array.should.equal([
		"b:1", "b:2", "b:3",
		"b:2.1", "b:2.2",
		"b:2.1.1",
		"b:2.1.1.1", "b:2.1.1.2", "b:2.1.1.3", /*no "a:2.2" */ "b:2.1.1.5", "b:2.1.1.6", "b:2.1.1.7",
		"b:2.1.1.1.1", "b:2.1.1.2.1", "b:2.1.1.3.1", "b:2.1.1.5.1", "b:2.1.1.6.1", "b:2.1.1.7.1",
	]);
}

@("replace with root")
unittest {
	auto tree_a = new N("a");
	tree_a.appendChild(new N("a1"));
	tree_a.appendChild(new N("a2"));
	auto tree_b = new N("b");
	tree_b.appendChild(new N("b1"));
	tree_b.appendChild(new N("b2"));
	
	tree_a.first_child.replaceWith(tree_b).id.should.equal("b");
	tree_a.pprinter.toString.should.equal(`
	[#] a
	 ├─[#] b
	 │  ├───> b1
	 │  └───> b2
	 └───> a2
	`.mls);
}

@("replace root")
unittest {
	auto tree_a = new N("a");
	tree_a.appendChild(new N("a1"));
	tree_a.appendChild(new N("a2"));
	auto tree_b = new N("b");
	tree_b.appendChild(new N("b1"));
	tree_b.appendChild(new N("b2"));
	
	tree_a.replaceWith(tree_b).pprinter.toString.should.equal(`
	[#] b
	 ├───> b1
	 └───> b2
	`.mls);
}

@("swap")
unittest {
	auto tree_a = buildTree("a:");
	auto tree_b = buildTree("b:");
	
	tree_a["2.2"].swapWith(tree_b["2.1.1.4"]);
	tree_a["root"].descendants.map!"a.id".array.should.equal([
		"a:1", "a:2", "a:3",
		"a:2.1", "b:2.1.1.4",
		"a:2.1.1", "b:2.1.1.4.1", "b:2.1.1.4.2",
		"a:2.1.1.1", "a:2.1.1.2", "a:2.1.1.3", "a:2.1.1.4", "a:2.1.1.5", "a:2.1.1.6", "a:2.1.1.7", "b:2.1.1.4.1.1", "b:2.1.1.4.1.2", "b:2.1.1.4.2.1", "b:2.1.1.4.2.2",
		"a:2.1.1.1.1", "a:2.1.1.2.1", "a:2.1.1.3.1", "a:2.1.1.4.1", "a:2.1.1.4.2", "a:2.1.1.5.1", "a:2.1.1.6.1", "a:2.1.1.7.1",
		"a:2.1.1.4.1.1", "a:2.1.1.4.1.2", "a:2.1.1.4.2.1", "a:2.1.1.4.2.2",
	]);
	tree_b["root"].descendants.map!"a.id".array.should.equal([
		"b:1", "b:2", "b:3",
		"b:2.1", "b:2.2",
		"b:2.1.1",
		"b:2.1.1.1", "b:2.1.1.2", "b:2.1.1.3", "a:2.2", "b:2.1.1.5", "b:2.1.1.6", "b:2.1.1.7",
		"b:2.1.1.1.1", "b:2.1.1.2.1", "b:2.1.1.3.1", "b:2.1.1.5.1", "b:2.1.1.6.1", "b:2.1.1.7.1",
	]);
}

@("height/size timing")
unittest {
	import std.random : dice;
	import core.time : msecs;
	
	foreach(round; 0..30) {
		DList!N parents;
		N tree;
		foreach(i; 0..1000) {
			auto node = new N(i.to!string);
			
			if(tree is null) {
				tree = node;
			}
			else {
				node.parent = parents.pop();
			}
			
			foreach(times; 0..dice(parents.empty ? 0 : 15, 15, 10)) {
				parents.insertBack(node);
			}
		}
		
		size_t height, size;
		({ height = tree.height; }).should.haveExecutionTime.lessThan(20.msecs); // A bogous implementation was in the range of minutes
		({ size = tree.size; }).should.haveExecutionTime.lessThan(20.msecs);
		height.should.be.greaterThan(100); // This is not actually guaranteed to be less than 100, but it is normaly around 500
		size.should.equal(1000);
	}
}
