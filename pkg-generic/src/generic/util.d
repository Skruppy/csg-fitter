// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module generic.util;

import std.array : join;
import std.array : split;
import std.container.dlist : DList;
import std.container.slist : SList;
import std.conv : to;
import std.range.primitives : back;
import std.range.primitives : isInputRange;
import std.functional : unaryFun;
import std.traits : ForeachType;
import std.datetime.stopwatch : StopWatch;
import std.datetime.stopwatch : AutoStart;
import std.socket : Socket;
import std.socket : getAddress;
import std.socket : SocketException;



public struct AttrInfo(T) {
	string name;
	T properties;
	TypeInfo type;
}


public AttrInfo!A[string] attributes(alias T, alias A)() {
	import std.traits : getSymbolsByUDA;
	import std.traits : getUDAs;
	
	AttrInfo!A[string] ret;
	
	foreach(alias member; getSymbolsByUDA!(T, A)) {
		ret[member.stringof] = AttrInfo!A(
			member.stringof,
			getUDAs!(member, A)[0](),
			typeid(typeof(member))
		);
	}
	
	return ret;
}


public string className(TypeInfo ti) {
	return ti.toString.split(".").back;
}


void fromStrings(S)(ref S s, string[string] data) {
	import std.traits : FieldNameTuple;
	
	foreach(name; FieldNameTuple!S) { // Or: https://forum.dlang.org/post/plezhdwbekxjokowobpp@forum.dlang.org
		if(name !in data) continue;
		__traits(getMember, s, name) = data[name].to!(typeof(__traits(getMember, s, name)));
		data.remove(name);
	}
	if(data.length > 0) {
		throw new Exception("Missing properties in "~S.stringof~": "~data.keys.join(", "));
	}
}


T pop(T)(ref DList!T list) {
	T ret = list.front;
	list.removeFront();
	return ret;
}

T pop(T)(ref SList!T list) {
	T ret = list.front;
	list.removeFront();
	return ret;
}

auto pop(R)(ref R r) if(isInputRange!R) {
	auto ret = r.front;
	r.popFront();
	return ret;
}

void push(T)(ref DList!T list, T value) {
	list.insertFront(value);
}

void push(T)(ref SList!T list, T value) {
	list.insertFront(value);
}


template importMaybe(string path, string fallback) {
	static if(__traits(compiles, import(path)))
		enum importMaybe = import(path);
	else
		enum importMaybe = fallback;
}


auto stfu(T)(lazy T fn) {
	import std.stdio : File;
	import std.stdio : stderr;
	import std.stdio : stdout;
	
	auto oldStdout = stdout;
	auto oldStderr = stderr;
	
	scope(exit) {
		stdout.flush(); stdout = oldStdout;
		stderr.flush(); stderr = oldStderr;
	}
	
	stdout.flush(); stdout = File("/dev/null", "w");
	stderr.flush(); stderr = File("/dev/null", "w");
	
	return fn();
}


pragma(inline, true);
auto as_bytes(T)(T t) pure @nogc {
	union Storage {
		T val;
		ubyte[T.sizeof] bytes;
	}
	return Storage(t).bytes;
}


pragma(inline, true);
auto mls(in string s) pure {
	import std.string : outdent;
	return s[1..$].outdent;
}


auto safe_fold(alias fun, R)(R r) if(isInputRange!R) {
	import std.algorithm.iteration : fold;
	
	assert( ! r.empty);
	
	auto first = r.pop;
	if(r.empty) {
		return first;
	}
	else {
		return r.fold!fun(first);
	}
}


// https://en.wikipedia.org/wiki/Integer_square_root#Using_bitwise_operations
uint isqrt(in uint x) pure {
	if(x < 2) { return x; }
	else {
		uint small_candidate = isqrt(x / 4) * 2;
		uint large_candidate = small_candidate + 1;
		return large_candidate^^2 > x ? small_candidate : large_candidate;
	}
}


uint step(in uint x) pure {
	return (isqrt(8*x+1)-1)/2;
}


U first(alias is_match, T, U=ForeachType!T)(T arr, U fallback=U.init) {
	foreach(el; arr) {
		if(unaryFun!is_match(el)) {
			return el;
		}
	}
	
	return fallback;
}


auto rainbow(size_t n, float l=60, float c=39) {
	import std.experimental.color.lab;
	import std.experimental.color.rgb;
	
	auto colors = new ubyte[3][n];
	foreach(i, ref color; colors) {
		auto lch_color = LCh!float(l, c, i*360.0/n);
		auto rgb_color = cast(RGB!("rgb", ubyte))lch_color;
		auto tuple = rgb_color.tristimulus;
		color[0] = tuple[0].value;
		color[1] = tuple[1].value;
		color[2] = tuple[2].value;
	}
	return colors;
}


pragma(inline, true);
auto fresh_timer() {
	return StopWatch(AutoStart.yes);
}


pragma(inline, true);
auto get_time(ref StopWatch sw) {
	sw.stop();
	return sw.peek.total!"usecs";
}


string hostname() {
	auto hostname = Socket.hostName;
	try {
		foreach(address; getAddress(hostname)) {
			if(auto fqdn = address.toHostNameString()) {
				return fqdn;
			}
		}
	}
	catch(SocketException e) {
		// nop
	}
	
	return hostname;
}



version(unittest) {
private:
	import fluent.asserts;
}

@("push/pop")
unittest {
	import std.array : array;
	import std.range : only;
	import core.exception : AssertError;
	
	void test(T)() {
		T l;
		
		l.dup.array.should.equal([]);
		
		l.push(11); l.dup.array.should.equal([11]);
		l.push(22); l.dup.array.should.equal([22, 11]);
		l.push(33); l.dup.array.should.equal([33, 22, 11]);
		
		l.pop.should.equal(33); l.dup.array.should.equal([22, 11]);
		l.pop.should.equal(22); l.dup.array.should.equal([11]);
		l.pop.should.equal(11); l.dup.array.should.equal([]);
		
		({l.pop;}).should.throwException!AssertError;
	}
	
	test!(SList!int);
	test!(DList!int);
	
	auto r = only(11, 22, 33);
	r.pop.should.equal(11);
	r.pop.should.equal(22);
	r.pop.should.equal(33);
}

@("className")
unittest {
	class Simple {}
	class Complex(T) : Simple {}
	
	typeid(new Simple()).className.should.equal("Simple");
	typeid(new Complex!int()).className.should.equal("Complex");
}

@("fromStrings")
unittest {
	enum E { FOO, BAR, }
	struct S { string a; bool b; char c; int d; E e; }
	S s;
	s.fromStrings([
		"a": "foo",
		"b": "true",
		"c": "x",
		"d": "-23",
		"e": "BAR",
	]);
	s.a.should.equal("foo");
	s.b.should.equal(true);
	s.c.should.equal('x');
	s.d.should.equal(-23);
	s.e.should.equal(E.BAR);

	s.fromStrings([
		"A": "foo",
		"B": "true",
	]).should.throwException!Exception.withMessage.should.equal("Missing properties in S: A, B");
}

@("import maybe")
unittest {
	auto a = importMaybe!("does-not-exist", "nope");
	a.should.equal("nope");
	
	auto b = importMaybe!("generic/util.d", "nope");
	b.should.not.equal("nope");
}

@("as_bytes")
unittest {
	struct S { ushort a = 0x42; bool b = true; char c = 'c'; }
	S s;
	as_bytes(s).should.equal([0x42, 0x00, 0x01, 0x63]);
	as_bytes(0x01020304).should.equal([0x04, 0x03, 0x02, 0x01]);
}

@("mls")
unittest {
	`
		foo
	bar
		baz
	`.mls.should.equal("\tfoo\nbar\n\tbaz\n");
	
	`
		foo
		bar
		baz
	`.mls.should.equal("foo\nbar\nbaz\n");
}

@("pop range")
unittest {
	import std.range : only;
	import std.array : array;
	
	auto r1 = only(5, 2, 1);
	r1.pop.should.equal(5);
	r1.array.should.equal([2, 1]);
	
	auto r2 = only(5);
	r2.pop.should.equal(5);
	r2.empty.should.equal(true);
}

@("safe fold")
unittest {
	import std.range : only;
	
	only(2).safe_fold!"a*10+b".should.equal(2);
	only(2, 1).safe_fold!("a*10+b").should.equal(21);
	only(5, 2, 1).safe_fold!("a*10+b").should.equal(521);
	only(5, 2, 1).safe_fold!((a, b) => a*10+b).should.equal(521);
}

@("step")
unittest {
	import std.range : iota;
	import std.array : array;
	import std.algorithm.iteration : map;
	
	iota(0, 20).map!step.array.should.equal([0, 1,1, 2,2,2, 3,3,3,3, 4,4,4,4,4, 5,5,5,5,5, /*...*/]);
}

@("isqrt")
unittest {
	import std.range : iota;
	import std.array : array;
	import std.algorithm.iteration : map;
	import std.random : uniform;
	import std.math : sqrt;
	import std.math : floor;
	
	iota(0, 20).map!isqrt.array.should.equal([0, 1,1,1, 2,2,2,2,2, 3,3,3,3,3,3,3, 4,4,4,4/*...*/]);
	
	foreach(i; 0..100) {
		auto x = uniform(0, uint.max);
		isqrt(x).should.equal(sqrt(x.to!double).floor.to!uint).because(x.to!string);
	}
}

@("first")
unittest {
	[1, 2, 13, 4, 5, 6].first!"a>10".should.equal(13);
	[1, 2, 13, 4, 15, 6].first!"a>10".should.equal(13);
	[11, 2, 3, 4, 5, 6].first!"a>10".should.equal(11);
	[1, 2, 3, 4, 5, 16].first!"a>10".should.equal(16);
	[1, 2, 3, 4, 5, 6].first!"a>10".should.equal(0);
	[1, 2, 3, 4, 5, 6].first!"a>10"(42).should.equal(42);
}
