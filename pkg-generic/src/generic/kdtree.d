// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module generic.kdtree;

import std.algorithm.sorting : sort;
import std.array : array;
import std.container.binaryheap : heapify;
import std.functional : binaryFun;
import std.functional : unaryFun;
import std.math : sqrt;
import std.meta : Alias;
import std.range.primitives : ElementType;
import std.traits : ReturnType;
import std.traits : isNumeric;



// https://gopalcdas.com/2017/05/24/construction-of-k-d-tree-and-using-it-for-nearest-neighbour-search/

struct Node(T, C : F[K], F, alias K) {
	T point;
	F[K] coord; // Saved here for data locality
	typeof(this)* le;
	typeof(this)* gt;
	
	F distance(F[] other) {
		F acc = 0.0;
		foreach(i; 0..K) {
			immutable auto t = coord[i] - other[i];
			acc += t * t;
		}
		return sqrt(acc);
	}
}


template k_d_tree_constructor(
	alias K_ = 0,   // (Optional) Number of coordinate dimensions
	alias A_ = "a", // (Optional) Function to access coordinates of data point
	T,              // (Optional) Type of data point (stored inside tree)
) {
private:
	static if(isNumeric!(typeof(K_))) {
		alias A = A_;
		alias C = ReturnType!((T t) { return t.unaryFun!A; });
		
		static if(K_ == 0) alias K = Alias!(C.length);
		else               alias K = K_;
	}
	else {
		alias A = K_;
		alias C = ReturnType!((T t) { return t.unaryFun!A; });
		alias K = Alias!(C.length);
	}
	
	alias F = ElementType!C;
	alias N = Node!(T, F[K]);
	
	class NodeSpawner {
		private N[] nodes;
		private size_t i;
		
		this(size_t n) {
			nodes = new N[n];
		}
		
		N* nextNode() {
			return &nodes[i++];
		}
		
		NodeSpawner branch() {
			return this;
		}
	}
	
	N* helper(T[] points, NodeSpawner ns, size_t dim=0){
		if(points.length == 0) return null;
		
		// Sort on the current dimension
		points.sort!((a, b) => a.unaryFun!A[dim] < b.unaryFun!A[dim]);
		
		// Find median / split point
		size_t split = points.length / 2;
		auto med = points[split].unaryFun!A[dim];
		
		while(split < points.length-1 && points[split+1].unaryFun!A[dim] == med) split++;
		
		//
		N* node = ns.nextNode;
		node.point = points[split];
		node.coord = points[split].unaryFun!A;
		
		//
		dim = (dim+1) % K;
		node.le = helper(points[0..split], ns.branch(), dim);
		node.gt = helper(points[split+1..$], ns.branch(), dim);
		
		return node;
	}
	
public:
	N* k_d_tree_constructor(T[] points) {
		auto ns = new NodeSpawner(points.length);
		auto root = points.helper(ns);
		return root;
	}
}


template k_d_tree_find(N : Node!(T, C), T, C : F[K], F, alias K) {
private:
	T* helper(N* root, F[] target, size_t dim) {
		if(target[dim] > root.coord[dim]) {
			if(root.gt is null) return null;
			return root.gt.helper(target, (dim+1) % K);
		}
		else if(target == root.coord){
			return &root.point;
		}
		else {
			if(root.le is null) return null;
			return root.le.helper(target, (dim+1) % K);
		}
	}
	
public:
	T* k_d_tree_find(N* root, F[] target)  {
		return helper(root, target, 0);
	}
}


template k_d_tree_nn(N : Node!(T, C), T, C : F[K], F, alias K) {
private:
	void helper(N* root, F[] target, size_t dim, double* dBest, N** nBest) {
		// Calculate distance: Query point to border
		auto dBorder = target[dim] - root.coord[dim];
		
		// Calculate distance: Query point to node
		auto dRoot = root.distance(target);
		
		// Save best match
		if(dRoot < *dBest) {
			*dBest = dRoot;
			*nBest = root;
		}
		
		//== Visit children
		N* first;
		N* second;
		
		// Determine which child to visit first
		if(dBorder <= 0) {
			first = root.le;
			second = root.gt;
			dBorder = -dBorder;
		}
		else {
			first = root.gt;
			second = root.le;
		}
		
		// Ready fot the next dimension? Woaaaaa!
		dim = (dim+1) % K;
		
		// Always visit your first child
		if(first !is null) {
			first.helper(target, dim, dBest, nBest);
		}
		
		// Nah, visit your second child sometimes
		if(second !is null && dBorder < *dBest) {
			second.helper(target, dim, dBest, nBest);
		}
	}
	
public:
	T* k_d_tree_nn(N* root, F[] target, double limit = double.max)  {
		N* nBest;
		root.helper(target, 0, &limit, &nBest);
		return nBest is null ? null : &nBest.point;
	}
}


template k_d_tree_knn(N : Node!(T, C), T, C : F[K], F, alias K) {
private:
	void helper(H)(N* root, F[] target, size_t dim, H heap) {
		// Calculate distance: Query point to border
		auto dBorder = target[dim] - root.coord[dim];
		
		// Calculate distance: Query point to node
		auto dRoot = root.distance(target);
		
		// Save best match
		heap.conditionalInsert(Result(root, dRoot));
		
		//== Visit children
		N* first;
		N* second;
		
		// Determine which child to visit first
		if(dBorder <= 0) {
			first = root.le;
			second = root.gt;
			dBorder = -dBorder;
		}
		else {
			first = root.gt;
			second = root.le;
		}
		
		// Ready fot the next dimension? Woaaaaa!
		dim = (dim+1) % K;
		
		// Always visit your first child
		if(first !is null) {
			first.helper(target, dim, heap);
		}
		
		// Nah, visit your second child sometimes
		if(second !is null && (heap.length < heap.capacity || dBorder < heap.front.distance)) {
			second.helper(target, dim, heap);
		}
	}
	
public:
	struct Result {
		N* node;
		double distance;
	}
	
	Result[] k_d_tree_knn(N* root, F[] target, size_t k)  {
		if(k == 0) return [];
		alias cmp = binaryFun!"a.distance < b.distance";
		auto heap = new Result[k].heapify!cmp(0);
		root.helper(target, 0, &heap);
		return heap.release.sort!cmp.array;
	}
}


/*
Test Base array unchanged
*/

version(unittest) {
private:
	import fluent.asserts;
	import std.algorithm.comparison : min;
	import std.algorithm.sorting : isSorted;
	import std.random : randomShuffle;
	import std.random : uniform;
	import std.range : chain;
	
	struct BasePoint {
		double[3] coord;
	}
	
	double dist(double[] a, double[] b) {
		double d = 0.0;
		foreach(i; 0..3) {
			d += (a[i] - b[i]) ^^ 2;
		}
		return sqrt(d);
	}
}


@("Construct: empty")
unittest {
	double[][] points;
	points.k_d_tree_constructor!(3).should.equal(null).because("nothing comes from nothing");
}


@("Construct: Difernt types")
unittest {
	struct Coord {
		double[3] a;
		double opIndex(size_t i) {
			return a[i];
		}
	}
	
	BasePoint[10] a;
	BasePoint[] b = new BasePoint[10];
	auto c = chain(a[], b);
	double[][] d = [[0.0, 0.0, 0.0], [0.0, 0.0, 0.0], [0.0, 0.0, 0.0], [0.0, 0.0, 0.0]];
	Coord[] e = [Coord([0.0, 0.0, 0.0])];
	
	a[].k_d_tree_constructor!(3, "a.coord").should.not.equal(null);
	b.k_d_tree_constructor!(3, "a.coord").should.not.equal(null);
	c.array.k_d_tree_constructor!(3, "a.coord").should.not.equal(null);
	
	d.k_d_tree_constructor!(3).should.not.equal(null);
}


@("Construct: Handle different sizes")
unittest {
	foreach(size; 1..16) {
		double[][] points;
		foreach(i; 0..size) points ~= [uniform(-1.0, 1.0), uniform(-1.0, 1.0), uniform(-1.0, 1.0)];
		points.randomShuffle;
		points.k_d_tree_constructor!(3).should.not.equal(null);
	}
}


unittest {
	double[][] points;
	
	size_t fac = 100;
	foreach(i; 0..1*fac) points ~= [uniform(-1.0, 1.0), uniform(-1.0, 1.0), -1.0];
	foreach(i; 0..1*fac) points ~= [uniform(-1.0, 1.0), uniform(-1.0, 1.0),  0.0];
	foreach(i; 0..1*fac) points ~= [uniform(-1.0, 1.0), uniform(-1.0, 1.0),  1.0];
	
	foreach(i; 0..1*fac) points ~= [uniform(-1.0, 1.0), -1.0, uniform(-1.0, 1.0)];
	foreach(i; 0..1*fac) points ~= [uniform(-1.0, 1.0),  0.0, uniform(-1.0, 1.0)];
	foreach(i; 0..1*fac) points ~= [uniform(-1.0, 1.0),  1.0, uniform(-1.0, 1.0)];
	
	foreach(i; 0..1*fac) points ~= [-1.0, uniform(-1.0, 1.0), uniform(-1.0, 1.0)];
	foreach(i; 0..1*fac) points ~= [ 0.0, uniform(-1.0, 1.0), uniform(-1.0, 1.0)];
	foreach(i; 0..1*fac) points ~= [ 1.0, uniform(-1.0, 1.0), uniform(-1.0, 1.0)];
	
	foreach(i; 0..1*fac) points ~= [uniform(-1.0, 1.0), uniform(-1.0, 1.0), uniform(-1.0, 1.0)];
	
	points.randomShuffle;
	points.k_d_tree_constructor!(3).should.not.equal(null);
}


unittest {
	double[][] points;
	foreach(i; 0..10) points ~= [0.0, 0.0, 0.0];
	points.k_d_tree_constructor!(3).should.not.equal(null);
}


@("Find: 50:50 test")
unittest {
	double[][] points;
	
	foreach(x; 0..10)
	foreach(y; 0..10)
	foreach(z; 0..10) {
		points ~= [x, y, z];
	}
	
	points.randomShuffle;
	
	auto root = points[0..$/2].k_d_tree_constructor!(3);
	
	foreach(ref p; points[0..$/2]) {
		auto result = root.k_d_tree_find(p);
		result.should.not.equal(null);
		(*result).should.equal(p);
	}
	
	foreach(ref p; points[$/2..$]) {
		root.k_d_tree_find(p).should.equal(null);
	}
}


@("NN")
unittest {
	double[][] points;
	
	foreach(i; 0..100) points ~= [uniform(-1.0, 1.0), uniform(-1.0, 1.0), uniform(-1.0, 1.0)];
	points.randomShuffle;
	auto root = points.k_d_tree_constructor!(3);
	
	foreach(i; 0..100) {
		// Make up question
		double[] q = [uniform(-1.0, 1.0), uniform(-1.0, 1.0), uniform(-1.0, 1.0)];
		
		// Find answer
		auto a = root.k_d_tree_nn(q);
		
		// Compute reference answer
		auto dRef = double.max;
		foreach(p; points) {
			dRef = min(dRef, dist(q, p));
		}
		
		// Test
		dRef.should.equal(dist(q, *a));
	}
}


@("kNN")
unittest {
	double[][] points;
	
	foreach(i; 0..100) points ~= [uniform(-1.0, 1.0), uniform(-1.0, 1.0), uniform(-1.0, 1.0)];
	points.randomShuffle;
	auto root = points.k_d_tree_constructor!(3);
	
	foreach(i; 0..100) {
		// Make up question
		double[] q = [uniform(-1.0, 1.0), uniform(-1.0, 1.0), uniform(-1.0, 1.0)];
		
		// Compute reference answer
		auto dRefHeap = new double[10].heapify(0);
		foreach(ref p; points) dRefHeap.conditionalInsert(dist(q, p));
		
		auto aRef = dRefHeap.release.sort;
		
		foreach(size; [0, 1, 2, 5, 10]) {
			// Find answer
			auto a = root.k_d_tree_knn(q, size);
			
			// Test
			a.length.should.equal(size);
			a.isSorted!"a.distance < b.distance".should.equal(true);
			foreach(j; 0..size) {
				a[j].distance.should.equal(aRef[a.length-size+j]);
			}
		}
	}
}


unittest {
	double[][] points;
	
	foreach(i; 0..5) points ~= [uniform(-1.0, 1.0), uniform(-1.0, 1.0), uniform(-1.0, 1.0)];
	points.randomShuffle;
	auto root = points.k_d_tree_constructor!(3);
	
	// Make up question
	double[] q = [uniform(-1.0, 1.0), uniform(-1.0, 1.0), uniform(-1.0, 1.0)];
	
	// Find answer
	auto a = root.k_d_tree_knn(q, 10);
	
	// Test
	a.length.should.equal(5);
	a.isSorted!"a.distance < b.distance".should.equal(true);
}
