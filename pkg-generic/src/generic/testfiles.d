// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module generic.testfiles;

import std.algorithm.iteration : each;
import std.file : FileException;
import std.file : mkdirRecurse;
import std.file : rmdir;
import std.file : rmdirRecurse;
import std.file : tempDir;
import std.file : thisExePath;
import std.path : baseName;
import std.path : buildPath;
import std.process : environment;
import std.random : uniform;


private shared string test_session_dir;
private shared string test_base_dir;
private shared bool keep_tests;


public string test_path(string[] paths...) {
	if(test_session_dir == "") {
		init();
	}
	assert(test_session_dir != "");
	return buildPath(test_session_dir ~ paths);
}


private char rnd_alpha_num() {
	static immutable char[] alphaNum = "0123456789abcdefghijklmnopqrstuvwxyz";
	return alphaNum[uniform(0, alphaNum.length)];
}


private static init() {
	keep_tests = "KEEP_TEST_DIR" in environment;
	
	// Construct base dir path
	test_base_dir =  tempDir.buildPath(thisExePath.baseName);
	
	// Construct session dir path
	if(keep_tests) {
		test_session_dir = test_base_dir.buildPath("test-static");
	}
	else {
		char[20] id = "test-";
		id[5..$].each!((ref x) {x = rnd_alpha_num();});
		test_session_dir = test_base_dir.buildPath(id);
	}
	
	// Create base and session dir
	test_session_dir.mkdirRecurse();
}


private shared static ~this() {
	if( test_session_dir != "" && ! keep_tests) {
		// Remove session dir
		test_session_dir.rmdirRecurse();
		
		// Remove base dir, if there are no other tests running (base dir is empty)
		try { test_base_dir.rmdir(); }
		catch(FileException ex) { /* NOP */ }
		
		test_session_dir = "";
	}
}



version(unittest) {
private:
	import fluent.asserts;
}

unittest {
	import std.random : rndGen;
	import std.random : Random;
	import std.file : isDir;
	import std.algorithm.searching : startsWith;
	
	rndGen = Random(42);
	auto p1 = test_path();
	p1[0..5].should.equal("/tmp/");
	p1.isDir.should.equal(true);
}
