// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module generic.lexer;

import generic.parser.symbols;
import std.format : format;
import std.range : empty;
import generic.re;



class Token : Terminal {
	size_t line;
	size_t col;
}

alias token_gen = Token function(Captures);

struct Matcher {
	string pattern;
	token_gen gen;
}

private struct CompiledMatcher {
	string pattern;
	Regex compiled_regex;
	token_gen gen;
}

struct Lexer {
	private CompiledMatcher[] matchers;
	
	this(Matcher[] matchers) {
		this.matchers = new CompiledMatcher[matchers.length];
		foreach (i, matcher; matchers) {
			this.matchers[i] = CompiledMatcher(matcher.pattern, regex("^(?:" ~ matcher.pattern ~ ")"), matcher.gen);
		}
	}
	
	auto lex(string s) {
		return TokenRange(matchers, s);
	}
}

private struct TokenRange {
	private CompiledMatcher[] matchers;
	private string remaining;
	private size_t col = 1;
	private size_t line = 1;
	private Token token;
	
	private this(CompiledMatcher[] matchers, string s) {
		this.matchers = matchers;
		remaining = s;
		selectNextToken();
	}
	
	@property bool empty() const {
		return token is null;
	}
	
	@property Token front() {
		assert( ! empty, "Attempting to fetch the front of an empty token stream.");
		return token;
	}
	
	void popFront() {
		assert( ! empty, "Attempting to popFront of an empty token stream.");
		selectNextToken();
	}
	
	private void selectNextToken() {
		while( ! remaining.empty) {
			if((token = tokenFromInput()) !is null) { return; }
		}
		token = null;
	}
	
	private Token tokenFromInput() {
		Captures best_match;
		CompiledMatcher best_matcher;
		size_t best_length;
		
		foreach(matcher; matchers) {
			Captures match = remaining.matchFirst(matcher.compiled_regex);
			assert(match.empty || match.hit.length > 0);
			if(match.empty || match.hit.length <= best_length) { continue; }
			best_match = match;
			best_matcher = matcher;
			best_length = match.hit.length;
		}
		
		if(best_length == 0) {
			string snippet = remaining.length > 20 ? remaining[0..20] ~ "..." : remaining;
			throw new LexerException("Unknown input \"%s\"".format(snippet), line, col);
		}
		
		auto token = best_matcher.gen(best_match);
		if(token !is null) {
			token.line = line;
			token.col = col;
		}
		
		remaining = best_match.post;
		foreach(c; best_match.hit) {
			if(c == '\n') { line++; col=1; }
			else          { col++; }
		}
		
		return token;
	}
}

class LexerException : Exception {
	immutable size_t input_line;
	immutable size_t input_col;
	
	this(string msg, size_t input_line, size_t input_col, string src_file=__FILE__, ulong src_line=__LINE__) pure nothrow @nogc @safe {
		super(msg, src_file, src_line);
		this.input_line = input_line;
		this.input_col = input_col;
	}
	
	override string toString() {
		return "Failed lexing input at %s:%s: %s".format(input_line, input_col, msg);
	}
}



version(unittest) {
private:
	import fluent.asserts;
	import std.algorithm.iteration : map;
	import std.array : array;
	import std.array : join;
	import std.conv : to;
	
	class DummyToken : Token {
		string name;
		this(string name) { this.name = name; }
		override string toString() const { return "%s @ %s:%s".format(name, line, col); }
	}
	
	auto build_lexer() {
		return Lexer([
			Matcher(`\s`,      m => null),
			Matcher(`a`,       m => new DummyToken("a")),
			Matcher(`b`,       m => new DummyToken("b")),
			Matcher(`ab`,      m => new DummyToken("ab")),
			Matcher(`"[^"]*"`, m => new DummyToken("comment")),
			Matcher(`\d+`,     m => new DummyToken(m[0].to!string)),
		]);
	}
	
	string serialize(R)(R tokens) {
		return tokens.map!(to!string).join(" // ");
	}
}

@("Lexer")
unittest {
	auto lexer = build_lexer;
	lexer.lex("").serialize.should.equal("");
	lexer.lex("babaabb22a").serialize.should.equal("b @ 1:1 // ab @ 1:2 // a @ 1:4 // ab @ 1:5 // b @ 1:7 // 22 @ 1:8 // a @ 1:10");
	lexer.lex("\n  ba \"a\nb\n\"a").serialize.should.equal("b @ 2:3 // a @ 2:4 // comment @ 2:6 // a @ 4:2");
	lexer.lex("baba").serialize.should.equal("b @ 1:1 // ab @ 1:2 // a @ 1:4");
	lexer.lex("  baba").serialize.should.equal("b @ 1:3 // ab @ 1:4 // a @ 1:6");
	lexer.lex("ba  ba").serialize.should.equal("b @ 1:1 // a @ 1:2 // b @ 1:5 // a @ 1:6");
	lexer.lex("baba  ").serialize.should.equal("b @ 1:1 // ab @ 1:2 // a @ 1:4");
}

@("Unknown tokens")
unittest {
	auto tokens = build_lexer.lex("\n  a!oh no, something went terribly wrong");
	tokens.front.to!string.should.equal("a @ 2:3");
	auto ex = tokens.popFront().should.throwException!LexerException.original;
	ex.to!string.should.equal("Failed lexing input at 2:4: Unknown input \"!oh no, something we...\"");
}

@("LexerRange")
unittest {
	import core.exception : AssertError;
	
	auto r = build_lexer.lex("ba");
	
	r.empty.should.equal(false);
	r.front.to!string.should.equal("b @ 1:1");
	
	r.popFront();
	
	r.empty.should.equal(false);
	r.front.to!string.should.equal("a @ 1:2");
	
	r.popFront();
	
	r.empty.should.equal(true);
	r.front.should.throwException!AssertError;
	r.popFront().should.throwException!AssertError;
}

@("Empty lexer")
unittest {
	auto lexer = Lexer([]);
	
	lexer.lex("").array.should.equal([]);
	
	auto ex = ({lexer.lex("a");}).should.throwException!LexerException.original;
	ex.to!string.should.equal("Failed lexing input at 1:1: Unknown input \"a\"");
}

@("LexerException")
unittest {
	auto ex = new LexerException("fuu", 42, 23);
	ex.line.should.equal(__LINE__ - 1);
	ex.file.should.equal(__FILE__);
	ex.msg.should.equal("fuu");
	ex.input_line.should.equal(42);
	ex.input_col.should.equal(23);
	ex.to!string.should.equal("Failed lexing input at 42:23: fuu");
}

// a|bbbb
// x(a)x
