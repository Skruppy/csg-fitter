// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module generic.bron_kerbosch;

import generic.set;
import generic.graph;



// BronKerbosch1(R, P, X):
//     if P and X are both empty:
//         report R as a maximal clique
//     for each vertex v in P:
//         BronKerbosch1(R ⋃ {v}, P ⋂ N(v), X ⋂ N(v))
//         P := P \ {v}
//         X := X ⋃ {v}
//
// BronKerbosch2(R,P,X):
//     if P and X are both empty:
//         report R as a maximal clique
//     choose a pivot vertex u in P ⋃ X
//     for each vertex v in P \ N(u):
//         BronKerbosch2(R ⋃ {v}, P ⋂ N(v), X ⋂ N(v))
//         P := P \ {v}
//         X := X ⋃ {v}
template bron_kerbosch(T) {
	private struct State {
		private UndirectedGraph!T graph;
		private T[][] results;
		
		private void bron_kerbosch_int(Set!T r, Set!T p, Set!T x) {
			if(p.empty && x.empty) {
				results ~= r[];
				return;
			}
			
			foreach(v; p[]) {
				auto n = Set!T(graph.neighbours(v));
				bron_kerbosch_int(r~v, p&n, x&n);
				p.remove(v);
				x.insert(v);
			}
		}
	}
	
	public T[][] bron_kerbosch(UndirectedGraph!T graph) {
		auto state = State(graph, new T[][0]);
		state.bron_kerbosch_int(Set!T(), Set!T(graph.nodes), Set!T());
		return state.results;
	}
}



version(unittest) {
private:
	import fluent.asserts;
	import std.algorithm.sorting : sort;
}

@("Example 1")
unittest {
	auto g = UndirectedGraph!int([1, 2, 3, 4, 5]);
	g.add_edge(1, 2);
	g.add_edge(1, 3);
	g.add_edge(1, 4);
	g.add_edge(2, 3);
	g.add_edge(4, 3);
	g.add_edge(3, 5);
	
	auto mcs = g.bron_kerbosch;
	foreach(mc; mcs) {
		mc.sort;
	}
	mcs.length.should.equal(3);
	mcs.should.containOnly([[1, 2, 3], [1, 3, 4], [3, 5]]);
}

@("Example 2")
unittest {
	auto g = UndirectedGraph!int([1, 2, 3, 4, 5, 6]);
	g.add_edge(1, 2);
	g.add_edge(2, 3);
	g.add_edge(3, 4);
	g.add_edge(4, 5);
	g.add_edge(5, 1);
	g.add_edge(5, 2);
	g.add_edge(4, 6);
	
	auto mcs = g.bron_kerbosch;
	foreach(mc; mcs) {
		mc.sort;
	}
	mcs.length.should.equal(5);
	mcs.should.containOnly([[1, 2, 5], [2, 3], [3, 4], [4, 5], [4, 6]]);
}
