// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module libcsgfitter.reader.hcsg.core;

import libcsgfitter.csgtree;
import libcsgfitter.properties;
import libcsgfitter.reader.hcsg.lexer;
import libcsgfitter.reader.hcsg.parser;
import std.file : readText;



public auto read_hcsg_file(in string path) {
	return read_hcsg_str(readText(path));
}

public auto read_hcsg_str(in string str) {
	auto lexer = constructLexer();
	auto parser = constructParser();
	auto ast = parser.parse(lexer.lex(str)).term;
	if(ast is null) { return null; }
	auto visitor = new Visitor();
	ast.accept(visitor);
	return visitor.csgNodes[ast];
}


private final class Visitor : HcagAstVisitor {
	private CsgNode[HcsgTerm] csgNodes;
	
	private void set(scope HcsgTerm astNode, scope CsgNode csgNode) {
		assert(astNode !in csgNodes);
		csgNodes[astNode] = csgNode;
	}
	
	private CsgNode get(scope HcsgTerm astNode) {
		return csgNodes[astNode];
	}
	
	public override void visitPost(HcsgCube node) {
		set(node, new CubeGeo(
			Coord(node.pos),
			Dimension(node.dim),
			DegRotation(node.rot).asRad,
		));
	}
	
	public override void visitPost(HcsgCylinder node) {
		set(node, new CylinderGeo(
			Coord(node.pos),
			DegRotation(node.rot).asRad,
			node.diameter / 2,
			node.height,
		));
	}
	
	public override void visitPost(HcsgSphere node) {
		set(node, new SphereGeo(
			Coord(node.pos),
			node.diameter / 2,
		));
	}
	
	public override void visitPost(HcsgOp node) {
		final switch(node.op) {
			case EBoolOp.UNION:        set(node, new UnionOp([get(node.lhs), get(node.rhs)])); break;
			case EBoolOp.INTERSECTION: set(node, new InterOp([get(node.lhs), get(node.rhs)])); break;
			case EBoolOp.MINUS:        set(node, new SubOp(  [get(node.lhs), get(node.rhs)])); break;
		}
	}
}



version(unittest) {
private:
	import fluent.asserts;
	import generic.tree;
	import generic.util;
}

unittest {
	read_hcsg_str(``).should.equal(null);
	read_hcsg_str(`
		(
			move( rotate(block(1, 2, 3), (4, 5, 6)), (7, 8, 9))
			∪
			(
				(
					rotate(cylinder(10, 11), (12, 13, 14))
					∩
					sphere(15)
				)
				∖
				move(sphere(16), (17, 18, 19))
			)
		)
	`)
	.pprinter.toString.should.equal(`
	[#] UNION
	 ├───> Cube[ Coord( 7 | 8 | 9 ), Dimension( 1 | 2 | 3 ), RadRot( 4° | 5° | 6° ) ]
	 └─[#] SUBTRACT
	    ├─[#] INTERSECTION
	    │  ├───> Cylinder[ Coord( 0 | 0 | 0 ), RadRot( 12° | 13° | 14° ), 5.000000, 11.000000 ]
	    │  └───> Sphere[ Coord( 0 | 0 | 0 ), 7.500000 ]
	    └───> Sphere[ Coord( 17 | 18 | 19 ), 8.000000 ]
	`.mls);
}
