// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module libcsgfitter.reader.hcsg.lexer;

import generic.lexer;
import std.conv : to;



public enum EBoolOp {
	UNION = "∪",
	INTERSECTION = "∩",
	MINUS = "∖",
}


package class BracketOpenT : Token {
	override string toString() const { return "("; }
}

package class BracketCloseT : Token {
	override string toString() const { return ")"; }
}

package class CommaT : Token {
	override string toString() const { return ","; }
}

package class MoveT : Token {
	override string toString() const { return "move"; }
}

package class RotateT : Token {
	override string toString() const { return "rotate"; }
}

package class BlockT : Token {
	override string toString() const { return "block"; }
}

package class CylinderT : Token {
	override string toString() const { return "cylinder"; }
}

package class SphereT : Token {
	override string toString() const { return "sphere"; }
}

package class BoolOpT : Token {
	EBoolOp op;
	this(string op) {
		switch(op) {
			case "∪":
			case "&":
				this.op = EBoolOp.UNION;
				break;
			case "∩":
			case "|":
				this.op = EBoolOp.INTERSECTION;
				break;
			case "∖":
			case "-":
				this.op = EBoolOp.MINUS;
				break;
			default:
				assert(false);
		}
	}
	override string toString() const { return op; }
}

package class NumberT : Token {
	immutable double nr;
	this(string nr) { this.nr = nr.to!double; }
	override string toString() const { return nr.to!string; }
}

package auto ref constructLexer() {
	return Lexer([
		Matcher(`\s+`,      m => null),
		Matcher(`\(`,       m => new BracketOpenT()),
		Matcher(`\)`,       m => new BracketCloseT()),
		Matcher(`,`,        m => new CommaT()),
		Matcher(`move`,     m => new MoveT()),
		Matcher(`rotate`,   m => new RotateT()),
		Matcher(`block`,    m => new BlockT()),
		Matcher(`cylinder`, m => new CylinderT()),
		Matcher(`sphere`,   m => new SphereT()),
		Matcher(`∪|∩|∖|&|\||-`, m => new BoolOpT(m[0])), // https://www.fileformat.info/info/unicode/block/mathematical_operators/list.htm
		Matcher(`[+-]{0,1}(([0-9]+(\.[0-9]*){0,1})|(\.[0-9]+))`, m => new NumberT(m[0])),
	]);
}



version(unittest) {
private:
	import fluent.asserts;
	import std.algorithm.iteration : map;
	import std.array : join, array;
	
	auto test_lexer = constructLexer();
	
	void should_lexical_equal(string input, string reference) {
		test_lexer.lex(input).array.map!(to!string).join(" ").should.equal(reference);
	}
}

@("Simple Symbols")
unittest {
	foreach(name; ["(", ")", ",", "move", "rotate", "block", "cylinder", "sphere"]) {
		should_lexical_equal(name, name);
	}
	
	({test_lexer.lex("move  foo").array;}).should.throwException!LexerException
	.original.to!string.should.equal("Failed lexing input at 1:7: Unknown input \"foo\"");
}

@("Operations")
unittest {
	import core.exception : AssertError;
	should_lexical_equal("∪", "∪");
	should_lexical_equal("&", "∪");
	should_lexical_equal("∩", "∩");
	should_lexical_equal("|", "∩");
	should_lexical_equal("∖", "∖");
	should_lexical_equal("-", "∖");
	({new BoolOpT("?");}).should.throwException!AssertError;
}

@("Numbers")
unittest {
	foreach(pre; ["", "+", "-"]) {
		auto pre2 = pre == "-" ? "-" : "";
		should_lexical_equal(pre~"42", pre2~"42");
		should_lexical_equal(pre~"42.", pre2~"42");
		should_lexical_equal(pre~"4.2", pre2~"4.2");
		should_lexical_equal(pre~".42", pre2~"0.42");
		should_lexical_equal(pre~"00.42", pre2~"0.42");
	}
}

@("Complex")
unittest {
	test_lexer.lex("
		( )
		∪ &
		∩ |
		∖ -
		,
		move rotate
		block cylinder sphere
		12 3.4 56. .78
	").array.map!(to!string).join(" ").should.equal("( ) ∪ ∪ ∩ ∩ ∖ ∖ , move rotate block cylinder sphere 12 3.4 56 0.78");
}
