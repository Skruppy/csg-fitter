// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module libcsgfitter.reader.hcsg.parser;

import generic.parser.ll1;
import generic.parser.ll1.helper;
import libcsgfitter.reader.hcsg.lexer;
import std.format : format;



// ==== Abstract Syntax Tree (AST) ============================================
package struct Pos {
	public size_t line;
	public size_t col;
}

package abstract class HcagAstVisitor {
	public void visitPre(HcsgCube node)      { visitPreGeo(node); }
	public void visitPre(HcsgCylinder node)  { visitPreGeo(node); }
	public void visitPre(HcsgSphere node)    { visitPreGeo(node); }
	public void visitPre(HcsgOp node)        { visitPreAll(node); }
	public void visitPreGeo(HcsgPrim node)   { visitPreAll(node); }
	public void visitPreAll(HcsgTerm)        {  }
	
	public void visitPost(HcsgCube node)     { visitPostGeo(node); }
	public void visitPost(HcsgCylinder node) { visitPostGeo(node); }
	public void visitPost(HcsgSphere node)   { visitPostGeo(node); }
	public void visitPost(HcsgOp node)       { visitPostAll(node); }
	public void visitPostGeo(HcsgPrim node)  { visitPostAll(node); }
	public void visitPostAll(HcsgTerm)       {  }
}

/*
*  HcsgTerm
*   +-- HcsgOp
*   `-- HcsgPrim
*        +-- Cube
*        +-- Cylinder
*        `-- Sphere
*/
package abstract class HcsgTerm {
	public Pos input_pos;
	public override abstract string toString() const;
	public abstract void accept(scope HcagAstVisitor);
}

package final class HcsgOp : HcsgTerm {
	public EBoolOp op;
	public HcsgTerm lhs;
	public HcsgTerm rhs;
	public override void accept(scope HcagAstVisitor visitor) {
		visitor.visitPre(this);
		lhs.accept(visitor);
		rhs.accept(visitor);
		visitor.visitPost(this);
	}
	public override string toString() const {
		return format("( %s  %s  %s )", lhs, cast(string)op, rhs);
	}
}

package abstract class HcsgPrim : HcsgTerm {
	public double[3] pos = 0;
	public double[3] rot = 0;
	protected void setPos(in NumberT v1, in NumberT v2, in NumberT v3) {
		pos[0] = v1.nr; pos[1] = v2.nr; pos[2] = v3.nr;
	}
	protected void setRot(in NumberT v1, in NumberT v2, in NumberT v3) {
		rot[0] = v1.nr; rot[1] = v2.nr; rot[2] = v3.nr;
	}
}

package final class HcsgCube : HcsgPrim {
	public double[3] dim;
	public override void accept(scope HcagAstVisitor visitor) {
		visitor.visitPre(this);
		visitor.visitPost(this);
	}
	public override string toString() const {
		return format("Cube[ Pos( %(%s | %) ), Rot( %(%s | %) ), Dim( %(%s | %) ) ]", pos, rot, dim);
	}
}

package final class HcsgCylinder : HcsgPrim {
	public double diameter;
	public double height;
	public override void accept(scope HcagAstVisitor visitor) {
		visitor.visitPre(this);
		visitor.visitPost(this);
	}
	public override string toString() const {
		return format("Cylinder[ Pos( %(%s | %) ), Rot( %(%s | %) ), %s, %s ]", pos, rot, diameter, height);
	}
}

package final class HcsgSphere : HcsgPrim {
	public double diameter;
	protected override void setRot(in NumberT, in NumberT, in NumberT) {
		throw new Exception("Can't set rotation on a sphere.");
	}
	public override void accept(scope HcagAstVisitor visitor) {
		visitor.visitPre(this);
		visitor.visitPost(this);
	}
	public override string toString() const {
		return format("Sphere[ Pos( %(%s | %) ), Rot( %(%s | %) ), %s ]", pos, rot, diameter);
	}
}



// ==== Parse Tree ============================================================
/*
S -> .
S -> COMPLEX_OBJECT .

COMPLEX_OBJECT -> PRIMITIVE_MOVE .
COMPLEX_OBJECT -> ( COMPLEX_OBJECT boolean_operation COMPLEX_OBJECT ) .

PRIMITIVE_MOVE -> PRIMITIVE_ROTATE .
PRIMITIVE_MOVE -> move ( PRIMITIVE_ROTATE , ( nr , nr , nr ) ) .

PRIMITIVE_ROTATE -> PRIMITIVE_GEO .
PRIMITIVE_ROTATE -> rotate ( PRIMITIVE_GEO , ( nr , nr , nr ) ) .

PRIMITIVE_GEO -> block    ( nr , nr , nr ) .
PRIMITIVE_GEO -> sphere   ( nr ) .
PRIMITIVE_GEO -> cylinder ( nr , nr ) .
*/


private final class Ctx {}

private final class StartNT : NonTerminal {
	HcsgTerm term;
	
	@ll1production!(End)
	this(Ctx ctx) {}
	
	@ll1production!(BracketOpenT, MoveT, RotateT, BlockT, CylinderT, SphereT)
	this(Ctx ctx,  ComplexObjectNT obj) {
		this.term = obj.term;
	}
}

private final class ComplexObjectNT : NonTerminal {
	HcsgTerm term;
	
	@ll1production!(MoveT, RotateT, BlockT, CylinderT, SphereT)
	this(Ctx ctx,  PrimitiveMoveNT primObj) {
		this.term = primObj.term;
	}
	
	@ll1production!(BracketOpenT)
	this(Ctx ctx,  BracketOpenT,  ComplexObjectNT lhs,  BoolOpT op,  ComplexObjectNT rhs,  BracketCloseT) {
		auto term = new HcsgOp();
		term.lhs = lhs.term;
		term.op = op.op;
		term.rhs = rhs.term;
		this.term = term;
	}
}

private final class PrimitiveMoveNT : NonTerminal {
	HcsgPrim term;
	
	@ll1production!(RotateT, BlockT, CylinderT, SphereT)
	this(Ctx ctx,  PrimitiveRotateNT rot) {
		this.term = rot.term;
	}
	
	@ll1production!(MoveT)
	this(Ctx ctx,  MoveT,  BracketOpenT,  PrimitiveRotateNT rot,  CommaT,  BracketOpenT,  NumberT p_x,  CommaT,
		NumberT p_y,  CommaT,  NumberT p_z,  BracketCloseT,  BracketCloseT
	) {
		this.term = rot.term;
		this.term.setPos(p_x, p_y, p_z);
	}
}

private final class PrimitiveRotateNT : NonTerminal {
	HcsgPrim term;
	
	@ll1production!(BlockT, CylinderT, SphereT)
	this(Ctx ctx,  PrimitiveGeoNT geo) {
		this.term = geo.term;
	}
	
	@ll1production!(RotateT)
	this(Ctx ctx,  RotateT,  BracketOpenT,  PrimitiveGeoNT geo,  CommaT,  BracketOpenT,  NumberT r_x,  CommaT,
		NumberT r_y,  CommaT,  NumberT r_z,  BracketCloseT,  BracketCloseT
	) {
		this.term = geo.term;
		this.term.setRot(r_x, r_y, r_z);
	}
}

private final class PrimitiveGeoNT : NonTerminal {
	HcsgPrim term;
	
	@ll1production!(BlockT)
	this(Ctx ctx,  BlockT,  BracketOpenT,  NumberT d_x,  CommaT,  NumberT d_y,  CommaT,  NumberT d_z,  BracketCloseT) {
		auto term = new HcsgCube();
		term.dim[0] = d_x.nr;
		term.dim[1] = d_y.nr;
		term.dim[2] = d_z.nr;
		this.term = term;
	}
	
	@ll1production!(CylinderT)
	this(Ctx ctx,  CylinderT,  BracketOpenT,  NumberT diameter,  CommaT,  NumberT height,  BracketCloseT) {
		auto term = new HcsgCylinder();
		term.diameter = diameter.nr;
		term.height = height.nr;
		this.term = term;
	}
	
	@ll1production!(SphereT)
	this(Ctx ctx,  SphereT,  BracketOpenT,  NumberT diameter,  BracketCloseT) {
		auto term = new HcsgSphere();
		term.diameter = diameter.nr;
		this.term = term;
	}
}

package auto constructParser() {
	return LL1Parser!(Ctx, StartNT)(collectLL1Productions!(
		Ctx,
		StartNT,
		ComplexObjectNT,
		PrimitiveMoveNT,
		PrimitiveRotateNT,
		PrimitiveGeoNT,
	));
}



version(unittest) {
private:
	import fluent.asserts;
	import std.conv : to;
}

unittest {
	auto lexer = constructLexer();
	auto parser = constructParser();
	
	parser.parse(lexer.lex(``)).term.should.equal(null);
	
	parser.parse(lexer.lex(`sphere(4)`)).term.to!string.should
	.equal(`Sphere[ Pos( 0 | 0 | 0 ), Rot( 0 | 0 | 0 ), 4 ]`);
	
	parser.parse(lexer.lex(`cylinder(4, 2)`)).term.to!string.should
	.equal(`Cylinder[ Pos( 0 | 0 | 0 ), Rot( 0 | 0 | 0 ), 4, 2 ]`);
	
	parser.parse(lexer.lex(`block(4, 2, 1)`)).term.to!string.should
	.equal(`Cube[ Pos( 0 | 0 | 0 ), Rot( 0 | 0 | 0 ), Dim( 4 | 2 | 1 ) ]`);
	
	parser.parse(lexer.lex(`move(cylinder(4, 2), (5, 6, 7))`)).term.to!string.should
	.equal(`Cylinder[ Pos( 5 | 6 | 7 ), Rot( 0 | 0 | 0 ), 4, 2 ]`);
	
	parser.parse(lexer.lex(`rotate(cylinder(4, 2), (5, 6, 7))`)).term.to!string.should
	.equal(`Cylinder[ Pos( 0 | 0 | 0 ), Rot( 5 | 6 | 7 ), 4, 2 ]`);
	
	auto ex = parser.parse(lexer.lex(`rotate(sphere(4), (5, 6, 7))`)).should.throwException!Exception
	.withMessage.should.equal(`Can't set rotation on a sphere.`);
	
	parser.parse(lexer.lex(`
		move(
			rotate(
				cylinder(4, 2),
				(1, 2, 3)
			),
			(11, 22, 33)
		)
	`)).term.to!string.should
	.equal(`Cylinder[ Pos( 11 | 22 | 33 ), Rot( 1 | 2 | 3 ), 4, 2 ]`);
	
	parser.parse(lexer.lex(`(sphere(3) ∪ sphere(4))`)).term.to!string.should
	.equal(`( Sphere[ Pos( 0 | 0 | 0 ), Rot( 0 | 0 | 0 ), 3 ]  ∪  Sphere[ Pos( 0 | 0 | 0 ), Rot( 0 | 0 | 0 ), 4 ] )`);
	
	parser.parse(lexer.lex(`(sphere(3) ∩ (sphere(4) ∖ sphere(5)))`)).term.to!string.should
	.equal(`( Sphere[ Pos( 0 | 0 | 0 ), Rot( 0 | 0 | 0 ), 3 ]  ∩  ( Sphere[ Pos( 0 | 0 | 0 ), Rot( 0 | 0 | 0 ), 4 ]  ∖  Sphere[ Pos( 0 | 0 | 0 ), Rot( 0 | 0 | 0 ), 5 ] ) )`);
	
	parser.parse(lexer.lex(`sphere 4)`)).should.throwException!ParserException;
	parser.parse(lexer.lex(`sphere(4`)).should.throwException!ParserException;
	parser.parse(lexer.lex(`sphere(4))`)).should.throwException!ParserException;
	parser.parse(lexer.lex(`rotate( move(cylinder(4, 2), (1, 2, 3)), (11, 22, 33))`)).should.throwException!ParserException;
}
