// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module libcsgfitter.reader.json.str2json;

import generic.tree;



package auto str2json(in string str) {
	import generic.util : pop;
	import std.container.slist : SList;
	import std.json : parseJSON;
	
	auto root = new JsonNode(str.parseJSON, 0);
	int nid = 1;
	
	auto unparsedObjects = SList!JsonNode([root]);
	while(!unparsedObjects.empty) {
		auto node = unparsedObjects.pop();
		
		auto children = "childs" in node.data;
		if(children == null) continue;
		
		foreach(child; children.array) {
			unparsedObjects.insert(new JsonNode(child, nid++, node));
		}
	}
	
	return root;
}

package class JsonNode : Node!(JsonNode, JsonVisitor) {
	import std.conv : to;
	import std.json : JSONType;
	import std.json : JSONValue;
	
	mixin TreeVisit!JsonVisitor;
	
	private JSONValue _data;
	private int _nid;
	
	public this(JSONValue data, int nid, JsonNode parent=null) {
		super(parent);
		if(data.type != JSONType.object) throw new Exception("Failed to parse tree. Unknown type of object: "~data.type);
		_data = data;
		_nid = nid;
	}
	
	public JSONValue data() {
		return _data;
	}
	
	public int nid() {
		return _nid;
	}
	
	public override string toString() {
		return parent is null ? _nid.to!string : _nid.to!string~" ["~position.to!string~"]";
	}
}

package class JsonVisitor {
	void visitPre(JsonNode node) { visitPreAll(node); }
	void visitPreAll(JsonNode node) {}
	
	void visitPost(JsonNode node) { visitPostAll(node); }
	void visitPostAll(JsonNode node) {}
}
