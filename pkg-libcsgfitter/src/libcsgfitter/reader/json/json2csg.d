// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module libcsgfitter.reader.json.json2csg;

import generic.tree : walk;
import libcsgfitter.csgtree;
import libcsgfitter.properties;
import libcsgfitter.reader.json.str2json;
import std.algorithm.iteration : map;
import std.conv : to;
import std.format : format;
import std.json : JSONException;
import std.json : JSONType;
import std.json : JSONValue;
import std.range : array;



package auto json2csg(scope JsonNode root) {
	CsgNode[JsonNode] annotations;
	auto scope visitor = new class JsonVisitor {
		override public void visitPre(JsonNode node) {}
		
		override public void visitPost(JsonNode node) {
			try {
				annotations[node] = "op" in node.data
					? parseBranch(node, node.children.map!(x => annotations[x]).array)
					: parseLeaf(node);
			}
			catch(JSONException ex) {
				throw new Exception("Error while handling node "~node.path, __FILE__, __LINE__, ex);
			}
		}
	};
	visitor.walk(root);
	return annotations[root];
}

private CsgNode parseBranch(scope JsonNode node, scope CsgNode[] children) {
	switch(node.data["op"].str) {
		case "union":     return new UnionOp(children);
		case "intersect": return new InterOp(children);
		case "subtract":  return new SubOp(children);
		default: throw new Exception("Unknown operation \"%s\"".format(node.data["op"].str));
	}
}

private CsgNode parseLeaf(scope JsonNode node) {
	auto params = node.data["params"];
	switch(node.data["geo"].str) {
		case "cube":
			return new CubeGeo(
				Coord(params["center"].triple),
				Dimension(params["radius"].triple.map!"a*2".array),
				RadRotation(params["rotation"].triple),
			);
		
		case "cylinder":
			return new CylinderGeo(
				Coord(params["center"].triple),
				RadRotation(params["rotation"].triple),
				params["radius"].number,
				params["height"].number,
			);
		
		case "sphere":
			return new SphereGeo(
				Coord(params["center"].triple),
				params["radius"].number,
			);
		
		default:
			throw new Exception("Unknown geometry \"%s\"".format(node.data["geo"].str));
	}
}

private double[] triple(in JSONValue v) {
	return v.array.map!(x => x.number).array;
}

private auto number(in JSONValue v) {
	switch(v.type) {
		case JSONType.float_:   return v.floating.to!double;
		case JSONType.integer:  return v.integer.to!double;
		case JSONType.uinteger: return v.uinteger.to!double;
		default: throw new JSONException("Not a number type");
	}
}
