// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module libcsgfitter.reader.json.core;

import libcsgfitter.reader.json.json2csg;
import libcsgfitter.reader.json.str2json;



public auto read_json_file(in string path) {
	import std.file : readText;
	
	return read_json_str(readText(path));
}

public auto read_json_str(in string str) {
	return str.str2json.json2csg;
}
