// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module libcsgfitter.transformation;

import libcsgfitter.properties;
import libcsgfitter.point;
import std.math : cos;
import std.math : sin;



struct Transformation {
	public double[4][4] m = [[0.0, 0.0, 0.0, 0.0], [0.0, 0.0, 0.0, 0.0], [0.0, 0.0, 0.0, 0.0], [0.0, 0.0, 0.0, 0.0]];
	public double[3][3] normals = [[0.0, 0.0, 0.0], [0.0, 0.0, 0.0], [0.0, 0.0, 0.0]];
	
	
	public Transformation opBinary(string op)(Transformation b) const if(op == "*") {
		alias a = this;
		Transformation r;
		
		foreach(x; 0..4)
		foreach(y; 0..4)
		foreach(i; 0..4)
			r.m[y][x] += a.m[y][i] * b.m[i][x];
		
		foreach(x; 0..3)
		foreach(y; 0..3)
		foreach(i; 0..3)
			r.normals[y][x] += a.normals[y][i] * b.normals[i][x];
		
		return r;
	}
	
	
	public Coord opBinary(string op)(Coord b) const if(op == "*") {
		Coord ret;
		
		foreach(i, ref a; m[0..3])
			ret.triple[i] = a[0]*b.x + a[1]*b.y + a[2]*b.z + a[3];
		
		return ret;
	}
	
	
	public Point opBinary(string op)(Point b) const if(op == "*") {
		Point ret;
		ret.origin = b.origin;
		
		foreach(i, ref a; m[0..3])
			ret.position.triple[i] = a[0]*b.position.x + a[1]*b.position.y + a[2]*b.position.z + a[3];
		
		return ret;
	}
	
	
	public NormalPoint opBinary(string op)(NormalPoint b) const if(op == "*") {
		NormalPoint ret;
		ret.origin = b.origin;
		
		foreach(i, ref a; m[0..3])
			ret.position.triple[i] = a[0]*b.position.x + a[1]*b.position.y + a[2]*b.position.z + a[3];
		
		foreach(i, ref a; normals[0..3])
			ret.normal.triple[i] = a[0]*b.normal.x + a[1]*b.normal.y + a[2]*b.normal.z;
		
		return ret;
	}
}



Transformation translation(Coord newOrigin) {
	return Transformation([
		[1.0, 0.0, 0.0, newOrigin.x],
		[0.0, 1.0, 0.0, newOrigin.y],
		[0.0, 0.0, 1.0, newOrigin.z],
		[0.0, 0.0, 0.0, 1.0],
	],
	[
		[1.0, 0.0, 0.0],
		[0.0, 1.0, 0.0],
		[0.0, 0.0, 1.0]
	]);
}


Transformation scale(Dimension s) {
	return Transformation([
		[s.x, 0.0, 0.0, 0.0],
		[0.0, s.y, 0.0, 0.0],
		[0.0, 0.0, s.z, 0.0],
		[0.0, 0.0, 0.0, 1.0],
	],
	[
		[1.0, 0.0, 0.0],
		[0.0, 1.0, 0.0],
		[0.0, 0.0, 1.0]
	]);
}


Transformation scale(double s) {
	return Transformation([
		[s  , 0.0, 0.0, 0.0],
		[0.0, s  , 0.0, 0.0],
		[0.0, 0.0, s  , 0.0],
		[0.0, 0.0, 0.0, 1.0],
	],
	[
		[1.0, 0.0, 0.0],
		[0.0, 1.0, 0.0],
		[0.0, 0.0, 1.0]
	]);
}


Transformation rotateX(double phi) {
	return Transformation([
		[1.0,      0.0,       0.0, 0.0],
		[0.0, cos(phi), -sin(phi), 0.0],
		[0.0, sin(phi),  cos(phi), 0.0],
		[0.0,      0.0,       0.0, 1.0],
	],
	[
		[1.0,      0.0,       0.0],
		[0.0, cos(phi), -sin(phi)],
		[0.0, sin(phi),  cos(phi)]
	]);
}


Transformation rotateY(double phi) {
	return Transformation([
		[ cos(phi), 0.0, sin(phi), 0.0],
		[      0.0, 1.0,      0.0, 0.0],
		[-sin(phi), 0.0, cos(phi), 0.0],
		[      0.0, 0.0,      0.0, 1.0],
	],
	[
		[ cos(phi), 0.0, sin(phi)],
		[      0.0, 1.0,      0.0],
		[-sin(phi), 0.0, cos(phi)]
	]);
}


Transformation rotateZ(double phi) {
	return Transformation([
		[cos(phi), -sin(phi), 0.0, 0.0],
		[sin(phi),  cos(phi), 0.0, 0.0],
		[     0.0,       0.0, 1.0, 0.0],
		[     0.0,       0.0, 0.0, 1.0],
	],
	[
		[cos(phi), -sin(phi), 0.0],
		[sin(phi),  cos(phi), 0.0],
		[     0.0,       0.0, 1.0],
	]);
}


Transformation rotate(RadRotation rotation) {
	return rotateX(rotation.x) * rotateY(rotation.y) * rotateZ(rotation.z);
}


Transformation rotate(DegRotation rotation) {
	return rotate(rotation.asRad());
}



version(unittest) {
private:
	import fluent.asserts;
}

@("Multiply Transformation by Coordinate")
unittest {
	immutable Transformation m = Transformation([
		[2.0, 0.0, 0.0,  30.0],
		[0.0, 3.0, 0.0,  23.0],
		[0.0, 0.0, 1.5, 100.0],
		[0.0, 0.0, 0.0,   1.0],
	]);
	
	immutable Coord a = m * Coord(2.0, 3.0, 4.0);
	a.x.should.equal(34.0);
	a.y.should.equal(32.0);
	a.z.should.equal(106.0);
}

@("Multiply Transformation by Transformation")
unittest {
	immutable Transformation a = Transformation([
		[7,11,31,41],
		[23,29,2,53],
		[43,3,17,5],
		[13,37,19,47]
	]);
	
	immutable Transformation b = Transformation([
		[21,12,25,10],
		[6,18,14,24],
		[26,20,8,22],
		[9,4,16,15]
	]);
	
	(a*b).m.should.equal([
		[1388.0, 1066.0, 1233.0, 1631.0],
		[1186.0, 1050.0, 1845.0, 1765.0],
		[1408.0,  930.0, 1333.0,  951.0],
		[1412.0, 1390.0, 1747.0, 2141.0]
	]);
}
