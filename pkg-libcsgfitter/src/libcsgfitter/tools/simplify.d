// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module libcsgfitter.tools.simplify;

import generic.tree;
import generic.set;
import generic.util;
import libcsgfitter.csgtree;
import std.container.dlist : DList;
import std.range : lockstep;
import std.algorithm.iteration : each;


public auto simplify(CsgNode root) {
	scope fakeRoot = new UnionOp([root]);
	
	scope visitor = new Deduplicator();
	visitor.walk(root);
	
	auto new_root = fakeRoot.first_child;
	new_root.parent = null;
	return new_root;
}


private bool is_empty_op(CsgNode node) {
	return node.type.is_op && ! node.hasChildren;
}


// See csgnode.cpp:1160
private class Deduplicator : CsgVisitor {
	auto hashCash = HashInfo(false);
	
	override public void visitPost(UnionOp op) {
		// Remove duplicates or empty child ops
		Set!uint seen;
		CsgNode[] killList;
		
		foreach(child; op.children) {
			auto hash = child.hash(hashCash);
			if(seen.contains(hash) || child.is_empty_op) {
				killList ~= child;
			}
			else {
				seen.insert(hash);
			}
		}
		
		killList.each!"a.remove";
		
		// Replace self by only child, if applicable
		if(op.first_child !is null && op.first_child == op.last_child) {
			op.replaceWith(op.first_child); // TODO
		}
	}
	
	override public void visitPost(InterOp op) {
		// Remove duplicates and empty ops with empty children
		Set!uint seen;
		CsgNode[] killList;
		
		foreach(child; op.children) {
			if(child.is_empty_op) {
				op.clear();
				return;
			}
			
			auto hash = child.hash(hashCash);
			if(seen.contains(hash)) {
				killList ~= child;
			}
			else {
				seen.insert(hash);
			}
		}
		
		killList.each!"a.remove";
		
		// Replace self by only child, if applicable
		if(op.first_child !is null && op.first_child == op.last_child) {
			op.replaceWith(op.first_child); // TODO
		}
	}
	
	override public void visitPost(SubOp op) {
		if( ! op.hasChildren) {
			return;
		}
		
		// Remove duplicates or empty child ops
		Set!uint seen;
		CsgNode[] killList;
		auto children = op.children;
		
		auto baseNode = children.pop;
		if(baseNode.is_empty_op) {
			op.clear();
			return;
		}
		
		auto baseHash = baseNode.hash(hashCash);
		
		foreach(child; children) {
			auto hash = child.hash(hashCash);
			if(hash == baseHash) {
				op.clear();
				return;
			}
			else if(seen.contains(hash) || child.is_empty_op) {
				killList ~= child;
			}
			else {
				seen.insert(hash);
			}
		}
		
		killList.each!"a.remove";
		
		// Replace self by only child, if applicable
		if(op.first_child !is null && op.first_child == op.last_child) {
			op.replaceWith(op.first_child); // TODO
		}
	}
	
	override public void visitPostOp(Op op) {
		throw new Exception("Op is unhandled by simplify()");
	}
}



version(unittest) {
private:
	import fluent.asserts;
	import std.algorithm.sorting : sort;
	import std.functional : binaryFun;
	import std.conv;
	import generic.tree;
	import generic.util;
	import libcsgfitter.properties;
}

@("UnionOp: Duplicates")
unittest {
	CsgNode root = new UnionOp([
		new UnionOp([new SphereGeo(Coord(), 1), new SphereGeo(Coord(), 2)]),
		new UnionOp([new SphereGeo(Coord(), 3), new SphereGeo(Coord(), 4)]),
		new UnionOp([new SphereGeo(Coord(), 2), new SphereGeo(Coord(), 1)]),
	]);
	root.simplify().pprinter.toString.should.equal(`
	[#] UNION
	 ├─[#] UNION
	 │  ├───> Sphere[ Coord( 0 | 0 | 0 ), 1.000000 ]
	 │  └───> Sphere[ Coord( 0 | 0 | 0 ), 2.000000 ]
	 └─[#] UNION
	    ├───> Sphere[ Coord( 0 | 0 | 0 ), 3.000000 ]
	    └───> Sphere[ Coord( 0 | 0 | 0 ), 4.000000 ]
	`.mls);
}


@("UnionOp: Empty child ops")
unittest {
	CsgNode root = new UnionOp([
		new UnionOp(),
		new InterOp(),
		new SubOp(),
	]);
	root.simplify().pprinter.toString.should.equal(`
	──> UNION
	`.mls);
}

@("UnionOp: Single child")
unittest {
	CsgNode root = new UnionOp([
		new UnionOp([new SphereGeo(Coord(), 1)]),
	]);
	root.simplify().pprinter.toString.should.equal(`
	──> Sphere[ Coord( 0 | 0 | 0 ), 1.000000 ]
	`.mls);
}



@("InterOp: Duplicates")
unittest {
	CsgNode root = new InterOp([
		new UnionOp([new SphereGeo(Coord(), 1), new SphereGeo(Coord(), 2)]),
		new UnionOp([new SphereGeo(Coord(), 3), new SphereGeo(Coord(), 4)]),
		new UnionOp([new SphereGeo(Coord(), 2), new SphereGeo(Coord(), 1)]),
	]);
	root.simplify().pprinter.toString.should.equal(`
	[#] INTERSECTION
	 ├─[#] UNION
	 │  ├───> Sphere[ Coord( 0 | 0 | 0 ), 1.000000 ]
	 │  └───> Sphere[ Coord( 0 | 0 | 0 ), 2.000000 ]
	 └─[#] UNION
	    ├───> Sphere[ Coord( 0 | 0 | 0 ), 3.000000 ]
	    └───> Sphere[ Coord( 0 | 0 | 0 ), 4.000000 ]
	`.mls);
}


@("InterOp: Empty child ops")
unittest {
	CsgNode root = new InterOp([
		new UnionOp([new SphereGeo(Coord(), 1), new SphereGeo(Coord(), 2)]),
		new UnionOp(),
		new UnionOp([new SphereGeo(Coord(), 3), new SphereGeo(Coord(), 4)]),
	]);
	root.simplify().pprinter.toString.should.equal(`
	──> INTERSECTION
	`.mls);
}

@("InterOp: Single child")
unittest {
	CsgNode root = new InterOp([
		new InterOp([new SphereGeo(Coord(), 1)]),
	]);
	root.simplify().pprinter.toString.should.equal(`
	──> Sphere[ Coord( 0 | 0 | 0 ), 1.000000 ]
	`.mls);
}

@("SubOp: Empty op")
unittest {
	CsgNode root = new SubOp();
	root.simplify().pprinter.toString.should.equal(`
	──> SUBTRACT
	`.mls);
}

@("SubOp: minuend \\in subtrahend")
unittest {
	CsgNode root = new SubOp([
		new UnionOp([new SphereGeo(Coord(), 1), new SphereGeo(Coord(), 2)]),
		new UnionOp([new SphereGeo(Coord(), 3), new SphereGeo(Coord(), 4)]),
		new UnionOp([new SphereGeo(Coord(), 2), new SphereGeo(Coord(), 1)]),
	]);
	root.simplify().pprinter.toString.should.equal(`
	──> SUBTRACT
	`.mls);
}

@("SubOp: Duplicated subtrahend")
unittest {
	CsgNode root = new SubOp([
		new UnionOp([new SphereGeo(Coord(), 3), new SphereGeo(Coord(), 4)]),
		new UnionOp([new SphereGeo(Coord(), 1), new SphereGeo(Coord(), 2)]),
		new UnionOp([new SphereGeo(Coord(), 2), new SphereGeo(Coord(), 1)]),
	]);
	root.simplify().pprinter.toString.should.equal(`
	[#] SUBTRACT
	 ├─[#] UNION
	 │  ├───> Sphere[ Coord( 0 | 0 | 0 ), 3.000000 ]
	 │  └───> Sphere[ Coord( 0 | 0 | 0 ), 4.000000 ]
	 └─[#] UNION
	    ├───> Sphere[ Coord( 0 | 0 | 0 ), 1.000000 ]
	    └───> Sphere[ Coord( 0 | 0 | 0 ), 2.000000 ]
	`.mls);
}

@("SubOp: Empty subtrahend")
unittest {
	CsgNode root = new SubOp([
		new UnionOp([new SphereGeo(Coord(), 3), new SphereGeo(Coord(), 4)]),
		new UnionOp(),
		new UnionOp([new SphereGeo(Coord(), 1), new SphereGeo(Coord(), 2)]),
	]);
	root.simplify().pprinter.toString.should.equal(`
	[#] SUBTRACT
	 ├─[#] UNION
	 │  ├───> Sphere[ Coord( 0 | 0 | 0 ), 3.000000 ]
	 │  └───> Sphere[ Coord( 0 | 0 | 0 ), 4.000000 ]
	 └─[#] UNION
	    ├───> Sphere[ Coord( 0 | 0 | 0 ), 1.000000 ]
	    └───> Sphere[ Coord( 0 | 0 | 0 ), 2.000000 ]
	`.mls);
}

@("SubOp: Empty minuend")
unittest {
	CsgNode root = new SubOp([
		new UnionOp(),
		new UnionOp([new SphereGeo(Coord(), 1), new SphereGeo(Coord(), 2)]),
		new UnionOp([new SphereGeo(Coord(), 3), new SphereGeo(Coord(), 4)]),
	]);
	root.simplify().pprinter.toString.should.equal(`
	──> SUBTRACT
	`.mls);
}

@("SubOp: Single child")
unittest {
	CsgNode root = new SubOp([new SphereGeo(Coord(), 1)]);
	root.simplify().pprinter.toString.should.equal(`
	──> Sphere[ Coord( 0 | 0 | 0 ), 1.000000 ]
	`.mls);
}
