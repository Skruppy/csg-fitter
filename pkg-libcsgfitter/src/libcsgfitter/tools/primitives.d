// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module libcsgfitter.tools.primitives;

import generic.tree : walk;
import libcsgfitter.csgtree;



public Geo[] extract_primitives(CsgNode root) {
	scope auto visitor = new Visitor;
	visitor.walk(root);
	return visitor.primitives;
}


private class Visitor : CsgVisitor {
	private Geo[] primitives;
	override public void visitPostGeo(Geo node) {
		primitives ~= cast(Geo)node.clone;
	}
}



version(unittest) {
private:
	import fluent.asserts;
	import std.algorithm.iteration : map;
	import libcsgfitter.properties;
}

unittest {
	auto geos = [
		new CubeGeo(Coord(42, -23, 0), Dimension(0.5, 1, 3), RadRotation(-1, 0, 2)),
		new CylinderGeo(Coord(42, -23, 0), RadRotation(-1, 0, 2), 3, 0.5),
		new SphereGeo(Coord(42, -23, 0), 3),
		new CubeGeo(Coord(), Dimension(1, 1, 1), RadRotation()),
	];
	
	auto root = new UnionOp([
		new SubOp([
			geos[0],
			new InterOp([geos[1]]),
		]),
		geos[2],
		geos[3],
	]);
	
	auto found_geos = root.extract_primitives();
	geos.should.not.equal(found_geos).because("geos should be cloned");
	geos.map!"a.hash".should.equal(found_geos.map!"a.hash").because("cloned geos should still represent the same primitives");
}
