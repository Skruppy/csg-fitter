// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module libcsgfitter.tools.score;

import libcsgfitter.csgtree;
import libcsgfitter.evaluator.base;
import libcsgfitter.point;
import std.experimental.logger : infof;
import std.json : JSONValue;
import std.json : toJSON;
import std.stdio : File;



public auto score(NormalPoint[] reference, CsgNode model, Evaluator evaluator, string path="") {
	evaluator.update_reference(reference);
	auto score = evaluator.evaluate_model(model);
	
	infof("Using %s reference points, the model scored %s", reference.length, score);
	
	auto data_set = JSONValue([
		"score": JSONValue(score),
		"reference_point_cnt": JSONValue(reference.length),
	]);
	
	if(path != "") {
		File(path, "w").write(data_set.toJSON(true));
	}
	
	return score;
}
