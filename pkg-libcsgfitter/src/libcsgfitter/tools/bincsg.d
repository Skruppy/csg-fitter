// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module libcsgfitter.tools.bincsg;

import generic.tree;
import libcsgfitter.csgtree;
import std.container.dlist : DList;
import std.range : lockstep;



public auto binarize(CsgNode root) {
	scope visitor = new Visitor();
	visitor.walk(root);
	return visitor.annotations[root];
}


private class Visitor : CsgVisitor {
	private CsgNode[CsgNode] annotations;
	
	private auto children_of(Op op) {
		CsgNode[] children;
		foreach(child; op.children) {
			auto new_child = annotations[child];
			if(new_child !is null) {
				children ~= new_child;
			}
			annotations.remove(child);
		}
		return children;
	}
	
	override public void visitPost(UnionOp node) {
		scope auto children = children_of(node);
		
		if(children.length == 0) {
			annotations[node] = null;
		}
		else if(children.length == 1) {
			annotations[node] = children[0];
		}
		else {
			annotations[node] = binarize!UnionOp(children);
		}
	}
	
	override public void visitPost(InterOp node) {
		scope auto children = children_of(node);
		
		if(children.length == 0) {
			annotations[node] = null;
		}
		else if(children.length == 1) {
			annotations[node] = children[0];
		}
		else {
			annotations[node] = binarize!InterOp(children);
		}
	}
	
	override public void visitPost(SubOp node) {
		scope auto children = children_of(node);
		
		if(children.length == 0) {
			annotations[node] = null;
		}
		else if(children.length == 1) {
			annotations[node] = children[0];
		}
		else if(children.length == 2) {
			annotations[node] = new SubOp([children[0], children[1]]);
		}
		else {
			auto subtrahends = children[1..$];
			annotations[node] = new SubOp([children[0], binarize!UnionOp(subtrahends)]);
		}
	}
	
	override public void visitPostGeo(Geo node) {
		annotations[node] = node.clone();
	}
}


private auto binarize(T)(scope CsgNode[] children) {
	assert(children.length >= 2);
	auto root = new T();
	DList!CsgNode freeEnds = DList!CsgNode([root, root]);
	
	foreach(i; 2..children.length) {
		auto branch = new T(freeEnds.front);
		freeEnds.removeFront();
		freeEnds.insertBack(branch);
		freeEnds.insertBack(branch);
	}
	
	foreach(child, end; lockstep(children, freeEnds[])) {
		child.parent = end;
	}
	
	return root;
}



version(unittest) {
private:
	import fluent.asserts;
	import std.array : array;
	import libcsgfitter.properties;
	
	auto assert_binary(CsgNode root) {
		size_t geoCnt = 0;
		
		scope auto visitor = new class CsgVisitor {
			override void visitPostOp(Op node) {
				node.children.array.length.should.equal(2);
			}
			
			override void visitPostGeo(Geo node) {
				node.hasChildren.should.equal(false).because("primitives don't have children");
				geoCnt++;
			}
		};
		visitor.walk(root);
		
		return geoCnt;
	}
}

@("Ops only")
unittest {
	foreach(root; [
		new UnionOp(),
		new UnionOp([new UnionOp()]),
		new UnionOp([new UnionOp(), new UnionOp()]),
		new UnionOp([new UnionOp(), new UnionOp(), new UnionOp()]),
		new UnionOp([new UnionOp([new UnionOp(), new UnionOp()]), new UnionOp([new UnionOp(), new UnionOp()])]),
		new InterOp(),
		new InterOp([new UnionOp()]),
		new InterOp([new UnionOp(), new UnionOp()]),
		new InterOp([new UnionOp(), new UnionOp(), new UnionOp()]),
		new InterOp([new UnionOp([new UnionOp(), new UnionOp()]), new UnionOp([new UnionOp(), new UnionOp()])]),
		new SubOp(),
		new SubOp([new UnionOp()]),
		new SubOp([new UnionOp(), new UnionOp()]),
		new SubOp([new UnionOp(), new UnionOp(), new UnionOp()]),
		new SubOp([new UnionOp([new UnionOp(), new UnionOp()]), new UnionOp([new UnionOp(), new UnionOp()])]),
	]) {
		root.binarize.should.equal(null);
	}
}

@("Geo only")
unittest {
	auto a = new CubeGeo(Coord(42, -23, 0), Dimension(0.5, 1, 3), RadRotation(-1, 0, 2));
	auto b = a.binarize;
	b.should.not.equal(null);
	b.should.not.equal(a).because("They should be cloned");
	b.hash.should.equal(a.hash).because("They should represet the same geometry, since they are cloned");
}

@("Bin trait")
unittest {
	foreach(root_template; [new UnionOp(), new InterOp(), new SubOp()]) {
		foreach(i; 1..15) {
			CsgNode root = root_template.clone();
			foreach(j; 0..i) {
				new SphereGeo(Coord(), 1, root);
			}
			
			auto bin_root = root.binarize();
			bin_root.assert_binary.should.equal(i);
			
			if(i > 1) {
				bin_root.should.not.equal(root).because("They should be cloned");
				typeid(bin_root).should.equal(typeid(root_template));
			}
		}
	}
}
