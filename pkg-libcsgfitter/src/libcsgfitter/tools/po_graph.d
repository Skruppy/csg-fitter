// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module libcsgfitter.tools.po_graph;

import generic.graph;
import libcsgfitter.csgtree;
import libcsgfitter.properties;
import std.algorithm.iteration : map;
import std.array : array;



public alias PrimitivesGraph = UndirectedGraph!Geo;


struct OverlappingTest {
	public double sample_rate = 10;
	private Coord[][Geo] surface_samples;
	
	public bool overlapping(Geo a, Geo b) {
		return
			fast_test(a, b) ||
			fast_test(b, a) ||
			slow_test(a, b);
	}
	
	// Test whether the nearest point of B to the center of A is within A
	private bool fast_test(Geo a, Geo b) {
		return a.contains_equal(a.transformationToUnit * b.nearestPoint(a.hull_center).position);
	}
	
	// Test whether any surface point of B is within A
	// Fail if B is completely enclosed by A
	private bool slow_test(Geo a, Geo b) {
		Coord[]* coords = b in surface_samples;
		
		if(coords is null) {
			surface_samples[b] = b.surfaceSample(sample_rate).map!"a.position".array;
			coords = &surface_samples[b];
		}
		
		foreach(coord; *coords) {
			if(a.contains_equal(a.transformationToUnit * coord)) {
				return true;
			}
		}
		
		return false;
	}
}


public PrimitivesGraph po_graph(Geo[] primitives, double sample_rate = 10) {
	PrimitivesGraph graph;
	OverlappingTest test;
	test.sample_rate = sample_rate;
	
	foreach(primitive; primitives) {
		graph.add_node(primitive);
	}
	
	// Find candidates (test if they definitely DO NOT overlap)
	foreach(a_idx, a; primitives[0..$-1]) {
		foreach(b_idx, b; primitives[a_idx+1..$]) {
			if(a.hull_center.distance_to(b.hull_center) > a.hull_radius + b.hull_radius) {
				continue;
			}
			
			// Filter candidates (test if they definitely DO overlap)
			if(test.overlapping(a, b)) {
				graph.add_edge(a, b);
			}
		}
	}
	
	graph.rehash();
	
	return graph;
}



version(unittest) {
private:
	import fluent.asserts;
	import std.range : iota;
	import std.algorithm.searching : all;
	import std.algorithm.searching : any;
}

@("OverlappingTest - Asymetric")
unittest {
	auto a = new CubeGeo(Coord(0.0, 10.0, 20.0), Dimension(2, 2, 2), RadRotation());
	auto b = new CubeGeo(Coord(2.5, 11.0, 20.0), Dimension(8, 2, 2), DegRotation(0, 0, 45).asRad);
	
	auto tester = OverlappingTest();
	
	tester.fast_test(a, b).should.equal(true);
	tester.fast_test(b, a).should.equal(false);
	
	iota(100).map!(x => tester.slow_test(a, b)).any.should.equal(true);
	iota(100).map!(x => tester.slow_test(b, a)).any.should.equal(true);
	
	tester.overlapping(a, b).should.equal(true);
	tester.overlapping(b, a).should.equal(true);
}

@("OverlappingTest - Symetric")
unittest {
	auto a = new CubeGeo(Coord(-1.1, 10.0, 20.0), Dimension(2, 20, 2), DegRotation(0, 0, -10).asRad);
	auto b = new CubeGeo(Coord( 1.1, 10.0, 20.0), Dimension(2, 20, 2), DegRotation(0, 0,  10).asRad);
	
	auto tester = OverlappingTest();
	
	tester.fast_test(a, b).should.equal(false);
	tester.fast_test(b, a).should.equal(false);
	
	iota(100).map!(x => tester.slow_test(a, b)).any.should.equal(true);
	iota(100).map!(x => tester.slow_test(b, a)).any.should.equal(true);
	
	iota(100).map!(x => tester.overlapping(a, b)).any.should.equal(true);
	iota(100).map!(x => tester.overlapping(b, a)).any.should.equal(true);
}

@("OverlappingTest - No overlap")
unittest {
	auto a = new SphereGeo(Coord(-10, 10.0, 20.0), 1);
	auto b = new SphereGeo(Coord( 10, 10.0, 20.0), 1);
	
	auto tester = OverlappingTest();
	
	tester.fast_test(a, b).should.equal(false);
	tester.fast_test(b, a).should.equal(false);
	
	iota(100).map!(x => tester.slow_test(a, b)).any.should.equal(false);
	iota(100).map!(x => tester.slow_test(b, a)).any.should.equal(false);
	
	iota(100).map!(x => !tester.overlapping(a, b)).all.should.equal(true);
	iota(100).map!(x => !tester.overlapping(b, a)).all.should.equal(true);
}

@("OverlappingTest - Enclosed")
unittest {
	auto a = new SphereGeo(Coord(), 4);
	auto b = new SphereGeo(Coord(2, 0, 0), 1);
	
	auto tester = OverlappingTest();
	
	tester.fast_test(a, b).should.equal(true);
	tester.fast_test(b, a).should.equal(false);
	
	tester.slow_test(a, b).should.equal(true);
	tester.slow_test(b, a).should.equal(false);
	
	iota(100).map!(x => tester.overlapping(a, b)).all.should.equal(true);
	iota(100).map!(x => tester.overlapping(b, a)).all.should.equal(true);
}

@("OverlappingTest - Equal")
unittest {
	auto a = new SphereGeo(Coord(), 1);
	auto b = new SphereGeo(Coord(), 1);
	
	auto tester = OverlappingTest();
	
	tester.fast_test(a, b).should.equal(true);
	tester.fast_test(b, a).should.equal(true);
	
	tester.slow_test(a, b).should.equal(true);
	tester.slow_test(b, a).should.equal(true);
	
	iota(100).map!(x => tester.overlapping(a, b)).all.should.equal(true);
	iota(100).map!(x => tester.overlapping(b, a)).all.should.equal(true);
}

@("po_graph")
unittest {
	//     C
	//    / \
	//   B---A---E   F
	//    \ /
	//     D
	
	Geo A = new SphereGeo(Coord( 1, 0, 0), 1.5);
	Geo B = new SphereGeo(Coord(-1, 0, 0), 1.5);
	
	Geo C = new SphereGeo(Coord( 0,  2, 0), 1.5);
	Geo D = new SphereGeo(Coord( 0, -2, 0), 1.5);
	
	Geo E = new SphereGeo(Coord( 3, 0, 0), 1.5);
	Geo F = new SphereGeo(Coord( 7, 0, 0), 1.5);
	
	Geo[] geos = [A, B, C, D, E, F];
	
	auto graph = po_graph(geos);
	graph.nodes.should.containOnly(geos);
	graph.neighbours(A).should.containOnly([B, C, D, E]);
	graph.neighbours(B).should.containOnly([A, C, D]);
	graph.neighbours(C).should.containOnly([A, B]);
	graph.neighbours(D).should.containOnly([A, B]);
	graph.neighbours(E).should.containOnly([A]);
	graph.neighbours(F).should.containOnly([]);
}
