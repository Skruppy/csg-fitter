// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module libcsgfitter.tools.convert;

import libcsgfitter.csgtree : CsgNode;
import libcsgfitter.tools.bincsg : binarize;
import libcsgfitter.reader.hcsg;
import libcsgfitter.reader.json;
import libcsgfitter.writer.gv;
import libcsgfitter.writer.hcsg;
import libcsgfitter.writer.json;
import libcsgfitter.writer.scad;
import std.algorithm.searching : endsWith;
import std.format : format;
import std.string : toLower;



public enum FromFormat {
	auto_detect,
	json,
	hcsg,
}

public enum ToFormat {
	auto_detect,
	json,
	hcsg,
	scad,
	
	// see libcsgfitter.writer.gv.Format
	gv,
	png,
	pdf,
	svg,
	jpeg,
}

public struct ConvertProperties {
	public string from;
	public FromFormat from_format;
	public string to;
	public ToFormat to_format;
}

public void convert_csg(scope ConvertProperties properties) {
	scope CsgNode csg;
	bool is_bin;
	
	if(properties.from_format == FromFormat.auto_detect) {
		if(properties.from.toLower.endsWith(".json"))      { properties.from_format = FromFormat.json; }
		else if(properties.from.toLower.endsWith(".hcsg")) { properties.from_format = FromFormat.hcsg; }
		else {
			throw new Exception("connvert: Can't detect file type of source \"%s\"".format(properties.from));
		}
	}
	
	if(properties.to_format == ToFormat.auto_detect) {
		if(properties.to.toLower.endsWith(".json"))      { properties.to_format = ToFormat.json; }
		else if(properties.to.toLower.endsWith(".hcsg")) { properties.to_format = ToFormat.hcsg; }
		else if(properties.to.toLower.endsWith(".scad")) { properties.to_format = ToFormat.scad; }
		else if(properties.to.toLower.endsWith(".gv"))   { properties.to_format = ToFormat.gv;   }
		else if(properties.to.toLower.endsWith(".png"))  { properties.to_format = ToFormat.png;  }
		else if(properties.to.toLower.endsWith(".pdf"))  { properties.to_format = ToFormat.pdf;  }
		else if(properties.to.toLower.endsWith(".svg"))  { properties.to_format = ToFormat.svg;  }
		else if(properties.to.toLower.endsWith(".jpeg")) { properties.to_format = ToFormat.jpeg; }
		else {
			throw new Exception("connvert: Can't detect file type of destination \"%s\"".format(properties.to));
		}
	}
	
	final switch(properties.from_format) {
		case FromFormat.json: csg = read_json_file(properties.from); break;
		case FromFormat.hcsg:
			csg = read_hcsg_file(properties.from);
			is_bin = true;
			break;
		case FromFormat.auto_detect: assert(false);
	}
	
	final switch(properties.to_format) {
		case ToFormat.json: csg.write_json(JsonProperties(properties.to)); break;
		case ToFormat.hcsg:
			if( ! is_bin) { csg = csg.binarize; }
			csg.write_hcsg(HcsgProperties(properties.to));
			break;
		case ToFormat.scad: csg.write_scad(ScadProperties(properties.to)); break;
		case ToFormat.gv: csg.write_gv(GvProperties(properties.to, Format.gv)); break;
		case ToFormat.png: csg.write_gv(GvProperties(properties.to, Format.png)); break;
		case ToFormat.pdf: csg.write_gv(GvProperties(properties.to, Format.pdf)); break;
		case ToFormat.svg: csg.write_gv(GvProperties(properties.to, Format.svg)); break;
		case ToFormat.jpeg: csg.write_gv(GvProperties(properties.to, Format.jpeg)); break;
		case ToFormat.auto_detect: assert(false);
	}
}



version(unittest) {
private:
	import fluent.asserts;
	import generic.testfiles;
	import std.file : read;
	import std.file : write;
}

@("Unknown format")
unittest {
	auto null_file = test_path("libcsgfitter-convert_unknown-format.json");
	
	convert_csg(ConvertProperties("foo.bar", FromFormat.auto_detect, null_file, ToFormat.auto_detect))
	.should.throwException!Exception.withMessage.equal("connvert: Can't detect file type of source \"foo.bar\"");
	
	convert_csg(ConvertProperties(null_file, FromFormat.auto_detect, "foo.bar", ToFormat.auto_detect))
	.should.throwException!Exception.withMessage.equal("connvert: Can't detect file type of destination \"foo.bar\"");
}

@("Forced format")
unittest {
	auto a = test_path("libcsgfitter-convert_forced-format-a.json");
	auto b = test_path("libcsgfitter-convert_forced-format-b.json");
	auto c = test_path("libcsgfitter-convert_forced-format-c.json");
	a.write("sphere(6)\n");
	b.write("\n");
	c.write("\n");
	
	// Force src
	read(b)[0..1].should.equal("\n");
	
	convert_csg(ConvertProperties(a, FromFormat.hcsg, b, ToFormat.auto_detect))
	.should.not.throwAnyException;
	
	read(b)[0..1].should.equal("{");
	
	// Force dst
	c.read().should.equal("\n");
	
	convert_csg(ConvertProperties(b, FromFormat.auto_detect, c, ToFormat.hcsg))
	.should.not.throwAnyException;
	
	c.read().should.equal("sphere(6)\n");
}

@("From .. To")
unittest {
	auto from_hcsg = test_path("libcsgfitter-convert_from.hcsg");
	auto from_json = test_path("libcsgfitter-convert_from.json");
	from_hcsg.write("(sphere(3) ∪ sphere(6))\n");
	from_json.write(`
		{ "op": "union",
		  "childs": [
		    {"geo":"sphere","params":{"center":[0,0,0],"radius":1}},
		    {"geo":"sphere","params":{"center":[0,-2,0],"radius":2}},
		    {"geo":"sphere","params":{"center":[0,0,5],"radius":3}}
		  ]
		}
	`);
	
	auto props = ConvertProperties();
	foreach(from_type, from; ["hcsg": from_hcsg, "json": from_json]) {
		props.from = from;
		foreach(to_type, start; [
			"json": "{\n",
			"hcsg": "(\n",
			"scad": "$fa",
			"gv": "digraph",
			"png": "\x89PNG\r\n\x1A\n",
			"pdf": "%PDF",
			"svg": "<?xml",
			"jpeg": "\xFF\xD8\xFF",
		]) {
			props.to = test_path("libcsgfitter-convert_from-%s-to.%s".format(from_type, to_type));
			convert_csg(props);
			read(props.to)[0..start.length].should.equal(start);
		}
	}
}
