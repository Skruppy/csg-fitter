// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module libcsgfitter.tools.reduce;

import generic.util;
import libcsgfitter.csgtree;
import libcsgfitter.evaluator.base;
import libcsgfitter.point;
import std.experimental.logger : infof;
import std.experimental.logger : warningf;
import std.random : randomCover;
import std.range : take;
import std.array : array;
import std.json : JSONValue;
import std.json : toJSON;
import std.stdio : File;
import xxhash : XXHash;
import std.algorithm : sort;



public struct ReduceStats {
	uint geo;
	uint bucket_key;
	size_t requested_points;
	size_t available_points;
}

public auto reduce(NormalPoint[] points, Geo[] geos, out ReduceStats[] stats) {
	HashInfo hash_cash;
	NormalPoint[][uint] buckets;
	
	// Make keys deterministic, by sorting their elements.
	geos = geos.dup();
	geos.sort!((a, b) => a.hash(hash_cash) < b.hash(hash_cash));
	
	foreach(ref point; points) {
		XXHash xkey;
		xkey.start();
		
		// Put the geo the point was sampled from first in the key. This may
		// create multiple keys for the same region.
		xkey.put(point.origin.as_bytes);
		
		// Add all geos that may have an influce on the sampled point.
		foreach(ref geo; geos) {
			auto hash = geo.hash(hash_cash);
			if(
				hash != point.origin &&
				geo.contains_equal(geo.transformationToUnit * point.position)
			) {
				xkey.put(hash.as_bytes);
			}
		}
		
		auto key = xkey.finish();
		
		// Add the point to the bucket
		buckets[key] ~= point;
	}
	
	NormalPoint[] remaining;
	size_t bucket_i = 1;
	stats.reserve(buckets.length);
	foreach(key, ref points; buckets) {
		size_t n = 3;
		
		stats ~= ReduceStats(points[0].origin, key, n, points.length);
		
		// Only keep the requested amount of random points from each bucket
		if(n <= points.length) {
			infof(
				"Bucket %s/%s (%x): Taking %s/%s points",
				bucket_i, buckets.length, key, n, points.length
			);
			remaining ~= points.randomCover.take(n).array;
		}
		// In case of an underflow (less points than requested), add all points.
		else {
			warningf(
				"Bucket %s/%s (%x): Underflow! %s points requested, %s available",
				bucket_i, buckets.length, key, n, points.length
			);
			remaining ~= points;
		}
		
		bucket_i++;
	}
	
	return remaining;
}
