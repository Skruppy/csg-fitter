// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module libcsgfitter.writer.gv;

import generic.tree;
import libcsgfitter.csgtree;
import libcsgfitter.properties;
import std.conv : to;
import std.format : format;
import std.math : PI;
import std.process : Pipe;
import std.process : pipe;
import std.process : spawnProcess;
import std.process : wait;
import std.stdio : File;



enum Format {
	gv,
	png,
	pdf,
	svg,
	jpeg,
}


struct GvProperties {
	string path;
	Format format = Format.gv;
	bool ltr = false;
	bool geo_details = true;
}


void write_gv(scope CsgNode root, in GvProperties properties) {
	File file;
	Pipe pipeIn;
	immutable bool render = properties.format != Format.gv;
	
	if(render) {
		pipeIn = pipe();
		file = pipeIn.writeEnd;
	}
	else {
		file = File(properties.path, "w");
	}
	
	root.write_gv(file, properties.ltr, properties.geo_details);
	
	if(render) {
		auto fileOut = File(properties.path, "w");
		immutable string[2] args = ["dot", "-T"~properties.format.to!string];
		auto proc = spawnProcess(args, pipeIn.readEnd, fileOut);
		file.close();
		proc.wait();
	}
}


void write_gv(scope CsgNode root, scope File file, in bool ltr=false, in bool geo_details=true) {
	file.writeln("digraph G {");
	if(ltr) { file.writeln("\trankdir=LR;"); }
	file.writeln("\tnode [fontname=\"sans\", fontsize=\"14\"]");
	
	scope visitor = new Visitor(file);
	visitor.renderer.geo_details = geo_details;
	visitor.walk(root);
	
	file.writeln("}");
	file.flush();
}


private class Visitor : CsgVisitor {
	GvRenderer renderer;
	File file;
	
	this(scope File file) {
		this.file = file;
	}
	
	override public void visitPre(UnionOp node)     { file.writef(renderer.render(node)); }
	override public void visitPre(InterOp node)     { file.writef(renderer.render(node)); }
	override public void visitPre(SubOp node)       { file.writef(renderer.render(node)); }
	override public void visitPre(CubeGeo node)     { file.writef(renderer.render(node)); }
	override public void visitPre(CylinderGeo node) { file.writef(renderer.render(node)); }
	override public void visitPre(SphereGeo node)   { file.writef(renderer.render(node)); }
}


private struct Props {
	string key;
	string value;
}


public struct GvRenderer {
	bool geo_details = true;
	string style_extras = "";
	
	
	// ## Ops
	public string render(UnionOp node) {
		return renderOp(node, "∪");
	}
	
	public string render(InterOp node) {
		return renderOp(node, "∩");
	}
	
	public string render(SubOp node) {
		return renderOp(node, "∖");
	}
	
	private string renderOp(scope CsgNode node, in string type) {
		string ret;
		
		ret = "\top_%x [label = \"%s\"];\n".format(cast(void *)node, type);
		
		if(node.parent !is null) {
			ret ~= "\top_%x -> op_%x;\n".format(cast(void *)node.parent, cast(void *)node);
		}
		
		return ret;
	}
	
	
	// ## Geos
	public string render(CubeGeo node) {
		return renderGeo(node, "cube", [
			Props("position",  fmt_triple(node.center.triple)),
			Props("rotation",  fmt_rotation(node.rotation)),
			Props("dimension", fmt_triple(node.dimension.triple)),
		]);
	}
	
	public string render(CylinderGeo node) {
		return renderGeo(node, "cylinder", [
			Props("position", fmt_triple(node.center.triple)),
			Props("rotation", fmt_rotation(node.rotation)),
			Props("height",   fmt_scalar(node.height)),
			Props("radius",   fmt_scalar(node.radius)),
		]);
	}
	
	public string render(SphereGeo node) {
		return renderGeo(node, "sphere", [
			Props("center", fmt_triple(node.center.triple)),
			Props("radius", fmt_scalar(node.radius)),
		]);
	}
	
	private string renderGeo(scope CsgNode node, in string name, in Props[] attributes) {
		// ## graph node
		string ret = "\top_%x [label = <<TABLE BORDER=\"0\" CELLSPACING=\"1\" CELLPADDING=\"0\">\n".format(cast(void *)node);
		
		// name
		ret ~= "\t\t<TR><TD COLSPAN=\"2\"><FONT POINT-SIZE=\"14\">%s</FONT></TD></TR>\n".format(name);
		
		// properties
		if(geo_details) {
			foreach(ref p; attributes) {
				ret ~= "\t\t<TR><TD ALIGN=\"RIGHT\">%s: </TD><TD ALIGN=\"LEFT\">%s</TD></TR>\n".format(p.key, p.value);
			}
		}
		
		ret ~= "\t</TABLE>>, shape=box, fontsize=10%s];\n".format(style_extras);
		
		// ## graph link (to parent)
		if(node.parent !is null) {
			ret ~= "\top_%x -> op_%x;\n".format(cast(void *)node.parent, cast(void *)node);
		}
		
		return ret;
	}
	
	
	private string fmt_triple(ref in double[3] triple) {
		return "%g, %g, %g".format(triple[0], triple[1], triple[2]);
	}
	
	private string fmt_rotation(ref in RadRotation rot) {
		return "%g°, %g°, %g°".format(rot.x/PI*180, rot.y/PI*180, rot.z/PI*180);
	}
	
	private string fmt_scalar(in double scalar) {
		return "%g".format(scalar);
	}
}



version(unittest) {
private:
	import fluent.asserts;
	import generic.testfiles;
	import generic.util : mls;
	import std.file : exists;
	import std.file : getSize;
	import std.file : read;
	import std.file : remove;
	import std.range : chain;
	import std.range : only;
	import std.range : replace;
	import std.traits : EnumMembers;
	
	private void from_csg(string gv, CsgNode root, GvProperties props=GvProperties(), int line=__LINE__) {
		props.path = test_path("libcsgfitter-writer-gv-%s.gv".format(line));
		
		gv = gv.mls;
		int i;
		foreach(node; chain(only(root), root.descendants)) {
			gv = gv.replace("~%s~".format(i++), "%x".format(cast(void*)node));
		}
		
		root.write_gv(props);
		read(props.path).should.equal(gv);
	}
}

@("All Ops/Ops at bottom")
unittest {
	q"[
	digraph G {
		node [fontname="sans", fontsize="14"]
		op_~0~ [label = "∪"];
	}
	]".from_csg(new UnionOp());
	
	q"[
	digraph G {
		node [fontname="sans", fontsize="14"]
		op_~0~ [label = "∩"];
	}
	]".from_csg(new InterOp());
	
	q"[
	digraph G {
		node [fontname="sans", fontsize="14"]
		op_~0~ [label = "∖"];
	}
	]".from_csg(new SubOp());
}

@("All Geos with details/Geos at top")
unittest {
	q"[
	digraph G {
		node [fontname="sans", fontsize="14"]
		op_~0~ [label = <<TABLE BORDER="0" CELLSPACING="1" CELLPADDING="0">
			<TR><TD COLSPAN="2"><FONT POINT-SIZE="14">cube</FONT></TD></TR>
			<TR><TD ALIGN="RIGHT">position: </TD><TD ALIGN="LEFT">42, -23, 0</TD></TR>
			<TR><TD ALIGN="RIGHT">rotation: </TD><TD ALIGN="LEFT">-57.2958°, 0°, 114.592°</TD></TR>
			<TR><TD ALIGN="RIGHT">dimension: </TD><TD ALIGN="LEFT">0.5, 1, 3</TD></TR>
		</TABLE>>, shape=box, fontsize=10];
	}
	]".from_csg(new CubeGeo(Coord(42, -23, 0), Dimension(0.5, 1, 3), RadRotation(-1, 0, 2)));
	
	q"[
	digraph G {
		node [fontname="sans", fontsize="14"]
		op_~0~ [label = <<TABLE BORDER="0" CELLSPACING="1" CELLPADDING="0">
			<TR><TD COLSPAN="2"><FONT POINT-SIZE="14">cylinder</FONT></TD></TR>
			<TR><TD ALIGN="RIGHT">position: </TD><TD ALIGN="LEFT">42, -23, 0</TD></TR>
			<TR><TD ALIGN="RIGHT">rotation: </TD><TD ALIGN="LEFT">-57.2958°, 0°, 114.592°</TD></TR>
			<TR><TD ALIGN="RIGHT">height: </TD><TD ALIGN="LEFT">0.5</TD></TR>
			<TR><TD ALIGN="RIGHT">radius: </TD><TD ALIGN="LEFT">3</TD></TR>
		</TABLE>>, shape=box, fontsize=10];
	}
	]".from_csg(new CylinderGeo(Coord(42, -23, 0), RadRotation(-1, 0, 2), 3, 0.5));
	
	q"[
	digraph G {
		node [fontname="sans", fontsize="14"]
		op_~0~ [label = <<TABLE BORDER="0" CELLSPACING="1" CELLPADDING="0">
			<TR><TD COLSPAN="2"><FONT POINT-SIZE="14">sphere</FONT></TD></TR>
			<TR><TD ALIGN="RIGHT">center: </TD><TD ALIGN="LEFT">42, -23, 0</TD></TR>
			<TR><TD ALIGN="RIGHT">radius: </TD><TD ALIGN="LEFT">3</TD></TR>
		</TABLE>>, shape=box, fontsize=10];
	}
	]".from_csg(new SphereGeo(Coord(42, -23, 0), 3));
}

@("All Geos without details/Geos at top")
unittest {
	GvProperties porps = {geo_details: false};
	
	q"[
	digraph G {
		node [fontname="sans", fontsize="14"]
		op_~0~ [label = <<TABLE BORDER="0" CELLSPACING="1" CELLPADDING="0">
			<TR><TD COLSPAN="2"><FONT POINT-SIZE="14">cube</FONT></TD></TR>
		</TABLE>>, shape=box, fontsize=10];
	}
	]".from_csg(new CubeGeo(Coord(), Dimension(), RadRotation()), porps);
	
	q"[
	digraph G {
		node [fontname="sans", fontsize="14"]
		op_~0~ [label = <<TABLE BORDER="0" CELLSPACING="1" CELLPADDING="0">
			<TR><TD COLSPAN="2"><FONT POINT-SIZE="14">cylinder</FONT></TD></TR>
		</TABLE>>, shape=box, fontsize=10];
	}
	]".from_csg(new CylinderGeo(Coord(), RadRotation(), 1, 1), porps);
	
	q"[
	digraph G {
		node [fontname="sans", fontsize="14"]
		op_~0~ [label = <<TABLE BORDER="0" CELLSPACING="1" CELLPADDING="0">
			<TR><TD COLSPAN="2"><FONT POINT-SIZE="14">sphere</FONT></TD></TR>
		</TABLE>>, shape=box, fontsize=10];
	}
	]".from_csg(new SphereGeo(Coord(), 1), porps);
}

@("Complex")
unittest {
	GvProperties props = {ltr: true};
	
	q"[
	digraph G {
		rankdir=LR;
		node [fontname="sans", fontsize="14"]
		op_~0~ [label = "∪"];
		op_~1~ [label = <<TABLE BORDER="0" CELLSPACING="1" CELLPADDING="0">
			<TR><TD COLSPAN="2"><FONT POINT-SIZE="14">sphere</FONT></TD></TR>
			<TR><TD ALIGN="RIGHT">center: </TD><TD ALIGN="LEFT">42, -23, 0</TD></TR>
			<TR><TD ALIGN="RIGHT">radius: </TD><TD ALIGN="LEFT">3</TD></TR>
		</TABLE>>, shape=box, fontsize=10];
		op_~0~ -> op_~1~;
		op_~2~ [label = "∖"];
		op_~0~ -> op_~2~;
		op_~3~ [label = <<TABLE BORDER="0" CELLSPACING="1" CELLPADDING="0">
			<TR><TD COLSPAN="2"><FONT POINT-SIZE="14">cylinder</FONT></TD></TR>
			<TR><TD ALIGN="RIGHT">position: </TD><TD ALIGN="LEFT">42, -23, 0</TD></TR>
			<TR><TD ALIGN="RIGHT">rotation: </TD><TD ALIGN="LEFT">-57.2958°, 0°, 114.592°</TD></TR>
			<TR><TD ALIGN="RIGHT">height: </TD><TD ALIGN="LEFT">0.5</TD></TR>
			<TR><TD ALIGN="RIGHT">radius: </TD><TD ALIGN="LEFT">3</TD></TR>
		</TABLE>>, shape=box, fontsize=10];
		op_~2~ -> op_~3~;
		op_~4~ [label = "∩"];
		op_~2~ -> op_~4~;
		op_~5~ [label = <<TABLE BORDER="0" CELLSPACING="1" CELLPADDING="0">
			<TR><TD COLSPAN="2"><FONT POINT-SIZE="14">cube</FONT></TD></TR>
			<TR><TD ALIGN="RIGHT">position: </TD><TD ALIGN="LEFT">42, -23, 0</TD></TR>
			<TR><TD ALIGN="RIGHT">rotation: </TD><TD ALIGN="LEFT">-57.2958°, 0°, 114.592°</TD></TR>
			<TR><TD ALIGN="RIGHT">dimension: </TD><TD ALIGN="LEFT">0.5, 1, 3</TD></TR>
		</TABLE>>, shape=box, fontsize=10];
		op_~4~ -> op_~5~;
	}
	]".from_csg(
		new UnionOp([
			new SphereGeo(Coord(42, -23, 0), 3),
			new SubOp([
				new CylinderGeo(Coord(42, -23, 0), RadRotation(-1, 0, 2), 3, 0.5),
				new InterOp([
					new CubeGeo(Coord(42, -23, 0), Dimension(0.5, 1, 3), RadRotation(-1, 0, 2))
				]),
			]),
		]),
		props
	);
}

@("Output formats")
unittest {
	GvProperties props;
	auto root = new UnionOp([
		new SphereGeo(Coord(42, -23, 0), 3),
		new SubOp([
			new CylinderGeo(Coord(42, -23, 0), RadRotation(-1, 0, 2), 3, 0.5),
			new InterOp([
				new CubeGeo(Coord(42, -23, 0), Dimension(0.5, 1, 3), RadRotation(-1, 0, 2)),
			]),
		]),
	]);
	
	foreach(format; EnumMembers!Format) {
		props.format = format;
		props.path = test_path("libcsgfitter-writer-gv-format.%s".format(format));
		
		if(props.path.exists) { props.path.remove(); }
		
		root.write_gv(props);
		
		props.path.getSize.should.be.greaterThan(42);
	}
}
