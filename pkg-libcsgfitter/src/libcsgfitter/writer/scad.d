// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module libcsgfitter.writer.scad;

import generic.tree;
import libcsgfitter.csgtree;
import libcsgfitter.properties;
import std.array : replicate;
import std.format : format;
import std.stdio : File;



struct ScadProperties {
	public string path;
	public int fa = 4;
	public float fs = 0.1;
	public int fn = -1;
}


void write_scad(scope CsgNode root, in ScadProperties properties) {
	scope auto file = File(properties.path, "w");
	if(properties.fa >= 0) file.writefln("$fa = %d;", properties.fa);
	if(properties.fs >= 0) file.writefln("$fs = %g;", properties.fs);
	if(properties.fn >= 0) file.writefln("$fn = %d;", properties.fn);
	
	scope visitor = new Visitor(file);
	visitor.walk(root);
}


private class Visitor : CsgVisitor {
	int indent;
	File file;
	
	public this(scope File file) {
		this.file = file;
	}
	
	// ## Ops
	private void openBlock(in string header) {
		file.writeln("\t".replicate(indent), header, " {");
		indent++;
	}
	
	override public void visitPre(UnionOp node) {openBlock("union()");}
	override public void visitPre(InterOp node) {openBlock("intersection()");}
	override public void visitPre(SubOp node)   {openBlock("difference()");}
	
	override public void visitPostOp(Op node) {
		indent--;
		file.writeln("\t".replicate(indent), "}");
	}
	
	// ## Geos
	private void statement(in string stm) {
		file.writeln("\t".replicate(indent), stm, ";");
	}
	
	override public void visitPre(CubeGeo node) {
		statement("%s %s cube(size=[%f, %f, %f], center=true)".format(
			fmt_translate(node.center),
			fmt_rotation(node.rotation),
			node.dimension.x, node.dimension.y, node.dimension.z,
		));
	}
	
	override public void visitPre(CylinderGeo node) {
		statement("%s %s rotate([-90, 0, 0]) cylinder(h=%f, r=%f, center=true)".format(
			fmt_translate(node.center),
			fmt_rotation(node.rotation),
			node.height, node.radius,
		));
	}
	
	override public void visitPre(SphereGeo node) {
		statement("%s sphere(%f)".format(
			fmt_translate(node.center),
			node.radius,
		));
	}
}


private string fmt_translate(ref in Coord dim) {
	return "translate([%f, %f, %f])".format(dim.x, dim.y, dim.z);
}

private string fmt_rotation(ref in RadRotation rot) {
	auto degRot = rot.asDeg;
	return "rotate([%f, 0, 0]) rotate([0, %f, 0]) rotate([0, 0, %f])".format(degRot.x, degRot.y, degRot.z);
}



version(unittest) {
private:
	import fluent.asserts;
	import generic.testfiles;
	import generic.util : mls;
	import std.file : read;
	
	void from_csg(string scad, CsgNode root, ScadProperties props=ScadProperties(), int line=__LINE__) {
		props.path = test_path("libcsgfitter-writer-scad-%s.scad".format(line));
		root.write_scad(props);
		read(props.path).should.equal(scad.mls);
	}
}

@("All Ops/Ops at bottom")
unittest {
	q"[
		$fa = 4;
		$fs = 0.1;
		union() {
		}
	]".from_csg(new UnionOp());
	
	q"[
		$fa = 4;
		$fs = 0.1;
		intersection() {
		}
	]".from_csg(new InterOp());
	
	q"[
		$fa = 4;
		$fs = 0.1;
		difference() {
		}
	]".from_csg(new SubOp());
}


@("All Geos/Geos at top")
unittest {
	ScadProperties props = {fa: 42, fs: -1, fn: -1};
	q"[
		$fa = 42;
		translate([42.000000, -23.000000, 0.000000]) rotate([-57.295780, 0, 0]) rotate([0, 0.000000, 0]) rotate([0, 0, 114.591559]) cube(size=[0.500000, 1.000000, 3.000000], center=true);
	]".from_csg(new CubeGeo(Coord(42, -23, 0), Dimension(0.5, 1, 3), RadRotation(-1, 0, 2)), props);
	
	props.fa = -1; props.fs = 4.2;
	q"[
		$fs = 4.2;
		translate([42.000000, -23.000000, 0.000000]) rotate([-57.295780, 0, 0]) rotate([0, 0.000000, 0]) rotate([0, 0, 114.591559]) rotate([-90, 0, 0]) cylinder(h=0.500000, r=3.000000, center=true);
	]".from_csg(new CylinderGeo(Coord(42, -23, 0), RadRotation(-1, 0, 2), 3, 0.5), props);
	
	props.fs = -1; props.fn = 23;
	q"[
		$fn = 23;
		translate([42.000000, -23.000000, 0.000000]) sphere(3.000000);
	]".from_csg(new SphereGeo(Coord(42, -23, 0), 3), props);
}


@("Complex")
unittest {
	q"[
		$fa = 4;
		$fs = 0.1;
		union() {
			translate([42.000000, -23.000000, 0.000000]) sphere(3.000000);
			difference() {
				translate([42.000000, -23.000000, 0.000000]) rotate([-57.295780, 0, 0]) rotate([0, 0.000000, 0]) rotate([0, 0, 114.591559]) rotate([-90, 0, 0]) cylinder(h=0.500000, r=3.000000, center=true);
				intersection() {
					translate([42.000000, -23.000000, 0.000000]) rotate([-57.295780, 0, 0]) rotate([0, 0.000000, 0]) rotate([0, 0, 114.591559]) cube(size=[0.500000, 1.000000, 3.000000], center=true);
				}
			}
		}
	]".from_csg(
		new UnionOp([
			new SphereGeo(Coord(42, -23, 0), 3),
			new SubOp([
				new CylinderGeo(Coord(42, -23, 0), RadRotation(-1, 0, 2), 3, 0.5),
				new InterOp([
					new CubeGeo(Coord(42, -23, 0), Dimension(0.5, 1, 3), RadRotation(-1, 0, 2))
				]),
			]),
		])
	);
}
