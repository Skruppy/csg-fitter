// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module libcsgfitter.writer.vtk;

import libcsgfitter.point;
import std.stdio : File;



void write_vtk(T)(in T[] points, in string path) if(isAnyPoint!T) {
	auto file = File(path, "w");
	file.writeln("# vtk DataFile Version 3.0");
	file.writeln("CSG"); // Just a title
	file.writeln("ASCII");
	file.writeln("DATASET POLYDATA");
	file.writefln("POINTS %s float", points.length);
	foreach(point; points) {
		file.writefln("%s %s %s", point.position.x, point.position.y, point.position.z);
	}
	static if(isNormalPoint!T) {
		file.writeln("POINT_DATA ", points.length);
		file.writeln("NORMALS Normals float");
		foreach(point; points) {
			file.writefln("%s %s %s", point.normal.x, point.normal.y, point.normal.z);
		}
	}
}



version(unittest) {
private:
	import fluent.asserts;
	import generic.testfiles;
	import generic.util : mls;
	import std.file : read;
	import libcsgfitter.csgtree;
	import libcsgfitter.properties;
}

@("Simple coords (n=3)")
unittest {
	immutable string path = test_path("libcsgfitter-writer-vtk-3s.vtk");
	auto g = [new SphereGeo(Coord(), 1), new SphereGeo(Coord(), 1)];
	Point[] points = [
		Point(1.0, 2.0, 3.0, g[0].hash),
		Point(1.1, 2.1, 3.1, g[0].hash),
		Point(1.2, 2.2, 3.2, g[1].hash),
	];
	write_vtk(points, path);
	read(path).should.equal(q"[
		# vtk DataFile Version 3.0
		CSG
		ASCII
		DATASET POLYDATA
		POINTS 3 float
		1 2 3
		1.1 2.1 3.1
		1.2 2.2 3.2
	]".mls);
}

@("Simple coords (n=0)")
unittest {
	immutable string path = test_path("libcsgfitter-writer-vtk-0s.vtk");
	Point[] points;
	write_vtk(points, path);
	read(path).should.equal(q"[
		# vtk DataFile Version 3.0
		CSG
		ASCII
		DATASET POLYDATA
		POINTS 0 float
	]".mls);
}

@("Normal coords (n=3)")
unittest {
	immutable string path = test_path("libcsgfitter-writer-vtk-3n.vtk");
	auto g = [new SphereGeo(Coord(), 1), new SphereGeo(Coord(), 1)];
	NormalPoint[] points = [
		NormalPoint(1.0, 2.0, 3.0, 1.0, 0.0, 0.0, g[0].hash),
		NormalPoint(1.1, 2.1, 3.1, 0.5, 0.5, 0.0, g[0].hash),
		NormalPoint(1.2, 2.2, 3.2, 0.0, 0.0, 1.0, g[1].hash),
	];
	write_vtk(points, path);
	read(path).should.equal(q"[
		# vtk DataFile Version 3.0
		CSG
		ASCII
		DATASET POLYDATA
		POINTS 3 float
		1 2 3
		1.1 2.1 3.1
		1.2 2.2 3.2
		POINT_DATA 3
		NORMALS Normals float
		1 0 0
		0.5 0.5 0
		0 0 1
	]".mls);
}

@("Normal coords (n=0)")
unittest {
	immutable string path = test_path("libcsgfitter-writer-vtk-0n.vtk");
	NormalPoint[] points;
	write_vtk(points, path);
	read(path).should.equal(q"[
		# vtk DataFile Version 3.0
		CSG
		ASCII
		DATASET POLYDATA
		POINTS 0 float
		POINT_DATA 0
		NORMALS Normals float
	]".mls);
}
