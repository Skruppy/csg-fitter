// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module libcsgfitter.writer.xyz;

import libcsgfitter.point;
import std.stdio : File;



public struct XYZProperties {
	public string path;
	public bool header = false;
	public string quotes = "";
	public string seperator = " ";
	public string line_end = "\n";
}


void write_xyz(T)(in T[] points, in XYZProperties properties) if(isAnyPoint!T) {
	auto file = File(properties.path, "w");
	immutable auto s = properties.seperator;
	immutable auto q = properties.quotes;
	
	if(properties.header) {
		file.write(q,"x",q , s , q,"y",q , s , q,"z",q);
		static if(isNormalPoint!T) {
			file.write(s , q,"nx",q , s , q,"ny",q , s , q,"nz",q);
		}
		file.write(properties.line_end);
	}
	
	foreach(point; points) {
		file.write(q,point.position.x,q , s , q,point.position.y,q , s , q,point.position.z,q);
		static if(isNormalPoint!T) {
			file.write(s , q,point.normal.x,q , s , q,point.normal.y,q , s , q,point.normal.z,q);
		}
		file.write(properties.line_end);
	}
}



version(unittest) {
private:
	import fluent.asserts;
	import generic.testfiles;
	import generic.util : mls;
	import std.file : read;
	import libcsgfitter.csgtree;
	import libcsgfitter.properties;
}

@("Simple coords (n=3)")
unittest {
	immutable string path = test_path("libcsgfitter-writer-xyz-3s.xyz");
	auto g = [new SphereGeo(Coord(), 1), new SphereGeo(Coord(), 1)];
	Point[] points = [
		Point(1.0, 2.0, 3.0, g[0].hash),
		Point(1.1, 2.1, 3.1, g[0].hash),
		Point(1.2, 2.2, 3.2, g[1].hash),
	];
	write_xyz(points, XYZProperties(path));
	read(path).should.equal(q"[
		1 2 3
		1.1 2.1 3.1
		1.2 2.2 3.2
	]".mls);
}

@("Simple coords (n=0)")
unittest {
	immutable string path = test_path("libcsgfitter-writer-xyz-0s.xyz");
	Point[] points;
	write_xyz(points, XYZProperties(path));
	read(path).should.equal(q"[
	]".mls);
}

@("Normal coords (n=3)")
unittest {
	immutable string path = test_path("libcsgfitter-writer-xyz-3n.xyz");
	auto g = [new SphereGeo(Coord(), 1), new SphereGeo(Coord(), 1)];
	NormalPoint[] points = [
		NormalPoint(1.0, 2.0, 3.0, 1.0, 0.0, 0.0, g[0].hash),
		NormalPoint(1.1, 2.1, 3.1, 0.5, 0.5, 0.0, g[0].hash),
		NormalPoint(1.2, 2.2, 3.2, 0.0, 0.0, 1.0, g[1].hash),
	];
	write_xyz(points, XYZProperties(path));
	read(path).should.equal(q"[
		1 2 3 1 0 0
		1.1 2.1 3.1 0.5 0.5 0
		1.2 2.2 3.2 0 0 1
	]".mls);
}

@("Normal coords (n=0)")
unittest {
	immutable string path = test_path("libcsgfitter-writer-xyz-0n.xyz");
	NormalPoint[] points;
	write_xyz(points, XYZProperties(path));
	read(path).should.equal(q"[
	]".mls);
}

@("CSV features")
unittest {
	immutable string path = test_path("libcsgfitter-writer-xyz-special.xyz");
	auto g = [new SphereGeo(Coord(), 1), new SphereGeo(Coord(), 1)];
	NormalPoint[] points = [
		NormalPoint(1.0, 2.0, 3.0, 1.0, 0.0, 0.0, g[0].hash),
		NormalPoint(1.1, 2.1, 3.1, 0.5, 0.5, 0.0, g[0].hash),
		NormalPoint(1.2, 2.2, 3.2, 0.0, 0.0, 1.0, g[1].hash),
	];
	write_xyz(points, XYZProperties(path, true, "'", " ♥ ", "\r\n"));
	read(path).should.equal(
		"'x' ♥ 'y' ♥ 'z' ♥ 'nx' ♥ 'ny' ♥ 'nz'\r\n"~
		"'1' ♥ '2' ♥ '3' ♥ '1' ♥ '0' ♥ '0'\r\n"~
		"'1.1' ♥ '2.1' ♥ '3.1' ♥ '0.5' ♥ '0.5' ♥ '0'\r\n"~
		"'1.2' ♥ '2.2' ♥ '3.2' ♥ '0' ♥ '0' ♥ '1'\r\n"
	);
}
