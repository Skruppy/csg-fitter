// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module libcsgfitter.writer.json;

import generic.tree;
import generic.util;
import libcsgfitter.csgtree;
import std.algorithm.iteration : map;
import std.array : array;
import std.json : JSONValue;
import std.json : toJSON;
import std.stdio : File;



public struct JsonProperties {
	public string path;
	public bool pretty = true;
}


public void write_json(scope CsgNode root, in JsonProperties properties) {
	scope auto file = File(properties.path, "w");
	file.write(as_json_str(root, properties.pretty));
}


public string as_json_str(scope CsgNode root, in bool pretty=true) {
	scope visitor = new Visitor();
	visitor.walk(root);
	return visitor.annotations[root].toJSON(pretty);
}


private class Visitor : CsgVisitor {
	private JSONValue[CsgNode] annotations;
	
	// ## Ops
	private void addOp(CsgNode node, string op) {
		auto j = JSONValue(null);
		j["op"] = op;
		j["childs"] = node.children.map!(x => annotations[x]).array; // [sic]
		annotations[node] = j;
	}
	
	override public void visitPost(UnionOp node) { addOp(node, "union"); }
	override public void visitPost(InterOp node) { addOp(node, "intersect"); }
	override public void visitPost(SubOp node)   { addOp(node, "subtract"); }
	
	// ## Geos
	override public void visitPost(CubeGeo node) {
		annotations[node] = JSONValue([
			"geo": JSONValue("cube"),
			"params": JSONValue([
				"center": JSONValue(node.center.triple),
				"radius": JSONValue(node.dimension.triple[].map!"a/2.0".array),
				"rotation": JSONValue(node.rotation.triple),
			]),
		]);
	}
	
	override public void visitPost(CylinderGeo node) {
		annotations[node] = JSONValue([
			"geo": JSONValue("cylinder"),
			"params": JSONValue([
				"center": JSONValue(node.center.triple),
				"radius": JSONValue(node.radius),
				"height": JSONValue(node.height),
				"rotation": JSONValue(node.rotation.triple),
			]),
		]);
	}
	
	override public void visitPost(SphereGeo node) {
		annotations[node] = JSONValue([
			"geo": JSONValue("sphere"),
			"params": JSONValue([
				"center": JSONValue(node.center.triple),
				"radius": JSONValue(node.radius),
			]),
		]);
	}
}



version(unittest) {
private:
	import fluent.asserts;
	import libcsgfitter.properties;
	
	private void from_csg(string json, CsgNode root, bool pretty=true) {
		root.as_json_str(pretty).should.equal(json.mls[0..$-1]);
	}
}

@("Ops")
unittest {
	`
	{
	    "childs": [],
	    "op": "union"
	}
	`.from_csg(new UnionOp());
	
	`
	{
	    "childs": [],
	    "op": "intersect"
	}
	`.from_csg(new InterOp());
	
	`
	{
	    "childs": [],
	    "op": "subtract"
	}
	`.from_csg(new SubOp());
}

@("Geos")
unittest {
	`
	{
	    "geo": "cube",
	    "params": {
	        "center": [
	            42,
	            -23,
	            0
	        ],
	        "radius": [
	            0.25,
	            0.5,
	            1.5
	        ],
	        "rotation": [
	            -1,
	            0,
	            2
	        ]
	    }
	}
	`.from_csg(new CubeGeo(Coord(42, -23, 0), Dimension(0.5, 1, 3), RadRotation(-1, 0, 2)));
	
	`
	{
	    "geo": "cylinder",
	    "params": {
	        "center": [
	            42,
	            -23,
	            0
	        ],
	        "height": 0.5,
	        "radius": 3,
	        "rotation": [
	            -1,
	            0,
	            2
	        ]
	    }
	}
	`.from_csg(new CylinderGeo(Coord(42, -23, 0), RadRotation(-1, 0, 2), 3, 0.5));
	
	`
	{
	    "geo": "sphere",
	    "params": {
	        "center": [
	            42,
	            -23,
	            0
	        ],
	        "radius": 3
	    }
	}
	`.from_csg(new SphereGeo(Coord(42, -23, 0), 3));
}

@("Non pretty print")
unittest {
	`
	{"geo":"sphere","params":{"center":[42,-23,0],"radius":3}}
	`.from_csg(new SphereGeo(Coord(42, -23, 0), 3), false);
}

@("Complex")
unittest {
	`
	{
	    "childs": [
	        {
	            "geo": "sphere",
	            "params": {
	                "center": [
	                    42,
	                    -23,
	                    0
	                ],
	                "radius": 3
	            }
	        },
	        {
	            "childs": [
	                {
	                    "childs": [
	                        {
	                            "geo": "cube",
	                            "params": {
	                                "center": [
	                                    0,
	                                    0,
	                                    0
	                                ],
	                                "radius": [
	                                    0.25,
	                                    0.5,
	                                    1.5
	                                ],
	                                "rotation": [
	                                    -1,
	                                    0,
	                                    2
	                                ]
	                            }
	                        }
	                    ],
	                    "op": "intersect"
	                },
	                {
	                    "geo": "cylinder",
	                    "params": {
	                        "center": [
	                            42,
	                            -23,
	                            0
	                        ],
	                        "height": 0.5,
	                        "radius": 1,
	                        "rotation": [
	                            0,
	                            0,
	                            0
	                        ]
	                    }
	                },
	                {
	                    "geo": "cylinder",
	                    "params": {
	                        "center": [
	                            42,
	                            -23,
	                            0
	                        ],
	                        "height": 0.5,
	                        "radius": 2,
	                        "rotation": [
	                            0,
	                            0,
	                            0
	                        ]
	                    }
	                },
	                {
	                    "geo": "cylinder",
	                    "params": {
	                        "center": [
	                            42,
	                            -23,
	                            0
	                        ],
	                        "height": 0.5,
	                        "radius": 3,
	                        "rotation": [
	                            0,
	                            0,
	                            0
	                        ]
	                    }
	                }
	            ],
	            "op": "subtract"
	        }
	    ],
	    "op": "union"
	}
	`.from_csg(
		new UnionOp([
			new SphereGeo(Coord(42, -23, 0), 3),
			new SubOp([
				new InterOp([
					new CubeGeo(Coord(), Dimension(0.5, 1, 3), RadRotation(-1, 0, 2))
				]),
				new CylinderGeo(Coord(42, -23, 0), RadRotation(), 1, 0.5),
				new CylinderGeo(Coord(42, -23, 0), RadRotation(), 2, 0.5),
				new CylinderGeo(Coord(42, -23, 0), RadRotation(), 3, 0.5),
			]),
		])
	);
}

@("Back and forth")
unittest {
	import libcsgfitter.reader.json;
	
	CsgNode csg_a = new UnionOp([
		new SphereGeo(Coord(42, -23, 0), 3),
		new SubOp([
			new InterOp([
				new CubeGeo(Coord(), Dimension(0.5, 1, 3), RadRotation(-1, 0, 2))
			]),
			new CylinderGeo(Coord(42, -23, 0), RadRotation(), 1, 0.5),
			new CylinderGeo(Coord(42, -23, 0), RadRotation(), 2, 0.5),
			new CylinderGeo(Coord(42, -23, 0), RadRotation(), 3, 0.5),
		]),
	]);
	
	auto json_string = csg_a.as_json_str();
	auto csg_b = read_json_str(json_string);
	
	pprinter(csg_a).toString.should.equal(pprinter(csg_b).toString);
}
