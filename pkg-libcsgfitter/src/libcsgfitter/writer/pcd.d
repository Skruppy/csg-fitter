// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module libcsgfitter.writer.pcd;

import libcsgfitter.point;
import std.stdio : File;



void write_pcd(T)(in T[] points, in string path) if(isAnyPoint!T) {
	auto file = File(path, "w");
	
	static if(isSimplePoint!T) {
		header(file, "x y z", "4 4 4", "F F F", "1 1 1", points.length);
		foreach(point; points) {
			file.writefln("%g %g %g", point.position.x, point.position.y, point.position.z);
		}
	}
	else {
		header(file, "x y z normal_x normal_y normal_z", "4 4 4 4 4 4", "F F F F F F", "1 1 1 1 1 1", points.length);
		foreach(point; points) {
			file.writefln("%g %g %g %g %g %g", point.position.x, point.position.y, point.position.z, point.normal.x, point.normal.y, point.normal.z);
		}
	}
}


private void header(scope File file, in string fields, in string size, in string type, in string count, in size_t length) {
	file.writeln("# .PCD v0.7 - Point Cloud Data file format");
	file.writeln("# http://pointclouds.org/documentation/tutorials/pcd_file_format.php");
	file.writeln("VERSION 0.7");
	file.writeln("FIELDS ", fields);
	file.writeln("SIZE ", size);
	file.writeln("TYPE ", type);
	file.writeln("COUNT ", count);
	file.writeln("WIDTH ", length);
	file.writeln("HEIGHT 1");
	file.writeln("VIEWPOINT 0 0 0 1 0 0 0");
	file.writeln("POINTS ", length);
	file.writeln("DATA ascii");
}



version(unittest) {
private:
	import fluent.asserts;
	import generic.testfiles;
	import generic.util : mls;
	import std.file : read;
	import libcsgfitter.csgtree;
	import libcsgfitter.properties;
}

@("Simple coords (n=3)")
unittest {
	immutable string path = test_path("libcsgfitter-writer-pcd-3s.pcd");
	auto g = [new SphereGeo(Coord(), 1), new SphereGeo(Coord(), 1)];
	Point[] points = [
		Point(1.0, 2.0, 3.0, g[0].hash),
		Point(1.1, 2.1, 3.1, g[0].hash),
		Point(1.2, 2.2, 3.2, g[1].hash),
	];
	write_pcd(points, path);
	read(path).should.equal(q"[
		# .PCD v0.7 - Point Cloud Data file format
		# http://pointclouds.org/documentation/tutorials/pcd_file_format.php
		VERSION 0.7
		FIELDS x y z
		SIZE 4 4 4
		TYPE F F F
		COUNT 1 1 1
		WIDTH 3
		HEIGHT 1
		VIEWPOINT 0 0 0 1 0 0 0
		POINTS 3
		DATA ascii
		1 2 3
		1.1 2.1 3.1
		1.2 2.2 3.2
	]".mls);
}

@("Simple coords (n=0)")
unittest {
	immutable string path = test_path("libcsgfitter-writer-pcd-0s.pcd");
	Point[] points;
	write_pcd(points, path);
	read(path).should.equal(q"[
		# .PCD v0.7 - Point Cloud Data file format
		# http://pointclouds.org/documentation/tutorials/pcd_file_format.php
		VERSION 0.7
		FIELDS x y z
		SIZE 4 4 4
		TYPE F F F
		COUNT 1 1 1
		WIDTH 0
		HEIGHT 1
		VIEWPOINT 0 0 0 1 0 0 0
		POINTS 0
		DATA ascii
	]".mls);
}

@("Normal coords (n=3)")
unittest {
	immutable string path = test_path("libcsgfitter-writer-pcd-3n.pcd");
	auto g = [new SphereGeo(Coord(), 1), new SphereGeo(Coord(), 1)];
	NormalPoint[] points = [
		NormalPoint(1.0, 2.0, 3.0, 1.0, 0.0, 0.0, g[0].hash),
		NormalPoint(1.1, 2.1, 3.1, 0.5, 0.5, 0.0, g[0].hash),
		NormalPoint(1.2, 2.2, 3.2, 0.0, 0.0, 1.0, g[1].hash),
	];
	write_pcd(points, path);
	read(path).should.equal(q"[
		# .PCD v0.7 - Point Cloud Data file format
		# http://pointclouds.org/documentation/tutorials/pcd_file_format.php
		VERSION 0.7
		FIELDS x y z normal_x normal_y normal_z
		SIZE 4 4 4 4 4 4
		TYPE F F F F F F
		COUNT 1 1 1 1 1 1
		WIDTH 3
		HEIGHT 1
		VIEWPOINT 0 0 0 1 0 0 0
		POINTS 3
		DATA ascii
		1 2 3 1 0 0
		1.1 2.1 3.1 0.5 0.5 0
		1.2 2.2 3.2 0 0 1
	]".mls);
}

@("Normal coords (n=0)")
unittest {
	immutable string path = test_path("libcsgfitter-writer-pcd-0n.pcd");
	NormalPoint[] points;
	write_pcd(points, path);
	read(path).should.equal(q"[
		# .PCD v0.7 - Point Cloud Data file format
		# http://pointclouds.org/documentation/tutorials/pcd_file_format.php
		VERSION 0.7
		FIELDS x y z normal_x normal_y normal_z
		SIZE 4 4 4 4 4 4
		TYPE F F F F F F
		COUNT 1 1 1 1 1 1
		WIDTH 0
		HEIGHT 1
		VIEWPOINT 0 0 0 1 0 0 0
		POINTS 0
		DATA ascii
	]".mls);
}
