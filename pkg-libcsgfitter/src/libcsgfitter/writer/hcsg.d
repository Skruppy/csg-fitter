// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module libcsgfitter.writer.hcsg;

import generic.tree;
import generic.util;
import libcsgfitter.csgtree;
import libcsgfitter.properties;
import libcsgfitter.reader.hcsg.lexer;
import std.array : replicate;
import std.container.slist : SList;
import std.format : format;
import std.stdio : File;



public struct HcsgProperties {
	public string path;
}


public void write_hcsg(scope CsgNode root, in HcsgProperties properties) {
	scope auto file = File(properties.path, "w");
	scope visitor = new Visitor(file);
	visitor.walk(root);
}


private class Visitor : CsgVisitor {
	private uint indent;
	private File file;
	private SList!string openOps;
	
	public this(scope File file) {
		this.file = file;
	}
	
	private void inter() {
		if(openOps.empty) { return; }
		auto c = openOps.pop();
		if(c == "") { return; }
		file.writeln("\t".replicate(indent), c);
		openOps.push("");
	}
	
	// ## Ops
	private void openBlock(scope Op node, in string op) {
		assert(node.hasChildren && node.first_child.next_sibiling == node.last_child);
		
		file.writeln("\t".replicate(indent), "(");
		indent++;
		openOps.push(op);
	}
	
	override public void visitPre(UnionOp node) {openBlock(node, EBoolOp.UNION);}
	override public void visitPre(InterOp node) {openBlock(node, EBoolOp.INTERSECTION);}
	override public void visitPre(SubOp node)   {openBlock(node, EBoolOp.MINUS);}
	
	override public void visitPostOp(Op node) {
		indent--;
		file.writeln("\t".replicate(indent), ")");
		inter();
	}
	
	// ## Geos
	private void statement(in string stm, in Coord center=Coord(), in RadRotation radRot=RadRotation()) {
		file.write("\t".replicate(indent));
		if(center != Coord()) {
			file.write("move(");
		}
		if(radRot != RadRotation()) {
			file.write("rotate(");
		}
		file.write(stm);
		if(radRot != RadRotation()) {
			auto degRot = radRot.asDeg;
			file.writef(", (%g, %g, %g))", degRot.x, degRot.y, degRot.z);
		}
		if(center != Coord()) {
			file.writef(", (%g, %g, %g))", center.x, center.y, center.z);
		}
		file.write("\n");
	}
	
	override public void visitPre(CubeGeo node) {
		statement("block(%g, %g, %g)".format(
			node.dimension.x, node.dimension.y, node.dimension.z,
		), node.center, node.rotation);
	}
	
	override public void visitPre(CylinderGeo node) {
		statement("cylinder(%g, %g)".format(
			2*node.radius, node.height,
		), node.center, node.rotation);
	}
	
	override public void visitPre(SphereGeo node) {
		statement("sphere(%g)".format(
			2*node.radius,
		), node.center);
	}
	
	override public void visitPostGeo(Geo) {
		inter();
	}
}



version(unittest) {
private:
	import fluent.asserts;
	import generic.testfiles;
	import std.file : read;
	
	private void from_csg(string hcsg, CsgNode root, HcsgProperties props=HcsgProperties(), int line=__LINE__) {
		props.path = test_path("libcsgfitter-writer-hcsg-%s.hcsg".format(line));
		root.write_hcsg(props);
		read(props.path).should.equal(hcsg.mls);
	}
}

@("All Ops")
unittest {
	`
		(
			sphere(6)
			∪
			sphere(6)
		)
	`.from_csg(new UnionOp([new SphereGeo(Coord(), 3), new SphereGeo(Coord(), 3)]));
	
	`
		(
			sphere(6)
			∩
			sphere(6)
		)
	`.from_csg(new InterOp([new SphereGeo(Coord(), 3), new SphereGeo(Coord(), 3)]));
	
	`
		(
			sphere(6)
			∖
			sphere(6)
		)
	`.from_csg(new SubOp([new SphereGeo(Coord(), 3), new SphereGeo(Coord(), 3)]));
}

@("All Geos")
unittest {
	`
		block(0.5, 1, 3)
	`.from_csg(new CubeGeo(Coord(), Dimension(0.5, 1, 3), DegRotation().asRad));
	
	`
		move(block(0.5, 1, 3), (42, -23, 0))
	`.from_csg(new CubeGeo(Coord(42, -23, 0), Dimension(0.5, 1, 3), DegRotation().asRad));
	
	`
		rotate(block(0.5, 1, 3), (-1, 0, 2))
	`.from_csg(new CubeGeo(Coord(), Dimension(0.5, 1, 3), DegRotation(-1, 0, 2).asRad));
	
	`
		move(rotate(block(0.5, 1, 3), (-1, 0, 2)), (42, -23, 0))
	`.from_csg(new CubeGeo(Coord(42, -23, 0), Dimension(0.5, 1, 3), DegRotation(-1, 0, 2).asRad));
	
	`
		cylinder(6, 0.5)
	`.from_csg(new CylinderGeo(Coord(), DegRotation().asRad, 3, 0.5));
	
	`
		move(cylinder(6, 0.5), (42, -23, 0))
	`.from_csg(new CylinderGeo(Coord(42, -23, 0), DegRotation().asRad, 3, 0.5));
	
	`
		rotate(cylinder(6, 0.5), (-1, 0, 2))
	`.from_csg(new CylinderGeo(Coord(), DegRotation(-1, 0, 2).asRad, 3, 0.5));
	
	`
		move(rotate(cylinder(6, 0.5), (-1, 0, 2)), (42, -23, 0))
	`.from_csg(new CylinderGeo(Coord(42, -23, 0), DegRotation(-1, 0, 2).asRad, 3, 0.5));
	
	`
		sphere(6)
	`.from_csg(new SphereGeo(Coord(), 3));
	
	`
		move(sphere(6), (42, -23, 0))
	`.from_csg(new SphereGeo(Coord(42, -23, 0), 3));
}

@("Complex")
unittest {
	`
		(
			move(sphere(6), (42, -23, 0))
			∪
			(
				move(rotate(cylinder(6, 0.5), (-1, 0, 2)), (42, -23, 0))
				∖
				(
					move(rotate(block(0.5, 1, 3), (-1, 0, 2)), (42, -23, 0))
					∩
					rotate(cylinder(6, 0.5), (-1, 0, 2))
				)
			)
		)
	`.from_csg(
		new UnionOp([
			new SphereGeo(Coord(42, -23, 0), 3),
			new SubOp([
				new CylinderGeo(Coord(42, -23, 0), DegRotation(-1, 0, 2).asRad, 3, 0.5),
				new InterOp([
					new CubeGeo(Coord(42, -23, 0), Dimension(0.5, 1, 3), DegRotation(-1, 0, 2).asRad),
					new CylinderGeo(Coord(), DegRotation(-1, 0, 2).asRad, 3, 0.5),
				]),
			]),
		])
	);
}

@("Back and forth")
unittest {
	import libcsgfitter.reader.hcsg;
	
	CsgNode csg_a = new UnionOp([
		new SphereGeo(Coord(42, -23, 0), 3),
		new SubOp([
			new CylinderGeo(Coord(42, -23, 0), DegRotation(-1, 0, 2).asRad, 3, 0.5),
			new InterOp([
				new CubeGeo(Coord(42, -23, 0), Dimension(0.5, 1, 3), DegRotation(-1, 0, 2).asRad),
				new CylinderGeo(Coord(), DegRotation(-1, 0, 2).asRad, 3, 0.5),
			]),
		]),
	]);
	
	auto path = test_path("libcsgfitter-writer-hcsg-%s.hcsg".format(__LINE__));
	auto props = HcsgProperties(path);
	csg_a.write_hcsg(props);
	auto csg_b = read_hcsg_file(path);
	
	pprinter(csg_a).toString.should.equal(pprinter(csg_b).toString);
}

@("Non binary")
unittest {
	import core.exception : AssertError;
	auto props = HcsgProperties();
	auto tests = [
		new UnionOp([]),
		new UnionOp([new SphereGeo(Coord(), 3)]),
		new UnionOp([new SphereGeo(Coord(), 3), new SphereGeo(Coord(), 3), new SphereGeo(Coord(), 3)]),
	];
	
	foreach(i, op; tests) {
		props.path = test_path("libcsgfitter-writer-hcsg-%s-%s.hcsg".format(__LINE__, i));
		({op.write_hcsg(props);}).should.throwException!AssertError;
	}
}
