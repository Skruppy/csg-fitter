// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module libcsgfitter.properties;

import std.format : format;
import std.math : PI;
import std.math : sqrt;



mixin template XYZDefaults() {
	union {
		struct {
			double x = 0.0;
			double y = 0.0;
			double z = 0.0;
		};
		double[3] triple;
	};
	public this(double x, double y, double z) pure { this.x = x; this.y = y; this.z = z; }
	public this(double[] l) pure { assert(l.length == 3); this(l[0], l[1], l[2]); }
}


// #### Coordinates
struct Coord {
	mixin XYZDefaults;
	public double distance_to(Coord c) pure { return sqrt((x-c.x)^^2 + (y-c.y)^^2 + (z-c.z)^^2); }
	public string toString() const { return "Coord( %g | %g | %g )".format(x, y, z); }
}


// #### Rotation
struct RadRotation {
	mixin XYZDefaults;
	public auto asDeg() inout { return DegRotation(x/PI*180, y/PI*180, z/PI*180); }
	public string toString() const { return "RadRot( %g° | %g° | %g° )".format(x/PI*180, y/PI*180, z/PI*180); }
}


struct DegRotation {
	mixin XYZDefaults;
	public auto asRad() inout { return RadRotation(x*PI/180, y*PI/180, z*PI/180); }
	public string toString() const { return "DegRot( %g° | %g° | %g° )".format(x, y, z); }
}


// #### Dimension
struct Dimension {
	mixin XYZDefaults;
	public string toString() const { return "Dimension( %g | %g | %g )".format(x, y, z); }
}



// #### Tests
version(unittest) {
private:
	import fluent.asserts;
}

@("Coord indexed access")
unittest {
	Coord a = Coord(10.0, 20.0, 30.0);
	a.triple[0].should.equal(10.0);
	a.triple[1].should.equal(20.0);
	a.triple[2].should.equal(30.0);
	
	a.triple[1] = 110.0;
	a.triple[0].should.equal(10.0);
	a.triple[1].should.equal(110.0);
	a.triple[2].should.equal(30.0);
}
