// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module libcsgfitter.sampler.volume;

import generic.tree;
import libcsgfitter.csgtree;
import libcsgfitter.point;
import std.algorithm.comparison : max;
import std.algorithm.comparison : min;
import std.algorithm.iteration : fold;
import std.algorithm.iteration : map;
import std.array : array;
import std.bitmanip : BitArray;
import std.functional : binaryFun;
import std.math : isFinite;
import std.math : lround;
import std.random : uniform;



public struct SampleProperties {
	double min_x;
	double min_y;
	double min_z;
	double max_x;
	double max_y;
	double max_z;
	double sample_rate;
}


public Point[] sample_volume(CsgNode root, SampleProperties properties) {
	assert(root !is null);
	assert(properties.sample_rate.isFinite);
	
	// Allocate memory for samples
	auto samples = new Point[lround(
		properties.sample_rate *
		dimension_rate(properties.min_x, properties.max_x) *
		dimension_rate(properties.min_y, properties.max_y) *
		dimension_rate(properties.min_z, properties.max_z)
	).max(0UL)];
	
	// Generate random samples
	foreach(ref sample; samples) {
		sample.position.x = uniform(properties.min_x, properties.max_x);
		sample.position.y = uniform(properties.min_y, properties.max_y);
		sample.position.z = uniform(properties.min_z, properties.max_z);
	}
	
	// Filter out samples not in csg volume
	return
		root.volume_contains(samples)
		.bitsSet
		.map!(x => samples[x])
		.array;
}


double dimension_rate(double min, double max) {
	assert(min.isFinite);
	assert(max.isFinite);
	if(min >= max) {
		return 1; // "skip" "unused" diemensions.
	}
	else {
		return max - min;
	}
}


BitArray volume_contains(alias F = "contains_equal", C)(CsgNode root, C[] samples) if(isAnyPoint!C) {
	BitArray[CsgNode] annotations;
	alias comp = binaryFun!("a."~F~"(b)");
	
	scope auto visitor = new class CsgVisitor {
		override public void visitPost(UnionOp node) {
			if(node.first_child is null) {
				auto empty = BitArray();
				empty.length = samples.length;
				annotations[node] = empty;
			}
			else if(node.first_child == node.last_child) {
				annotations[node] = annotations[node.first_child];
			}
			else {
				annotations[node] = node.children.map!(x => annotations[x]).fold!((a, b) => a | b);
			}
		}
		
		override public void visitPost(InterOp node) {
			if(node.first_child is null) {
				auto empty = BitArray();
				empty.length = samples.length;
				annotations[node] = empty;
			}
			else if(node.first_child == node.last_child) {
				annotations[node] = annotations[node.first_child];
			}
			else {
				annotations[node] = node.children.map!(x => annotations[x]).fold!((a, b) => a & b);
			}
		}
		
		override public void visitPost(SubOp node) {
			if(node.first_child is null) {
				auto empty = BitArray();
				empty.length = samples.length;
				annotations[node] = empty;
			}
			else if(node.first_child == node.last_child) {
				annotations[node] = annotations[node.first_child];
			}
			else {
				annotations[node] = node.children.map!(x => annotations[x]).fold!((a, b) => a - b);
			}
		}
		
		override public void visitPostGeo(Geo node) {
			annotations[node] = BitArray(samples.map!(x => comp(node, (node.transformationToUnit*x).position)).array );
		}
	};
	visitor.walk(root);
	
	return annotations[root];
}
