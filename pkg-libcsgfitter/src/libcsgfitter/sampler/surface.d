// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module libcsgfitter.sampler.surface;

import generic.tree;
import generic.util;
import libcsgfitter.csgtree;
import libcsgfitter.point;
import libcsgfitter.sampler.volume;
import std.algorithm.iteration : filter;
import std.algorithm.iteration : map;
import std.algorithm.iteration : fold;
import std.algorithm.iteration : sum;
import std.array : array;
import std.math : isNaN;
import std.range : lockstep;



struct SurfaceProperties {
	double rate = double.nan;
}


NormalPoint[] surface_sample(CsgNode root, double rate) {
	NormalPoint[][CsgNode] samples;
	
	scope auto visitor = new class CsgVisitor {
		override public void visitPost(UnionOp node) {
			// 0 children: No samples
			if(node.first_child is null) {
				samples[node] = new NormalPoint[0];
			}
			// 1 child: Samples of child
			else if(node.first_child == node.last_child) {
				samples[node] = samples[node.first_child];
				samples.remove(node.first_child);
			}
			// >=2 children: Yeah, well, ...
			else {
				auto bitSets = node.children.map!(me =>
					node.children                   // within all children
					.filter!(child => child != me)  // select all siblings
					.map!(sibling => ~sibling.volume_contains!"contains"(samples[me]))
// 					.array.map!"a" /// @todo Workaround for dmd(?) bug: Segfault when inlining by calling front()
					.safe_fold!"a&b" // and check whether all of them (<-) don't contain my sample (^)
				).array;
				
				auto new_samples = new NormalPoint[bitSets.map!"a.count".sum];
				
				size_t i;
				foreach(ref bitSet, child; lockstep(bitSets, node.children)) {
					// Copy kept samples from each child into a single new sample collection.
					auto my_samples = samples[child];
					foreach(j; bitSet.bitsSet) { // get bitsSet in a bitSet form bitSets o.O
						new_samples[i++] = my_samples[j];
					}
					
					// Free memory of samples not needed anymore.
					samples.remove(child);
				}
				
				samples[node] = new_samples;
			}
		}
		
		
		override public void visitPost(InterOp node) {
			// 0 children: No samples
			if(node.first_child is null) {
				samples[node] = new NormalPoint[0];
			}
			// 1 child: Samples of child
			else if(node.first_child == node.last_child) {
				samples[node] = samples[node.first_child];
				samples.remove(node.first_child);
			}
			// >=2 children: Yeah, well, ...
			else {
				auto bitSets = node.children.map!(me =>
					node.children                   // within all children
					.filter!(child => child != me)  // select all siblings
					.map!(sibling => sibling.volume_contains!"contains_equal"(samples[me]))
// 					.array.map!"a" /// @todo Workaround for dmd(?) bug: Segfault when inlining by calling front()
					.safe_fold!"a&b" // and check whether all of them (<-) contain my sample (^)
				).array;
				
				auto new_samples = new NormalPoint[bitSets.map!"a.count".sum];
				
				size_t i;
				foreach(ref bitSet, child; lockstep(bitSets, node.children)) {
					// Copy kept samples from each child into a single new sample collection.
					auto my_samples = samples[child];
					foreach(j; bitSet.bitsSet) { // get bitsSet in a bitSet form bitSets o.O
						new_samples[i++] = my_samples[j];
					}
					
					// Free memory of samples not needed anymore.
					samples.remove(child);
				}
				
				samples[node] = new_samples;
			}
		}
		
		
		override public void visitPost(SubOp node) {
			// 0 children: No samples
			if(node.first_child is null) {
				samples[node] = new NormalPoint[0];
			}
			// 1 child: Samples of child
			else if(node.first_child == node.last_child) {
				samples[node] = samples[node.first_child];
				samples.remove(node.first_child);
			}
			// >=2 children: Yeah, well, ...
			else {
				auto children = node.children;
				auto minuend = children.pop;
				auto subtrahends = children.array;
				
				// Check minuend samples against all volumes of subtrahends
				auto keep_from_minuend = subtrahends
				.map!(s => ~s.volume_contains!"contains_equal"(samples[minuend]))
				.fold!"a&b"; // and check whether all of them (<-) don't contain my sample (^)
				
				// Check all samples of subtrahands against the _final_ volume
				auto keep_from_subtrahends = subtrahends
				.map!(s => node.volume_contains!"contains"(samples[s]))
				.array;
				
				auto new_samples = new NormalPoint[
					keep_from_minuend.count +
					keep_from_subtrahends.map!"a.count".sum
				];
				
				size_t i;
				auto my_samples = samples[minuend];
				foreach(j; keep_from_minuend.bitsSet) {
					new_samples[i++] = my_samples[j];
				}
				samples.remove(minuend);
				
				foreach(ref bitSet, child; lockstep(keep_from_subtrahends, subtrahends)) {
					// Copy kept samples from each child into a single new sample collection.
					auto child_samples = samples[child];
					foreach(j; bitSet.bitsSet) { // get bitsSet in a bitSet form bitSets o.O
						new_samples[i++] = child_samples[j].inverted;
					}
					
					// Free memory of samples not needed anymore.
					samples.remove(child);
				}
				
				samples[node] = new_samples;
			}
		}
		
		
		override public void visitPostGeo(Geo node) {
			samples[node] = node.surfaceSample(rate);
		}
	};
	visitor.walk(root);
	
	return samples[root];
}
