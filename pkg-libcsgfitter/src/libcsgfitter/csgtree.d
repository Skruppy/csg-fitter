// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module libcsgfitter.csgtree;

import generic.tree;
import generic.util;
import libcsgfitter.properties;
import libcsgfitter.point;
import libcsgfitter.transformation;
import std.algorithm.comparison : clamp;
import std.format : format;
import std.functional : binaryFun;
import std.math : PI;
import std.math : abs;
import std.math : asin;
import std.math : cos;
import std.math : sin;
import std.math : sqrt;
import std.random : uniform;
import xxhash : XXHash;
import xxhash : xxhashOf;
import std.algorithm.iteration : map;
import std.algorithm.iteration : each;
import std.array : array;
import std.algorithm.sorting : sort;



class CsgVisitor {
	public void visitPre(UnionOp node)      { visitPreOp(node);  }
	public void visitPre(InterOp node)      { visitPreOp(node);  }
	public void visitPre(SubOp node)        { visitPreOp(node);  }
	public void visitPre(CubeGeo node)      { visitPreGeo(node); }
	public void visitPre(CylinderGeo node)  { visitPreGeo(node); }
	public void visitPre(SphereGeo node)    { visitPreGeo(node); }
	public void visitPreOp(Op node)         { visitPreAll(node); }
	public void visitPreGeo(Geo node)       { visitPreAll(node); }
	public void visitPreAll(CsgNode node)   { }
	
	public void visitPost(UnionOp node)     { visitPostOp(node);  }
	public void visitPost(InterOp node)     { visitPostOp(node);  }
	public void visitPost(SubOp node)       { visitPostOp(node);  }
	public void visitPost(CubeGeo node)     { visitPostGeo(node); }
	public void visitPost(CylinderGeo node) { visitPostGeo(node); }
	public void visitPost(SphereGeo node)   { visitPostGeo(node); }
	public void visitPostOp(Op node)        { visitPostAll(node); }
	public void visitPostGeo(Geo node)      { visitPostAll(node); }
	public void visitPostAll(CsgNode node)  { }
}


public enum ECsgNodeType {
	UNION_OP,
	INTERSECTION_OP,
	SUBTRACT_OP,
	CUBE_GEO,
	CYLINDER_GEO,
	SPHERE_GEO,
}

public bool is_op(ECsgNodeType type) {
	final switch(type) {
		case ECsgNodeType.UNION_OP:
		case ECsgNodeType.INTERSECTION_OP:
		case ECsgNodeType.SUBTRACT_OP:
			return true;
		
		case ECsgNodeType.CUBE_GEO:
		case ECsgNodeType.CYLINDER_GEO:
		case ECsgNodeType.SPHERE_GEO:
			return false;
	}
}

Geo[] geos(CsgNode root) {
	Geo[] nodes;
	scope auto visitor = new class CsgVisitor {
		override public void visitPreGeo(Geo node) {
			nodes ~= node;
		}
	};
	visitor.walk(root);
	return nodes;
}

Op[] ops(CsgNode root) {
	Op[] nodes;
	scope auto visitor = new class CsgVisitor {
		override public void visitPreOp(Op node) {
			nodes ~= node;
		}
	};
	visitor.walk(root);
	return nodes;
}

public struct HashInfo {
	bool only_structure = true;
	uint[CsgNode] cache;
}

abstract class CsgNode : Node!(CsgNode, CsgVisitor) {
	mixin NodeConstructors!(CsgNode);
	public abstract CsgNode clone();
	
	@property public uint hash(ref HashInfo info) {
		if(auto h2 = (this in info.cache)) {
			return *h2;
		}
		else {
			uint h;
			XXHash xh;
			
			xh.start();
			xh.put(type.as_bytes);
			node_hash(xh, info);
			h = xh.finish();
			
			info.cache[this] = h;
			return h;
		}
	}
	
	@property public uint hash(bool only_structure = true) {
		auto info = HashInfo(only_structure);
		return hash(info);
	}
	
	@property abstract void node_hash(ref XXHash, ref HashInfo);
	@property public abstract ECsgNodeType type() const;
	
	// Number of children, where the node has some effect (range can be smaller
	// than allowed range of children)
	@property abstract size_t effective_max_children() const pure;
	@property abstract size_t effective_min_children() const pure;
}


// #### Operations

abstract class Op : CsgNode {
	mixin NodeConstructors!(CsgNode);
}


class UnionOp : Op {
	mixin NodeConstructors!(CsgNode);
	override mixin TreeVisit!CsgVisitor;
	@property override public string toString() const { return "UNION"; }
	@property override public ECsgNodeType type() const { return ECsgNodeType.UNION_OP; }
	override UnionOp clone() { return new UnionOp(); }
	
	@property override public void node_hash(ref XXHash xh, ref HashInfo info) {
		if(info.only_structure) {
			children.map!(a => a.hash(info).as_bytes).each!(a => xh.put(a));
		}
		else {
			children.map!(a => a.hash(info).as_bytes).array.sort.each!(a => xh.put(a));
		}
	}
	
	@property override size_t effective_max_children() const pure { return 2; }
	@property override size_t effective_min_children() const pure { return typeof(return).max; }
}

class InterOp : Op {
	mixin NodeConstructors!(CsgNode);
	mixin TreeVisit!CsgVisitor;
	@property override public string toString() const { return "INTERSECTION"; }
	@property override public ECsgNodeType type() const { return ECsgNodeType.INTERSECTION_OP; }
	override InterOp clone() { return new InterOp(); }
	
	@property override public void node_hash(ref XXHash xh, ref HashInfo info) {
		if(info.only_structure) {
			children.map!(a => a.hash(info).as_bytes).each!(a => xh.put(a));
		}
		else {
			children.map!(a => a.hash(info).as_bytes).array.sort.each!(a => xh.put(a));
		}
	}
	
	@property override size_t effective_max_children() const pure { return 2; }
	@property override size_t effective_min_children() const pure { return typeof(return).max; }
}

class SubOp : Op {
	mixin NodeConstructors!(CsgNode);
	mixin TreeVisit!CsgVisitor;
	@property override public string toString() const { return "SUBTRACT"; }
	@property override public ECsgNodeType type() const { return ECsgNodeType.SUBTRACT_OP; }
	override SubOp clone() { return new SubOp(); }
	
	@property override public void node_hash(ref XXHash xh, ref HashInfo info) {
		if(info.only_structure) {
			children.map!(a => a.hash(info).as_bytes).each!(a => xh.put(a));
		}
		else {
			scope auto hashes = children.map!(a => a.hash(info).as_bytes).array;
			if(hashes.length > 2) {
				hashes[1..$].sort;
			}
			hashes.each!(a => xh.put(a));
		}
	}
	
	@property override size_t effective_max_children() const pure { return 2; }
	@property override size_t effective_min_children() const pure { return typeof(return).max; }
}


// #### Primitives
mixin template GeoContainsHelper() {
	// `a` has to be a "subset" of `b`.  Do not write "a > b",
	// since  a₁ > b₁ ∧ a₂ > b₂  ≠  !(a₁ ≤ b₁ ∧ a₂ ≤ b₂).
	// Negate instead the whole isPartOfUnity() call.
	override bool contains(inout Coord coord) pure       { return isPartOfUnity!"a <  b"(coord); }
	override bool contains_equal(inout Coord coord) pure { return isPartOfUnity!"a <= b"(coord); }
}


abstract class Geo : CsgNode {
	this(CsgNode parent, Transformation transformationToUnit, Transformation transformationFromUnit) {
		super(parent);
		this.transformationToUnit = transformationToUnit;
		this.transformationFromUnit = transformationFromUnit;
	}
	
	immutable Transformation transformationToUnit;
	immutable Transformation transformationFromUnit;
	
	@property
	abstract double volume() pure;
	@property
	abstract double surface() pure;
	@property
	abstract double hull_radius() pure;
	@property
	abstract Coord hull_center() pure;
	abstract bool contains(inout Coord coord) pure;
	abstract bool contains_equal(inout Coord coord) pure;
	abstract NormalPoint nearestPoint(inout Coord coord);
	abstract NormalPoint[] surfaceSample(double sampleRate);
	
	@property override size_t effective_max_children() const pure { return 0; }
	@property override size_t effective_min_children() const pure { return 0; }
}


// Einheiswürfel:
//  V = 1
//  A = 6
//  Kantenlänge = 1
//  Origin at center of gravity
//  https://de.wikipedia.org/wiki/W%C3%BCrfel_(Geometrie)#Verallgemeinerung
class CubeGeo : Geo {
	mixin TreeVisit!CsgVisitor;
	
	public immutable Coord center;
	public immutable Dimension dimension;
	public immutable RadRotation rotation;
	
	
	public this(Coord center, Dimension dimension, RadRotation rotation, CsgNode parent=null) {
		super(parent,
			scale(Dimension(1/dimension.x, 1/dimension.y, 1/dimension.z)) *
			rotateZ(-rotation.z) *
			rotateY(-rotation.y) *
			rotateX(-rotation.x) *
			translation(Coord(-center.x, -center.y, -center.z))
			,
			translation(center) *
			rotateX(rotation.x) *
			rotateY(rotation.y) *
			rotateZ(rotation.z) *
			scale(dimension)
		);
		this.center = center;
		this.dimension = dimension;
		this.rotation = rotation;
	}
	
	@property override public string toString() const {
		return "Cube[ %s, %s, %s ]".format(center, dimension, rotation);
	}
	
	@property override public ECsgNodeType type() const {
		return ECsgNodeType.CUBE_GEO;
	}
	
	override CubeGeo clone() { return new CubeGeo(center, dimension, rotation); }
	
	override void node_hash(ref XXHash xh, ref HashInfo info) {
		xh.put(center.as_bytes ~ dimension.as_bytes ~ rotation.as_bytes);
	}
	
	@property
	override double volume() pure {
		return dimension.x * dimension.y * dimension.z;
	}
	
	@property
	override double surface() pure {
		return dimension.x*dimension.y + dimension.x*dimension.z + dimension.y*dimension.z;
	}
	
	@property
	override double hull_radius() pure {
		return sqrt((dimension.x/2)^^2.0 + (dimension.y/2)^^2.0 + (dimension.z/2)^^2);
	}
	
	@property
	override Coord hull_center() pure {
		return center;
	}
	
	mixin GeoContainsHelper;
	private bool isPartOfUnity(alias less)(inout Coord coord) pure {
		alias comp = binaryFun!(less);
		return
			comp(-0.5, coord.x) && comp(coord.x, 0.5) &&
			comp(-0.5, coord.y) && comp(coord.y, 0.5) &&
			comp(-0.5, coord.z) && comp(coord.z, 0.5);
	}
	
	
	override NormalPoint nearestPoint(inout Coord coord) {
		auto unitCoord = transformationToUnit*coord;
		auto ax = abs(unitCoord.x);
		auto ay = abs(unitCoord.y);
		auto az = abs(unitCoord.z);
		
		// coord is closest to the x axis
		if(ax >= ay && ax >= az) {
			auto x = unitCoord.x < 0 ? -0.5 : 0.5;
			auto y = unitCoord.y.clamp(-0.5, 0.5);
			auto z = unitCoord.z.clamp(-0.5, 0.5);
			return transformationFromUnit*NormalPoint(
				x,   y, z,
				x*2, 0, 0,
				hash()
			);
		}
		// coord is closest to the y axis
		else if(ay >= ax && ay >= az) {
			auto x = unitCoord.x.clamp(-0.5, 0.5);
			auto y = unitCoord.y < 0 ? -0.5 : 0.5;
			auto z = unitCoord.z.clamp(-0.5, 0.5);
			return transformationFromUnit*NormalPoint(
				x, y,   z,
				0, y*2, 0,
				hash()
			);
		}
		else {
			assert(az >= ax && az >= ay);
			
			auto x = unitCoord.x.clamp(-0.5, 0.5);
			auto y = unitCoord.y.clamp(-0.5, 0.5);
			auto z = unitCoord.z < 0 ? -0.5 : 0.5;
			return transformationFromUnit*NormalPoint(
				x, y, z,
				0, 0, z*2,
				hash()
			);
		}
	}
	
	
	override NormalPoint[] surfaceSample(double sampleRate) {
		immutable int fixedXCnt = cast(int)(dimension.y * dimension.z * sampleRate);
		immutable int fixedYCnt = cast(int)(dimension.x * dimension.z * sampleRate);
		immutable int fixedZCnt = cast(int)(dimension.x * dimension.y * sampleRate);
		
		auto samples = new NormalPoint[(fixedXCnt + fixedYCnt + fixedZCnt) * 2];
		immutable h = hash();
		int i = 0;
		
		foreach(j; 0..fixedXCnt) {
			samples[i++] = transformationFromUnit * NormalPoint(-0.5, uniform(-0.5, 0.5), uniform(-0.5, 0.5), -1, 0, 0, h);
			samples[i++] = transformationFromUnit * NormalPoint( 0.5, uniform(-0.5, 0.5), uniform(-0.5, 0.5),  1, 0, 0, h);
		}
		foreach(j; 0..fixedYCnt) {
			samples[i++] = transformationFromUnit * NormalPoint(uniform(-0.5, 0.5), -0.5, uniform(-0.5, 0.5), 0, -1, 0, h);
			samples[i++] = transformationFromUnit * NormalPoint(uniform(-0.5, 0.5),  0.5, uniform(-0.5, 0.5), 0,  1, 0, h);
		}
		foreach(j; 0..fixedZCnt) {
			samples[i++] = transformationFromUnit * NormalPoint(uniform(-0.5, 0.5), uniform(-0.5, 0.5), -0.5, 0, 0, -1, h);
			samples[i++] = transformationFromUnit * NormalPoint(uniform(-0.5, 0.5), uniform(-0.5, 0.5),  0.5, 0, 0,  1, h);
		}
		
		return samples;
	}
}



class CylinderGeo : Geo {
	mixin TreeVisit!CsgVisitor;
	
	public immutable Coord center;
	public immutable RadRotation rotation;
	public immutable double radius;
	public immutable double height;
	
	
	public this(Coord center, RadRotation rotation, double radius, double height, CsgNode parent=null) {
		super(parent,
			scale(Dimension(1/radius, 1/height, 1/radius)) *
			rotateZ(-rotation.z) *
			rotateY(-rotation.y) *
			rotateX(-rotation.x) *
			translation(Coord(-center.x, -center.y, -center.z))
			,
			translation(center) *
			rotateX(rotation.x) *
			rotateY(rotation.y) *
			rotateZ(rotation.z) *
			scale(Dimension(radius, height, radius))
		);
		this.center = center;
		this.rotation = rotation;
		this.radius = radius;
		this.height = height;
	}
	
	
	@property override public string toString() const {
		return "Cylinder[ %s, %s, %f, %f ]".format(center, rotation, radius, height);
	}
	
	@property override public ECsgNodeType type() const  {
		return ECsgNodeType.CYLINDER_GEO;
	}
	
	override CylinderGeo clone() { return new CylinderGeo(center, rotation, radius, height); }
	
	override void node_hash(ref XXHash xh, ref HashInfo info) {
		xh.put(center.as_bytes ~ rotation.as_bytes ~ radius.as_bytes ~ height.as_bytes);
	}
	
	@property
	override double volume() pure {
		return PI*(radius^^2) * height;
	}
	
	@property
	override double surface() pure {
		return 2*PI*(radius^^2) + 2*PI*radius*height;
	}
	
	@property
	override double hull_radius() pure {
		return sqrt(radius^^2.0 + (height/2)^^2.0);
	}
	
	@property
	override Coord hull_center() pure {
		return center;
	}
	
	mixin GeoContainsHelper;
	private bool isPartOfUnity(alias less)(inout Coord coord) pure {
		alias comp = binaryFun!(less);
		return
			comp(sqrt(coord.x^^2 + coord.z^^2), 1.0) &&
			comp(-0.5, coord.y) && comp(coord.y, 0.5);
	}
	
	
	override NormalPoint nearestPoint(inout Coord coord) {
		auto unitCoord = transformationToUnit*coord;
		auto d = sqrt(unitCoord.x^^2 + unitCoord.z^^2);
		double y;
		
		if(d > abs(2*unitCoord.y)) { // point is on the the rounded side of the cylinder
			y = unitCoord.y.clamp(-0.5, 0.5);
			
			return transformationFromUnit*NormalPoint(
				unitCoord.x/d, y, unitCoord.z/d,
				unitCoord.x/d, y, unitCoord.z/d,
				hash()
			);
		}
		else { // point is on the top/bottom
			if(d <= 1) { // directly above/below the flat surfaces
				return transformationFromUnit*NormalPoint(
					unitCoord.x, unitCoord.y > 0 ? 0.5 : -0.5, unitCoord.z,
					0,           unitCoord.y > 0 ? 1   : -1,   0,
					hash()
				);
			}
			else { // next to flat surface, but not directly above
				return transformationFromUnit*NormalPoint(
					unitCoord.x/d, unitCoord.y > 0 ? 0.5 : -0.5, unitCoord.z/d,
					0,             unitCoord.y > 0 ? 1   : -1,   0,
					hash()
				);
			}
		}
	}
	
	override NormalPoint[] surfaceSample(double sampleRate) {
		int sideCnt = cast(int)(2*PI*radius*height * sampleRate);
		int capCnt = cast(int)(PI*(radius^^2) * sampleRate);
		
		auto samples = new NormalPoint[sideCnt + 2*capCnt];
		immutable h = hash();
		int i = 0;
		
		foreach(j; 0..sideCnt) {
			immutable double deg = uniform(0.0, 2*PI);
			samples[i++] = transformationFromUnit * NormalPoint(sin(deg), uniform(-0.5, 0.5), cos(deg), sin(deg), 0, cos(deg), h);
		}
		foreach(j; 0..capCnt) {
			immutable double deg = uniform(0.0, 2*PI);
			immutable double r = sqrt(uniform(0.0, 1.0));
			samples[i++] = transformationFromUnit * NormalPoint(r*sin(deg), -0.5, r*cos(deg), 0, -1, 0, h);
		}
		foreach(j; 0..capCnt) {
			immutable deg = uniform(0.0, 2*PI);
			immutable r = sqrt(uniform(0.0, 1.0));
			samples[i++] = transformationFromUnit * NormalPoint(r*sin(deg), 0.5, r*cos(deg), 0,  1, 0, h);
		}
		
		return samples;
	}
}



// Einheitskugel
//  A = ~12,57
//  V =  ~4,19
//  r =   1
//  Center of gravity at origin
//  https://de.wikipedia.org/wiki/Einheitskugel
class SphereGeo : Geo {
	mixin TreeVisit!CsgVisitor;
	
	public immutable Coord center;
	public immutable double radius;
	
	
	public this(Coord center, double radius, CsgNode parent=null) {
		super(parent,
			scale(Dimension(1/radius, 1/radius, 1/radius)) *
			translation(Coord(-center.x, -center.y, -center.z))
			,
			translation(center) *
			scale(Dimension(radius, radius, radius))
		);
		this.center = center;
		this.radius = radius;
	}
	
	
	@property override public string toString() const {
		return "Sphere[ %s, %f ]".format(center, radius);
	}
	
	@property override public ECsgNodeType type() const {
		return ECsgNodeType.SPHERE_GEO;
	}
	
	override SphereGeo clone() { return new SphereGeo(center, radius); }
	
	override void node_hash(ref XXHash xh, ref HashInfo info) {
		xh.put(center.as_bytes ~ radius.as_bytes);
	}
	
	@property
	override double volume() pure {
		return 4/3 * PI * (radius^^3);
	}
	
	@property
	override double surface() pure {
		return 4*PI*(radius^^2);
	}
	
	@property
	override double hull_radius() pure {
		return radius;
	}
	
	@property
	override Coord hull_center() pure {
		return center;
	}
	
	mixin GeoContainsHelper;
	private bool isPartOfUnity(alias less)(inout Coord coord) pure {
		alias comp = binaryFun!(less);
		return
			comp(sqrt(coord.x^^2 + coord.y^^2 + coord.z^^2), 1);
	}
	
	override NormalPoint nearestPoint(inout Coord coord) {
		immutable unitCoord = transformationToUnit*coord;
		immutable d = sqrt(unitCoord.x^^2 + unitCoord.y^^2 + unitCoord.z^^2);
		
		if(d > 0) {
			return transformationFromUnit*NormalPoint(
				unitCoord.x / d, unitCoord.y / d, unitCoord.z / d,
				unitCoord.x / d, unitCoord.y / d, unitCoord.z / d,
				hash()
			);
		}
		else {
			return transformationFromUnit*NormalPoint(1, 0, 0, 1, 0, 0, hash());
		}
	}
	
	override NormalPoint[] surfaceSample(double sampleRate) {
		int cnt = cast(int)(surface * sampleRate);
		
		auto samples = new NormalPoint[cnt];
		immutable h = hash();
		
		foreach(i; 0..cnt) {
			immutable double lon = uniform(0.0, 2*PI);
			immutable double lat = asin(uniform(-1.0, 1.0));
			immutable double r = cos(lat);
			samples[i] = transformationFromUnit * NormalPoint(r*sin(lon), sin(lat), r*cos(lon), r*sin(lon), sin(lat), r*cos(lon), h);
		}
		
		return samples;
	}
}

version(unittest) {
private:
	import fluent.asserts;
	import libcsgfitter.sampler.surface : surface_sample;
	import std.math : isFinite;
	import std.range : lockstep;
	
	void test_from_to(Geo g) {
		foreach(i; 0..50) {
			const auto real_base_coord = Coord(uniform(-100, 100), uniform(-100, 100), uniform(-100, 100));
			auto unit_coord = g.transformationToUnit * real_base_coord;
			auto real_rebased_coord = g.transformationFromUnit * unit_coord;
			foreach(j; 0..3) {
				isFinite(unit_coord.triple[j]).should.equal(true).because("%s".format(g));
				isFinite(real_rebased_coord.triple[j]).should.equal(true).because("%s".format(g));
				real_base_coord.triple[j].should.approximately(real_rebased_coord.triple[j], 0.01).because("%s".format(g));
			}
		}
	}
	
	void test_near(Geo g) {
		auto samples = g.surface_sample(200);
		foreach(sample; samples) {
			const auto np = g.nearestPoint(sample.position);
			np.position.x.should.approximately(sample.position.x, 0.01);
			np.position.y.should.approximately(sample.position.y, 0.01);
			np.position.z.should.approximately(sample.position.z, 0.01);
		}
	}
	
	auto test_geos() {
		return [
			new CubeGeo(Coord(), Dimension(1, 1, 1), RadRotation()),
			new CubeGeo(Coord(42, -23, 0), Dimension(1, 1, 1), RadRotation()),
			new CubeGeo(Coord(), Dimension(0.5, 1, 3), RadRotation()),
			new CubeGeo(Coord(), Dimension(1, 1, 1), RadRotation(-1, 0, 2)),
			new CubeGeo(Coord(42, -23, 0), Dimension(0.5, 1, 3), RadRotation(-1, 0, 2)),
			new CylinderGeo(Coord(), RadRotation(), 1, 1),
			new CylinderGeo(Coord(42, -23, 0), RadRotation(), 1, 1),
			new CylinderGeo(Coord(), RadRotation(-1, 0, 2), 1, 1),
			new CylinderGeo(Coord(), RadRotation(), 3, 1),
			new CylinderGeo(Coord(), RadRotation(), 1, 0.5),
			new CylinderGeo(Coord(42, -23, 0), RadRotation(-1, 0, 2), 3, 0.5),
			new SphereGeo(Coord(), 1),
			new SphereGeo(Coord(42, -23, 0), 1),
			new SphereGeo(Coord(), 3),
			new SphereGeo(Coord(42, -23, 0), 3),
		];
	}
}

@("transtion to/from unit")
unittest {
	foreach(geo; test_geos()) {
		test_from_to(geo);
	}
}

@("Cube: nearest point: surfeace")
unittest {
	test_near(new CubeGeo(Coord(42, -23, 0), Dimension(0.5, 1, 3), RadRotation(-1, 0, 2)));
}

@("Cylinder: nearest point: surfeace")
unittest {
	test_near(new CylinderGeo(Coord(42, -23, 0), RadRotation(-1, 0, 2), 3, 0.5));
}

@("Sphere: nearest point: surfeace")
unittest {
	test_near(new SphereGeo(Coord(42, -23, 0), 3));
}

@("Sphere: nearest point: origin")
unittest {
	auto origin = Coord(2, 4, 2);
	auto sphere = new SphereGeo(origin, 1);
	const auto np = sphere.nearestPoint(origin);
	np.position.x.should.equal(3);
	np.position.y.should.equal(4);
	np.position.z.should.equal(2);
	np.normal.x.should.equal(1);
	np.normal.y.should.equal(0);
	np.normal.z.should.equal(0);
}

@("Hashes are stable")
unittest {
	CsgNode[][2] nodesSets;
	foreach(ref nodeSet; nodesSets) {
		nodeSet ~= test_geos();
		nodeSet ~= new UnionOp([
			new CubeGeo(Coord(), Dimension(1, 1, 1), RadRotation()),
			new CylinderGeo(Coord(), RadRotation(), 1, 1)
		]);
		nodeSet ~= new InterOp([
			new CubeGeo(Coord(), Dimension(1, 1, 1), RadRotation()),
			new CylinderGeo(Coord(), RadRotation(), 1, 1)
		]);
		nodeSet ~= new SubOp([
			new CubeGeo(Coord(), Dimension(1, 1, 1), RadRotation()),
			new CylinderGeo(Coord(), RadRotation(), 1, 1)
		]);
	}
	
	foreach(a, b; lockstep(nodesSets[0], nodesSets[1])) {
		a.should.not.equal(b).because("the sample data shold contain multiple instances of objects with the same parameters");
		a.hash.should.equal(b.hash).because("they have the same parameters");
	}
}

@("Hashes are unique")
unittest {
	CsgNode[] nodes;
	nodes ~= test_geos();
	nodes ~= new UnionOp();
	nodes ~= new UnionOp([
		new CubeGeo(Coord(), Dimension(1, 1, 1), RadRotation()),
		new CylinderGeo(Coord(), RadRotation(), 1, 1)
	]);
	nodes ~= new InterOp();
	nodes ~= new InterOp([
		new CubeGeo(Coord(), Dimension(1, 1, 1), RadRotation()),
		new CylinderGeo(Coord(), RadRotation(), 1, 1)
	]);
	nodes ~= new SubOp();
	nodes ~= new SubOp([
		new CubeGeo(Coord(), Dimension(1, 1, 1), RadRotation()),
		new CylinderGeo(Coord(), RadRotation(), 1, 1)
	]);
	
	foreach(a; nodes) {
		foreach(b; nodes) {
			if(a == b) continue;
			a.hash.should.not.equal(b.hash).because("they have different parameters");
		}
	}
}

@("Non structural hashes")
unittest {
	CsgNode[][2] nodesSets;
	foreach(ref nodeSet; nodesSets) {
		// [x][0]
		nodeSet ~= new UnionOp([
			new CubeGeo(Coord(), Dimension(1, 1, 1), RadRotation()),
			new CylinderGeo(Coord(), RadRotation(), 1, 1),
		]);
		
		// [x][1]
		nodeSet ~= new UnionOp([
			new CylinderGeo(Coord(), RadRotation(), 1, 1),
			new CubeGeo(Coord(), Dimension(1, 1, 1), RadRotation()),
		]);
		
		// [x][2]
		nodeSet ~= new SubOp([
		]);
		
		// [x][3]
		nodeSet ~= new SubOp([
			new SphereGeo(Coord(), 1),
		]);
		
		// [x][4]
		nodeSet ~= new SubOp([
			new SphereGeo(Coord(), 1),
			new SphereGeo(Coord(), 2),
		]);
		
		// [x][5]
		nodeSet ~= new SubOp([
			new SphereGeo(Coord(), 2),
			new SphereGeo(Coord(), 1),
		]);
		
		// [x][6]
		nodeSet ~= new SubOp([
			new SphereGeo(Coord(), 1),
			new SphereGeo(Coord(), 2),
			new SphereGeo(Coord(), 3),
		]);
		
		// [x][7]
		nodeSet ~= new SubOp([
			new SphereGeo(Coord(), 1),
			new SphereGeo(Coord(), 3),
			new SphereGeo(Coord(), 2),
		]);
	}
	
	nodesSets[0][0].hash(true) .should    .equal(nodesSets[1][0].hash(true));
	nodesSets[0][0].hash(true) .should.not.equal(nodesSets[1][1].hash(true));
	nodesSets[0][0].hash(false).should    .equal(nodesSets[1][0].hash(false));
	nodesSets[0][0].hash(false).should    .equal(nodesSets[1][1].hash(false));
	
	nodesSets[0][2].hash(true) .should    .equal(nodesSets[1][2].hash(true));
	nodesSets[0][2].hash(false).should    .equal(nodesSets[1][2].hash(false));
	
	nodesSets[0][3].hash(true) .should    .equal(nodesSets[1][3].hash(true));
	nodesSets[0][3].hash(false).should    .equal(nodesSets[1][3].hash(false));
	
	nodesSets[0][4].hash(true) .should    .equal(nodesSets[1][4].hash(true));
	nodesSets[0][4].hash(true) .should.not.equal(nodesSets[1][5].hash(true));
	nodesSets[0][4].hash(false).should    .equal(nodesSets[1][4].hash(false));
	nodesSets[0][4].hash(false).should.not.equal(nodesSets[1][5].hash(false));
	
	nodesSets[0][6].hash(true) .should    .equal(nodesSets[1][6].hash(true));
	nodesSets[0][6].hash(true) .should.not.equal(nodesSets[1][7].hash(true));
	nodesSets[0][6].hash(false).should    .equal(nodesSets[1][6].hash(false));
	nodesSets[0][6].hash(false).should    .equal(nodesSets[1][7].hash(false));
}
