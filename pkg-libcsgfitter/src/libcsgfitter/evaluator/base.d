// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module libcsgfitter.evaluator.base;

import libcsgfitter.point : NormalPoint;
import libcsgfitter.csgtree : CsgNode;



public abstract class Evaluator {
	public abstract void update_reference(NormalPoint[] reference);
	public abstract float evaluate_model(CsgNode model);
}
