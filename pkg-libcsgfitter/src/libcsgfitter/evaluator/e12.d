// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module libcsgfitter.evaluator.e12;

import generic.tree : walk;
import generic.util : pop;
import generic.util : safe_fold;
import libcsgfitter.csgtree;
import libcsgfitter.point;
import libcsgfitter.evaluator.base;
import libcsgfitter.sampler.volume : volume_contains;
import std.algorithm.iteration : filter;
import std.algorithm.iteration : map;
import std.algorithm.iteration : fold;
import std.algorithm.iteration : sum;
import std.container.array : Array;
import std.numeric : dotProduct;
import std.range : array;
import std.range : empty;
import std.range : lockstep;
import std.math : PI;
import std.math : cos;



private struct AnnotatedNormalPoint {
	NormalPoint point;
	size_t source;
	@property AnnotatedNormalPoint inverted() pure { return AnnotatedNormalPoint(point.inverted, source); }
}

struct Evaluator12Parameters {
	double max_dist = 0.001;
	float max_n_div_deg = 1; // = max. 90deg
}

class Evaluator12 : Evaluator {
	private const Evaluator12Parameters* parameters;
	private NormalPoint[] reference;
	
	public this(const Evaluator12Parameters* parameters) {
		this.parameters = parameters;
	}
	
	public override void update_reference(NormalPoint[] reference) {
		if(reference.length == 0) throw new Exception("Evaluator needs at least one refrence point");
		this.reference = reference;
	}
	
	public override float evaluate_model(CsgNode model) {
		return score_12(model, reference, parameters.max_n_div_deg, parameters.max_dist);
	}
}


public float score_12(CsgNode csg, NormalPoint[] r_points, float max_div_deg, double max_dist=double.max) {
	Array!(AnnotatedNormalPoint)[CsgNode] samples;
	
	scope auto visitor = new class CsgVisitor {
		override public void visitPost(UnionOp node) {
			// 0 children: No samples
			if(node.first_child is null) {
				samples[node] = Array!AnnotatedNormalPoint();
			}
			// 1 child: Samples of child
			else if(node.first_child == node.last_child) {
				samples[node] = samples[node.first_child];
				samples.remove(node.first_child);
			}
			// >=2 children: Yeah, well, ...
			else {
				auto bitSets = node.children.map!(me =>
					node.children                   // within all children
					.filter!(child => child != me)  // select all siblings
					.map!(sibling => ~sibling.volume_contains!"contains"(samples[me][].map!"a.point".array))
// 					.array.map!"a" /// @todo Workaround for dmd(?) bug: Segfault when inlining by calling front()
					.safe_fold!"a&b" // and check whether all of them (<-) don't contain my sample (^)
				).array;
				
				Array!AnnotatedNormalPoint new_samples;
				
				foreach(ref bitSet, child; lockstep(bitSets, node.children)) {
					// Copy kept samples from each child into a single new sample collection.
					auto my_samples = samples[child];
					foreach(j; bitSet.bitsSet) { // get bitsSet in a bitSet form bitSets o.O
						new_samples ~= my_samples[j];
					}
					
					// Free memory of samples not needed anymore.
					samples.remove(child);
				}
				
				samples[node] = new_samples;
			}
		}
		
		
		override public void visitPost(InterOp node) {
			// 0 children: No samples
			if(node.first_child is null) {
				samples[node] = Array!AnnotatedNormalPoint();
			}
			// 1 child: Samples of child
			else if(node.first_child == node.last_child) {
				samples[node] = samples[node.first_child];
				samples.remove(node.first_child);
			}
			// >=2 children: Yeah, well, ...
			else {
				auto bitSets = node.children.map!(me =>
					node.children                   // within all children
					.filter!(child => child != me)  // select all siblings
					.map!(sibling => sibling.volume_contains!"contains"(samples[me][].map!"a.point".array))
// 					.array.map!"a" /// @todo Workaround for dmd(?) bug: Segfault when inlining by calling front()
					.safe_fold!"a&b" // and check whether all of them (<-) contain my sample (^)
				).array;
				
				Array!AnnotatedNormalPoint new_samples;
				
				foreach(ref bitSet, child; lockstep(bitSets, node.children)) {
					// Copy kept samples from each child into a single new sample collection.
					auto my_samples = samples[child];
					foreach(j; bitSet.bitsSet) { // get bitsSet in a bitSet form bitSets o.O
						new_samples ~= my_samples[j];
					}
					
					// Free memory of samples not needed anymore.
					samples.remove(child);
				}
				
				samples[node] = new_samples;
			}
		}
		
		
		override public void visitPost(SubOp node) {
			// 0 children: No samples
			if(node.first_child is null) {
				samples[node] = Array!AnnotatedNormalPoint();
			}
			// 1 child: Samples of child
			else if(node.first_child == node.last_child) {
				samples[node] = samples[node.first_child];
				samples.remove(node.first_child);
			}
			// >=2 children: Yeah, well, ...
			else {
				auto children = node.children;
				auto minuend = children.pop;
				auto subtrahends = children.array;
				
				// Check minuend samples against all volumes of subtrahends
				auto keep_from_minuend = subtrahends
				.map!(s => ~s.volume_contains!"contains_equal"(samples[minuend][].map!"a.point".array))
				.fold!"a&b"; // and check whether all of them (<-) don't contain my sample (^)
				
				// Check all samples of subtrahands against volume of minuend
				auto keep_from_subtrahends = subtrahends
				.map!(s => minuend.volume_contains!"contains"(samples[s][].map!"a.point".array))
				.array;
				
				Array!AnnotatedNormalPoint new_samples;
				
				auto my_samples = samples[minuend];
				foreach(j; keep_from_minuend.bitsSet) {
					new_samples ~= my_samples[j];
				}
				samples.remove(minuend);
				
				foreach(ref bitSet, child; lockstep(keep_from_subtrahends, subtrahends)) {
					// Copy kept samples from each child into a single new sample collection.
					auto child_samples = samples[child];
					foreach(j; bitSet.bitsSet) { // get bitsSet in a bitSet form bitSets o.O
						new_samples ~= child_samples[j].inverted;
					}
					
					// Free memory of samples not needed anymore.
					samples.remove(child);
				}
				
				samples[node] = new_samples;
			}
		}
		
		
		override public void visitPostGeo(Geo node) {
			Array!AnnotatedNormalPoint neares_points;
			foreach(j, ref r_point; r_points) {
				auto np = node.nearestPoint(r_point.position);
				if(np.position.distance_to(r_point.position) > max_dist) { continue; }
				neares_points ~= AnnotatedNormalPoint(np, j);
			}
			
			samples[node] = neares_points;
		}
	};
	visitor.walk(csg);
	
	if(r_points.empty) {
		// Return the best score since e12 can never detect extra surfaces in the model.
		return 0;
	}
	
	scope float[] best_scores = new float[r_points.length];
	best_scores[] = 1.0;
	auto dp_min = cos(max_div_deg / 180 * PI);
	foreach(ref f; samples[csg]) {
		auto dp = f.point.normal.triple.dotProduct(r_points[f.source].normal.triple);
		if(dp < dp_min) { continue; }
		
		auto score = 0.0; /* or arccos(dp)? */
		if(best_scores[f.source] > score) {
			best_scores[f.source] = score;
		}
	}
	
	return cast(float)sum(best_scores[]) / r_points.length;
}


// float score_12(CsgNode csg, NormalCoord[] r_points, float max_div_rad, double max_dist=double.max) {
// 	assert(r_points.length > 0);
// 	
// 	// Collect all primitives
// 	Array!Geo primitives;
// 	scope auto visitor = new class CsgVisitor {
// 		override public void visitPostGeo(Geo node) {
// 			primitives ~= node;
// 		}
// 	};
// 	visitor.walk(csg);
// 	
// 	// Find nearest points
// 	Array!AnnotatedNormalPoint neares_points;
// 	foreach(primitive; primitives[]) {
// 		foreach(j, ref r_point; r_points) {
// 			auto np = primitive.nearestPoint(r_point);
// 			if(np.distance_to(r_point) > max_dist) continue;
// 			
// 			auto dp = np.normal.dotProduct(r_point.normal);
// 			if(dp < max_div_rad) continue;
// 			
// 			neares_points ~= AnnotatedNormalPoint(np, 0 /* or arccos(dp)? */, j);
// 		}
// 	}
// 	
// 	auto bar = csg.volume_contains!"is_on"(neares_points[].map!(x => x.coord).array);
// 	
// 	Array!float best_scores;
// 	best_scores.length = r_points.length;
// 	best_scores[] = 1.0;
// 	import std.stdio;
// 	
// 	foreach(ref f; neares_points[]) {
// 		if(bar[f.source] && best_scores[f.source] > f.score) {
// 			best_scores[f.source] = f.score;
// 		}
// 	}
// // 	writefln("%s", neares_points[]);
// 	
// 	return cast(float)sum(best_scores[]) / r_points.length;
// }

// 			annotations[node] = r_points.map!((x) {
// 				auto np = node.nearestPoint(x);
// 				if(np.distance_to(x) > max_dist) return null;
// 				
// 				auto dp = np.normal.dotProduct(x.normal);
// 				if(dp < max_div_rad) return null;
// 				
// 				return new NormalCoord(node.nearestPoint(x));
// 			}).array;
// float score_12(CsgNode csg, NormalCoord[] r_points, float max_div_rad, double max_dist=double.max) {
// 	assert(r_points.length > 0);
// 	NormalCoord*[][CsgNode] annotations;
// 	
// 	max_div_rad = cos(max_div_rad);
// 	
// 	scope auto visitor = new class CsgVisitor {
// 		override public void visitPost(UnionOp node) {
// 			// 0 children: No samples
// 			if(node.first_child is null) {
// 				annotations[node] = new NormalCoord*[r_points.length];
// 			}
// 			// 1 child: Samples of child
// 			else if(node.first_child == node.last_child) {
// 				annotations[node] = annotations[node.first_child];
// 				annotations.remove(node.first_child);
// 			}
// 			// >=2 children: Yeah, well, ...
// 			else {
// // 				auto points_a = annotations[node.first_child]; annotations.remove(node.first_child);
// // 				auto points_b = annotations[node.last_child];  annotations.remove(node.last_child);
// // 				
// 				auto bitSets = node.children.map!(me => 
// 					node.children                   // within all children
// 					.filter!(child => child != me)  // select all siblings
// 					.map!(sibling => sibling.volume_contains!"contains_not"(annotations[me]))
// 					.reduce!"a&b" // and check whether any (<-) of them they contain (^) my samples
// 				).array;
// 				
// // 				foreach(me; node.children) {
// // 					auto my_samples = annotations[me];
// // 					node.children                  // within all children
// // 					.filter!(child => child != me) // select all siblings
// // 					.map!(sibling => sibling.volume_contains!"contains"(my_samples))
// // 					.reduce!"a|b" // and check whether any (<-) of them they contain (^) my samples
// // 					.bitsSet
// // 					.each!(i => my_samples[i] = null);
// // 				}
// 				
// 				
// 				auto cs = node.children.map!(child => annotations[child]).array;
// 				auto child_cnt = cs.length;
// 				
// 				auto ret = new NormalCoord*[r_points.length];
// 				foreach(i, ref r_point; r_points) {
// 					NormalCoord* best;
// 					foreach(j; 0..child_cnt) {
// 						if( ! bitSets[j][i] || cs[j][i] is null) continue;
// 						if(best is null || best.distance_to(r_point) > cs[j][i].distance_to(r_point)) {
// 							best = cs[j][i];
// 						}
// 					}
// 					ret[i] = best;
// 					
// // 					auto x = zip(bitSets, node.children)
// // 					.filter!((ref bitSet, child) => bitSet[i]);
// // 					foreach(ref bitSet, child; lockstep(bitSets, node.children)) {
// // 						
// // 					}
// // 					
// // 					bitSets.filter!(bitSet => 
// // 					auto keep_a = keep_from_a[i] && points_a[i] !is null;
// // 					auto keep_b = keep_from_b[i] && points_b[i] !is null;
// // 					
// // 					if(!keep_a && !keep_b) { ret[i] = null; continue; }
// // 					if(!keep_a) { ret[i] = points_b[i]; continue; }
// // 					if(!keep_b) { ret[i] = points_a[i]; continue; }
// // 					
// // 					assert(keep_a && keep_b);
// // 					
// // 					ret[i] = points_a[i].distance_to(r_point) < points_b[i].distance_to(r_point) ? points_a[i] : points_b[i];
// 				}
// 				annotations[node] = ret;
// 				import std.stdio;
// 				writefln("%s: %s\n%s", node, annotations[node], bitSets);
// 			}
// 		}
// 		
// 		
// 		override public void visitPost(SubOp node) {
// 			// 0 children: No samples
// 			if(node.first_child is null) {
// 				annotations[node] = new NormalCoord*[r_points.length];
// 				import std.stdio;
// 				writefln("%s: %s", node, annotations[node]);
// 			}
// 			// 1 child: Samples of child
// 			else if(node.first_child == node.last_child) {
// 				annotations[node] = annotations[node.first_child];
// 				annotations.remove(node.first_child);
// 			}
// 			// >=2 children: Yeah, well, ...
// 			else {
// 				auto points_a = annotations[node.first_child]; annotations.remove(node.first_child);
// 				auto points_b = annotations[node.last_child];  annotations.remove(node.last_child);
// 				
// 				auto keep_from_a = node.last_child.volume_contains!"contains_not"(points_a);
// 				auto keep_from_b = node.first_child.volume_contains!"contains"(points_b);
// 				
// 				auto ret = new NormalCoord*[r_points.length];
// 				foreach(i, ref r_point; r_points) {
// 					auto keep_a = keep_from_a[i] && points_a[i] !is null;
// 					auto keep_b = keep_from_b[i] && points_b[i] !is null;
// 					
// 					if(!keep_a && !keep_b) { ret[i] = null; continue; }
// 					if(!keep_a) { ret[i] = new NormalCoord(points_b[i].invert); continue; }
// 					if(!keep_b) { ret[i] = points_a[i]; continue; }
// 					
// 					assert(keep_a && keep_b);
// 					
// 					ret[i] = points_a[i].distance_to(r_point) < points_b[i].distance_to(r_point) ? points_a[i] : new NormalCoord(points_b[i].invert);
// 				}
// 				annotations[node] = ret;
// 			}
// 		}
// 		
// 		
// 		override public void visitPostGeo(Geo node) {
// 			annotations[node] = r_points.map!((x) {
// 				auto np = node.nearestPoint(x);
// 				if(np.distance_to(x) > max_dist) return null;
// 				
// 				auto dp = np.normal.dotProduct(x.normal);
// 				if(dp < max_div_rad) return null;
// 				
// 				return new NormalCoord(node.nearestPoint(x));
// 			}).array;
// 			
// 			import std.stdio;
// 			writefln("%s: %s", node, annotations[node]);
// 		}
// 	};
// 	visitor.walk(csg);
// 	
// 	return cast(float)sum(annotations[csg].map!"a is null ? 1 : 0") / r_points.length;
// }



// #### Unittests
version(unittest) {
private:
	import fluent.asserts;
	import libcsgfitter.properties;
	import libcsgfitter.reader.json;
	import libcsgfitter.sampler.surface;
	
	void testFile(string path, double desnsity=20, float threshold=0.01) {
		auto csg = read_json_file(path);
		auto reference = csg.surface_sample(desnsity);
		score_12(csg, reference, 60, 0.01).should.be.below(threshold).because("reference and model are the same");
	}
}

@("high resolution")
unittest {
	auto root = new CubeGeo(Coord(), Dimension(1.0, 1.0, 1.0), RadRotation());
	score_12(root, root.surface_sample(1000), 60, 0.01).should.below(0.01).because("reference and model are the same");
}

@("complex")
unittest {
	testFile("tests/renderings/complex.json", 10, 0.04); /// @todo The threshold should be lower
}

@("sub_0_fail")
unittest {
	score_12(new SubOp(), [NormalPoint(0,0,0,0,0,0, 0)], 60, 0.01).should.equal(1).because("nothing does not match something");
}
@("sub_0")
unittest {
	score_12(new SubOp(), [], 60, 0.01).should.equal(0).because("nothing does match nothing");
}
@("sub_1")
unittest {
	testFile("tests/renderings/op_sub_1.json");
}
@("sub_2")
unittest {
	testFile("tests/renderings/op_sub_2.json");
}
@("sub_3")
unittest {
	testFile("tests/renderings/op_sub_3.json");
}
@("sub_4")
unittest {
	testFile("tests/renderings/op_sub_4.json");
}

@("union_0_fail")
unittest {
	score_12(new UnionOp(), [NormalPoint(0,0,0,0,0,0, 0)], 60, 0.01).should.equal(1).because("nothing does not match something");
}
@("union_0")
unittest {
	score_12(new UnionOp(), [], 60, 0.01).should.equal(0).because("nothing does match nothing");
}
@("union_1")
unittest {
	testFile("tests/renderings/op_union_1.json");
}
@("union_2")
unittest {
	testFile("tests/renderings/op_union_2.json");
}
@("union_3")
unittest {
	testFile("tests/renderings/op_union_3.json");
}
@("union_4")
unittest {
	testFile("tests/renderings/op_union_4.json");
}

@("op_inter_3_sphere")
unittest {
	testFile("tests/renderings/op_inter_3_sphere.json", 100);
}
@("op_inter_3_sphere_partial")
unittest {
	auto csg = read_json_file("tests/renderings/op_inter_3_sphere.json");
	auto reference = new SphereGeo(Coord(), 1).surface_sample(100);
	score_12(csg, reference, 60, 0.01).should.be.approximately(1 - (4.075693/3) / (4*PI), 0.05);
}
