// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module libcsgfitter.evaluator.e11;

import generic.kdtree : Node;
import generic.kdtree : k_d_tree_constructor;
import generic.kdtree : k_d_tree_nn;
import libcsgfitter.csgtree;
import libcsgfitter.evaluator.base;
import libcsgfitter.point;
import libcsgfitter.sampler.surface : surface_sample;
import std.algorithm.comparison : max;
import std.algorithm.iteration : map;
import std.algorithm.iteration : sum;
import std.math : PI;
import std.math : acos;
import std.math : lround;
import std.numeric : dotProduct;
import std.random : randomCover;
import std.range : array;
import std.range : chain;
import std.range : isInputRange;
import std.range : take;
import libcsgfitter.evaluator.base : Evaluator;



public alias Tree = Node!(NormalPoint*, double[3])*;

public struct Evaluator11Parameters {
	public float sample_density = 1.0;
	public float max_dist = 1.0;
	public float N_rel = 0.05;
}


class Evaluator11 : Evaluator {
	private const Evaluator11Parameters* parameters;
	private NormalPoint*[] points;
	private Tree tree;
	
	public this(const Evaluator11Parameters* parameters) {
		this.parameters = parameters;
	}
	
	public override void update_reference(NormalPoint[] reference) {
		if(reference.length == 0) throw new Exception("Evaluator needs at least one refrence point");
		this.points = reference.map!"&a".array;
		this.tree = this.points.k_d_tree_constructor!("a.position.triple");
	}
	
	public override float evaluate_model(CsgNode model) {
		return score_11(model, points, tree, *parameters);
	}
}


public float score_11(CsgNode csg, NormalPoint*[] r_points, Tree r_tree, const Evaluator11Parameters p) {
	auto m_points = csg.surface_sample(p.sample_density).map!"&a".array;
	if(m_points.length == 0 || r_points.length == 0) {
		return m_points.length == 0 && r_points.length == 0 ? 0 : 1;
	}
	auto m_tree = m_points.k_d_tree_constructor!("a.position.triple");
	auto m_samples = m_points.randomCover;
	
	auto r_samples = r_points.randomCover;
	
	size_t m_n = max(1, lround(p.N_rel * m_samples.length));
	size_t r_n = max(1, lround(p.N_rel * r_samples.length));
	auto m_scores = cloudScore(m_samples, r_tree, m_n, p.max_dist);
	auto r_scores = cloudScore(r_samples, m_tree, r_n, p.max_dist);
	
	auto s = chain(m_scores, r_scores).sum / (m_n + r_n);
	return s;
}


private auto cloudScore(R)(R a_samples, Tree b_tree, size_t N, float max_dist) if(isInputRange!R) {
	return
		a_samples
		.take(N)
		.map!((NormalPoint* a_point) {
			if(b_tree is null)  return 1.0;
			auto b_point = b_tree.k_d_tree_nn([a_point.position.x, a_point.position.y, a_point.position.z], max_dist);
			if(b_point is null)  return 1.0;
			
			auto dp = (*b_point).normal.triple.dotProduct(a_point.normal.triple);
			if     (dp >=  1.0) return 0.0;
			else if(dp <= -1.0) return 1.0;
			else                return dp.acos / PI;
		});
}



// #### Unittests
version(unittest) {
private:
	import fluent.asserts;
	import libcsgfitter.properties;
	import libcsgfitter.reader.json;
	import std.algorithm : mean;
	import std.range : iota;
	
	void testFile(string path, double desnsity=100, float threshold=0.1) {
		auto csg = read_json_file(path);
		auto reference = csg.surface_sample(desnsity).map!"&a".array;
		auto tree = reference.k_d_tree_constructor!("a.position.triple");
		
		Evaluator11Parameters params;
		params.sample_density = desnsity;
		params.max_dist = 0.2;
		
		score_11(csg, reference, tree, params).should.be.below(threshold).because("reference and model are the same");
	}
}

@("high resolution")
unittest {
	auto root = new CubeGeo(Coord(), Dimension(1.0, 1.0, 1.0), RadRotation());
	Evaluator11Parameters params;
	params.sample_density = 1000.0;
	params.max_dist = 0.1;
	
	auto points = root.surface_sample(params.sample_density).map!"&a".array;
	auto tree = points.k_d_tree_constructor!("a.position.triple");
	
	score_11(root, points, tree, params).should.be.below(0.03).because("reference and model are the same");
}

@("complex")
unittest {
	testFile("tests/renderings/complex.json", 50);
}

@("sub_0_fail")
unittest {
	Evaluator11Parameters params;
	params.max_dist = 0.2;
	
	auto points = [NormalPoint(0,0,0,0,0,0, 0)].map!"&a".array;
	auto tree = points.k_d_tree_constructor!("a.position.triple");
	
	score_11(new SubOp(), points, tree, params).should.equal(1).because("nothing does not match something");
}
@("sub_0")
unittest {
	Evaluator11Parameters params;
	params.max_dist = 0.2;
	
	NormalPoint*[] points;
	auto tree = points.k_d_tree_constructor!("a.position.triple");
	
	score_11(new SubOp(), points, tree, params).should.equal(0).because("nothing does match nothing");
}
@("sub_1")
unittest {
	testFile("tests/renderings/op_sub_1.json");
}
@("sub_2")
unittest {
	testFile("tests/renderings/op_sub_2.json");
}
@("sub_3")
unittest {
	testFile("tests/renderings/op_sub_3.json");
}
@("sub_4")
unittest {
	testFile("tests/renderings/op_sub_4.json");
}

@("union_0_fail")
unittest {
	Evaluator11Parameters params;
	params.max_dist = 0.2;
	
	auto points = [NormalPoint(0,0,0,0,0,0, 0)].map!"&a".array;
	auto tree = points.k_d_tree_constructor!("a.position.triple");
	
	score_11(new UnionOp(), points, tree, params).should.equal(1).because("nothing does not match something");
}
@("union_0")
unittest {
	Evaluator11Parameters params;
	params.max_dist = 0.2;
	
	NormalPoint*[] points;
	auto tree = points.k_d_tree_constructor!("a.position.triple");
	
	score_11(new UnionOp(), points, tree, params).should.equal(0).because("nothing does match nothing");
}
@("union_1")
unittest {
	testFile("tests/renderings/op_union_1.json");
}
@("union_2")
unittest {
	testFile("tests/renderings/op_union_2.json");
}
@("union_3")
unittest {
	testFile("tests/renderings/op_union_3.json");
}
@("union_4")
unittest {
	testFile("tests/renderings/op_union_4.json");
}

@("op_inter_3_sphere")
unittest {
	testFile("tests/renderings/op_inter_3_sphere.json", 100);
}
@("op_inter_3_sphere_partial")
unittest {
	auto csg = read_json_file("tests/renderings/op_inter_3_sphere.json");
	Evaluator11Parameters params;
	params.sample_density = 100.0;
	params.max_dist = 0.05;
	
	auto reference = new SphereGeo(Coord(), 1).surface_sample(params.sample_density).map!"&a".array;
	auto tree = reference.k_d_tree_constructor!("a.position.triple");
	
	auto sphere_s = 4*PI * 1^^2;
	auto inter_3_s = 4.075693;
	auto common_s = inter_3_s/3;
	iota(100).map!(n => score_11(csg, reference, tree, params)).mean.should.be.approximately(1 - 2*common_s/(sphere_s+inter_3_s), 0.1);
}
