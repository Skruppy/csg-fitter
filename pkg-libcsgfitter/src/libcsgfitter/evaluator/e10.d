// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module libcsgfitter.evaluator.e10;

import generic.kdtree : k_d_tree_constructor;
import generic.kdtree : k_d_tree_knn;
import libcsgfitter.csgtree;
import libcsgfitter.point;
import libcsgfitter.evaluator.base : Evaluator;
import libcsgfitter.sampler.surface : surface_sample;
import std.algorithm.comparison : max;
import std.algorithm.comparison : min;
import std.algorithm.iteration : filter;
import std.algorithm.iteration : fold;
import std.algorithm.iteration : map;
import std.algorithm.iteration : sum;
import std.algorithm.iteration : cache;
import std.math : PI;
import std.math : acos;
import std.math : lround;
import std.numeric : dotProduct;
import std.random : randomCover;
import std.range : array;
import std.range : chain;
import std.range : take;
import std.range : empty;
import libcsgfitter.csgtree : CsgNode;



public struct Evaluator10Parameters {
	float sample_density = 1.0;
	float N_rel = 0.05;
}


class Evaluator10 : Evaluator {
	private const Evaluator10Parameters* parameters;
	private NormalPoint[] reference;
	
	public this(const Evaluator10Parameters* parameters) {
		this.parameters = parameters;
	}
	
	public override void update_reference(NormalPoint[] reference) {
		this.reference = reference;
	}
	
	public override float evaluate_model(CsgNode model) {
		return score_10(model, reference, *parameters);
	}
}


public float score_10(CsgNode csg, NormalPoint[] r_samples, Evaluator10Parameters p) {
	auto m_samples = csg.surface_sample(p.sample_density);
	if(m_samples.empty || r_samples.empty) {
		return m_samples.empty && r_samples.empty ? 0 : 1;
	}
	
	auto points = chain(m_samples, r_samples).map!"&a".array;
	auto root = points.k_d_tree_constructor!("a.position.triple");
	
	auto seperator = max(&m_samples[0], &r_samples[0]);
	auto samples = points.randomCover;
	size_t N = max(1, lround(p.N_rel * points.length));
	
	auto pointScores = samples
	.take(N)
	.map!((NormalPoint* refPoint) {
		auto refType = refPoint >= seperator;
		
		return root.k_d_tree_knn([refPoint.position.x, refPoint.position.y, refPoint.position.z], 17)
		.filter!(x => (x.node.point >= seperator) != refType)
		.map!((x) {
			auto dp = x.node.point.normal.triple.dotProduct(refPoint.normal.triple);
			if     (dp >=  1.0) return 0.0;
			else if(dp <= -1.0) return 1.0;
			else                return dp.acos / PI;
		})
		.cache.fold!min(1.0);
	})
	.array;
	
	auto s = pointScores.sum / N;
	return s;
}



// #### Unittests
version(unittest) {
private:
	import fluent.asserts;
	import libcsgfitter.properties;
	import libcsgfitter.reader.json;
	import std.algorithm : mean;
	import std.range : iota;
	
	void testFile(string path, double desnsity=20, float threshold=0.1) {
		auto csg = read_json_file(path);
		auto reference = csg.surface_sample(desnsity);
		Evaluator10Parameters params;
		params.sample_density = desnsity;
		score_10(csg, reference, params).should.be.below(threshold).because("reference and model are the same");
	}
}

@("high resolution")
unittest {
	auto root = new CubeGeo(Coord(), Dimension(1.0, 1.0, 1.0), RadRotation());
	Evaluator10Parameters params;
	params.sample_density = 1000.0;
	auto s = score_10(root, root.surface_sample(params.sample_density), params).should.be.below(0.01).because("reference and model are the same");
}

@("complex")
unittest {
	testFile("tests/renderings/complex.json", 10, 0.04); /// @todo The threshold should be lower
}

@("sub_0_fail")
unittest {
	Evaluator10Parameters params;
	score_10(new SubOp(), [NormalPoint(0,0,0,0,0,0, 0)], params).should.equal(1).because("nothing does not match something");
}
@("sub_0")
unittest {
	Evaluator10Parameters params;
	score_10(new SubOp(), [], params).should.equal(0).because("nothing does match nothing");
}
@("sub_1")
unittest {
	testFile("tests/renderings/op_sub_1.json");
}
@("sub_2")
unittest {
	testFile("tests/renderings/op_sub_2.json");
}
@("sub_3")
unittest {
	testFile("tests/renderings/op_sub_3.json");
}
@("sub_4")
unittest {
	testFile("tests/renderings/op_sub_4.json");
}

@("union_0_fail")
unittest {
	Evaluator10Parameters params;
	score_10(new UnionOp(), [NormalPoint(0,0,0,0,0,0, 0)], params).should.equal(1).because("nothing does not match something");
}
@("union_0")
unittest {
	Evaluator10Parameters params;
	score_10(new UnionOp(), [], params).should.equal(0).because("nothing does match nothing");
}
@("union_1")
unittest {
	testFile("tests/renderings/op_union_1.json");
}
@("union_2")
unittest {
	testFile("tests/renderings/op_union_2.json");
}
@("union_3")
unittest {
	testFile("tests/renderings/op_union_3.json");
}
@("union_4")
unittest {
	testFile("tests/renderings/op_union_4.json");
}

@("op_inter_3_sphere")
unittest {
	testFile("tests/renderings/op_inter_3_sphere.json", 100);
}
@("op_inter_3_sphere_partial")
unittest {
	auto csg = read_json_file("tests/renderings/op_inter_3_sphere.json");
	Evaluator10Parameters params;
	params.sample_density = 100.0;
	
	auto reference = new SphereGeo(Coord(), 1).surface_sample(params.sample_density);
	
	// The result seems to be very unstable (hence this big range).  Experiments
	// suggest it is around 0.80 with a SR of 1000 and 0.7 with a SR of 100.
	iota(100).map!(n => score_10(csg, reference, params)).mean.should.be.above(0.6);
}
