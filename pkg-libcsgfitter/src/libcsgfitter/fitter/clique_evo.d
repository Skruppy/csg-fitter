// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module libcsgfitter.fitter.clique_evo;

import generic.tree;
import generic.util;
import generic.graph;
import generic.set;
import generic.bron_kerbosch;
import libcsgfitter.fitter.evo;
import libcsgfitter.csgtree;
import libcsgfitter.properties;
import libcsgfitter.point;
import libcsgfitter.evaluator.base;
import libcsgfitter.tools.po_graph;
import std.array : array;
import std.array : empty;
import std.algorithm.searching : any;
import std.algorithm.sorting : sort;
import std.algorithm.iteration : map;
import std.algorithm.iteration : filter;
import std.experimental.logger : infof;
import std.experimental.logger : tracef;
import std.range.primitives : front;
import std.range.primitives : back;
import libcsgfitter.writer.gv;
import libcsgfitter.tools.simplify;
import std.container.dlist : DList;
import std.format : format;
import std.random : randomCover;



public struct CliqueStats {
	string type = "clique_evo";
	
	size_t primiteve_cnt;
	size_t point_cnt;
	
	long detect_po_durartion;
	long find_cliques_durartion;
	
	struct Clique {
		size_t primiteve_cnt;
		size_t point_cnt;
		long collect_points_durartion;
		long evo_durartion;
		EvoStats evo;
	}
	Clique[] cliques;
	
	uint successful_merges;
	uint failed_merges;
	long merge_durartion;
	string clique_gv;
	string merge_gv;
}


// TODO structure hash?
auto subtrees(CsgNode root) {
	HashInfo cache;
	CsgNode[][uint] subtrees;
	
	auto x = new CsgNode[1];
	x[0] = root;
	subtrees[root.hash(cache)] = x;
	
	foreach(node; root.descendants) {
		auto hash = node.hash(cache);
		
		if(auto list = hash in subtrees) {
			*list ~= node;
		}
		else {
			auto list = new CsgNode[1];
			list[0] = node;
			subtrees[hash] = list;
		}
	}
	
	return subtrees;
}


struct CommonSubtree {
	CsgNode[] subtrees1;
	CsgNode[] subtrees2;
	size_t size;
}


// https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Longest_common_substring#C++_2
auto common_subtrees(CsgNode root1, CsgNode root2) {
	CsgNode[][uint] sts_map1 = root1.subtrees;
	CsgNode[][uint] sts_map2 = root2.subtrees;
	
	CommonSubtree[] csts;
	foreach(hash, subtrees1; sts_map1) {
		if(auto subtrees2 = hash in sts_map2) {
			csts ~= CommonSubtree(subtrees1, *subtrees2, subtrees1[0].size);
		}
	}
	
	csts.sort!"a.size > b.size";
	
	return csts;
}


bool isValidMergeNode(CsgNode cur_node, CsgNode needle, bool merge_inter) {
	// Merge candicate (needle) reached by traversing the tree
	// -> valid merge candidate
	if(cur_node == needle) { return true; }
	
	final switch(cur_node.type) {
		case ECsgNodeType.UNION_OP:
			return cur_node.children.any!(x => isValidMergeNode(x, needle, merge_inter));
		
		case ECsgNodeType.INTERSECTION_OP:
			return merge_inter;
		
		case ECsgNodeType.SUBTRACT_OP:
			return isValidMergeNode(cur_node.first_child, needle, merge_inter);
		
		// Don't merge primitives, only op-nodes.
		case ECsgNodeType.CUBE_GEO:
		case ECsgNodeType.CYLINDER_GEO:
		case ECsgNodeType.SPHERE_GEO:
			return false;
	}
}


CsgNode mergeNodes(CsgNode root1, CsgNode root2, bool merge_inter) {
	// Try to merge, starting with the biggest "subtree type"
	foreach(cst_list; common_subtrees(root1, root2)) {
		// Find any valid subtree of the current "subtree type"
		auto subtree1 = cst_list.subtrees1.first!(x => isValidMergeNode(root1, x, merge_inter));
		auto subtree2 = cst_list.subtrees2.first!(x => isValidMergeNode(root2, x, merge_inter));
		
		// Replace subtree 1, if availabale, with tree 2 (except if subtree 2 is
		// also availabale and tree 1 is smaller than tree 2, which would yield smaller results)
		if(subtree1 !is null && !(subtree2 !is null && root1.size < root2.size)) {
			if(subtree1 == root1) { return root2; }
			subtree1.replaceWith(root2);
			return root1;
		}
		// Replace subtree 2, if available, with tree 1
		else if(subtree2 !is null) {
			if(subtree2 == root2) { return root1; }
			subtree2.replaceWith(root1);
			return root2;
		}
	}
	
	return null;
}


public CsgNode clique_evo(NormalPoint[] reference_points, Geo[] primitives, Evaluator evaluator, EvoParameters parameters, out CliqueStats stats) {
	/** @{ stats */
	stats.primiteve_cnt = primitives.length;
	stats.point_cnt = reference_points.length;
	if(primitives.empty) {
		return new UnionOp();
	}
	stats.clique_gv = "graph G {\n";
	stats.merge_gv = "digraph G {\n";
	/** @} */
	
	// Compute primitives overlaping graph
	auto sw = fresh_timer();
	auto graph = po_graph(primitives);
	stats.detect_po_durartion = sw.get_time;
	
	/** @{ stats */
	stats.clique_gv ~= "\t\n\tedge [style=\"bold\"]\n";
	GvRenderer renderer;
	renderer.style_extras = ", fontname=\"sans\"";
	foreach(ref geo; primitives) {
		if(auto speci_geo = cast(CubeGeo)geo) {
			stats.clique_gv ~= renderer.render(speci_geo);
		}
		else if(auto speci_geo = cast(CylinderGeo)geo) {
			stats.clique_gv ~= renderer.render(speci_geo);
		}
		else if(auto speci_geo = cast(SphereGeo)geo) {
			stats.clique_gv ~= renderer.render(speci_geo);
		}
		
		foreach(ref neighbour; graph.neighbours(geo)) {
			if (cast(void *)neighbour >= cast(void *)geo) {
				stats.clique_gv ~= "\top_%x -- op_%x\n".format(cast(void *)neighbour, cast(void *)geo);
			}
		}
	}
	/** @} */
	
	// Find cliques
	sw = fresh_timer();
	auto cliques = graph.bron_kerbosch;
	stats.find_cliques_durartion = sw.get_time;
	
	stats.cliques.length = cliques.length;
	DList!CsgNode clique_trees;
	size_t[CsgNode] clique_nrs;
	auto colors = rainbow(cliques.length, 87, 18).randomCover;
	foreach(i, ref clique; cliques) {
		auto clique_color = "%(%02x%)".format(colors.pop());
		// Collect reference points of clique
		sw = fresh_timer();
		auto primitive_hashes = Set!uint(clique.map!"a.hash");
		auto clique_reference = reference_points.filter!(x => primitive_hashes.contains(x.origin)).array;
		stats.cliques[i].collect_points_durartion = sw.get_time;
		
		/** @{ stats */
		infof("clique %s/%s with %s/%s points and %s geos", i+1, cliques.length, clique_reference.length, reference_points.length, clique.length);
		stats.cliques[i].primiteve_cnt = clique.length;
		stats.cliques[i].point_cnt = clique_reference.length;
		/** @} */
		
		// Build clique tree out of clique primitives and points using evo()
		sw = fresh_timer();
		auto clique_root = evo(clique_reference, clique, evaluator, parameters, stats.cliques[i].evo).simplify;
		stats.cliques[i].evo_durartion = sw.get_time;
		
		clique_trees ~= clique_root;
		
		/** @{ stats */
		clique_nrs[clique_root] = i;
		stats.clique_gv ~= "\top_cl_%s [fillcolor=\"#%s\", style=filled, label=\"Clique %s\"];\n".format(i, clique_color, i);
		foreach(ref geo; clique) {
			stats.clique_gv ~= "\top_cl_%s -- op_%x [color=\"#%s\"]\n".format(i, cast(void *)geo, clique_color);
		}
		stats.merge_gv ~= "\top_cl_%s [fillcolor=\"#%s\", style=filled, label=\"Clique %s\"];\n".format(i, clique_color, i);
		/** @} */
	}
	
	sw = fresh_timer();
	
	auto allowIntersections = false;
	while( ! clique_trees.empty && clique_trees.front != clique_trees.back) {
		tracef("Next round with %s", clique_trees[].map!"a.hash");
		auto n1 = clique_trees.pop();
		auto n2 = clique_trees.pop();
		auto firstN2 = n2;
		auto hash_1 = clique_nrs[n1];
		
		while(true) {
			auto hash_2 = clique_nrs[n2];
			
			tracef("  Try %s with %s (intersections = %s)", hash_1, hash_2, allowIntersections);
			if(auto mergedNode = mergeNodes(n1, n2, allowIntersections)) {
				/** @{ stats */
				auto hash_merged = clique_nrs[mergedNode] = mergedNode.hash;
				stats.merge_gv ~= "\t\n\top_cl_%s -> op_cl_%s;\n\top_cl_%s -> op_cl_%s;\n\top_cl_%s [label = \"%s\"];\n".format(hash_1, hash_merged, hash_2, hash_merged, hash_merged, hash_merged);
				stats.successful_merges++;
				tracef("  -> OK, new %s", hash_merged);
				/** @} */
				
				clique_trees.insertFront(mergedNode);
				allowIntersections = false;
				break;
			}
			else {
				/** @{ stats */
				stats.failed_merges++;
				tracef("  -> Fail");
				/** @} */
				
				// Try the next node (put old candicate back into list and take new one)
				clique_trees.insertBack(n2);
				n2 = clique_trees.pop();
				
				// If we have tried all candidates
				if(n2 == firstN2) {
					// Try all again, this time with intersections (2nd round)
					if( ! allowIntersections) {
						allowIntersections = true;
					}
					// Stop after 2nd round with intersections
					else {
						allowIntersections = false;
						break;
					}
				}
			}
		}
	}
	/** @{ stats */
	stats.merge_durartion = sw.get_time;
	stats.clique_gv ~= "}\n";
	stats.merge_gv ~= "}\n";
	/** @} */
	
	return clique_trees.empty ? null : clique_trees.front;
}



version(unittest) {
private:
	import fluent.asserts;
	import std.algorithm.mutation : reverse;
	import std.experimental.logger : globalLogLevel;
	import std.experimental.logger : LogLevel;
	
	shared static this() {
		globalLogLevel = LogLevel.warning;
	}
}

@("subtrees")
unittest {
	CsgNode[string] nodes;
	
	CsgNode root = new UnionOp([
		nodes["a"] = new UnionOp([
			nodes["a.s"] = new SphereGeo(Coord(), 0),
			nodes["a.c"] = new CubeGeo(Coord(), Dimension(), RadRotation()),
		]),
		nodes["r"] = new UnionOp([
			nodes["b"] = new UnionOp([
				nodes["b.s"] = new SphereGeo(Coord(), 0),
				nodes["b.c"] = new CubeGeo(Coord(), Dimension(), RadRotation()),
			]),
			nodes["c"] = new UnionOp([
				nodes["c.c"] = new CubeGeo(Coord(), Dimension(), RadRotation()),
				nodes["c.s"] = new SphereGeo(Coord(), 0),
			]),
		]),
	]);
	
	auto trees = root.subtrees;
	
	trees.keys.should.containOnly([
		root.hash, nodes["a"].hash, nodes["r"].hash, nodes["c"].hash, nodes["a.s"].hash, nodes["a.c"].hash,
	]);
	
	trees[root.hash].should.containOnly([root]);
	trees[nodes["a"].hash].should.containOnly([nodes["a"], nodes["b"]]);
	trees[nodes["r"].hash].should.containOnly([nodes["r"]]);
	trees[nodes["c"].hash].should.containOnly([nodes["c"]]);
	trees[nodes["a.s"].hash].should.containOnly([nodes["a.s"], nodes["b.s"], nodes["c.s"]]);
	trees[nodes["a.c"].hash].should.containOnly([nodes["a.c"], nodes["b.c"], nodes["c.c"]]);
}

@("common_subtrees - order")
unittest {
	CsgNode root1 = new UnionOp([
		new SphereGeo(Coord(), 1),
		new UnionOp([new SphereGeo(Coord(), 2), new SphereGeo(Coord(), 3)]),
	]);
	
	CsgNode root2 = new UnionOp([
		new UnionOp([new SphereGeo(Coord(), 2), new SphereGeo(Coord(), 3)]),
		new SphereGeo(Coord(), 1),
	]);
	
	foreach(pair ; [[root1, root2], [root2, root1]]) {
		auto sts = common_subtrees(pair[0], pair[1]);
		sts.map!"a.size".should.equal([3Lu, 1Lu, 1Lu, 1Lu]);
		sts.map!"a.subtrees1.length".should.equal([1Lu, 1Lu, 1Lu, 1Lu]);
		sts.map!"a.subtrees2.length".should.equal([1Lu, 1Lu, 1Lu, 1Lu]);
		sts.map!"a.subtrees1[0].size".should.equal([3Lu, 1Lu, 1Lu, 1Lu]);
		sts.map!"a.subtrees2[0].size".should.equal([3Lu, 1Lu, 1Lu, 1Lu]);
	}
	
	foreach(pair ; [[root1, root1], [root2, root2]]) {
		auto sts = common_subtrees(pair[0], pair[1]);
		sts.map!"a.size".should.equal([5Lu, 3Lu, 1Lu, 1Lu, 1Lu]);
		sts.map!"a.subtrees1.length".should.equal([1Lu, 1Lu, 1Lu, 1Lu, 1Lu]);
		sts.map!"a.subtrees2.length".should.equal([1Lu, 1Lu, 1Lu, 1Lu, 1Lu]);
		sts.map!"a.subtrees1[0].size".should.equal([5Lu, 3Lu, 1Lu, 1Lu, 1Lu]);
		sts.map!"a.subtrees2[0].size".should.equal([5Lu, 3Lu, 1Lu, 1Lu, 1Lu]);
	}
}

@("common_subtrees")
unittest {
	CsgNode[string] nodes;
	
	CsgNode root1 = new UnionOp([
		nodes["0.small-op[a]"] = new UnionOp([
			nodes["0.s[a]"] = new SphereGeo(Coord(), 1),
			nodes["0.s[b]"] = new SphereGeo(Coord(), 1),
		]),
		new UnionOp([new SphereGeo(Coord(), 42)]),
		nodes["0.big-op"] = new UnionOp([
			nodes["0.small-op[b]"] = new UnionOp([
				nodes["0.s[c]"] = new SphereGeo(Coord(), 1),
				nodes["0.s[d]"] = new SphereGeo(Coord(), 1),
			]),
			nodes["0.s[e]"] = new SphereGeo(Coord(), 1),
		]),
	]);
	
	CsgNode root2 = new UnionOp([
		nodes["1.big-op"] = new UnionOp([
			nodes["1.small-op[b]"] = new UnionOp([
				nodes["1.s[c]"] = new SphereGeo(Coord(), 1),
				nodes["1.s[d]"] = new SphereGeo(Coord(), 1),
			]),
			nodes["1.s[e]"] = new SphereGeo(Coord(), 1),
		]),
		new UnionOp([new SphereGeo(Coord(), 1337)]),
		nodes["1.small-op[a]"] = new UnionOp([
			nodes["1.s[a]"] = new SphereGeo(Coord(), 1),
			nodes["1.s[b]"] = new SphereGeo(Coord(), 1),
		]),
	]);
	
	auto sts = common_subtrees(root1, root2);
	sts.map!"a.size".should.equal([5Lu, 3Lu, 1Lu]);
	sts[0].subtrees1.should.containOnly([nodes["0.big-op"]]);
	sts[1].subtrees1.should.containOnly([nodes["0.small-op[a]"], nodes["0.small-op[b]"]]);
	sts[2].subtrees1.should.containOnly([nodes["0.s[a]"], nodes["0.s[b]"], nodes["0.s[c]"], nodes["0.s[d]"], nodes["0.s[e]"]]);
	sts[0].subtrees2.should.containOnly([nodes["1.big-op"]]);
	sts[1].subtrees2.should.containOnly([nodes["1.small-op[a]"], nodes["1.small-op[b]"]]);
	sts[2].subtrees2.should.containOnly([nodes["1.s[a]"], nodes["1.s[b]"], nodes["1.s[c]"], nodes["1.s[d]"], nodes["1.s[e]"]]);
}

@("isValidMergeNode")
unittest {
	CsgNode foo;
	
	foreach(inter; [false, true]) {
		isValidMergeNode(new SphereGeo(Coord(), 1), new SphereGeo(Coord(), 1), inter).should.equal(false);
		isValidMergeNode(foo = new SphereGeo(Coord(), 1), foo, inter).should.equal(true);
		
		isValidMergeNode(foo = new UnionOp([new SphereGeo(Coord(), 1), new SphereGeo(Coord(), 1)]), foo, inter).should.equal(true);
		isValidMergeNode(new UnionOp([foo = new SphereGeo(Coord(), 1), new SphereGeo(Coord(), 1)]), foo, inter).should.equal(true);
		isValidMergeNode(new UnionOp([new SphereGeo(Coord(), 1), foo = new SphereGeo(Coord(), 1)]), foo, inter).should.equal(true);
		isValidMergeNode(new UnionOp([new SphereGeo(Coord(), 1), new SphereGeo(Coord(), 1)]), foo, inter).should.equal(false);
		
		isValidMergeNode(foo = new SubOp([new SphereGeo(Coord(), 1), new SphereGeo(Coord(), 1)]), foo, inter).should.equal(true);
		isValidMergeNode(new SubOp([foo = new SphereGeo(Coord(), 1), new SphereGeo(Coord(), 1)]), foo, inter).should.equal(true);
		isValidMergeNode(new SubOp([new SphereGeo(Coord(), 1), foo = new SphereGeo(Coord(), 1)]), foo, inter).should.equal(false);
		isValidMergeNode(new SubOp([new SphereGeo(Coord(), 1), new SphereGeo(Coord(), 1)]), foo, inter).should.equal(false);
		
		isValidMergeNode(foo = new InterOp([new SphereGeo(Coord(), 1), new SphereGeo(Coord(), 1)]), foo, inter).should.equal(true);
		isValidMergeNode(new InterOp([foo = new SphereGeo(Coord(), 1), new SphereGeo(Coord(), 1)]), foo, inter).should.equal(inter);
		isValidMergeNode(new InterOp([new SphereGeo(Coord(), 1), foo = new SphereGeo(Coord(), 1)]), foo, inter).should.equal(inter);
		isValidMergeNode(new InterOp([new SphereGeo(Coord(), 1), new SphereGeo(Coord(), 1)]), foo, inter).should.equal(inter); // TODO soll das so?
	}
}

@("mergeNodes - largest subgraph")
unittest {
	CsgNode buildTree(int offset, bool reverse) {
		CsgNode[] subtrees = [
			new UnionOp([ // Mergeable (largest subtree)
				new SphereGeo(Coord(), 1), // Mergeable
				new SphereGeo(Coord(), 2), // Mergeable
			]),
			new UnionOp([ // Not mergeable (but even larger subtree)
				new SphereGeo(Coord(), offset+1), // Not mergeable
				new SphereGeo(Coord(), offset+2), // Not mergeable
				new SphereGeo(Coord(), offset+3), // Not mergeable
			]),
			new UnionOp([new SphereGeo(Coord(), 3)]), // Mergeable
		];
		if(reverse) { subtrees.reverse(); }
		return new UnionOp(subtrees);
	}
	
	mergeNodes(buildTree(10, false), buildTree(20, true), false).pprinter.toString.should.equal(`
	[#] UNION
	 ├─[#] UNION
	 │  ├─[#] UNION
	 │  │  └───> Sphere[ Coord( 0 | 0 | 0 ), 3.000000 ]
	 │  ├─[#] UNION
	 │  │  ├───> Sphere[ Coord( 0 | 0 | 0 ), 21.000000 ]
	 │  │  ├───> Sphere[ Coord( 0 | 0 | 0 ), 22.000000 ]
	 │  │  └───> Sphere[ Coord( 0 | 0 | 0 ), 23.000000 ]
	 │  └─[#] UNION
	 │     ├───> Sphere[ Coord( 0 | 0 | 0 ), 1.000000 ]
	 │     └───> Sphere[ Coord( 0 | 0 | 0 ), 2.000000 ]
	 ├─[#] UNION
	 │  ├───> Sphere[ Coord( 0 | 0 | 0 ), 11.000000 ]
	 │  ├───> Sphere[ Coord( 0 | 0 | 0 ), 12.000000 ]
	 │  └───> Sphere[ Coord( 0 | 0 | 0 ), 13.000000 ]
	 └─[#] UNION
	    └───> Sphere[ Coord( 0 | 0 | 0 ), 3.000000 ]
	`.mls);
	
	mergeNodes(buildTree(10, false), buildTree(20, false), false).pprinter.toString.should.equal(`
	[#] UNION
	 ├─[#] UNION
	 │  ├─[#] UNION
	 │  │  ├───> Sphere[ Coord( 0 | 0 | 0 ), 1.000000 ]
	 │  │  └───> Sphere[ Coord( 0 | 0 | 0 ), 2.000000 ]
	 │  ├─[#] UNION
	 │  │  ├───> Sphere[ Coord( 0 | 0 | 0 ), 21.000000 ]
	 │  │  ├───> Sphere[ Coord( 0 | 0 | 0 ), 22.000000 ]
	 │  │  └───> Sphere[ Coord( 0 | 0 | 0 ), 23.000000 ]
	 │  └─[#] UNION
	 │     └───> Sphere[ Coord( 0 | 0 | 0 ), 3.000000 ]
	 ├─[#] UNION
	 │  ├───> Sphere[ Coord( 0 | 0 | 0 ), 11.000000 ]
	 │  ├───> Sphere[ Coord( 0 | 0 | 0 ), 12.000000 ]
	 │  └───> Sphere[ Coord( 0 | 0 | 0 ), 13.000000 ]
	 └─[#] UNION
	    └───> Sphere[ Coord( 0 | 0 | 0 ), 3.000000 ]
	`.mls);
	
	mergeNodes(buildTree(10, true), buildTree(20, false), false).pprinter.toString.should.equal(`
	[#] UNION
	 ├─[#] UNION
	 │  └───> Sphere[ Coord( 0 | 0 | 0 ), 3.000000 ]
	 ├─[#] UNION
	 │  ├───> Sphere[ Coord( 0 | 0 | 0 ), 11.000000 ]
	 │  ├───> Sphere[ Coord( 0 | 0 | 0 ), 12.000000 ]
	 │  └───> Sphere[ Coord( 0 | 0 | 0 ), 13.000000 ]
	 └─[#] UNION
	    ├─[#] UNION
	    │  ├───> Sphere[ Coord( 0 | 0 | 0 ), 1.000000 ]
	    │  └───> Sphere[ Coord( 0 | 0 | 0 ), 2.000000 ]
	    ├─[#] UNION
	    │  ├───> Sphere[ Coord( 0 | 0 | 0 ), 21.000000 ]
	    │  ├───> Sphere[ Coord( 0 | 0 | 0 ), 22.000000 ]
	    │  └───> Sphere[ Coord( 0 | 0 | 0 ), 23.000000 ]
	    └─[#] UNION
	       └───> Sphere[ Coord( 0 | 0 | 0 ), 3.000000 ]
	`.mls);
	
	mergeNodes(buildTree(10, true), buildTree(20, true), false).pprinter.toString.should.equal(`
	[#] UNION
	 ├─[#] UNION
	 │  └───> Sphere[ Coord( 0 | 0 | 0 ), 3.000000 ]
	 ├─[#] UNION
	 │  ├───> Sphere[ Coord( 0 | 0 | 0 ), 11.000000 ]
	 │  ├───> Sphere[ Coord( 0 | 0 | 0 ), 12.000000 ]
	 │  └───> Sphere[ Coord( 0 | 0 | 0 ), 13.000000 ]
	 └─[#] UNION
	    ├─[#] UNION
	    │  └───> Sphere[ Coord( 0 | 0 | 0 ), 3.000000 ]
	    ├─[#] UNION
	    │  ├───> Sphere[ Coord( 0 | 0 | 0 ), 21.000000 ]
	    │  ├───> Sphere[ Coord( 0 | 0 | 0 ), 22.000000 ]
	    │  └───> Sphere[ Coord( 0 | 0 | 0 ), 23.000000 ]
	    └─[#] UNION
	       ├───> Sphere[ Coord( 0 | 0 | 0 ), 1.000000 ]
	       └───> Sphere[ Coord( 0 | 0 | 0 ), 2.000000 ]
	`.mls);
}

@("mergeNodes - only mergeable")
unittest {
	CsgNode buildTree(int offset, bool reverse) {
		CsgNode[] subtrees = [
			new SubOp([
				new SphereGeo(Coord(), offset+1),
				new UnionOp([ // lcs, but not reachable
					new SphereGeo(Coord(), 1),
					new SphereGeo(Coord(), 2),
				]),
			]),
			new UnionOp([
				new SphereGeo(Coord(), offset+2),
				new UnionOp([ // reachable lcs
					new SphereGeo(Coord(), 1),
					new SphereGeo(Coord(), 2),
				]),
			]),
		];
		if(reverse) { subtrees.reverse(); }
		return new UnionOp(subtrees);
	}
	
	mergeNodes(buildTree(10, false), buildTree(20, true), false).pprinter.toString.should.equal(`
	[#] UNION
	 ├─[#] SUBTRACT
	 │  ├───> Sphere[ Coord( 0 | 0 | 0 ), 11.000000 ]
	 │  └─[#] UNION
	 │     ├───> Sphere[ Coord( 0 | 0 | 0 ), 1.000000 ]
	 │     └───> Sphere[ Coord( 0 | 0 | 0 ), 2.000000 ]
	 └─[#] UNION
	    ├───> Sphere[ Coord( 0 | 0 | 0 ), 12.000000 ]
	    └─[#] UNION
	       ├─[#] UNION
	       │  ├───> Sphere[ Coord( 0 | 0 | 0 ), 22.000000 ]
	       │  └─[#] UNION
	       │     ├───> Sphere[ Coord( 0 | 0 | 0 ), 1.000000 ]
	       │     └───> Sphere[ Coord( 0 | 0 | 0 ), 2.000000 ]
	       └─[#] SUBTRACT
	          ├───> Sphere[ Coord( 0 | 0 | 0 ), 21.000000 ]
	          └─[#] UNION
	             ├───> Sphere[ Coord( 0 | 0 | 0 ), 1.000000 ]
	             └───> Sphere[ Coord( 0 | 0 | 0 ), 2.000000 ]
	`.mls);
	
	mergeNodes(buildTree(10, false), buildTree(20, false), false).pprinter.toString.should.equal(`
	[#] UNION
	 ├─[#] SUBTRACT
	 │  ├───> Sphere[ Coord( 0 | 0 | 0 ), 11.000000 ]
	 │  └─[#] UNION
	 │     ├───> Sphere[ Coord( 0 | 0 | 0 ), 1.000000 ]
	 │     └───> Sphere[ Coord( 0 | 0 | 0 ), 2.000000 ]
	 └─[#] UNION
	    ├───> Sphere[ Coord( 0 | 0 | 0 ), 12.000000 ]
	    └─[#] UNION
	       ├─[#] SUBTRACT
	       │  ├───> Sphere[ Coord( 0 | 0 | 0 ), 21.000000 ]
	       │  └─[#] UNION
	       │     ├───> Sphere[ Coord( 0 | 0 | 0 ), 1.000000 ]
	       │     └───> Sphere[ Coord( 0 | 0 | 0 ), 2.000000 ]
	       └─[#] UNION
	          ├───> Sphere[ Coord( 0 | 0 | 0 ), 22.000000 ]
	          └─[#] UNION
	             ├───> Sphere[ Coord( 0 | 0 | 0 ), 1.000000 ]
	             └───> Sphere[ Coord( 0 | 0 | 0 ), 2.000000 ]
	`.mls);
	
	mergeNodes(buildTree(10, true), buildTree(20, false), false).pprinter.toString.should.equal(`
	[#] UNION
	 ├─[#] UNION
	 │  ├───> Sphere[ Coord( 0 | 0 | 0 ), 12.000000 ]
	 │  └─[#] UNION
	 │     ├─[#] SUBTRACT
	 │     │  ├───> Sphere[ Coord( 0 | 0 | 0 ), 21.000000 ]
	 │     │  └─[#] UNION
	 │     │     ├───> Sphere[ Coord( 0 | 0 | 0 ), 1.000000 ]
	 │     │     └───> Sphere[ Coord( 0 | 0 | 0 ), 2.000000 ]
	 │     └─[#] UNION
	 │        ├───> Sphere[ Coord( 0 | 0 | 0 ), 22.000000 ]
	 │        └─[#] UNION
	 │           ├───> Sphere[ Coord( 0 | 0 | 0 ), 1.000000 ]
	 │           └───> Sphere[ Coord( 0 | 0 | 0 ), 2.000000 ]
	 └─[#] SUBTRACT
	    ├───> Sphere[ Coord( 0 | 0 | 0 ), 11.000000 ]
	    └─[#] UNION
	       ├───> Sphere[ Coord( 0 | 0 | 0 ), 1.000000 ]
	       └───> Sphere[ Coord( 0 | 0 | 0 ), 2.000000 ]
	`.mls);
	
	mergeNodes(buildTree(10, true), buildTree(20, true), false).pprinter.toString.should.equal(`
	[#] UNION
	 ├─[#] UNION
	 │  ├───> Sphere[ Coord( 0 | 0 | 0 ), 12.000000 ]
	 │  └─[#] UNION
	 │     ├─[#] UNION
	 │     │  ├───> Sphere[ Coord( 0 | 0 | 0 ), 22.000000 ]
	 │     │  └─[#] UNION
	 │     │     ├───> Sphere[ Coord( 0 | 0 | 0 ), 1.000000 ]
	 │     │     └───> Sphere[ Coord( 0 | 0 | 0 ), 2.000000 ]
	 │     └─[#] SUBTRACT
	 │        ├───> Sphere[ Coord( 0 | 0 | 0 ), 21.000000 ]
	 │        └─[#] UNION
	 │           ├───> Sphere[ Coord( 0 | 0 | 0 ), 1.000000 ]
	 │           └───> Sphere[ Coord( 0 | 0 | 0 ), 2.000000 ]
	 └─[#] SUBTRACT
	    ├───> Sphere[ Coord( 0 | 0 | 0 ), 11.000000 ]
	    └─[#] UNION
	       ├───> Sphere[ Coord( 0 | 0 | 0 ), 1.000000 ]
	       └───> Sphere[ Coord( 0 | 0 | 0 ), 2.000000 ]
	`.mls);
}

@("model")
unittest {
	import libcsgfitter.evaluator.e12;
	import libcsgfitter.reader.json;
	import libcsgfitter.writer.scad;
	import libcsgfitter.tools.primitives;
	import libcsgfitter.sampler.surface;
	import generic.testfiles;
	
	auto csg = read_json_file("tests/renderings/complex.json");
	auto reference = csg.surface_sample(1.5);
	auto primitives = csg.extract_primitives;
	
	auto evaluator_parameters = Evaluator12Parameters();
	
	auto parameters = EvoParameters();
	parameters.max_rounds = 10;
	parameters.population_size = 100;
	auto evaluator = new Evaluator12(&evaluator_parameters);
	
	CliqueStats stats;
	auto model = reference.clique_evo(primitives, evaluator, parameters, stats);
	model.write_scad(ScadProperties(test_path("libcsgfitter-fitter-cliqu_evo.scad")));
}
