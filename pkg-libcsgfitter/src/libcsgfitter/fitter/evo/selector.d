// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module libcsgfitter.fitter.evo.selector;

import generic.util;
import libcsgfitter.csgtree;
import libcsgfitter.fitter.evo;
import std.conv : to;
import std.random : randomCover;
import std.random : uniform;
import std.range : iota;
import std.algorithm : min;



public enum Selector {
	rnd_prop,
	rnd_uniform,
	rnd_swipe,
	best_inverse,
	best_with_best,
	tournament,
}


package struct ParentSelector {
	package Selector selector;
	package int tournament_t = 5;
	package size_t[] parent_ids;
	
	
	package size_t[] update_selection(RankedTree[] population) {
		final switch(selector) {
			case Selector.rnd_prop:       selectRndProp(population); break;
			case Selector.rnd_uniform:    selectRndUniform(population); break;
			case Selector.rnd_swipe:      selectRndSwipe(population); break;
			case Selector.best_inverse:   selectBestInverse(population); break;
			case Selector.best_with_best: selectBestWithBest(population); break;
			case Selector.tournament:     selectTournament(population); break;
		}
		return parent_ids[];
	}
	
	
	// n = 3
	// Index:  0   1   2   3   4   5   6   7
	// Score: [0] [1] [2] [3] [4] [5] [6] [7] (worst; less is better)
	// Pairs:  +---+   +---+   +---+
	private void selectBestWithBest(RankedTree[] population) {
		assert(population.length >= parent_ids.length);
		
		foreach(i, ref parent_id; parent_ids) {
			parent_id = i;
		}
	}
	
	
	// n = 3
	// Index:  0   1   2   3   4   5   6   7
	// Score: [0] [1] [2] [3] [4] [5] [6] [7] (worst; less is better)
	// Pairs:  |   |   +---+   |   |
	//         |   +-----------+   |
	//         +-------------------+
	private void selectBestInverse(RankedTree[] population) {
		assert(population.length >= parent_ids.length);
		
		foreach(i; 0..parent_ids.length/2) {
			parent_ids[i*2] = i;
			parent_ids[i*2+1] = parent_ids.length - 1 - i;
		}
	}
	
	
	// Index:    0   1   2   3
	// Score:   [0] [1] [2] [3] (worst; less is better)
	// Weights:  #   #   #   #
	//           1   1   1   1
	private void selectRndSwipe(RankedTree[] population) {
		assert(population.length >= parent_ids.length);
		
		auto id_gen = iota(0, population.length).randomCover;
		foreach(ref parent_id; parent_ids) {
			parent_id = id_gen.pop;
		}
	}
	
	
	// Index:    0   1   2   3
	// Score:   [0] [1] [2] [3] (worst; less is better)
	// Weights:  #   #   #   #
	//           1   1   1   1
	private void selectRndUniform(RankedTree[] population) {
		foreach(ref parent_id; parent_ids) {
			parent_id = uniform(0, population.length);
		}
	}
	
	
	// Index:    0   1   2   3
	// Score:   [0] [1] [2] [3] (worst; less is better)
	// Weights:  #
	//           #   #
	//           #   #   #
	//           #   #   #   #
	//           4   3   2   1
	private void selectRndProp(RankedTree[] population) {
		auto max = ((population.length^^2 + population.length) / 2).to!uint;
		
		foreach(ref parent_id; parent_ids) {
			parent_id = population.length - uniform(0, max).step - 1;
		}
	}
	
	
	// Index:    0   1   2   3
	// Score:   [0] [1] [2] [3] (worst; less is better)
	// Weights:  #
	//           #   
	//           #   #   
	//           #   #   #   #
	private void selectTournament(RankedTree[] population) {
		auto n = population.length;
		
		foreach(ref parent_id; parent_ids) {
			size_t winner_id = n;
			foreach(j; 0..tournament_t) {
				winner_id = min(winner_id, uniform(0, n));
			}
			parent_id = winner_id;
		}
	}
}



version(unittest) {
private:
	import fluent.asserts;
	import std.format;
	import xxhash : XXHash;
	
	class Node : CsgNode {
		size_t score;
		this(size_t score) { this.score = score; }
		override void acceptPre(CsgVisitor visitor) { }
		override void acceptPost(CsgVisitor visitor) { }
		override Node clone() { return new Node(score); }
		@property override public void node_hash(ref XXHash, ref HashInfo) {}
		@property override ECsgNodeType type() const { return ECsgNodeType.UNION_OP; }
		@property override size_t effective_max_children() const pure { return 0; }
		@property override size_t effective_min_children() const pure { return 0; }
	}
	
	auto create_population(size_t size, size_t offset=0) {
		auto population = new RankedTree[size];
		foreach(i, ref el; population) {
			size_t score = 10 * i + offset;
			el = RankedTree(new Node(score), score);
		}
		return population;
	}
}

@("selectBestWithBest")
unittest {
	ParentSelector s = { selector: Selector.best_with_best, parent_ids: new size_t[6] };
	s.update_selection(create_population(8));
	s.parent_ids.should.equal([0Lu,1Lu , 2Lu,3Lu , 4Lu,5Lu]);
}

@("selectBestInverse")
unittest {
	ParentSelector s = { selector: Selector.best_inverse, parent_ids: new size_t[6] };
	s.update_selection(create_population(8));
	s.parent_ids.should.equal([0Lu,5Lu , 1Lu,4Lu , 2Lu,3Lu]);
}

@("selectRndSwipe")
unittest {
	immutable size_t sample_size = 600; // multiple of 2
	immutable float error = 0.05;
	size_t[3] counters;
	ParentSelector s = { selector: Selector.rnd_swipe, parent_ids: new size_t[2] };
	auto population = create_population(counters.length);
	foreach(i; 0..sample_size/2) {
		s.update_selection(population);
		s.parent_ids[0].should.not.equal(s.parent_ids[1]);
		foreach(parent_id; s.parent_ids) {
			counters[parent_id]++;
		}
	}
	
	foreach(i, cnt; counters) {
		cnt.should.be.between(sample_size/3-to!size_t(sample_size*error), sample_size/3+to!size_t(sample_size*error)).because("%s".format(counters));
	}
}

@("selectRndUniform")
unittest {
	immutable size_t sample_size = 1_000; // multiple of 2
	immutable float error = 0.1;
	size_t[3] counters;
	ParentSelector s = { selector: Selector.rnd_uniform, parent_ids: new size_t[4] };
	auto population = create_population(counters.length);
	foreach(i; 0..sample_size) {
		s.update_selection(population);
		foreach(parent_id; s.parent_ids) {
			counters[parent_id]++;
		}
	}
	
	auto target = sample_size * s.parent_ids.length / counters.length;
	foreach(i, cnt; counters) {
		cnt.should.be.between(
			target - to!size_t(target*error),
			target + to!size_t(target*error),
		).because("%s".format(counters));
	}
}

@("selectRndProp")
unittest {
	immutable size_t sample_size = 1_000; // multiple of 2
	immutable float error = 0.05;
	size_t[3] counters;
	ParentSelector s = { selector: Selector.rnd_prop, parent_ids: new size_t[2] };
	auto population = create_population(counters.length);
	foreach(i; 0..sample_size/2) {
		s.update_selection(population);
		foreach(parent_id; s.parent_ids) {
			counters[parent_id]++;
		}
	}
	
	counters[0].should.be.approximately(sample_size/2, to!size_t(sample_size*error)).because("%s".format(counters));
	counters[1].should.be.approximately(sample_size/3, to!size_t(sample_size*error)).because("%s".format(counters));
	counters[2].should.be.approximately(sample_size/6, to!size_t(sample_size*error)).because("%s".format(counters));
}


@("selectTournament(t=1)")
unittest {
	immutable size_t sample_size = 10;
	immutable float error = 0.1;
	size_t[3] counters;
	ParentSelector s = { selector: Selector.tournament, parent_ids: new size_t[1_000], tournament_t: 1 };
	auto population = create_population(counters.length);
	foreach(i; 0..sample_size) {
		s.update_selection(population);
		foreach(parent_id; s.parent_ids) {
			counters[parent_id]++;
		}
	}
	
	auto target = sample_size * s.parent_ids.length / counters.length;
	foreach(i, cnt; counters) {
		cnt.should.be.between(
			target - to!size_t(target*error),
			target + to!size_t(target*error),
		).because("%s".format(counters));
	}
}


@("selectTournament(t=5)")
unittest {
	immutable size_t sample_size = 100;
	immutable float error = 0.1;
	size_t[3] counters;
	ParentSelector s = { selector: Selector.tournament, parent_ids: new size_t[1_000], tournament_t: 5 };
	auto population = create_population(counters.length);
	foreach(i; 0..sample_size) {
		s.update_selection(population);
		foreach(parent_id; s.parent_ids) {
			counters[parent_id]++;
		}
	}
	
	float N = counters.length;
	foreach(i, cnt; counters) {
		auto p = ((N-i)/N)^^s.tournament_t - ((N-i-1)/N)^^s.tournament_t;
		auto target = to!size_t(sample_size * s.parent_ids.length * p);
		cnt.should.be.between(
			target - to!size_t(target*error),
			target + to!size_t(target*error),
		).because("%s".format(counters));
	}
}
