// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module libcsgfitter.fitter.evo.reinserter;

import generic.util;
import libcsgfitter.csgtree;
import libcsgfitter.fitter.evo.helper;
import libcsgfitter.fitter.evo;
import std.algorithm.sorting : sort;
import std.array : array;
import std.bitmanip : BitArray;
import std.random : randomCover;
import std.range : iota;



public enum ReplacementStrategy {
	worst_parents_first, // better name: worst_but_parents_first or parents_fst_worst_snd
	random_parents_first, // better name: random_but_parents_first or parents_fst_random_snd
	best_overall,
	worst,
	random,
}


package void reinsert(RankedTree[] population, RankedTree[] selected_children, size_t[] parent_ids, ReplacementStrategy replacement_strategy) {
	assert(population.length >= selected_children.length);
	// parrent_ids can be:
	//   * > population (mating_rate > 1)
	//   * < selected_children (parents producing more offspring)
	//   * > selected_children (mating_rate > 1)
	
	size_t pos;
	size_t[] remaining_ids;
	
	final switch(replacement_strategy) {
		// 1. Replace all parents by selected children
		// 2. Replace worst non parents by remaining selected children (if any)
		case ReplacementStrategy.worst_parents_first:
			remaining_ids = population.reinsert_parents(selected_children, parent_ids, pos);
			
			foreach_reverse(id; remaining_ids) {
				if(pos == selected_children.length) { break; }
				population[id] = selected_children[pos++];
			}
			break;
		
		// 1. Replace all parents by selected children
		// 2. Replace random non parents by remaining selected children (if any)
		case ReplacementStrategy.random_parents_first:
			remaining_ids = population.reinsert_parents(selected_children, parent_ids, pos);
			
			foreach(id; remaining_ids.randomCover) {
				if(pos == selected_children.length) { break; }
				population[id] = selected_children[pos++];
			}
			break;
		
		// Keep the best individualas of the population and selected children combined
		case ReplacementStrategy.best_overall:
			selected_children.sort!"a.score < b.score";
			
			//     v p
			// [ 0 | 1 | 2 | 3 ] len=4  Compare p with c. Cut on v/^.
			//     [ 0 | 1 | 2 ] len=3
			//               c ^
			for(
				long child_pos = selected_children.length - 1, pop_pos = population.length - selected_children.length;
				child_pos >= 0;
				child_pos--, pop_pos++
			) {
				if(selected_children[child_pos].score <= population[pop_pos].score) {
					population[pop_pos..$] = selected_children[0..child_pos+1];
					break;
				}
			}
			break;
		
		// Replace worst individual in population
		case ReplacementStrategy.worst:
			foreach_reverse(ref individual; population[$-selected_children.length..$]) {
				individual = selected_children[pos++];
			}
			break;
		
		// Replace random individuals in population
		case ReplacementStrategy.random:
			auto ids = iota(population.length).randomCover;
			foreach(ref child; selected_children) {
				population[ids.pop] = child;
			}
			break;
	}
}


/**
* Replace parents by selected children.  Replace a parent only one, even if they
* where in more than one relationship.  Stop if there are no more selected
* children.  Return an array of unused/unreplaced individuals in the population
* and (as a refference) the position of the last inserted child + 1.
*/
private size_t[] reinsert_parents(RankedTree[] population, RankedTree[] selected_children, size_t[] parent_ids, out size_t pos) {
	BitArray free;
	free.length = population.length;
	free.flip();
	
	// (Thre are garanteed more or equal children than unique parents)
	foreach(parent_id; parent_ids) {
		if(pos == selected_children.length) { break; }
		if( ! free[parent_id]) { continue; }
		population[parent_id] = selected_children[pos++];
		free[parent_id] = false;
	}
	
	return free.bitsSet.array;
}



version(unittest) {
private:
	import fluent.asserts;
	import std.format;
	import xxhash : XXHash;
	import std.conv : to;
	import std.algorithm.iteration : map;
	
	class Node : CsgNode {
		size_t score;
		this(size_t score) { this.score = score; }
		override void acceptPre(CsgVisitor visitor) { }
		override void acceptPost(CsgVisitor visitor) { }
		override Node clone() { return new Node(score); }
		@property override public void node_hash(ref XXHash, ref HashInfo) {}
		@property override ECsgNodeType type() const { return ECsgNodeType.UNION_OP; }
		@property override size_t effective_max_children() const pure { return 0; }
		@property override size_t effective_min_children() const pure { return 0; }
	}
	
	auto create_population(size_t size, size_t offset=0) {
		auto population = new RankedTree[size];
		foreach(i, ref el; population) {
			size_t score = 10 * i + offset;
			el = RankedTree(new Node(score), score);
		}
		return population;
	}
}

@("reinsert worst_parents_first")
unittest {
	auto population = create_population(8);
	auto children = create_population(4, 21);
	auto parent_ids = [5Lu, 2Lu, 5Lu, 1Lu];
	population.reinsert(children, parent_ids, ReplacementStrategy.worst_parents_first);
	population.map!(a => (cast(Node)a.root).score).should.equal([0Lu, 41Lu, 31Lu, 30Lu, 40Lu, 21Lu, 60Lu, 51Lu]);
	//                                                          [0]  [1]   [2]   [3]   [4]   [5]   [6]   [7]
}

@("reinsert worst")
unittest {
	auto population = create_population(8);
	auto children = create_population(4, 21);
	auto parent_ids = [5Lu, 2Lu, 5Lu, 1Lu];
	population.reinsert(children, parent_ids, ReplacementStrategy.worst);
	population.map!(a => (cast(Node)a.root).score).should.equal([0Lu, 10Lu, 20Lu, 30Lu, 51Lu, 41Lu, 31Lu, 21Lu]);
	//                                                          [0]  [1]   [2]   [3]   [4]   [5]   [6]   [7]
}

@("reinsert best_overall")
unittest {
	RankedTree[] population;
	RankedTree[] children;
	auto parent_ids = [5Lu, 2Lu, 5Lu, 1Lu];
	
	population = create_population(8);
	children = create_population(4, 21);
	population.reinsert(children, parent_ids, ReplacementStrategy.best_overall);
	population.map!(a => (cast(Node)a.root).score).should.equal([0Lu, 10Lu, 20Lu, 30Lu, 40Lu, 21Lu, 31Lu, 41Lu]);
	//                                                          [0]  [1]   [2]   [3]   [4]   [5]   [6]   [7]
	
	population = create_population(8);
	children = create_population(8, 21);
	population.reinsert(children, parent_ids, ReplacementStrategy.best_overall);
	population.map!(a => (cast(Node)a.root).score).should.equal([0Lu, 10Lu, 20Lu, 30Lu, 40Lu, 21Lu, 31Lu, 41Lu]);
	//                                                          [0]  [1]   [2]   [3]   [4]   [5]   [6]   [7]
	
	population = create_population(8);
	children = create_population(4, 91);
	population.reinsert(children, parent_ids, ReplacementStrategy.best_overall);
	population.map!(a => (cast(Node)a.root).score).should.equal([0Lu, 10Lu, 20Lu, 30Lu, 40Lu, 50Lu, 60Lu, 70Lu]);
	//                                                          [0]  [1]   [2]   [3]   [4]   [5]   [6]   [7]
	
	population = create_population(8);
	children = create_population(8, 91);
	population.reinsert(children, parent_ids, ReplacementStrategy.best_overall);
	population.map!(a => (cast(Node)a.root).score).should.equal([0Lu, 10Lu, 20Lu, 30Lu, 40Lu, 50Lu, 60Lu, 70Lu]);
	//                                                          [0]  [1]   [2]   [3]   [4]   [5]   [6]   [7]
	
	population = create_population(8, 90);
	children = create_population(4, 1);
	population.reinsert(children, parent_ids, ReplacementStrategy.best_overall);
	population.map!(a => (cast(Node)a.root).score).should.equal([90Lu, 100Lu, 110Lu, 120Lu, 1Lu, 11Lu, 21Lu, 31Lu]);
	//                                                          [0]   [1]    [2]    [3]    [4]  [5]   [6]   [7]
	
	population = create_population(8, 90);
	children = create_population(8, 1);
	population.reinsert(children, parent_ids, ReplacementStrategy.best_overall);
	population.map!(a => (cast(Node)a.root).score).should.equal([1Lu, 11Lu, 21Lu, 31Lu, 41Lu, 51Lu, 61Lu, 71Lu]);
	//                                                          [0]  [1]   [2]   [3]   [4]   [5]   [6]   [7]
}

@("reinsert random_parents_first")
unittest {
	import std.algorithm.iteration : sum;
	
	immutable size_t sample_size = 1_000;
	immutable float error = 0.15;
	
	auto children = create_population(4, 21);
	auto parent_ids = [4Lu, 2Lu, 4Lu, 1Lu];
	size_t[3] counters;
	
	foreach(i; 0..sample_size) {
		auto population = create_population(6);
		
		population.reinsert(children, parent_ids, ReplacementStrategy.random_parents_first);
		auto ids = population.map!(a => (cast(Node)a.root).score).array;
		ids[4].should.equal(21Lu).because("%s".format(ids));
		ids[2].should.equal(31Lu).because("%s".format(ids));
		ids[1].should.equal(41Lu).because("%s".format(ids));
		
		foreach(counter_pos, pop_pos; [0, 3, 5]) {
			if(ids[pop_pos] == 51) {
				counters[counter_pos]++;
			}
		}
	}
	
	counters[].sum.should.equal(sample_size);
	foreach(counter_pos, counter; counters) {
		counter.should.be.approximately(sample_size/counters.length, to!size_t(sample_size/counters.length*error)).because("%s".format(counters));
	}
}

@("reinsert random")
unittest {
	import std.algorithm.iteration : sum;
	
	immutable size_t sample_size = 2_000;
	immutable float error = 0.15;
	
	auto children = create_population(2, 21);
	auto parent_ids = [0Lu, 0Lu];
	size_t[4] counters;
	
	foreach(i; 0..sample_size) {
		auto population = create_population(counters.length);
		
		population.reinsert(children, parent_ids, ReplacementStrategy.random);
		auto ids = population.map!(a => (cast(Node)a.root).score).array;
		size_t score_sum;
		foreach(counter_pos, score; ids) {
			if(score % 10 == 1) {
				score_sum += score;
				counters[counter_pos]++;
			}
		}
		score_sum.should.equal(21+31);
	}
	
	counters[].sum.should.equal(sample_size*children.length);
	foreach(counter; counters) {
		counter.should.be.approximately(sample_size*children.length/counters.length, to!size_t(sample_size/counters.length*error)).because("%s".format(counters));
	}
}
