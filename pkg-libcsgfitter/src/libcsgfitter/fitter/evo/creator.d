// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module libcsgfitter.fitter.evo.creator;

import generic.util;
import libcsgfitter.csgtree;
import std.algorithm.iteration : sum;
import std.container.dlist : DList;
import std.random : dice;
import std.random : randomCover;
import std.random : uniform01;



// package enum Creator : CreatorFn {
// 	random = &createCycle,
// 	lin = &createLin,
// }
// 
// 
// package alias CreatorFn = CsgNode function(Geo[]);


package struct TreeCreator {
	package float create_semi_binary_weight = 0.0;
	package float create_linear_weight = 0.0;
	
	package float union_op_weight = 0.4;
	package float inter_op_weight = 0.3;
	package float sub_op_weight = 0.3;
	
	package float semi_binary_leaf_chance = 0.2;
	
	
	package CsgNode create(Geo[] geos) {
		auto weights = [
			create_semi_binary_weight,
			create_linear_weight,
		];
		
		if(weights.sum == 0.0) {
			weights[0] = 1.0;
		}
		
		switch(dice(weights)) {
			case 0: return create_semi_binary(geos);
			case 1: return create_linear(geos);
			default: assert(false);
		}
	}
	
	
	// Requirements:
	//  * exactly n leafs
	//  * average depth <= a*log(n)
	package CsgNode create_semi_binary(Geo[] geos) {
		auto r = geos.randomCover;
		
		CsgNode root = random_op();
		auto parent_candidates = DList!CsgNode([root, root]);
		size_t parent_candidates_cnt = 2;
		size_t remaining_geos = geos.length;
		
		
		while(parent_candidates_cnt < remaining_geos) {
			auto parent = parent_candidates.pop;
			parent_candidates_cnt--;
			
			if(parent_candidates_cnt > 0 && uniform01() < semi_binary_leaf_chance) {
				auto geo = r.pop.clone;
				parent.appendChild(geo);
				remaining_geos--;
			}
			else {
				auto op = random_op();
				parent.appendChild(op);
				parent_candidates.insertBack(op);
				parent_candidates.insertBack(op);
				parent_candidates_cnt += 2;
			}
		}
		
		
		foreach(geo; r) {
			parent_candidates.pop().appendChild(geo.clone);
		}
		
		return root;
	}
	
	/**
	* Creates a degraded CSG tree which looks like a linked list out of union
	* operations containing the nodes in random order.
	*/
	private CsgNode create_linear(Geo[] geos) {
		CsgNode root = new UnionOp();
		
		auto r = geos.randomCover;
		CsgNode lastNode = r.pop.clone;
		CsgNode currentOp = root;
		
		if(!r.empty) {
			while(true) {
				currentOp.appendChild(r.pop.clone);
				
				if(r.empty) break;
				
				Op op = random_op();
				currentOp.appendChild(op);
				currentOp = op;
			}
		}
		
		currentOp.appendChild(lastNode);
		
		return root;
	}
	
	
	package Op random_op() {
		switch(dice(union_op_weight, inter_op_weight, sub_op_weight)) {
			case 0: return new UnionOp();
			case 1: return new InterOp();
			case 2: return new SubOp();
			default: assert(false);
		}
	}
}



version(unittest) {
private:
	import fluent.asserts;
	import libcsgfitter.properties;
	import generic.tree;
	import std.random : rndGen;
	import std.random : Random;
	import std.stdio;
	
	Geo[] create_geos(size_t n) {
		auto geos = new Geo[n];
		foreach(i, ref geo; geos) {
			geo = new SphereGeo(Coord(), i+1);
		}
		return geos;
	}
}

@("createCycle - different sizes")
unittest {
	TreeCreator c;
	c.create_semi_binary_weight = 1.0;
	
// 	foreach(i; 0..6) {
// 		rndGen = Random(0);
// 		writefln("create_semi_binary SIZE %s:\n%s\n", i, c.create(create_geos(i)).pprinter.toString);
// 	}
// 	foreach(i; 0..20) {
// 		rndGen = Random(i);
// 		writefln("create_semi_binary SEED %s:\n%s\n", i, c.create(create_geos(4)).pprinter.toString);
// 	}
	
	rndGen = Random(0);
	c.create(create_geos(0)).pprinter.toString.should.equal(`
		──> INTERSECTION
	`.mls);
	
	rndGen = Random(0);
	c.create(create_geos(1)).pprinter.toString.should.equal(`
		[#] SUBTRACT
		 └───> Sphere[ Coord( 0 | 0 | 0 ), 1.000000 ]
	`.mls);
	
	rndGen = Random(0);
	c.create(create_geos(2)).pprinter.toString.should.equal(`
		[#] SUBTRACT
		 ├───> Sphere[ Coord( 0 | 0 | 0 ), 2.000000 ]
		 └───> Sphere[ Coord( 0 | 0 | 0 ), 1.000000 ]
	`.mls);
	
	rndGen = Random(0);
	c.create(create_geos(3)).pprinter.toString.should.equal(`
		[#] SUBTRACT
		 ├─[#] SUBTRACT
		 │  ├───> Sphere[ Coord( 0 | 0 | 0 ), 2.000000 ]
		 │  └───> Sphere[ Coord( 0 | 0 | 0 ), 1.000000 ]
		 └───> Sphere[ Coord( 0 | 0 | 0 ), 3.000000 ]
	`.mls);
	
	rndGen = Random(0);
	c.create(create_geos(4)).pprinter.toString.should.equal(`
		[#] SUBTRACT
		 ├─[#] SUBTRACT
		 │  ├───> Sphere[ Coord( 0 | 0 | 0 ), 2.000000 ]
		 │  └───> Sphere[ Coord( 0 | 0 | 0 ), 3.000000 ]
		 └─[#] SUBTRACT
		    ├───> Sphere[ Coord( 0 | 0 | 0 ), 4.000000 ]
		    └───> Sphere[ Coord( 0 | 0 | 0 ), 1.000000 ]
	`.mls);
	
	rndGen = Random(1);
	c.create(create_geos(4)).pprinter.toString.should.equal(`
		[#] SUBTRACT
		 ├───> Sphere[ Coord( 0 | 0 | 0 ), 1.000000 ]
		 └─[#] SUBTRACT
		    ├───> Sphere[ Coord( 0 | 0 | 0 ), 2.000000 ]
		    └─[#] UNION
		       ├───> Sphere[ Coord( 0 | 0 | 0 ), 3.000000 ]
		       └───> Sphere[ Coord( 0 | 0 | 0 ), 4.000000 ]
	`.mls);
	
	rndGen = Random(0);
	c.create(create_geos(5)).pprinter.toString.should.equal(`
		[#] SUBTRACT
		 ├─[#] SUBTRACT
		 │  ├─[#] INTERSECTION
		 │  │  ├───> Sphere[ Coord( 0 | 0 | 0 ), 4.000000 ]
		 │  │  └───> Sphere[ Coord( 0 | 0 | 0 ), 5.000000 ]
		 │  └───> Sphere[ Coord( 0 | 0 | 0 ), 3.000000 ]
		 └─[#] SUBTRACT
		    ├───> Sphere[ Coord( 0 | 0 | 0 ), 2.000000 ]
		    └───> Sphere[ Coord( 0 | 0 | 0 ), 1.000000 ]
	`.mls);
	
	rndGen = Random(8);
	c.create(create_geos(15)).pprinter.toString.should.equal(`
		[#] UNION
		 ├─[#] UNION
		 │  ├─[#] INTERSECTION
		 │  │  ├─[#] INTERSECTION
		 │  │  │  ├─[#] UNION
		 │  │  │  │  ├───> Sphere[ Coord( 0 | 0 | 0 ), 9.000000 ]
		 │  │  │  │  └───> Sphere[ Coord( 0 | 0 | 0 ), 6.000000 ]
		 │  │  │  └─[#] SUBTRACT
		 │  │  │     ├───> Sphere[ Coord( 0 | 0 | 0 ), 11.000000 ]
		 │  │  │     └───> Sphere[ Coord( 0 | 0 | 0 ), 1.000000 ]
		 │  │  └───> Sphere[ Coord( 0 | 0 | 0 ), 7.000000 ]
		 │  └───> Sphere[ Coord( 0 | 0 | 0 ), 5.000000 ]
		 └─[#] SUBTRACT
		    ├─[#] SUBTRACT
		    │  ├─[#] SUBTRACT
		    │  │  ├───> Sphere[ Coord( 0 | 0 | 0 ), 10.000000 ]
		    │  │  └─[#] INTERSECTION
		    │  │     ├───> Sphere[ Coord( 0 | 0 | 0 ), 8.000000 ]
		    │  │     └───> Sphere[ Coord( 0 | 0 | 0 ), 12.000000 ]
		    │  └─[#] UNION
		    │     ├───> Sphere[ Coord( 0 | 0 | 0 ), 13.000000 ]
		    │     └─[#] INTERSECTION
		    │        ├───> Sphere[ Coord( 0 | 0 | 0 ), 14.000000 ]
		    │        └───> Sphere[ Coord( 0 | 0 | 0 ), 4.000000 ]
		    └─[#] SUBTRACT
		       ├─[#] UNION
		       │  ├───> Sphere[ Coord( 0 | 0 | 0 ), 15.000000 ]
		       │  └───> Sphere[ Coord( 0 | 0 | 0 ), 3.000000 ]
		       └───> Sphere[ Coord( 0 | 0 | 0 ), 2.000000 ]
	`.mls);
}
