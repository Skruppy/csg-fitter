// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module libcsgfitter.fitter.evo.helper;

import generic.tree;
import libcsgfitter.csgtree;



/**
* Creates a copy of a CSG tee and counts all nodes.
*/
package CsgNode copyAndCount(CsgNode root, size_t* x = null) {
	CsgNode[CsgNode] annotations;
	
	scope auto visitor = new class CsgVisitor {
		override public void visitPreAll(CsgNode node) {
			auto newNode = node.clone;
			annotations[node] = newNode;
			
			if(!node.isRoot) {
				newNode.parent = annotations[node.parent];
			}
		}
	};
	visitor.walk(root);
	
	if(x !is null) *x = annotations.length;
	
	return annotations[root];
}

/**
* Get the node of the nth ancestor of an CSG tree.
*/
package CsgNode subtree(CsgNode root, size_t nr) {
	CsgNode subtree;
	
	scope auto visitor = new class CsgVisitor {
		override public void visitPreAll(CsgNode node) {
			if(nr-- == 0) subtree = node;
		}
	};
	visitor.walk(root);
	
	return subtree;
}
