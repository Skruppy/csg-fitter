// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module libcsgfitter.fitter.evo.core;

import generic.bron_kerbosch;
import generic.graph;
import generic.tree;
import generic.util;
import libcsgfitter.csgtree;
import libcsgfitter.evaluator.base;
import libcsgfitter.fitter.evo.selector;
import libcsgfitter.fitter.evo.creator;
import libcsgfitter.fitter.evo.helper;
import libcsgfitter.fitter.evo.mutator;
import libcsgfitter.fitter.evo.reinserter;
import libcsgfitter.point;
import libcsgfitter.properties;
import std.algorithm : max;
import std.algorithm : min;
import std.algorithm.iteration : map;
import std.algorithm.searching : maxElement;
import std.algorithm.sorting : sort;
import std.array : array;
import std.conv : roundTo;
import std.datetime.stopwatch : StopWatch;
import std.experimental.logger : infof;
import std.random : uniform;
import std.random : uniform01;
import std.range : chunks;



public struct EvoParameters {
	size_t population_size = 200;
	float mating_rate = 0.25;
	float reinsertion_rate = -1;
	float crossover_rate = 1.0;
	
	ReplacementStrategy replacement_strategy;
	
	size_t max_rounds = 40;
	
	/// @see creator.d
	/// @{
	float creator_semi_binary_weight = 0.0;
	float creator_linear_weight = 0.0;
	
	float create_union_op_weight = 0.4;
	float create_inter_op_weight = 0.3;
	float create_sub_op_weight = 0.3;
	
	float create_semi_binary_leaf_chance = 0.2;
	/// @}
	
	/// @see selector.d
	/// @{
	Selector selector;
	int selector_t = 5;
	/// @}
	
	/// @see mutator.d
	/// @{
	float mutation_probability = 0.3;
	
	float mutator_add_missing_geos_weight = 0.0;
	float mutator_add_all_geos_weight = 0.0;
	float mutator_flip_op_weight = 0.0;
	
	float mutation_flip_probability = 0.3;
	/// @}
}


public struct EvoStats {
	string type = "evo";
	
	struct Round {
		float[] scores;
		float cache_miss_rate;
		
		long select_parents_duration;
		long crossover_duration;
		long offspring_selection_duration;
		long reinsertion_duration;
		long final_sort_duration;
	}
	Round[] rounds;
}


public void check_validity(ref EvoParameters parameters) {
	if(parameters.population_size < 2)    { throw new Exception("population_size must be greater or equal two"); }
	if(parameters.population_size % 2)    { throw new Exception("population_size has to be a multiple of two"); }
	if(parameters.mating_rate < 0.0)      { throw new Exception("mating_rate must be greater zero"); }
	if(parameters.reinsertion_rate > 1.0) { throw new Exception("reinsertion_rate can't be greater one"); }
}


public CsgNode evo(NormalPoint[] reference, Geo[] primitives, Evaluator evaluator, EvoParameters parameters, out EvoStats stats) {
	// Handle trivial cases
	if(primitives.length == 0) return new UnionOp();
	if(primitives.length == 1) return primitives[0];
	
	// Preparations
	parameters.check_validity();
	if(parameters.reinsertion_rate < 0) {
		parameters.reinsertion_rate = min(parameters.mating_rate, 1.0);
	}
	auto reinsertion_cnt = max(2, roundTo!size_t(parameters.population_size * parameters.reinsertion_rate / 2) * 2);
	auto parent_cnt = max(2, roundTo!size_t(parameters.population_size * parameters.mating_rate / 2) * 2);
	
	evaluator.update_reference(reference);
	auto population = Population(parameters.population_size, evaluator);
	auto scope children = new RankedTree[parent_cnt];
	RankedTree[] selected_children;
	TreeCreator creator = {
		create_semi_binary_weight: parameters.creator_semi_binary_weight,
		create_linear_weight:      parameters.creator_linear_weight,
		
		union_op_weight:           parameters.create_union_op_weight,
		inter_op_weight:           parameters.create_inter_op_weight,
		sub_op_weight:             parameters.create_sub_op_weight,
		
		semi_binary_leaf_chance:   parameters.create_semi_binary_leaf_chance,
	};
	
	ParentSelector selector = {
		selector: parameters.selector,
		tournament_t: parameters.selector_t,
		parent_ids: new size_t[parent_cnt],
	};
	
	TreeMutator mutator = {
		mutation_probability:    parameters.mutation_probability,
		
		add_missing_geos_weight: parameters.mutator_add_missing_geos_weight,
		add_all_geos_weight:     parameters.mutator_add_all_geos_weight,
		flip_op_weight:          parameters.mutator_flip_op_weight,
		
		flip_probability:        parameters.mutation_flip_probability,
		
		creator:                 &creator,
	};
	
	assert(parameters.mating_rate >= 0.0);
	assert(parameters.population_size >= 2);
	assert(parameters.reinsertion_rate >= 0.0);
	assert(parameters.reinsertion_rate <= 1.0);
	assert(parent_cnt > 0);
	assert(parent_cnt % 2 == 0);
	
	// Create initial Population
	foreach(ref individual; population) {
		individual = population.create(creator.create(primitives));
	}
	population.finish_round();
	
	// Evolve
	StopWatch sw;
	foreach(i; 0..parameters.max_rounds) {
		EvoStats.Round round_stats;
		
		// #### Parent selection
		sw = fresh_timer();
		selector.update_selection(population);
		round_stats.select_parents_duration = sw.get_time();
		
		sw = fresh_timer();
		size_t j;
		foreach(couple_ids; selector.parent_ids.chunks(2)) {
			// Actually create offsprint
			if(
				parameters.crossover_rate == 1.0 ||
				parameters.crossover_rate > 0.0 && uniform01() < parameters.crossover_rate
			) {
				auto children_pair = crossover([population[couple_ids[0]].root, population[couple_ids[1]].root]);
				foreach(child; children_pair) {
					children[j++] = population.create(mutator.mutate(child, primitives));
				}
			}
			// Just take the parents on to the next round
			else {
				foreach(id; couple_ids) {
					children[j++] = population.create(population[id].root.copyAndCount());
				}
			}
			/// @todo mutation
			/// @todo fix trees?
		}
		round_stats.crossover_duration = sw.get_time();
		
		// #### Offspring selection
		sw = fresh_timer();
		if(children.length > reinsertion_cnt) {
			children.sort!"a.score < b.score";
			selected_children = children[0..reinsertion_cnt];
		}
		else {
			selected_children = children;
		}
		round_stats.offspring_selection_duration = sw.get_time();
		
		assert(selected_children.length == reinsertion_cnt);
		
		// #### Reinsertion
		sw = fresh_timer();
		population.reinsert(selected_children, selector.parent_ids, parameters.replacement_strategy);
		round_stats.reinsertion_duration = sw.get_time();
		
		// #### Cleanup and stats
		sw = fresh_timer();
		auto cached_delta = population.finish_round();
		round_stats.final_sort_duration = sw.get_time();
		
		auto miss_rate = cached_delta / (children.length * 2.0);
		
		/** @{ stats */
		round_stats.scores = population[].map!"a.geo_score".array;
		round_stats.cache_miss_rate = miss_rate;
		stats.rounds ~= round_stats;
		/** @} */
		
		infof(
			"Round %2s => Scores %.5f (best) - %.5f (worst) | Cache hit rate %s%% / miss rate %s%%",
			i+1, population.best.score, population.worst.score, (1.0 - miss_rate)*100, miss_rate*100
		);
		
		if(population.best.score == 0) {
			break;
		}
	}
	
	// Return Result
	return population.best.root;
}

/**
* Given two gen-donors (parents), this function creates two offsprings
* (children) by swapping parts of the parent genes (CSG subtrees).
*/
private CsgNode[2] crossover(CsgNode[2] parents) {
	CsgNode[2] offsprings, subtrees;
	
	foreach(i; 0..2) {
		size_t size;
		offsprings[i] = parents[i].copyAndCount(&size);
		subtrees[i] = offsprings[i].subtree(uniform(1, size)); /// @todo 0 .. size??
	}
	
	subtrees[0].swapWith(subtrees[1]);
	
	return offsprings;
}


package struct RankedTree {
	CsgNode root;
	float score;
	float geo_score;
	float height;
	float size;
	
	this(CsgNode root, float geo_score) {
		this.root = root;
		this.geo_score = this.score = geo_score;
		this.height = root.height;
		this.size = root.size;
	}
	
	void update_score(float max_height, float max_size) {
		float penalty = 0.0;
		
		// Height penalty
		// (-) Tree height > Any tree height of last round: 0 < penalty
		// (~) Tree height = highest tree of last round: penalty = 0
		// (+) Tree height < Any tree height of last round: -1 < penalty < 0
		penalty += height / max_height - 1.0;
		
		// Size penalty (tree node count):
		// (-) Tree size > Any tree size of last round: 0 < penalty
		// (~) Tree size = biggest tree of last round: penalty = 0
		// (+) Tree size < Any tree size of last round: -1 < penalty < 0
		penalty += size / max_size - 1.0;
		
		// Cap penalty at -1 (cap artificial improvement) so the score can't be
		// better than x times the actual geo score.  This also prevents the
		// score from getting better than best (< 0).  Don't cap the other way
		// round.  The score can get infinitely worse.  If the geo score is the
		// best possible (=0) the mangled score is also always 0.
		score = geo_score + geo_score*0.5*max(penalty, -1.0);
	}
}


private struct Population {
	private RankedTree[] population;
	private CachedEvaluator cached_evaluator;
	private float max_height;
	private float max_size;
	private size_t last_cache_size;
	
	
	this(size_t n, Evaluator evaluator) {
		population = new RankedTree[n];
		this.cached_evaluator = CachedEvaluator(evaluator);
	}
	
	RankedTree create(CsgNode root) {
		auto rt = RankedTree(root, cached_evaluator.evaluate(root));
		if(max_size > 0) {
			rt.update_score(max_height, max_size);
		}
		return rt;
	}
	
	size_t finish_round() {
		max_height = population.map!(x => x.height).maxElement;
		max_size = population.map!(x => x.size).maxElement;
		foreach(ref x; population) {
			x.update_score(max_height, max_size);
		}
		population.sort!"a.score < b.score";
		
		auto cache_delta = cached_evaluator.cache_size - last_cache_size;
		last_cache_size = cached_evaluator.cache_size;
		return cache_delta;
	}
	
	@property RankedTree best() pure {
		return population[0];
	}
	
	@property RankedTree worst() pure {
		return population[$-1];
	}
	
	alias population this;
}


private struct CachedEvaluator {
	Evaluator evaluator;
	float[uint] cache;
	
	this(Evaluator evaluator) {
		this.evaluator = evaluator;
	}
	
	float evaluate(CsgNode root) {
		float score;
		auto hash = root.hash;
		if(auto score_p = (hash in cache)) {
			return *score_p;
		}
		score = evaluator.evaluate_model(root);
		cache[hash] = score;
		return score;
	}
	
	size_t cache_size() {
		return cache.length;
	}
}


// version(unittest) {
// private:
// 	import fluent.asserts;
// 	import libcsgfitter.primitives;
// 	import libcsgfitter.surface;
// 	import libcsgfitter.reader.json2csg;
// 	import libcsgfitter.evaluator.e12;
// 	import libcsgfitter.reader.json;
// 	import std.stdio;
// }
// 
// unittest {
// 	auto csg = json2csg(read_json_file("tests/renderings/model3.json"));
// // 	auto csg = json2csg(read_json_file("tests/renderings/unit_cube.json"));
// 	auto primitives = csg.extract_primitives;
// 	auto reference = csg.surface_sample(1.5);
// // 	auto reference = csg.surface_sample(1000);
// 	
// 	auto evaluator_parameters = Evaluator12Parameters();
// 	auto evaluator = new Evaluator12(reference, &evaluator_parameters);
// 	writefln("Best score %s (based on %s samples) / %s", evaluator.evaluate(csg), reference.length, evaluator_parameters);
// 	
// 	auto parameters = EvoParameters();
// 	
// 	primitives.evo(evaluator, parameters);
// }
