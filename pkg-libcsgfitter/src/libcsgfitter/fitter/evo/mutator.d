// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module libcsgfitter.fitter.evo.mutator;

import generic.tree;
import libcsgfitter.csgtree;
import libcsgfitter.fitter.evo.creator;
import libcsgfitter.fitter.evo.helper;
import std.algorithm : sort;
import std.algorithm.iteration : sum;
import std.array : array;
import std.random : dice;
import std.random : uniform01;
import std.random : uniform;



// package enum Mutator : MutatorFn {
// 	add_missing_geos = &mutate_add_missing_geos,
// 	add_all_geos = &mutate_add_all_geos,
// 	flip_op = &mutate_flip_op,
// }
// 
// package alias MutatorFn = CsgNode function(CsgNode, Geo[]);

package struct TreeMutator {
	package float mutation_probability = 0.3;
	
	package float add_missing_geos_weight = 0.0;
	package float add_all_geos_weight = 0.0;
	package float flip_op_weight = 0.0;
	
	package float flip_probability = 0.3;
	
	package TreeCreator* creator;
	
	
	package CsgNode mutate(CsgNode root, Geo[] geos) {
		if(uniform01() > mutation_probability) {
			return root;
		}
		
		auto weights = [
			add_missing_geos_weight,
			add_all_geos_weight,
			flip_op_weight,
		];
		
		if(weights.sum == 0.0) {
			foreach(ref weight; weights) {
				weight = 1.0;
			}
		}
		
		switch(dice(weights)) {
			case 0: return mutate_add_missing_geos(root, geos);
			case 1: return mutate_add_all_geos(root, geos);
			case 2: return mutate_flip_op(root, geos);
			default: assert(false);
		}
	}
	
	
	// Deterministic only if geos contains no element with the same parameters and in debug mode
	private CsgNode mutate_add_missing_geos(CsgNode root, Geo[] geos) {
		auto subtree_id = uniform(0, root.size);
		if(subtree_id == 0) {
			return creator.create(geos);
		}
		
		Geo[uint] missing_nodes;
		foreach(ref geo; geos) {
			missing_nodes[geo.hash] = geo;
		}
		
		CsgNode subtree;
		scope auto visitor = new class CsgVisitor {
			private bool blind = false;
			
			override public void visitPreAll(CsgNode node) {
				if(subtree_id-- == 0) {
					subtree = node;
					blind = true;
				}
				
				if(blind) { return; }
				if(Geo geo = cast(Geo)node) {
					missing_nodes.remove(geo.hash);
				}
			}
			
			override public void visitPostAll(CsgNode node) {
				if(node == subtree) { blind = false; }
			}
		};
		visitor.walk(root);
		
		Geo[] nodes = missing_nodes.values;
		version(unittest) { nodes.sort!("a.hash < b.hash")(); }
		subtree.swapWith(creator.create(nodes));
		return root;
	}
	
	
	private CsgNode mutate_add_all_geos(CsgNode root, Geo[] geos) {
		auto subtree_id = uniform(0, root.size);
		if(subtree_id == 0) {
			return creator.create(geos);
		}
		
		auto subtree = root.subtree(subtree_id);
		subtree.swapWith(creator.create(geos));
		return root;
	}
	
	
	private CsgNode mutate_flip_op(CsgNode root, Geo[] geos) {
		// change op node types
		Op[] ops;
		
		scope auto visitor = new class CsgVisitor {
			private Op subtree = null;
			
			override public void visitPreOp(Op node) {
				if(subtree is null && uniform01() > flip_probability) {
					ops ~= node;
					subtree = node;
				}
			}
			
			override public void visitPostOp(Op node) {
				if(node == subtree) { subtree = null; }
			}
		};
		visitor.walk(root);
		
		foreach(old_op; ops) {
			auto new_op = creator.random_op();
			if(old_op.type == new_op.type) {
				continue;
			}
			
			foreach(child; old_op.children.array) {
				child.parent = new_op;
			}
			
			if(old_op == root) {
				root = new_op;
			}
			else {
				old_op.replaceWith(new_op);
			}
		}
		
		return root;
	}
	
	// Further ideas:
	//  * internal swap (!subtree of subtrees)
	//  * add missing geos to the root node (or somewhere else? mutate()?)
}



version(unittest) {
private:
	import fluent.asserts;
	import std.format;
	import xxhash : XXHash;
	import libcsgfitter.properties;
	import generic.util;
	import generic.tree;
	import std.random : rndGen;
	import std.random : Random;
	import std.stdio;
	
	class SimpleGeo : SphereGeo {
		int id;
		int copy;
		this(int id) { super(Coord(), id); this.id = id; }
		this(int id, int copy) { this(id); this.copy = copy; }
		override SphereGeo clone() { return new SimpleGeo(id, copy+1); }
		override public void acceptPre(T)(T visitor) { cast(SphereGeo) visitor.visitPre(this); }
		override public void acceptPost(T)(T visitor) { cast(SphereGeo) visitor.visitPost(this); }
		@property override public string toString() const { return "Geo %s #%s".format(id, copy); }
	}
	
	class SimpleOp : UnionOp {
		string id;
		int copy;
		this(string id) { super(); this.id = id; }
		this(string id, int copy) { this(id); this.copy = copy; }
		override UnionOp clone() { return new SimpleOp(id, copy+1); }
		override public void acceptPre(T)(T visitor) { cast(UnionOp) visitor.visitPre(this); }
		override public void acceptPost(T)(T visitor) { cast(UnionOp) visitor.visitPost(this); }
		@property override public string toString() const { return "Op %s #%s".format(id, copy); }
	}
	
	CsgNode sample_tree1(out Geo[] geos, int seed) {
		rndGen = Random(seed);
		
		auto ops = [new SimpleOp("1"), new SimpleOp("2"), new SimpleOp("3")];
		geos = [new SimpleGeo(1), new SimpleGeo(2), new SimpleGeo(3), new SimpleGeo(4)];
		
		// [#] Op 1 #0
		//  ├─[#] Op 2 #0
		//  │  ├───> Geo 1 #0
		//  │  └───> Geo 2 #0
		//  └───> Op 3 #0
		ops[0].appendChild(ops[1]);
		ops[0].appendChild(ops[2]);
		ops[1].appendChild(geos[0].clone());
		ops[1].appendChild(geos[1].clone());
		
		return ops[0];
	}
}

// Base root for the following tests:
@("mutate_add_missing_geos")
unittest {
	Geo[] geos;
	TreeMutator m;
	TreeCreator c;
	m.creator = &c;
	m.mutation_probability = 1.0;
	m.add_missing_geos_weight = 1.0;
// 	foreach(i; 0..20) {
// 		writefln("mutate_add_missing_geos (seed %s):\n%s\n", i, m.mutate(sample_tree1(geos, i), geos).pprinter.toString);
// 	}
	
	m.mutate(sample_tree1(geos, 0), geos).pprinter.toString.should.equal(`
		[#] Op 1 #0
		 ├─[#] Op 2 #0
		 │  ├───> Geo 1 #1
		 │  └─[#] SUBTRACT
		 │     ├─[#] INTERSECTION
		 │     │  ├───> Geo 2 #1
		 │     │  └───> Geo 3 #1
		 │     └───> Geo 4 #1
		 └───> Op 3 #0
	`.mls).because("replace geo (add 3 new)");
	
	m.mutate(sample_tree1(geos, 9), geos).pprinter.toString.should.equal(`
		[#] Op 1 #0
		 ├─[#] UNION
		 │  ├───> Geo 3 #1
		 │  └─[#] UNION
		 │     ├───> Geo 4 #1
		 │     └─[#] UNION
		 │        ├───> Geo 2 #1
		 │        └───> Geo 1 #1
		 └───> Op 3 #0
	`.mls).because("replace op (add 2 new)");
	
	m.mutate(sample_tree1(geos, 3), geos).pprinter.toString.should.equal(`
		[#] Op 1 #0
		 ├─[#] Op 2 #0
		 │  ├───> Geo 1 #1
		 │  └───> Geo 2 #1
		 └─[#] INTERSECTION
		    ├───> Geo 4 #1
		    └───> Geo 3 #1
	`.mls).because("replace op (add 2 new)");
	
	m.mutate(sample_tree1(geos, 2), geos).pprinter.toString.should.equal(`
		[#] INTERSECTION
		 ├─[#] UNION
		 │  ├───> Geo 3 #1
		 │  └───> Geo 1 #1
		 └─[#] UNION
		    ├───> Geo 4 #1
		    └───> Geo 2 #1
	`.mls).because("replace root (add 4 new)");
}

@("mutate_add_all_geos - replace geo")
unittest {
	Geo[] geos;
	TreeMutator m;
	TreeCreator c;
	m.creator = &c;
	m.mutation_probability = 1.0;
	m.add_all_geos_weight = 1.0;
// 	foreach(i; 0..20) {
// 		writefln("mutate_add_all_geos (seed %s):\n%s\n", i, m.mutate(sample_tree1(geos, i), geos).pprinter.toString);
// 	}
	
	m.mutate(sample_tree1(geos, 0), geos).pprinter.toString.should.equal(`
		[#] Op 1 #0
		 ├─[#] Op 2 #0
		 │  ├───> Geo 1 #1
		 │  └─[#] SUBTRACT
		 │     ├─[#] INTERSECTION
		 │     │  ├───> Geo 4 #1
		 │     │  └───> Geo 1 #1
		 │     └─[#] UNION
		 │        ├───> Geo 2 #1
		 │        └───> Geo 3 #1
		 └───> Op 3 #0
	`.mls).because("replace geo");
	
	m.mutate(sample_tree1(geos, 3), geos).pprinter.toString.should.equal(`
		[#] Op 1 #0
		 ├─[#] Op 2 #0
		 │  ├───> Geo 1 #1
		 │  └───> Geo 2 #1
		 └─[#] INTERSECTION
		    ├─[#] INTERSECTION
		    │  ├───> Geo 1 #1
		    │  └───> Geo 4 #1
		    └─[#] UNION
		       ├───> Geo 2 #1
		       └───> Geo 3 #1
	`.mls).because("replace op");
	
	m.mutate(sample_tree1(geos, 2), geos).pprinter.toString.should.equal(`
		[#] INTERSECTION
		 ├─[#] UNION
		 │  ├───> Geo 3 #1
		 │  └───> Geo 1 #1
		 └─[#] UNION
		    ├───> Geo 4 #1
		    └───> Geo 2 #1
	`.mls).because("replace root");
}

@("mutate_flip_op")
unittest {
	Geo[] geos;
	TreeMutator m;
	TreeCreator c;
	m.creator = &c;
	m.mutation_probability = 1.0;
	m.flip_op_weight = 1.0;
	m.flip_probability = 0.6;
// 	foreach(i; 0..20) {
// 		writefln("mutate_flip_op (seed %s):\n%s\n", i, m.mutate(sample_tree1(geos, i), geos).pprinter.toString);
// 	}
	
	m.mutate(sample_tree1(geos, 0), geos).pprinter.toString.should.equal(`
		[#] SUBTRACT
		 ├─[#] Op 2 #0
		 │  ├───> Geo 1 #1
		 │  └───> Geo 2 #1
		 └───> Op 3 #0
	`.mls).because("root");
	
	m.mutate(sample_tree1(geos, 2), geos).pprinter.toString.should.equal(`
		[#] Op 1 #0
		 ├─[#] SUBTRACT
		 │  ├───> Geo 1 #1
		 │  └───> Geo 2 #1
		 └───> Op 3 #0
	`.mls).because("inner op with children");
	
	m.mutate(sample_tree1(geos, 4), geos).pprinter.toString.should.equal(`
		[#] Op 1 #0
		 ├─[#] Op 2 #0
		 │  ├───> Geo 1 #1
		 │  └───> Geo 2 #1
		 └───> SUBTRACT
	`.mls).because("inner op without children");
	
	m.mutate(sample_tree1(geos, 6), geos).pprinter.toString.should.equal(`
		[#] Op 1 #0
		 ├─[#] Op 2 #0
		 │  ├───> Geo 1 #1
		 │  └───> Geo 2 #1
		 └───> Op 3 #0
	`.mls).because("no flip");
	
	m.mutate(sample_tree1(geos, 13), geos).pprinter.toString.should.equal(`
		[#] Op 1 #0
		 ├─[#] SUBTRACT
		 │  ├───> Geo 1 #1
		 │  └───> Geo 2 #1
		 └───> SUBTRACT
	`.mls).because("multiple flips");
}
