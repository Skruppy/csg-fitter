// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module libcsgfitter.fitter.part_evo;

import generic.util;
import generic.graph;
import generic.set;
import libcsgfitter.fitter.evo;
import libcsgfitter.csgtree;
import libcsgfitter.point;
import libcsgfitter.evaluator.base;
import libcsgfitter.tools.po_graph;
import std.container.array : Array;
import std.array : array;
import std.array : join;
import std.array : empty;
import std.algorithm.searching : any;
import std.range : chain;
import std.algorithm.iteration : map;
import std.algorithm.iteration : filter;
import std.container.dlist : DList;
import std.numeric : dotProduct;
import std.math : PI;
import std.format : format;
import std.conv : to;
import libcsgfitter.writer.gv;
import std.random : randomCover;
import std.datetime.stopwatch : StopWatch;
import std.datetime.stopwatch : AutoStart;



private NormalPoint[][Geo] group_by(NormalPoint[] points, Geo[] primitives) {
	Geo[uint] hash_geo;
	NormalPoint[][Geo] geo_points;
	
	foreach(primitive; primitives) {
		hash_geo[primitive.hash] = primitive;
		geo_points[primitive] = [];
	}
	
	foreach(ref point; points) {
		geo_points[hash_geo[point.origin]] ~= point;
	}
	
	return geo_points;
}


struct PrunedGeo {
	Geo removed;
	Geo neighbour;
}


public struct PartStats {
	string type = "part_evo";
	
	string graph_gv;
	
	long prune_duration;
	long disconnect_duration;
	long identify_duration;
	long reattach_duration;
	long merge_duration;
	
	struct Cluster {
		size_t primiteve_cnt;
		size_t point_cnt;
		long collect_points_durartion;
		long evo_duration;
		EvoStats evo;
	}
	Cluster[] clusters;
}


struct PartEngine {
	                               // this
	                               // |  prune
	                               // |  |  disconnect_prim_imps (+helpers)
	                               // |  |  |  identify_components
	                               // |  |  |  |  reattach_pruned
	                               // |  |  |  |  |  merge
	                               // |  |  |  |  |  |
	UndirectedGraph!Geo graph;     // w  m  m  r  -  -
	NormalPoint[][Geo] geo_points; // w  -  -  -  -  r
	Array!Geo*[Geo] geo_components;// -  -  -  w  m  -
	Array!Geo*[] components;       // -  -  -  w  -  r
	PrunedGeo[] pruned;            // -  w  -  -  r  -
	Set!Geo prim_imps;             // -  -  w  -  -  - (saved only for stats)
	string[Array!Geo*] component_colors;
	
	PartStats* stats;
	
	
	
	private this(Geo[] primitives, NormalPoint[] reference_points, ref PartStats stats) {
		graph = po_graph(primitives);
		geo_points = reference_points.group_by(primitives);
		this.stats = &stats;
		
		stats.graph_gv ~= "graph G {\n";
	}
	
	
	// PRUNE IRRELEVANT primitives
	private void prune() {
		auto sw = fresh_timer();
		
		Set!Geo candidates = Set!Geo(graph.nodes);
		Set!Geo next_candidates; // Set to remove duplicates
		
		// TODO repeat until fixpoint?
		while( ! candidates.empty) {
			foreach(candidate; candidates) {
				// May be the element was enqued before it was removed in the last round
				if( ! graph.contains_node(candidate)) { continue; }
				
				auto neighbours = graph.neighbours(candidate);
				if(neighbours.length != 1) { continue; }
				
				graph.remove_node(candidate);
				next_candidates ~= neighbours[0];
				pruned ~= PrunedGeo(candidate, neighbours[0]);
			}
			
			candidates = next_candidates;
			next_candidates = Set!Geo.init;
		}
		
		/** @{ stats */
		stats.prune_duration = sw.get_time();
		/** @} */
	}
	
	
	// ## Detect and DISCONNECT PRIME IMPLICANTS
	// Implikant: "und-Term" P einer Formel F, so das gild P => F (daher auf jeden fall mit union operation verknüpfbar)
	// Primimplikant: Term P ist minimal (bei uns immer der Fall, da einzelnes Primitiv)
	// Belibige Terme (nicht Primimplikanten), die sich nicht beeinflussen (unverbunden sind, also cluster) können immer mit einer union verbunden werden.
	private void disconnect_prim_imps() {
		/** @{ stats */
		stats.graph_gv ~= "\t\n\tedge [style=\"dotted\"]\n";
		/** @} */
		
		auto sw = fresh_timer();
		
		foreach(node; graph.nodes) {
			if(graph.neighbours(node).length == 0) {
				prim_imps ~= node;
			}
			else if(
				! has_inward_normals(node) &&
				! contains_neighbours(node)
			) {
				prim_imps ~= node;
				/** @{ stats */
				foreach(neighbour; graph.neighbours(node)) {
					stats.graph_gv ~= "\top_%x -- op_%x\n".format(cast(void *)node, cast(void *)neighbour);
				}
				/** @} */
				graph.remove_all_edges(node);
			}
		}
		
		/** @{ stats */
		stats.disconnect_duration = sw.get_time();
		/** @} */
	}
	
	
	private bool contains_neighbours(Geo geo) {
		return graph
		.neighbours(geo)
		.map!(neighbour => geo_points[neighbour])
		.join
		.map!( point => geo.contains_equal( geo.transformationToUnit*(point.position) ) )
		.any;
	}
	
	
	private bool has_inward_normals(Geo geo) {
		foreach(r_point; geo_points[geo]) {
			auto np = geo.nearestPoint(r_point.position);
			if(np.normal.triple.dotProduct(r_point.normal.triple) < 0) {
				return true;
			}
		}
		return false;
	}
	
	
	// ## IDENTIFY connected COMPONENTS
	private void identify_components() {
		auto sw = fresh_timer();
		
		auto uncolored = Set!Geo(graph.nodes);
		
		while( ! uncolored.empty) {
			auto component = new Array!Geo();
			components ~= component;
			
			auto geo_fifo = DList!Geo(uncolored.removeAny());
			while( ! geo_fifo.empty) {
				auto geo = geo_fifo.pop;
				
				*component ~= geo;
				geo_components[geo] = component;
				
				foreach(neighbour; graph.neighbours(geo)) {
					if( ! uncolored.contains(neighbour)) { continue; }
					uncolored.remove(neighbour);
					geo_fifo ~= neighbour;
				}
			}
		}
		
		/** @{ stats */
		stats.identify_duration = sw.get_time();
		/** @} */
	}
	
	
	// ## REATTACH PRUNDED primitives
	private void reattach_pruned() {
		/** @{ stats */
		stats.graph_gv ~= "\t\n\tedge [style=\"bold\"]\n";
		Set!Geo visited;
		foreach(node; graph.nodes) {
			foreach(neighbour; graph.neighbours(node)) {
				if(visited.contains(neighbour)) { continue; }
				stats.graph_gv ~= "\top_%x -- op_%x\n".format(cast(void *)node, cast(void *)neighbour);
			}
			visited ~= node;
		}
		
		stats.graph_gv ~= "\t\n\tedge [style=\"\"]\n";
		foreach(p; pruned) {
			stats.graph_gv ~= "\top_%x -- op_%x\n".format(cast(void *)p.removed, cast(void *)p.neighbour);
		}
		/** @} */
		
		auto sw = fresh_timer();
		foreach_reverse(p; pruned) {
			if(auto component = p.neighbour in geo_components) {
				(**component) ~= p.removed;             // Add node to component
				geo_components[p.removed] = *component; // Map component to node
			}
			else assert(false);
		}
		
		/** @{ stats */
		// Record timings
		stats.reattach_duration = sw.get_time();
		
		// Generate cluster color palet for dot graph nodes
		auto colors = rainbow(components.length, 87, 18).randomCover;
		foreach(component; components) {
			component_colors[component] = "%(%02x%)".format(colors.pop());
		}
		
		// Add prime implicants and normal primitives (but not pruned ones) to dot graph
		foreach(node; graph.nodes) {
			stats_add_graph_node(node);
		}
		
		// Add pruned primitives to dot graph
		foreach(p; pruned) {
			stats_add_graph_node(p.removed, true);
		}
		/** @} */
	}
	
	
	// ## MERGE COMPONENTS
	private CsgNode merge(Evaluator evaluator, EvoParameters parameters) {
		stats.clusters = new PartStats.Cluster[components.length];
		
		auto sw = fresh_timer();
		auto sub_trees = new CsgNode[components.length];
		foreach(i, component; components) {
			auto sw2 = fresh_timer();
			auto component_reference = (*component)[].map!(geo => geo_points[geo]).join.array;
			stats.clusters[i].collect_points_durartion = sw2.get_time();
			
			sw2 = fresh_timer();
			sub_trees[i] = evo(component_reference, (*component).array, evaluator, parameters, stats.clusters[i].evo);
			stats.clusters[i].evo_duration = sw2.get_time();
			
			/** @{ stats */
			stats.clusters[i].primiteve_cnt = component.length;
			stats.clusters[i].point_cnt = component_reference.length;
			/** @} */
		}
		
		/** @{ stats */
		stats.merge_duration = sw.get_time();
		stats.graph_gv ~= "}\n";
		/** @} */
		
		return new UnionOp(sub_trees);
	}
	
	// ## Stats
	private void stats_add_graph_node(Geo geo, bool pruned=false) {
		GvRenderer renderer;
		
		// Set style
		renderer.style_extras = ", fontname=\"sans\", fillcolor=\"#%s\"".format(component_colors[geo_components[geo]]);
		if(pruned) {
			renderer.style_extras ~= ", style=\"filled\"";
		}
		else if(prim_imps.contains(geo)) {
			renderer.style_extras ~= ", style=\"filled,bold\", color=\"red\"";
		}
		else {
			renderer.style_extras ~= ", style=\"filled,bold\"";
		}
		
		// Render
		if(auto speci_geo = cast(CubeGeo)geo) {
			stats.graph_gv ~= renderer.render(speci_geo);
		}
		else if(auto speci_geo = cast(CylinderGeo)geo) {
			stats.graph_gv ~= renderer.render(speci_geo);
		}
		else if(auto speci_geo = cast(SphereGeo)geo) {
			stats.graph_gv ~= renderer.render(speci_geo);
		}
	}
}


public CsgNode part_evo(NormalPoint[] reference_points, Geo[] primitives, Evaluator evaluator, EvoParameters parameters, out PartStats stats) {
	if(primitives.empty) {
		return new UnionOp();
	}
	auto engine = PartEngine(primitives.map!(a => cast(Geo)a.clone).array, reference_points, stats);
	engine.prune();
	engine.disconnect_prim_imps();
	engine.identify_components();
	engine.reattach_pruned();
	return engine.merge(evaluator, parameters);
}



version(unittest) {
private:
	import fluent.asserts;
	import libcsgfitter.properties;
	import std.range : iota;
	import std.algorithm.sorting : sort;
	import std.experimental.logger : globalLogLevel;
	import std.experimental.logger : LogLevel;
	
	shared static this() {
		globalLogLevel = LogLevel.warning;
	}
}

@("this & group_by")
unittest {
	import std.random : randomCover;
	
	//     C
	//    / \
	//   B---A---E   F
	//    \ /
	//     D
	Geo A = new SphereGeo(Coord( 1, 0, 0), 1.5);
	Geo B = new SphereGeo(Coord(-1, 0, 0), 1.5);
	Geo C = new SphereGeo(Coord( 0,  2, 0), 1.5);
	Geo D = new SphereGeo(Coord( 0, -2, 0), 1.5);
	Geo E = new SphereGeo(Coord( 3, 0, 0), 1.5);
	Geo F = new SphereGeo(Coord( 7, 0, 0), 1.5);
	Geo[] geos = [A, B, C, D, E, F];
	
	auto all_points = geos.map!"a.surfaceSample(1)".join.randomCover.array;
	
	PartStats stats;
	auto e = PartEngine(geos, all_points, stats);
	
	e.graph.nodes.should.containOnly(geos);
	e.graph.neighbours(A).should.containOnly([B, C, D, E]);
	e.graph.neighbours(B).should.containOnly([A, C, D]);
	e.graph.neighbours(C).should.containOnly([A, B]);
	e.graph.neighbours(D).should.containOnly([A, B]);
	e.graph.neighbours(E).should.containOnly([A]);
	e.graph.neighbours(F).should.containOnly([]);
	
	e.geo_points.keys.should.containOnly(geos);
	foreach(geo, points; e.geo_points) {
		immutable hash = geo.hash;
		points.length.should.equal(28); // 4 * pi * 1.5^2
		foreach(point; points) {
			point.origin.should.equal(hash);
		}
	}
	
	e.pruned.length.should.equal(0);
	e.components.length.should.equal(0);
}

@("prune")
unittest {
	PartStats stats;
	PartEngine e;
	e.stats = &stats;
	Geo[] g = iota(8).map!(x => cast(Geo)new SphereGeo(Coord(), x)).array;
	
	// 0  1 (Single emelemts)
	e.graph = UndirectedGraph!Geo(g[0..2]);
	e.prune();
	e.graph.nodes.should.containOnly(g[0..2]);
	e.pruned.length.should.equal(0);
	
	// 0--1 (Even node count)
	e.graph = UndirectedGraph!Geo(g[0..2]);
	e.graph.add_edge(g[0], g[1]);
	e.prune();
	e.graph.node_count.should.equal(1);
	e.pruned.length.should.equal(1);
	
	// 0--1--2 (Uneven node count, only one from each side)
	e = PartEngine.init;
	e.stats = &stats;
	e.graph = UndirectedGraph!Geo(g[0..3]);
	e.graph.add_edge(g[0], g[1]); e.graph.add_edge(g[1], g[2]);
	e.prune();
	e.graph.node_count.should.equal(1);
	e.pruned.length.should.equal(2);
	
	// 0--1--2--3--4 (Uneven node count, multiple from each side)
	e = PartEngine.init;
	e.stats = &stats;
	e.graph = UndirectedGraph!Geo(g[0..5]);
	e.graph.add_edge(g[0], g[1]); e.graph.add_edge(g[1], g[2]); e.graph.add_edge(g[2], g[3]); e.graph.add_edge(g[3], g[4]);
	e.prune();
	e.graph.node_count.should.equal(1);
	e.pruned.length.should.equal(4);
	
	// 0-\
	// |  2--3--4--5--6 (Right order)
	// 1-/
	e = PartEngine.init;
	e.stats = &stats;
	e.graph = UndirectedGraph!Geo(g[0..7]);
	e.graph.add_edge(g[0], g[1]); e.graph.add_edge(g[0], g[2]); e.graph.add_edge(g[1], g[2]); e.graph.add_edge(g[2], g[3]); e.graph.add_edge(g[3], g[4]); e.graph.add_edge(g[4], g[5]); e.graph.add_edge(g[5], g[6]);
	e.prune();
	e.graph.nodes.should.containOnly(g[0..3]);
	e.pruned.should.equal([PrunedGeo(g[6], g[5]), PrunedGeo(g[5], g[4]), PrunedGeo(g[4], g[3]), PrunedGeo(g[3], g[2])]);
	
	//     /-2-\
	// 0--1     4--5--6  7 (Complex)
	//     \-3-/
	e = PartEngine.init;
	e.stats = &stats;
	e.graph = UndirectedGraph!Geo(g[0..8]);
	e.graph.add_edge(g[0], g[1]); e.graph.add_edge(g[1], g[2]); e.graph.add_edge(g[1], g[3]); e.graph.add_edge(g[2], g[4]); e.graph.add_edge(g[3], g[4]); e.graph.add_edge(g[4], g[5]); e.graph.add_edge(g[5], g[6]);
	e.prune();
	e.graph.nodes.should.containOnly([g[1], g[2], g[3], g[4], g[7]]);
	e.pruned.should.containOnly([PrunedGeo(g[0], g[1]), PrunedGeo(g[6], g[5]), PrunedGeo(g[5], g[4])]);
}

@("disconnect_prim_imps")
unittest {
	import libcsgfitter.tools.primitives;
	import libcsgfitter.sampler.surface;
	import std.conv;
	
	Geo A, B, C;
	auto root = new UnionOp([
		new SubOp([
			B = new SphereGeo(Coord(0, 0, 0), 0.8),
			A = new SphereGeo(Coord(-1, 0, 0), 0.8),
		]),
		C = new SphereGeo(Coord(1, 0, 0), 0.8),
	]);
	
	auto geos = [A, B, C];
	auto points = root.surface_sample(3);
	PartStats stats;
	auto e = PartEngine(geos, points, stats);
	e.graph.nodes.should.containOnly([A, B, C]).because(e.graph.to!string);
	e.graph.neighbours(A).should.containOnly([B]).because(e.graph.to!string);
	e.graph.neighbours(B).should.containOnly([A, C]).because(e.graph.to!string);
	e.graph.neighbours(C).should.containOnly([B]).because(e.graph.to!string);
	
	e.disconnect_prim_imps();
	e.graph.nodes.should.containOnly([A, B, C]).because(e.graph.to!string);
	e.graph.neighbours(A).should.containOnly([B]).because(e.graph.to!string);
	e.graph.neighbours(B).should.containOnly([A]).because(e.graph.to!string);
	e.graph.neighbours(C).should.containOnly([]).because(e.graph.to!string);
}

@("identify_components")
unittest {
	import std.array;
	PartStats stats;
	PartEngine e;
	e.stats = &stats;
	Geo[] g = iota(12).map!(x => cast(Geo)new SphereGeo(Coord(), x)).array;
	
	//     /-2-\             /-9-\
	// 0--1     4--5--6  7  8--+--10
	//     \-3-/             \-11/
	e.graph = UndirectedGraph!Geo(g);
	e.graph.add_edge(g[0], g[1]); e.graph.add_edge(g[1], g[2]); e.graph.add_edge(g[1], g[3]); e.graph.add_edge(g[2], g[4]); e.graph.add_edge(g[3], g[4]); e.graph.add_edge(g[4], g[5]); e.graph.add_edge(g[5], g[6]);
	e.graph.add_edge(g[8], g[9]); e.graph.add_edge(g[8], g[10]); e.graph.add_edge(g[8], g[11]); e.graph.add_edge(g[9], g[11]); e.graph.add_edge(g[10], g[9]); e.graph.add_edge(g[10], g[11]);
	
	e.identify_components();
	e.components.length.should.equal(3);
	e.components.sort!"a.length < b.length";
	e.components[0].array.should.containOnly([g[7]]);
	e.components[1].array.should.containOnly(g[8..12]);
	e.components[2].array.should.containOnly(g[0..7]);
}

@("reattach_pruned")
unittest {
	PartStats stats;
	PartEngine e;
	e.stats = &stats;
	Geo[] g = iota(11).map!(x => cast(Geo)new SphereGeo(Coord(), x)).array;
	
	//      /-2-\
	// (0)-1     4-(5)(6)  7  8--9-(10)
	//      \-3-/
	e.graph = UndirectedGraph!Geo([g[1], g[2], g[3], g[4], g[7], g[8], g[9]]);
	e.graph.add_edge(g[1], g[2]); e.graph.add_edge(g[1], g[3]); e.graph.add_edge(g[2], g[4]); e.graph.add_edge(g[3], g[4]);
	e.graph.add_edge(g[8], g[9]);
	e.identify_components();
	e.components.length.should.equal(3);
	
	e.pruned = [
		PrunedGeo(g[0], g[1]),
		PrunedGeo(g[6], g[5]),
		PrunedGeo(g[5], g[4]),
		PrunedGeo(g[10], g[9]),
	];
	e.reattach_pruned();
	e.components.length.should.equal(3);
	e.components.sort!"a.length < b.length";
	e.components[0].array.should.containOnly([g[7]]);
	e.components[1].array.should.containOnly(g[8..11]);
	e.components[2].array.should.containOnly(g[0..7]);
}

@("model")
unittest {
	import libcsgfitter.evaluator.e12;
	import libcsgfitter.reader.json;
	import libcsgfitter.writer.scad;
	import libcsgfitter.tools.primitives;
	import libcsgfitter.sampler.surface;
	import generic.testfiles;
	
	auto csg = read_json_file("tests/renderings/gecco19.json");
	auto reference = csg.surface_sample(1.5);
	auto primitives = csg.extract_primitives;
	
	auto evaluator_parameters = Evaluator12Parameters();
	
	auto parameters = EvoParameters();
	parameters.max_rounds = 10;
	parameters.population_size = 100;
	auto evaluator = new Evaluator12(&evaluator_parameters);
	
	PartStats stats;
	auto model = reference.part_evo(primitives, evaluator, parameters, stats);
	model.write_scad(ScadProperties(test_path("libcsgfitter-fitter-part_evo.scad")));
}

