// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module libcsgfitter.point;

import libcsgfitter.properties;
import libcsgfitter.csgtree;
import std.format;



enum bool isAnyPoint(C) = isSimplePoint!C || isNormalPoint!C;
enum bool isSimplePoint(C) = is(C == Point);
enum bool isNormalPoint(C) = is(C == NormalPoint);


struct Point {
	Coord position;
	uint origin;
	
	public this(double x, double y, double z, uint origin) pure {
		this.position.x = x;
		this.position.y = y;
		this.position.z = z;
		this.origin = origin;
	}
	
	public this(double[] l, uint origin) pure {
		assert(l.length == 3);
		this(l[0], l[1], l[2], origin);
	}
	
	public string toString() const {
		return "Point( %g | %g | %g )".format(position.x, position.y, position.z, origin);
	}
}


struct NormalPoint {
	Coord position;
	Coord normal;
	uint origin;
	
	public this(double x, double y, double z, double nx, double ny, double nz, uint origin) pure {
		this.position.x = x;
		this.position.y = y;
		this.position.z = z;
		this.normal.x = nx;
		this.normal.y = ny;
		this.normal.z = nz;
		this.origin = origin;
	}
	
	public this(double[] l, uint origin) pure {
		assert(l.length == 6);
		this(l[0], l[1], l[2], l[3], l[4], l[5], origin);
	}
	
	public string toString() const {
		return "NormalPoint( %g | %g | %g || %g | %g | %g )".format(position.x, position.y, position.z, normal.x, normal.y, normal.z);
	}
	
	@property public auto inverted() pure {
		return  NormalPoint(position.x, position.y, position.z, -normal.x, -normal.y, -normal.z, origin);
	}
}



version(unittest) {
private:
	import fluent.asserts;
}

@("Coord indexed access")
unittest {
	Geo  g = new SphereGeo(Coord(), 1);
	Point p = Point(10.0, 20.0, 30.0, g.hash);
	p.position.triple[0].should.equal(10.0);
	p.position.triple[1].should.equal(20.0);
	p.position.triple[2].should.equal(30.0);
	p.origin.should.equal(g.hash);
}
