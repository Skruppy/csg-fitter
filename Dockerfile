FROM debian:bullseye-slim

## For the current compiler versions see:
##  * https://dlang.org/changelog/
##  * https://github.com/ldc-developers/ldc/releases
## 
## Final packages:
##  * `git`: Needed by the build script to get the current version number.
##  * `gcc`: Needed by *dmd* to link.
##  * `libz-dev`:  Needed during D linking by *collect2*.
##  * `libssl-dev`: Required until dub fixes https://github.com/dlang/dub/pull/1148
##  * `openssl`: Required until dub fixes https://github.com/dlang/dub/pull/1148
##  * `python3`: Required by some helper scripts (e.g. memtime)
##  * `python3-pip`: Required by some helper scripts (e.g. render)
##  * `python3-setuptools`: Required by pip to install mako
##  * `python3-wheel`: Required by pip to install pyyaml
##  * `libxml2`: Required by ldc2
##  * `graphviz`: Required by csgfitter to render the CSG tree
## 
## Build time packages:
##  * `curl`: Required by dlang's install script.
##  * `xz-utils`: Required by dlang's install script.
##  * `gnupg`: Required by dlang's install script.

ENV \
	DMD_VERSION=2.096.1 \
	LDC_VERSION=1.24.0 \
	_PACKAGES="git gcc libz-dev libssl-dev openssl python3 python3-pip python3-setuptools python3-wheel libxml2 graphviz" \
	_DEV_PACKAGES="curl xz-utils gnupg"

RUN \
	set -ex && \
	apt-get update && \
	apt-get install -y --no-install-recommends ${_PACKAGES} ${_DEV_PACKAGES} && \
	ln -s /usr/bin/python3 /usr/bin/python && \
	mkdir /opt/dlang && \
	curl -fsS -o /opt/dlang/install.sh https://dlang.org/install.sh && \
	bash /opt/dlang/install.sh -p /opt/dlang dmd-${DMD_VERSION} && \
	bash /opt/dlang/install.sh -p /opt/dlang ldc-${LDC_VERSION} && \
	rm -r \
		/opt/dlang/d-keyring.gpg \
		/opt/dlang/install.sh \
		/opt/dlang/dmd-*/README.TXT \
		/opt/dlang/dmd-*/activate.fish \
		/opt/dlang/dmd-*/html \
		/opt/dlang/dmd-*/license.txt \
		/opt/dlang/dmd-*/man \
		/opt/dlang/dmd-*/samples \
		/opt/dlang/dmd-*/linux/*32 \
		/opt/dlang/dmd-*/linux/bin64/README.TXT \
		/opt/dlang/ldc-*/LICENSE \
		/opt/dlang/ldc-*/README \
		/opt/dlang/ldc-*/activate.fish \
		/opt/dlang/ldc-*/etc/bash_completion.d \
		/opt/dlang/ldc-*/lib32 \
		/root/.gnupg && \
	chown -R root:root /opt/dlang && \
	chmod 755 /opt/dlang/* && \
	curl -sSL https://github.com/python-poetry/poetry/raw/1.2.0a2/install-poetry.py | \
	POETRY_HOME=/usr/local/ python3 - --version 1.1.8 && \
	apt-get purge -y --auto-remove ${_DEV_PACKAGES} && \
	rm -rf /var/lib/apt/lists/*

ENV PATH=/opt/dlang/dmd-${DMD_VERSION}/linux/bin64/:/opt/dlang/ldc-${LDC_VERSION}/bin/:${PATH}
ENV USER=ci
ENV HOME="/home/${USER}"
RUN useradd -m "${USER}"
USER $USER
WORKDIR $HOME
RUN \
	set -ex && \
	poetry config virtualenvs.create false && \
	poetry config installer.parallel false
