#!/usr/bin/env python3
## This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0

import util
import argparse
import math
import numpy as np
import os
import os.path as op
import sys
import textwrap
import scipy.optimize
import scipy.stats
import matplotlib
import matplotlib.pyplot as plt



## ==== Main
def main():
	## Get configuration from command line arguments
	class Formatter(argparse.ArgumentDefaultsHelpFormatter, argparse.RawDescriptionHelpFormatter):
		pass
	
	parser = argparse.ArgumentParser(
		formatter_class=Formatter,
		epilog=textwrap.dedent("""
		Examples:
		$ test_evo tests/renderings/gecco19.json 1
		$ test_evo -e population_size=200 mating_rate=0.25 reinsertion_rate=-1 creator=random selector=rnd_prop replacement_strategy=worst_parents_first max_rounds=100 -- tests/renderings/gecco19.json 1
		""")
	)
	
	default_path = '/tmp/csgfitter_evo_stats'
	parser.add_argument('input', help='Path to JSON file containing CSG model.')
	parser.add_argument('rate', type=int, help='Sample rate of surface points on model.')
	parser.add_argument('-p', '--path', default=default_path, help='Path where to save the results.')
	parser.add_argument('-l', '--load', nargs='?', default=None, const='', help='Don\'t run csgfitter. Load old resuts from PATH.')
	parser.add_argument('-f', '--force', action='store_true', help='Force overwriting the stats file.')
	parser.add_argument('-e', '--evo', nargs='+', default=[], help='csgfitter evo block parameters.')
	parser.add_argument('-E', '--evaluator', nargs='+', default=['evaluator_12'], help='csgfitter aguments describing the evaluator block.')
	parser.add_argument('-F', '--format', nargs='+', default=['png'], help='Filetypes used for figures (e.g. "png" or "pdf") or "gui" for an interactive gui.')
	parser.add_argument('--forced-figures', nargs='+', default=[], help='Figures which have to be redrawn.')
	parser.add_argument('--forced-params', nargs='+', default=[], help='Parameters which have to be recalculated.')
	parser.add_argument('-v', '--verbose', action='store_true', help='Print also non error messages.')
	args = parser.parse_args()
	
	if args.load is None or args.load == '':
		stats_path = op.join(args.path, 'data.json')
	else:
		stats_path = args.load
	
	## Aquire stats
	if args.load is None:
		if not op.isdir(args.path):
			os.mkdir(args.path)
		elif args.path != default_path and not args.force:
			sys.exit(f'Stats directory "{args.path}" already exists. May the --force be with you.')
		
		csgfitter_path = op.join(util.script_dir(), '..', 'csgfitter')
		util.sh(
			'memtime', csgfitter_path,
			'-s', stats_path,
			*(['-v'] if args.verbose else []),
			'json_reader', f'path={args.input}', 'name=csg',
			'csg.', '!', 'surface_sampler', f'rate={args.rate}', 'name=points', '!', 'fitter.reference',
			'csg.', '!', 'extract_primitives', '!', 'fitter.primitives',
			*args.evaluator, '!', 'fitter.evaluator',
			'evo', *args.evo, 'name=fitter',
			'fitter.', '!', 'json_writer', f'path={op.join(args.path, "result.json")}',
			'fitter.', '!', 'scad_writer', f'path={op.join(args.path, "result.scad")}',
			'fitter.', '!', 'gv_writer', f'path={op.join(args.path, "result.pdf")}',
			'fitter.', '!', 'gv_writer', f'path={op.join(args.path, "result.gv")}',
			'points.', '!', '.npc', 'pcd_writer', f'path={op.join(args.path, "points.pcd")}',
		)
	
	elif not op.isfile(stats_path):
		sys.exit(f'No stats in {args.path}')
	
	analyze(stats_path, args.path, args.format, set(args.forced_params), set(args.forced_figures))


def round_params(di):
	params = {}
	
	for x in [49, 99, 149]:
		params[str(x)] = len(di.round_scores) < x
	
	if di.round_scores[-1][0] == 0.0:
		params['round'] = len(di.round_scores)
	else:
		params['round'] = None
	
	return params


def analyze(stats_path, output_path, formats, forced_params=set(), forced_figs=set()):
	di = util.Di()
	
	aggregated_stats_path = op.join(output_path, 'stats.json')
	aggregated_stats = util.read_json(aggregated_stats_path) if op.isfile(aggregated_stats_path) else {}
	
	di.add_factory('stats', lambda di: util.read_json(stats_path))
	di.add_factory('round_scores', lambda di: [sorted(x['scores']) for x in di.stats['processors']['fitter']['stats']['rounds']])
	di.add_factory('rounds_params', lambda di: util.Dobject(
		max=int(di.stats['processors']['fitter']['properties']['max_rounds']),
		actual=len(di.round_scores),
	))
	di.add_factory('unique',            lambda di: np.fromiter(map(lambda x: len(set(x)) / len(x), di.round_scores), float))
	di.add_factory('skew',              lambda di: np.fromiter(map(scipy.stats.skew, di.round_scores), float))
	
	di.add_factory('foo', lambda di: ra(di.stats['processors']['fitter']['properties']))
	
	## Parameters, written to stats.json
	def di_load_cached(name, fn):
		short_name = name[:-7]
		if short_name not in aggregated_stats or short_name in forced_params:
			## If the data is missing or forced to recalculate: add the factory
			di.add_factory(name, lambda di: fn(di))
		else:
			## else: add the instance of the old (cached) data
			di.add_factory(name, lambda di: util.dobjectify(aggregated_stats[short_name]))
	
	di_load_cached('uniqueness_params',          lambda di: util.Dobject(mean=np.mean(di.unique)))
	di_load_cached('skew_params',                lambda di: util.Dobject(mean=np.mean(di.skew), std=np.std(di.skew)))
	di_load_cached('fit_ab_all_med_params',      lambda di: fit_ab(di.round_scores, 'fit_ab_all_med',     lambda x: x[int(len(x)/2)]))
	di_load_cached('fit_ab_all_avg_params',      lambda di: fit_ab(di.round_scores, 'fit_ab_all_avg',     np.mean))
	di_load_cached('fit_ab_all_max_params',      lambda di: fit_ab(di.round_scores, 'fit_ab_all_max',     lambda x: x[0])) ## Former model_3
	di_load_cached('fit_ab_parents_med_params',  lambda di: fit_ab(di.round_scores, 'fit_ab_parents_med', lambda x: x[di.foo['med']])) ## Former model_1
	di_load_cached('fit_abc_parents_med_params', lambda di: fit_abc(di.round_scores, di.foo)) ## Former model_2
	di_load_cached('fit_ab_parants_avg_params',  lambda di: fit_ab(di.round_scores, 'fit_ab_parants_avg', lambda x: np.average(x, weights=di.foo['weights'])))
	di_load_cached('best_score_params',          lambda di: {x: (di.round_scores[x][0] if x < len(di.round_scores) else 0.0) for x in [49, 99, 149]})
	#di_load_cached('finish_params',     round_params)
	
	## Plots, storred in output_path
	di.add_factory('raw_overview_figure',       lambda di: draw_raw_data_overview(di))
	di.add_factory('raw_points_figure',         lambda di: draw_raw_data_points(di))
	di.add_factory('ranks_figure',              lambda di: draw_ranks(di))
	di.add_factory('fit_ab_all_med_figure',     lambda di: draw_fit(di.foo, di, di.fit_ab_all_med_params, 'Med. score of whole pop.', lambda x: x[int(len(x)/2)]))
	di.add_factory('fit_ab_all_avg_figure',     lambda di: draw_fit(di.foo, di, di.fit_ab_all_avg_params, 'Avg. score of whole pop.', np.mean))
	di.add_factory('fit_ab_all_max_figure',     lambda di: draw_fit(di.foo, di, di.fit_ab_all_max_params, 'Best score of whole pop.', lambda x: x[0])) ## former model3
	di.add_factory('fit_ab_parents_med_figure', lambda di: draw_fit(di.foo, di, di.fit_ab_parents_med_params, 'Med. of sampled parent\'s scores', lambda x: x[di.foo['med']])) ## former model2
	di.add_factory('fit_abc_parents_med_figure',lambda di: draw_fit(di.foo, di, di.fit_abc_parents_med_params, 'Med. of sampled parent\'s scores', lambda x: x[di.foo['med']])) ## former model1
	di.add_factory('fit_ab_parants_avg_figure', lambda di: draw_fit(di.foo, di, di.fit_ab_parants_avg_params, 'Avg. of sampled parent\'s scores', lambda x: np.average(x, weights=di.foo['weights'])))
	di.add_factory('uniqueness_figure',         lambda di: draw_unique(di))
	di.add_factory('skew_figure',               lambda di: draw_skew(di))
	
	## Save/Show plots
	for fig_name in ['raw_overview', 'raw_points', 'ranks', 'fit_ab_all_med', 'fit_ab_all_avg', 'fit_ab_all_max', 'fit_ab_parents_med', 'fit_abc_parents_med', 'fit_ab_parants_avg', 'uniqueness', 'skew']:
		for ext in formats:
			if ext == 'gui':
				if not forced_figs or fig_name in forced_figs:
					matplotlib.pyplot.show(block=di[f'{fig_name}_figure'])
			else:
				fig_path = op.join(output_path, f'figure-{fig_name}.{ext}')
				if not op.isfile(fig_path) or fig_name in forced_figs:
					di[f'{fig_name}_figure'].savefig(fig_path, bbox_inches='tight', pad_inches=0.03)
	
	## Update JSON
	updated = False
	for key in ['fit_ab_all_med', 'fit_ab_all_avg', 'fit_ab_all_max', 'fit_ab_parents_med', 'fit_abc_parents_med', 'fit_ab_parants_avg', 'skew', 'uniqueness', 'rounds', 'best_score']:#, 'did_finish', 'finish']:
		## * Update keys, which have already been generated (don't waste them)
		## * Add missing keys
		## * Overwrite keys, requested to be regenerated
		if di.is_instanciated(key+'_params') or key not in aggregated_stats or key in forced_params:
			aggregated_stats[key] = di[key+'_params']
			updated = True
	
	if updated:
		util.write_json(aggregated_stats, aggregated_stats_path)
	
	return aggregated_stats



## ==== Draw figures
def draw_raw_data_overview(di):
	fig = plt.figure(figsize=(16, 6))
	ax = fig.add_subplot()
	plot_boxplot(ax, di)
	
	return fig


def draw_raw_data_points(di):
	## Split Scores into four subgraphs of the same size, each showing every
	## second round.
	spacing = 2
	subgraph_cnt = 4
	violin_cnt = math.ceil(di.rounds_params.max/spacing)
	violin_per_plot_cnt = math.ceil(violin_cnt/subgraph_cnt)
	groups = [{
		'start_idx': 1 + group_id * violin_per_plot_cnt * spacing,
		'end_idx': 1 + ((group_id+1) * violin_per_plot_cnt - 1) * spacing,
	} for group_id in range(subgraph_cnt)]
	
	fig = plt.figure(constrained_layout=True, figsize=(12, 8))
	gs = fig.add_gridspec(2, 2)
	for group_id, grid in enumerate([gs[0, 0], gs[0, 1], gs[1, 0], gs[1, 1]]):
		plot_violinplot(fig.add_subplot(grid), di, groups, group_id)
	
	return fig


def draw_ranks(di):
	selected_round_ids = select_rounds(di.rounds_params)
	population_size = len(di.round_scores[0])
	slices = {
		1: 'Best score',
		1 + int(population_size / 8 * 1): 'Score at 1/8',
		1 + int(population_size / 8 * 3): 'Score at 3/8',
		1 + int(population_size / 8 * 5): 'Score at 5/8',
		1 + int(population_size / 8 * 7): 'Score at 7/8',
	}
	
	fig = plt.figure(constrained_layout=True, figsize=(14, 8))
	gs = fig.add_gridspec(2, 2)
	plot_abs_rank_changes(fig.add_subplot(gs[0, 0]), di, selected_round_ids, slices)
	plot_rel_rank_changes(fig.add_subplot(gs[1, 0]), di, selected_round_ids, slices)
	plot_rank(fig.add_subplot(gs[0, 1]), di, slices)
	
	return fig


def draw_fit(foo, di, model_params, name, round_evaluator):
	fig = plt.figure(figsize=(10, 6))
	ax = fig.add_subplot()
	plot_fit(ax, foo, di, model_params, name, round_evaluator)
	
	return fig


def draw_unique(di):
	fig = plt.figure(figsize=(10, 4))
	ax = fig.add_subplot()
	plot_unique(ax, di)
	
	return fig


def draw_skew(di):
	fig = plt.figure(figsize=(10, 4))
	ax = fig.add_subplot()
	plot_skew(ax, di)
	
	return fig


## ==== Plot axis
def plot_boxplot(ax, di):
	ax.set_xlabel('Population\nnewer ▶')
	ax.set_xlim(0, di.rounds_params.max+1)
	ax.set_xticks(range(0, di.rounds_params.max+1, 5))
	ax.set_xticklabels(range(0, di.rounds_params.max+1, 5))
	
	ax.set_ylabel('Score\nbetter ▶')
	util.set_limits(ax, y=[0, 1])
	ax.invert_yaxis()
	
	
	ax.boxplot(
		di.round_scores,
		widths=0.8,
		sym='+',
		manage_ticks=False,
		patch_artist=True,
		boxprops={'facecolor': 'grey'},
		medianprops={'lw': 2},
	)
	
	ax.grid(True)


def plot_violinplot(ax, di, groups, group_id):
	np.random.seed(42)
	group = groups[group_id]
	
	## Collect *available* data of group
	x = []; data = []
	for i in range(group['start_idx']-1, group['end_idx'], 2):
		if i < di.rounds_params.actual:
			x.append(i+1)
			data.append(di.round_scores[i])
	
	## Configure grid, independent of data availability
	ax.set_xlabel('Population\nnewer ▶')
	ax.set_xlim(group['start_idx']-2, group['end_idx']+2)
	ticks = np.arange(group['start_idx'], group['end_idx']+1, 4)
	ax.set_xticks(ticks)
	ax.set_xticklabels(ticks)
	
	ax.set_ylabel('Score\nbetter ▶')
	ax.invert_yaxis()
	
	if x:
		## Make some music
		vp = ax.violinplot(data, x, widths=1.9)
		for pc in vp['bodies']:
			pc.set_facecolor('#d43f3a')
		
		## Draw scatter plot overlay
		scatter_x = []
		scatter_y = []
		for cur_x, cur_data in zip(x, data):
			scatter_x += list(cur_x + 1.8*(np.random.rand(len(cur_data))-0.5))
			scatter_y += cur_data
		ax.scatter(
			scatter_x, scatter_y,
			alpha=0.2,
			s = 4,
			edgecolors = 'none',
			c = 'black',
			zorder=2,
		)
	
	ax.grid(True)


def plot_abs_rank_changes(ax, di, selected_round_ids, slices):
	## https://matplotlib.org/examples/color/colormaps_reference.html
	N = len(di.round_scores[0])
	
	ax.set_xlabel('Rank(Score)\nbetter ▶')
	util.set_limits(ax, x=[1, N+1])
	ax.invert_xaxis()
	
	ax.set_ylabel('Score\nbetter ▶')
	util.set_limits(ax, y=[0, 1])
	ax.invert_yaxis()
	
	for i, rank in enumerate(slices.keys()):
		ax.axvline(rank, color=plt.cm.tab10(i), linestyle='--', lw=1)
	
	cmap = lambda x: plt.cm.winter(1-x)
	ax.set_prop_cycle(matplotlib.cycler(color=cmap(np.linspace(0, 1, len(selected_round_ids)))))
	
	t = np.arange(1, N+1)
	for i in selected_round_ids:
		ax.plot(t, di.round_scores[i], label=f'Population {i+1}')
	
	ax.grid(True)
	
	ax.legend(
		[
			matplotlib.lines.Line2D([0], [0], color=cmap(0.), lw=4),
			matplotlib.lines.Line2D([0], [0], color=cmap(1.), lw=4),
			matplotlib.lines.Line2D([0], [0], color='gray', lw=2, linestyle=':'),
		], [
			'Population 1',
			f'Population {selected_round_ids[-1]+1}',
			'Slices of subfigure to the right',
		],
		#loc='upper left',
		bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=2, mode="expand", borderaxespad=0.,
	)



def plot_rel_rank_changes(ax, di, selected_round_ids, slices):
	## https://matplotlib.org/examples/color/colormaps_reference.html
	if len(selected_round_ids) == 1:
		return
	
	selected_round_scores = []
	
	round_distance = selected_round_ids[1] - selected_round_ids[0]
	for old_id, new_id in zip(selected_round_ids[:-1], selected_round_ids[1:]):
		old = np.array(di.round_scores[old_id])
		new = np.array(di.round_scores[new_id])
		
		d = list((old-new) / old / round_distance * 100)
		selected_round_scores.append(d)
	N = len(di.round_scores[0])
	
	ax.set_xlabel('Rank(Score)\nbetter ▶')
	util.set_limits(ax, x=[1, N+1])
	ax.invert_xaxis()
	
	ax.set_ylabel('Score improvement / round [%]\nbetter ▶')
	ax.set_ylim(-10, 8)
	
	cmap = lambda x: plt.cm.winter(1-x)
	ax.set_prop_cycle(matplotlib.cycler(color=cmap(np.linspace(0, 1, len(selected_round_ids)-1))))
	
	for i, rank in enumerate(slices.keys()):
		ax.axvline(rank, color=plt.cm.tab10(i), linestyle='--', lw=1)
	
	ax.axhline(0, color='black')
	t = np.arange(1, N+1)
	for scores in selected_round_scores:
		ax.plot(t, scores)
	
	ax.grid(True)
	
	ax.legend(
		[
			matplotlib.lines.Line2D([0], [0], color=cmap(0.), lw=4),
			matplotlib.lines.Line2D([0], [0], color=cmap(1.), lw=4),
			matplotlib.lines.Line2D([0], [0], color='gray', lw=2, linestyle=':'),
		],
		[
			f'Population ${selected_round_ids[0]+1} \\longrightarrow {selected_round_ids[1]+1}$',
			f'Population ${selected_round_ids[-2]+1} \\longrightarrow {selected_round_ids[-1]+1}$',
			'Slices of subfigure to the right',
		],
		bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=2, mode="expand", borderaxespad=0.,
	)


def plot_rank(ax, di, slices):
	N = len(di.round_scores)
	
	ax.set_xlabel('Population\nnewer ▶')
	ax.set_xlim(0, di.rounds_params.max)
	ax.set_xticks(range(0, di.rounds_params.max+1, 5))
	ax.set_xticklabels(range(0, di.rounds_params.max+1, 5))
	
	ax.set_ylabel('Score\nbetter ▶')
	util.set_limits(ax, y=[0, 1])
	ax.invert_yaxis()
	
	t = np.arange(1, N+1)
	for rank, name in slices.items():
		ax.plot(t, [x[rank] for x in di.round_scores], label=f'{name} ({rank}. rank)')
	
	ax.grid(True)
	
	ax.legend(loc='lower right')


def plot_fit(ax, foo, di, model_params, name, round_evaluator):
	round_scores = di.round_scores
	N = len(round_scores)
	
	ax.set_xlabel('Population\nnewer ▶')
	ax.set_xlim(0, di.rounds_params.max)
	ax.set_xticks(range(0, di.rounds_params.max+1, 5))
	ax.set_xticklabels(range(0, di.rounds_params.max+1, 5))
	
	ax.set_ylabel('Score\nbetter ▶')
	ax.set_ylim(-0, 1)
	ax.invert_yaxis()
	
	t = np.arange(1, N+1)
	ax.fill_between(
		t,
		[x[foo['best']] for x in round_scores],
		[x[foo['worst']] for x in round_scores],
		alpha=.4,
		color='lightskyblue',
		label='Mating score range'
	)
	ax.fill_between(
		t,
		[x[foo['worst']] for x in round_scores],
		[x[-1] for x in round_scores],
		alpha=.15,
		color='grey',
		label='Remaining pop. score range'
	)
	ax.plot(t, list(map(round_evaluator, round_scores)), color='red', label=name)
	
	if 'a' in model_params:
		if 'offset' in model_params:
			ax.plot(
				[0, di.rounds_params.max],
				[model_params['offset'], model_params['offset']],
				linestyle='--',
				color='black',
				label=f'fit offset ({model_params["offset"]:.2})'
			)
		
		x = np.linspace(0, di.rounds_params.max, di.rounds_params.max)
		
		if 'offset' in model_params:
			ax.plot(
				x,
				model_abc(x, model_params['a'], model_params['b'], model_params['offset']),
				linestyle='--',
				color='orange',
				label=f'fit (${model_params["a"]:.2f} \cdot e^{{{model_params["b"]:.2f} \cdot x}} + {model_params["offset"]:.2f}$)\nhalf-life time: {model_params["hl"]:.3f} rounds\ndecay rate: {model_params["rate"]*100:.3f}% / round\nstart score: {model_params["y0"]:.3f}\nRMSD: {model_params["rmsd"]:.4f}'
			)
		else:
			ax.plot(
				x,
				model_ab(x, model_params['a'], model_params['b']),
				linestyle='--',
				color='orange',
				label=f'fit (${model_params["a"]:.2f} \cdot e^{{{model_params["b"]:.2f} \cdot x}}$)\nhalf-life time: {model_params["hl"]:.3f} rounds\ndecay rate: {model_params["rate"]*100:.3f}% / round\nRMSD: {model_params["rmsd"]:.4f}'
			)
	
	ax.grid(True)
	ax.legend(loc='lower right')


def plot_unique(ax, di):
	ax.set_xlabel('Population\nnewer ▶')
	ax.set_xlim(0, di.rounds_params.max)
	ax.set_xticks(range(0, di.rounds_params.max+1, 5))
	ax.set_xticklabels(range(0, di.rounds_params.max+1, 5))
	
	ax.set_ylabel('Unique scores [%]\nmore unique values ▶')
	ax.set_ylim(0, 100)
	
	unique = 100*di.unique
	t = np.arange(1, di.rounds_params.actual+1)
	ax.fill_between(
		t,
		[100]*di.rounds_params.actual,
		unique,
		alpha=.15,
		color='grey',
		label=f'Duplicated scores (mean {100-100*di.uniqueness_params.mean:.1f}%)'
	)
	ax.plot(t, unique, label=f'Unique scores (mean {100*di.uniqueness_params.mean:.1f}%)')
	
	ax.grid(True)
	ax.legend(loc='lower left')


def plot_skew(ax, di):
	ax.set_xlabel('Population\nnewer ▶')
	ax.set_xlim(0, di.rounds_params.max)
	ax.set_xticks(range(0, di.rounds_params.max+1, 5))
	ax.set_xticklabels(range(0, di.rounds_params.max+1, 5))
	
	ax.set_ylabel('Skew\ntowards better scores ▶')
	ax.set_ylim(-5, 5)
	
	ax.axhline(0, color='black', linewidth=2)
	t = np.arange(1, di.rounds_params.actual+1)
	ax.plot(t, di.skew, label=f'Score skew (mean {di.skew_params.mean:.2f} / std {di.skew_params.std:.3f})')
	
	ax.grid(True)
	ax.legend(loc='lower left')


## ==== Helpers
def model_abc(x, a, b, c):
	return a * np.exp(b*x) + c

def model_ab(x, a, b):
	return a * np.exp(b*x)


## Model based on median score (x): a * e^(b*x) + c
def fit_abc(scores, foo):
	## https://math.stackexchange.com/questions/1337601/fit-exponential-with-constant
	## w/o optimisation: took 32:06,56 and 47/270 failed to fit
	## w/ optimisation: took 26:47,20 total and 59/270 failed to fit
	x = np.linspace(1, len(scores), len(scores), dtype=int) ## = population
	y = np.fromiter(map(lambda x: x[foo['med']], scores), float)  ## = quality of population
	n = x.size
	
	p0s = []
	if n >= 4:
		step = int(n / 4)
		x1 = x[1*step] ; y1 = y[x1]
		x2 = x[2*step] ; y2 = y[x2]
		x3 = x[3*step] ; y3 = y[x3]
		
		try:
			if y2 == y3:
				raise ValueError("Div by zero")
			
			b = 2/(x1-x3) * math.log((y1-y2)/(y2-y3))
			if b == 0:
				raise ValueError("Div by zero")
			
			a = (y1-y3)/(math.exp(b*x1)-math.exp(b*x3))
			
			c = y1 - a*math.exp(b*x1)
			
			p0s.append([a, b, c])
		except ValueError:
			pass
	p0s.append([0.85, -0.05, 0.05])
	
	for i, p0 in enumerate(p0s):
		try:
			reg_params, cov = scipy.optimize.curve_fit(model_abc, x, y, p0=p0, sigma=1/(1+y))
			(a, b, offset) = reg_params
			cov = cov.tolist()
			
			lambda_ = -b
			rate = 1 - math.exp(b)
			hl = math.log(2) / lambda_
			y0 = a + offset
			
			se = sum( (y - model_abc(x, *reg_params)) ** 2 )
			mse = 1/n * se
			rmsd = math.sqrt(mse)
			
			return util.ldict(['a', 'b', 'offset', 'cov', 'lambda_', 'rate', 'hl', 'y0', 'se', 'mse', 'rmsd'])
		except RuntimeError as ex:
			## "Optimal parameters not found: Number of calls to function has reached maxfev = 800."
			pass
	
	return {}


## Model based on median score (x): a * e^(b*x)
def fit_ab(scores, name, round_evaluator):
	## https://math.stackexchange.com/questions/1337601/fit-exponential-with-constant
	x = np.linspace(1, len(scores), len(scores), dtype=int) ## = population
	y = np.fromiter(map(round_evaluator, scores), float)  ## = quality of population
	n = x.size
	
	p0s = []
	if n >= 4:
		step = int(n / 4)
		x1 = x[1*step] ; y1 = y[x1]
		x2 = x[2*step] ; y2 = y[x2]
		x3 = x[3*step] ; y3 = y[x3]
		
		try:
			if y2 == y3:
				raise ValueError("Div by zero")
			
			b = 2/(x1-x3) * math.log((y1-y2)/(y2-y3))
			if b == 0:
				raise ValueError("Div by zero")
			
			a = (y1-y3)/(math.exp(b*x1)-math.exp(b*x3))
			
			p0s.append([a, b])
		except ValueError:
			pass
	p0s.append([0.8, -0.05])
	
	for i, p0 in enumerate(p0s):
		try:
			reg_params, cov = scipy.optimize.curve_fit(model_ab, x, y, p0=p0, sigma=1/(1+y))
			(a, b) = reg_params
			cov = cov.tolist()
			
			lambda_ = -b
			rate = 1 - math.exp(b)
			hl = math.log(2) / lambda_
			
			se = sum( (y - model_ab(x, *reg_params)) ** 2 )
			mse = 1/n * se
			rmsd = math.sqrt(mse)
			
			return util.ldict(['a', 'b', 'cov', 'lambda_', 'rate', 'hl', 'se', 'mse', 'rmsd'], model=name)
		except RuntimeError as ex:
			## "Optimal parameters not found: Number of calls to function has reached maxfev = 800."
			pass
	
	return {}


def ra(properties):
	N = int(properties['population_size'])
	best = 0
	
	if properties['selector'] == 'rnd_prop':
		med = int(N/3)
		worst = N-1
		weights = list(range(N, 0, -1))
	
	elif properties['selector'] in ['rnd_uniform', 'rnd_swipe']:
		med = int(N/2)
		worst = N-1
		weights = [1]*N
	
	elif properties['selector'] in ['best_inverse', 'best_with_best']:
		mating_rate = float(properties['mating_rate'])
		parent_cnt = max(2, 2 * int(N * mating_rate / 2))
		med = int(parent_cnt/2)
		worst = parent_cnt-1
		weights = [1]*parent_cnt + [0]*(N-parent_cnt)
	
	elif properties['selector'] in ['tournament']:
		t = float(properties['selector_t'])
		med = int(N - N * 2**(-1/t))
		worst = N-1
		weights = []
		for i in range(N):
			weights.append( ((N-i)/N)**t - ((N-i-1)/N)**t )
	
	else:
		raise Exception(f'Unknown selector "{properties["selector"]}"')
	
	return util.ldict(['best', 'med', 'worst', 'weights'])


def select_rounds(rounds_params):
	assert(rounds_params.actual > 0)
	
	if rounds_params.actual == 1:
		return [0]
	
	for distance in range(1, rounds_params.max+1):
		n = int(rounds_params.max/distance)
		
		if distance <= 1:
			if n > 4: continue
		if distance <= 3:
			if n > 5: continue
		
		if n > 7: continue
		
		break
	
	return np.arange(0, rounds_params.actual, distance)


if __name__ == '__main__':
	main()
