#!/usr/bin/env python3
## This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0

import sys
import os
import json
import util
import time
from abc import ABC
import datetime
import collections
from twisted.internet import reactor, protocol, endpoints
from twisted.protocols import basic



class Job:
	def __init__(self, hash, nr, args):
		self.hash = hash
		self.nr = nr
		self.args = args
		
		self.exit_code = None
		self.last_log_msg = ''
		
		self.id = f'{hash}/{nr}'
		self.worker = None
		self.paused = False
	
	@property
	def is_assignable(self):
		return not self.is_assigned and not self.is_done and not self.paused
	
	@property
	def is_done(self):
		return self.exit_code is not None
	
	@property
	def is_assigned(self):
		return self.worker is not None
	
	def update_last_log_msg(self, msg):
		self.last_log_msg = msg


class WorkerProxy:
	def __init__(self, msg_handler, id, name, start_time):
		self.msg_handler = msg_handler
		self.id = id
		self.name = name
		self.start_time = start_time
		
		self.paused = False
		self._job = None
		self.last_log_msg = ''
	
	@property
	def host(self):
		return self.msg_handler.host if self.msg_handler else None
	
	@property
	def is_available(self):
		return self.is_connected and not self.paused and not self.is_working
	
	@property
	def is_connected(self):
		return self.msg_handler is not None
	
	@property
	def is_working(self):
		return self._job is not None
	
	@property
	def job(self):
		return self._job
	
	@job.setter
	def job(self, new_job):
		if new_job == self._job: return
		if self._job is not None: self._job.worker = None
		if new_job is not None: new_job.worker = self
		self._job = new_job
	
	@property
	def job_id(self):
		return self._job.id if self._job else None
	
	def update_last_log_msg(self, msg):
		print(f'{self.id}: {msg}')
		self.last_log_msg = msg



class Manager:
	def __init__(self):
		self.workers = {}
		self.jobs = collections.OrderedDict()
	
	
	## Worker
	def add_worker(self, msg_handler, id, name, start_time, last_log_msg, job):
		if worker := self.workers.get(id):
			worker.msg_handler = msg_handler
			assert(worker.name == name)
			assert(worker.start_time == start_time)
		else:
			self.workers[id] = worker = WorkerProxy(msg_handler, id, name, start_time)
		
		worker.update_last_log_msg(last_log_msg) ## This could produce duplicates on reconnects
		worker.job = job
		if worker.is_available:
			self.employ_worker(worker)
		
		return worker
	
	
	def remove_worker(self, id):
		if worker := self.worker_from_id(id):
			worker.msg_handler = None
	
	def pause_worker(self, id, paused=True):
		if worker := self.worker_from_id(id):
			worker.paused = paused
			if worker.is_available:
				self.employ_worker(worker)
	
	def cancel_worker(self, id):
		if worker := self.worker_from_id(id):
			if worker.is_connected:
				worker.msg_handler.cancel()
	
	def stop_worker(self, id):
		if worker := self.worker_from_id(id):
			if worker.is_connected:
				worker.msg_handler.stop()
	
	def update_worker_log(self, id, msg):
		if worker := self.worker_from_id(id):
			worker.update_last_log_msg(msg)
	
	def update_assigned_job(self, worker_id, job_id):
		if worker := self.worker_from_id(worker_id):
			if job_id is None:
				worker.job = None
				self.employ_worker(worker)
			elif job := self.job_from_id(job_id):
				worker.job = job
			else:
				#worker.job = None
				pass
	
	
	## Job
	def add_job(self, hash, nr, args):
		job = Job(hash, nr, args)
		self.jobs[job.id] = job
		self.employ_worker(job=job)
	
	def pause_job(self, id, paused=True):
		if job := self.job_from_id(id):
			job.paused = paused
			if job.is_assignable:
				self.employ_worker(job=job)
	
	def update_job_exit_code(self, id, exit_code):
		if job := self.job_from_id(id):
			job.exit_code = exit_code
		
		if all(map(lambda x: x.is_done, self.jobs.values())):
			for worker in self.workers.values():
				if worker.is_connected:
					worker.msg_handler.stop()
	
	def update_job_log(self, id, msg):
		if job := self.job_from_id(id):
			job.update_last_log_msg(msg)
	
	def worker_from_id(self, id):
		if worker := self.workers.get(id):
			return worker
		else:
			print(f'Unknown worker "{id}"')
	
	def job_from_id(self, id):
		if job := self.jobs.get(id):
			return job
		else:
			print(f'Unknown job "{id}"')
	
	
	def employ_worker(self, worker=None, job=None):
		if worker is None and (worker := self.find_free_worker()) is None:
			return
		assert(worker.is_available)
		
		if job is None and (job := self.find_available_job()) is None:
			return
		assert(job.is_assignable)
		
		worker.job = job
		worker.msg_handler.send_job(job)
	
	
	def find_free_worker(self):
		for worker in self.workers.values():
			if worker.is_available:
				return worker
	
	def find_available_job(self):
		for job in self.jobs.values():
			if job.is_assignable:
				return job



class ConHandler(ABC):
	def __init__(self, con, manager):
		self.con = con
		self.manager = manager
	
	def on_disconnect(self):
		pass


## Handler for new connections who haven't registered so far as a worker or controller.
class RolelessConHandler(ConHandler):
	def __init__(self, con, manager):
		super().__init__(con, manager)
	
	def on_sig_register_worker(self, **kwargs):
		self.con.handler = WorkerConHandler(self.con, self.manager, **kwargs)
	
	def on_sig_register_controller(self):
		self.con.handler = ControllerConHandler(self.con, self.manager)


## Handles the connection of a worker.  Workers execute the queued jobs.
class WorkerConHandler(ConHandler):
	def __init__(self, con, manager, id, name, start_time, last_log_msg, job_id):
		super().__init__(con, manager)
		self.id = id
		addr = self.con.transport.getPeer()
		self.host = f'{addr.host}:{addr.port}'
		
		self.manager.add_worker(
			self, id, name,
			datetime.datetime.fromisoformat(start_time),
			last_log_msg,
			self.manager.jobs.get(job_id)
		)
	
	def on_disconnect(self):
		self.manager.remove_worker(self.id)
	
	## Receive messages from the worker
	def on_sig_update_worker_log(self, msg):
		self.manager.update_worker_log(self.id, msg)
	
	def on_sig_update_job_log(self, job_id, msg):
		self.manager.update_job_log(job_id, msg)
	
	def on_sig_update_assignment(self, job_id):
		self.manager.update_assigned_job(self.id, job_id)
	
	def on_sig_update_job_status(self, job_id, exit_code):
		self.manager.update_job_exit_code(job_id, exit_code)
	
	## Send messages to the worker
	def send_job(self, job):
		self.con['job'](id=job.id, hash=job.hash, nr=job.nr, args=job.args)
	
	def cancel(self):
		self.con['cancel']()
	
	def stop(self):
		self.con['stop']()



## Handles controller connections.  A Controller adds jobs to the queue and
## monitors the status.
class ControllerConHandler(ConHandler):
	def __init__(self, con, manager):
		super().__init__(con, manager)
	
	
	def on_sig_req_worker_report(self):
		workers = []
		for worker in self.manager.workers.values():
			workers.append(util.Dobject(
				id = worker.id,
				name = worker.name,
				start_time = str(worker.start_time),
				paused = worker.paused,
				host = worker.host,
				job_id = worker.job_id,
				last_log_msg = worker.last_log_msg
			))
		
		workers.sort(key=lambda x: x.name)
		self.con['worker_report'](workers=workers)
	
	
	def on_sig_req_job_report(self):
		jobs = []
		for job in self.manager.jobs.values():
			jobs.append(util.Dobject(
				id = job.id,
				hash = job.hash,
				nr = job.nr,
				args = job.args,
				exit_code = job.exit_code,
				last_log_msg = job.last_log_msg,
				paused = job.paused,
				worker = job.worker.id if job.worker is not None else None
			))
		
		self.con['job_report'](jobs=jobs)
	
	
	def on_sig_add_job(self, hash, nr, args):
		self.manager.add_job(hash, nr, args)
	
	def on_sig_workers_set_pause(self, worker_ids, paused):
		for worker_id in worker_ids:
			self.manager.pause_worker(worker_id, paused)
	
	def on_sig_jobs_set_pause(self, job_ids, paused):
		for job_id in job_ids:
			self.manager.pause_job(job_id, paused)
	
	def on_sig_workers_cancel(self, worker_ids):
		for worker_id in worker_ids:
			self.manager.cancel_worker(worker_id)
	
	def on_sig_workers_stop(self, worker_ids):
		for worker_id in worker_ids:
			self.manager.stop_worker(worker_id)




basic.LineReceiver.delimiter = b'\n'

class JsonlReceiver(basic.LineReceiver):
	def __init__(self):
		self.handler = self
		print('Started receiver')
		super().__init__()
	
	def lineReceived(self, line):
		msg = json.loads(line)
		print(f'C: {msg["signal"]}')
		getattr(self.handler, f'on_sig_{msg["signal"]}')(**msg['args'])
	
	def __getitem__(self, key):
		return lambda **args: self.signal(key, **args)
	
	def connectionLost(self, reason):
		print('Disconnect')
		self.handler.on_disconnect()
	
	def signal(self, signal, **args):
		print(f'S: {signal}')
		self.sendLine(json.dumps({
			'signal': signal,
			'args': args,
		}).encode('utf-8'))


class Connection(JsonlReceiver):
	def __init__(self, manager):
		self.manager = manager
		super().__init__()
	
	def connectionMade(self):
		self.handler = RolelessConHandler(self, self.manager)


class Listener(protocol.Factory):
	def __init__(self, manager):
		self.manager = manager
	
	def buildProtocol(self, addr):
		return Connection(self.manager)


def main():
	manager = Manager()
	#for i in range(10):
		#job = Job(
			#'0123456789abcdef', i, ['dummy', '5']
		#)
		#manager.jobs[job.id] = job
	endpoint = endpoints.serverFromString(reactor, "tcp:7492")
	endpoint.listen(Listener(manager))
	reactor.run()


if __name__ == '__main__':
	main()
