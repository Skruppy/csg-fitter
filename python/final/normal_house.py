## This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0

import math
from test_final_base import FinalCreator



class NormalHouseCreator(FinalCreator):
	sample_rate = 100
	
	
	#def build(self):
		#return {
		  #"op": "subtract",
		  #"childs": [
		    #{ "geo": "cube", ## Roof lower flat cutout
		      #"params": { "rotation": [0, 0, 0], "center": [0, 22, 0], "radius": [20, 10, 20], },
		    #},
		    #{ "geo": "cube", ## Chimney cutout
		      #"params": { "rotation": [0, 0, 0], "center": [6, 22, -2], "radius": [1.5, 8, 1.5], },
		    #},
		    #{ "geo": "cube", ## Roof lower cutout
		      #"params": { "rotation": [0, 0, math.pi/4], "center": [0, 0, 0], "radius": [16.5, 16.5, 13], },
		    #},
		  #]
		#}
	def build(self):
		return {
		  "op": "union",
		  "childs": [
		    { "op": "intersect",
		      "childs": [
		        { "op": "union",
		          "childs": [
		            { "geo": "cube", ## Roof volume
		              "params": { "rotation": [0, 0, math.pi/4], "center": [0, 0, 0], "radius": [17.5, 17.5, 12], },
		            },
		            { "geo": "cube", ## Chimney volume
		              "params": { "rotation": [0, 0, 0], "center": [6, 22, -2], "radius": [2, 6, 2], },
		            },
		            { "geo": "cube", ## Chimney rim volume
		              "params": { "rotation": [0, 0, 0], "center": [6, 28, -2], "radius": [2.5, 0.5, 2.5], },
		            },
		          ]
		        },
		        { "op": "subtract",
		          "childs": [
		            { "geo": "cube", ## Roof lower flat cutout
		              "params": { "rotation": [0, 0, 0], "center": [0, 22, 0], "radius": [20, 10, 20], },
		            },
		            { "geo": "cube", ## Chimney cutout
		              "params": { "rotation": [0, 0, 0], "center": [6, 22, -2], "radius": [1.5, 8, 1.5], },
		            },
		            { "geo": "cube", ## Roof lower cutout
		              "params": { "rotation": [0, 0, math.pi/4], "center": [0, 0, 0], "radius": [16.5, 16.5, 13], },
		            },
		          ]
		        },
		      ]
		    },
		    { "geo": "cube", ## Model base
		      "params": { "rotation": [0, 0, 0], "center": [0, -2, 0], "radius": [15, 2, 15], },
		    },
		    { "op": "intersect",
		      "childs": [
		        { "op": "subtract",
		          "childs": [
		            { "geo": "cube", ## Walls volume
		              "params": { "rotation": [0, 0, 0], "center": [0, 19, 0], "radius": [10, 20, 10], },
		            },
		            { "geo": "cube", ## Walls cutout
		              "params": { "rotation": [0, 0, 0], "center": [0, 10, 0], "radius": [9, 22, 9], },
		            },
		            { "geo": "cube", ## Rectangular window
		              "params": { "rotation": [0, 0, 0], "center": [-4, 5, 9.5], "radius": [4, 3, 1], },
		            },
		            { "geo": "cube", ## Door
		              "params": { "rotation": [0, 0, 0], "center": [4, 3, 9.5], "radius": [2, 5, 1], },
		            },
		            { "op": "intersect",
		              "childs": [
		                { "geo": "cylinder", ## Round window (wall cutout)
		                  "params": { "rotation": [math.pi/2, 0, 0], "center": [0, 17, 0], "radius": 3, "height": 22, },
		                },
		                { "op": "union",
		                  "childs": [
		                    { "geo": "cube", ## Round window upper right (wall cutout)
		                      "params": { "rotation": [0, 0, 0], "center": [2.5, 19.5, 0], "radius": [2, 2, 11], },
		                    },
		                    { "geo": "cube", ## Round window upper left (wall cutout)
		                      "params": { "rotation": [0, 0, 0], "center": [-2.5, 19.5, 0], "radius": [2, 2, 11], },
		                    },
		                    { "geo": "cube", ## Round window lower right (wall cutout)
		                      "params": { "rotation": [0, 0, 0], "center": [2.5, 14.5, 0], "radius": [2, 2, 11], },
		                    },
		                    { "geo": "cube", ## Round window lower left (wall cutout)
		                      "params": { "rotation": [0, 0, 0], "center": [-2.5, 14.5, 0], "radius": [2, 2, 11], },
		                    },
		                  ]
		                },
		              ]
		            },
		          ]
		        },
		        { "geo": "cube", ## Wall cutout
		          "params": { "rotation": [0, 0, math.pi/4], "center": [0, 0, 0], "radius": [17, 17, 20], },
		        },
		      ],
		    },
		  ],
		}
