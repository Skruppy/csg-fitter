## This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0

import math
from test_final_base import FinalCreator
import itertools



class NormalBrickCreator(FinalCreator):
	sample_rate = 1
	
	
	def build(self):
		nipples = []
		for x, z in itertools.product([-15, -5, 5, 15], [-5, 5]):
			nipples.append({
			  "geo": "cylinder",
			  "params": { "rotation": [0, 0, 0], "center": [x, 8, z], "radius": 3, "height": 3, },
			})
		
		return {
		  "op": "subtract",
		  "childs": [
		    { "op": "union",
		      "childs": [
		        { "geo": "cube",
		          "params": { "rotation": [0, 0, 0], "center": [0, 0, 0], "radius": [20, 7, 10], },
		        },
		        *nipples,
		      ],
		    },
		    { "geo": "cube",
		      "params": { "rotation": [0, 0, 0], "center": [0, -1, 0], "radius": [18, 7, 8], },
		    },
		  ]
		}
