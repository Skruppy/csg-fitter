## This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0

import math
from test_final_base import FinalCreator



class MatrjoschkaCreator(FinalCreator):
	configurations = {
		'1': {'n': 1},
		'5': {'n': 5},
	}
	
	
	def __init__(self, n=4):
		self.n = n
	
	
	sample_rate = 60
	
	
	def build(self):
		return {
			"op": "union",
			"childs": [self._b(i) for i in range(self.n)],
		}
	
	
	def _b(self, i):
		s = 2.2**i
		return {
		  "op": "subtract",
		  "childs": [
		    { "op": "intersect",
		      "childs": [
		        { "geo": "cube",
		          "params": { "rotation": [0, 0, 0], "center": [0, 0, 0], "radius": [1*s, 1*s, 1*s] }
		        },
		        { "geo": "sphere",
		          "params": { "rotation": [0, 0, 0], "center": [0, 0, 0], "radius": 1.3*s }
		        },
		      ]
		    },
		    { "op": "union",
		      "childs": [
		        { "geo": "cylinder",
		          "params": { "rotation": [0, 0, 0], "center": [0, 0, 0], "radius": 0.7*s, "height": 3*s }
		        },
		        { "geo": "cylinder",
		          "params": { "rotation": [math.pi/2, 0, 0], "center": [0, 0, 0], "radius": 0.7*s, "height": 3*s }
		        },
		        { "geo": "cylinder",
		          "params": { "rotation": [0, 0, math.pi/2], "center": [0, 0, 0], "radius": 0.7*s, "height": 3*s }
		        },
		      ]
		    }
		  ]
		}

