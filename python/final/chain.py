## This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0

import math
from test_final_base import FinalCreator

class ChainCreator(FinalCreator):
	configurations = {
		'':     {'n': 14, 'loop': False},
		'loop': {'n': 14, 'loop': True},
	}
	
	
	def __init__(self, n=20, loop=True):
		self.n = n
		self.loop = loop
	
	
	sample_rate = 5000
	
	
	def build(self):
		assert(self.n % 2 == 0)
		return {
			"op": "union",
			"childs": [self._link(i) for i in range(self.n)],
		}
	
	
	def _link(self, i):
		## Links are positioned around around the Z axis (on the X-Z plane).
		## The viewer looks in the middle of the ring (Y-up orientation).
		## We start (0deg) at the top (not the right).
		## Ratation is CCW, looking along the axis from the positiv side to the origin.
		## Rotations are applied in order Z, Y, X
		
		if self.loop:
			link_length = 5.7
			a = (2*math.pi) * (i/self.n)
		else:
			link_length = 5
			a = math.pi/2
		
		distance_from_center = self.n * link_length / (2*math.pi)
		
		if i % 2 == 0: ## For every _even_ link
			roll = 0
		else: ## For every _odd_ link
			roll = math.pi/2
			distance_from_center *= math.cos(2*math.pi/self.n)
		
		if self.loop:
			## As we are looking from above, the Z axis is looking down. Hence
			## we need to negate the Z Position to get CCW rotation.
			x =  distance_from_center * math.cos(a)
			z = -distance_from_center * math.sin(a)
		else:
			x = (i - self.n/2 + 0.5) * link_length
			z = 0
		
		return {
			"op": "subtract",
			"childs": [
				{
					"geo": "cylinder",
					"params": {
						"rotation": [0, a, roll],
						"center": [x, 0, z],
						"radius": 4,
						"height": 1,
					},
				},
				{
					"geo": "cylinder",
					"params": {
						"rotation": [0, a, roll],
						"center": [x, 0, z],
						"radius": 3,
						"height": 2,
					},
				},
			],
		}
