## This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0

import math
from test_final_base import FinalCreator
import random



class NormalKeyCreator(FinalCreator):
	sample_rate = 100
	
	
	def build(self):
		random.seed(7)
		notches = []
		for i in range(10, 24, 2):
			notches.append({
			  "geo": "cube",
			  "params": { "rotation": [0, math.pi/4, 0], "center": [i, 0, random.uniform(-3.75, -5)], "radius": [2, 2, 2], },
			})
		
		return {
		  "op": "subtract",
		  "childs": [
		    { "op": "union",
		      "childs": [
		        { "geo": "cylinder", ## Head
		          "params": { "rotation": [0, 0, 0], "center": [0, 0, 0], "radius": 6, "height": 2, },
		        },
		        { "geo": "cube", ## Blade
		          "params": { "rotation": [0, 0, 0], "center": [14, 0, 0], "radius": [10, 0.75, 2.75], },
		        },
		        { "geo": "cube", ## Tip
		          "params": { "rotation": [0, math.pi/4, 0], "center": [24, 0, 0], "radius": [math.sqrt(2)*2.75/2, 1, math.sqrt(2)*2.75/2], },
		        },
		      ],
		    },
		    { "geo": "cylinder", ## Hole
		      "params": { "rotation": [0, 0, 0], "center": [-3, 0, 0], "radius": 2, "height": 3, },
		    },
		    { "geo": "cube", ## Shoulder front
		      "params": { "rotation": [0, 0, 0], "center": [18, -1.5, 0], "radius": [10, 1, 3], },
		    },
		    { "geo": "cube", ## Shoulder back
		      "params": { "rotation": [0, 0, 0], "center": [18, 1.5, 0], "radius": [10, 1, 3], },
		    },
		    *notches,
		    { "geo": "cube", ## Lines front
		      "params": { "rotation": [math.pi/4, 0, 0], "center": [19, -1.6, -1.5], "radius": [10, 1, 1], },
		    },
		    { "geo": "cube", ## Lines front
		      "params": { "rotation": [math.pi/4, 0, 0], "center": [19, -1.6, -0.5], "radius": [10, 1, 1], },
		    },
		    { "geo": "cube", ## Lines back
		      "params": { "rotation": [math.pi/4, 0, 0], "center": [19, 1.6, -1.5], "radius": [10, 1, 1], },
		    },
		    { "geo": "cube", ## Lines back
		      "params": { "rotation": [math.pi/4, 0, 0], "center": [19, 1.6, -0.5], "radius": [10, 1, 1], },
		    },
		  ],
		}
