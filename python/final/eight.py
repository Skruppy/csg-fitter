## This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0

import math
from test_final_base import FinalCreator



class EightCreator(FinalCreator):
	configurations = {
		'line': {'edges': set([0,    3, 6, 4, 8, 9, 10])},
		'ring': {'edges': set([0, 2, 3, 6, 4, 8, 9, 10])},
		'full': {'edges': set(range(12))},
	}
	
	
	def __init__(self, edges):
		self.edges = edges
	
	
	sample_rate = 2000
	
	
	def build(self):
		radius = 3
		objects = []
		
		for x in [radius, -radius]:
			for y in [radius, -radius]:
				for z in [radius, -radius]:
					objects.append(self._b([x, y, z]))
		
		poses = []
		for a in [radius, -radius]:
			for b in [radius, -radius]:
				poses.append([a, b, 0])
				poses.append([a, 0, b])
				poses.append([0, a, b])
		
		for edge_nr, pos in enumerate(poses):
			if edge_nr in self.edges:
				r = list(map(lambda x: 0.3 if x else 2.5, pos))
				objects.append({ "geo": "cube",
				  "params": { "rotation": [0, 0, 0], "center": pos, "radius": r }
				})
		
		return {
			"op": "union",
			"childs": objects,
		}
	
	
	def _b(self, pos):
		return {
		  "op": "subtract",
		  "childs": [
		    { "op": "intersect",
		      "childs": [
		        { "geo": "cube",
		          "params": { "rotation": [0, 0, 0], "center": pos, "radius": [1, 1, 1] }
		        },
		        { "geo": "sphere",
		          "params": { "rotation": [0, 0, 0], "center": pos, "radius": 1.3 }
		        },
		      ]
		    },
		    { "op": "union",
		      "childs": [
		        { "geo": "cylinder",
		          "params": { "rotation": [0, 0, 0], "center": pos, "radius": 0.7, "height": 3 }
		        },
		        { "geo": "cylinder",
		          "params": { "rotation": [math.pi/2, 0, 0], "center": pos, "radius": 0.7, "height": 3 }
		        },
		        { "geo": "cylinder",
		          "params": { "rotation": [0, 0, math.pi/2], "center": pos, "radius": 0.7, "height": 3 }
		        },
		      ]
		    }
		  ]
		}
