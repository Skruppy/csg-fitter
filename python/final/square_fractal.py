## This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0

import math
from test_final_base import FinalCreator

class SquareFractalCreator(FinalCreator):
	configurations = {
		'3-4': {'sides': 3, 'depth': 4},
		#'3-6': {'sides': 3, 'depth': 6},
		#'6-4': {'sides': 6, 'depth': 4},
	}
	
	
	def __init__(self, sides=3, depth=5):
		self.sides = sides
		self.depth = depth
	
	
	def build(self):
		if   self.sides == 1: self._directions = [(1, 0, 0)]
		elif self.sides == 2: self._directions = [(1, 0, 0), (0, 1, 0)]
		elif self.sides == 3: self._directions = [(1, 0, 0), (0, 1, 0), (0, 0, 1)]
		elif self.sides == 4: self._directions = [(1, 0, 0), (-1, 0, 0), (0, 1, 0), (0, -1, 0)]
		elif self.sides == 6: self._directions = [(1, 0, 0), (-1, 0, 0), (0, 1, 0), (0, -1, 0), (0, 0, 1), (0, 0, -1)]
		else: raise Exception('Unsupported number of sides')
		
		self._geos = []
		self._cube(0, 0, 0, 5, self.depth)
		return {
			"op": "union",
			"childs": self._geos,
		}
	
	
	@property
	def sample_rate(self):
		width = 10 / 2**self.depth
		side_surface = width**2
		return 50 / side_surface * 5
	
	
	def _cube(self, x, y, z, size, depth):
		self._geos.append({
			"geo": "cube",
			"params": {
				"rotation": [0, 0, 0],
				"center": [x, y, z],
				"radius": [size, size, size],
			},
		})
		
		if depth:
			for o_x, o_y, o_z in self._directions:
				self._cube(
					x+size*o_x,
					y+size*o_y,
					z+size*o_z,
					size/2,
					depth-1,
				)
