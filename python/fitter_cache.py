## This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0

from typing import List
import dataclasses
import hashlib
import json
import os
import os.path as op
import pathlib
import shell_worker
import shlex
import subprocess
import sys
import tempfile
import util
from pathlib import Path



class FitterCache:
	def __init__(self, producer):
		self._production_strategy = producer
		self._hit_items = []
		self._missed_items = []
		self._base_path = op.dirname(util.script_dir())
	
	
	def request(self, args, n=1, rebuild=False):
		inputs, outputs = _find_outputs(args)
		hash = self._lookup_hash(args, inputs)
		
		items = []
		for i in range(n):
			## Don't compute hash inside CacheItem, since args don't change with iterations
			item = CacheItem(args=args, hash=hash, nr=i, inputs=inputs, outputs=outputs)
			if item.is_cached and not rebuild:
				self._hit_items.append(item)
			else:
				util.mkdirs(item.asset_path(''))
				self._missed_items.append(item)
			
			items.append(item)
		
		return items
	
	
	def fetch(self):
		requet_cnt = len(self._hit_items) + len(self._missed_items)
		miss_rate = len(self._missed_items) / requet_cnt if requet_cnt else 0
		print(f'{requet_cnt} requests, {100*miss_rate}% missed')
		if len(self._missed_items) > 0:
			return self._production_strategy.produce(self._missed_items)
		else:
			return False
	
	def _compute_args_hash(self, args, inputs):
		## Calculate a simple hash first, only consisting of (1) command
		## arguments and (2) input files, which will act as the cache key to
		## lookup the hash wich is more complex/expensive to compute.
		m = hashlib.sha1()
		m.update(' '.join(map(shlex.quote, args)).encode('utf-8'))
		for input in sorted(inputs):
			m.update(util.read_file(input).encode('utf-8'))
		return m.hexdigest()
	
	
	## Returns a hash consisting of (1a) pipeline and (1b) processor
	## parameters, as well as (2) input files.
	def _compute_hash(self, args, inputs):
		key = {'processors': {}, 'inputs': {}}
		
		## Convert args to processor configurations
		cache_stats_path = '/tmp/csgfitter_fixer.json'
		csgfitter_args = ['./csgfitter-static', '-p', '-s', cache_stats_path, *args]
		subprocess.run(csgfitter_args, cwd=self._base_path, check=True)
		stats = util.read_json(cache_stats_path)
		os.remove(cache_stats_path)
		
		## Add processor configurations to cache key
		for processor in stats['processors'].values():
			key['processors'][processor['name']] = {k: processor[k] for k in ['inputs', 'name', 'outputs', 'properties', 'type']}
		
		## Add input files to cache key
		missing_inputs = list(filter(lambda x: not op.isfile(x), inputs))
		if missing_inputs:
			msg = f'Missing input files: {csgfitter_args}'
			for missing_input in missing_inputs:
				msg += f'\n  {missing_input}'
			raise Exception(msg)
		
		for input in inputs:
			with open(input, 'rb') as f:
				key['inputs'][input] = util.sha1sum(f)
		
		## Assemble ans store full text key
		content = json.dumps(key, indent='\t', sort_keys=True) + '\n'
		
		## Return hash of fulltext key
		return (util.sha1sum(content), content)
	
	
	def _lookup_hash(self, args, input):
		args_hash = self._compute_args_hash(args, input)
		cache_path = op.join(self._base_path, 'results_cache')
		cache_file = op.join(cache_path, args_hash)
		
		## Fast way: Try to lookup hash in cache
		if op.isfile(cache_file):
			return util.read_file(cache_file).strip()
		
		## Slow way: Compute hash (and store in cache)
		hash, key = self._compute_hash(args, input)
		util.mkdirs(cache_path)
		util.write_file(hash, cache_file)
		
		results_path = op.join(self._base_path, 'results', hash)
		results_file = op.join(results_path, 'key.json')
		util.mkdirs(results_path)
		if not op.isfile(results_file):
			util.write_file(key, results_file)
		elif key != util.read_file(results_file):
			raise Exception(f'Keys in "{results_file}" differs from new key:\n{key}')
		
		return hash



## http://hlit.jinr.ru/en/user_guide_eng/
class SlurmProduction:
	def produce(self, items):
		with open('slurm.sh', 'w') as f:
			## Header
			f.write(util.realign(f'''
				#!/bin/bash
				## Copy files to cluster
				##   $ rsync -r csgfitter-static tests results slurm.sh lmu:csgfitter
				## 
				## Put evaluation batch job into queue and watch
				##   $ sbatch -p Gobi --time=02:30:00 --mail-user=???@??? --mail-type=ALL slurm.sh
				##   $ watch squeue -u $(whoami)
				## 
				## Fetch results
				##   $ rsync -r lmu:csgfitter/results .
				
				#SBATCH --array=0-{len(items)-1}
				#SBATCH --export=NONE
				#SBATCH --ntasks=1
				#SBATCH --mem-per-cpu=100mb
				
				set -e
				
				echo "$(date "+%F %H:%M:%S") @ $(hostname -f): Job $SLURM_ARRAY_TASK_ID started"
				
				case $SLURM_ARRAY_TASK_ID in
			'''))
			
			## Jobs
			for i, item in enumerate(items):
				call = ' '.join(map(shlex.quote, item.expanded_args))
				f.write(util.realign(f'''
					{i})
						echo RUN {call}
						mkdir -p {item.asset_path('')}
						{call} >{item.log_path} 2>&1
						;;
				'''))
			
			## Footer
			f.write(util.realign(f'''
				esac
				
				echo "$(date "+%F %H:%M:%S") @ $(hostname -f): Job $SLURM_ARRAY_TASK_ID finished"
			'''))
		
		return len(items) > 0


class LocalProduction:
	def __init__(self, n=len(os.sched_getaffinity(0))):
		self._n = n
		self._base_path = op.dirname(util.script_dir())
	
	def produce(self, items):
		self._sh = shell_worker.ShellWorker(self._n)
		for item in items:
			self._sh.run(*item.expanded_args, cwd=self._base_path)
		self._sh.stop()
		return False


class JobFileProduction:
	def __init__(self, path='slurm.json'):
		self.path = path
	
	def produce(self, items):
		jobs = []
		
		for item in items:
			jobs.append({
				'hash': item.hash,
				'nr': item.nr,
				'args': item.expanded_args,
				'log_path': item.log_path,
			})
		
		with open(self.path, 'w') as f:
			json.dump(jobs, f, sort_keys=True, indent='\t')
		
		return len(items) > 0


@dataclasses.dataclass
class CacheItem:
	args: List[str]
	hash: str
	nr: int
	inputs: List[str]
	outputs: List[str]
	
	@property
	def base_path(self):
		return f'results/{self.hash}/{self.nr}'
	
	def asset_path(self, file_name):
		return f'{self.base_path}/output/{file_name}'
	
	@property
	def stats_path(self):
		return f'{self.base_path}/stats.json'
	
	@property
	def log_path(self):
		return f'{self.base_path}/csgfitter.log'
	
	@property
	def stats(self):
		return util.read_json(self.stats_path)
	
	@property
	def is_cached(self):
		(Path(self.base_path) / 'last_requested').touch()
		return op.exists(self.stats_path)
	
	
	@property
	def expanded_args(self):
		def foo(s):
			if s.startswith('IN:'):
				return s[3:]
			elif s.startswith('OUT:'):
				return self.asset_path(s[4:])
			else:
				assert(False)
		
		args = ['./csgfitter-static', '-v', '-s', self.stats_path]
		for arg in self.args:
			args.append(_block_parse(arg, foo))
		return args



def _block_parse(s, fn):
	new_s = ''
	normal_start = 0
	block_start = None
	for i, c in enumerate(s):
		if c == '<':
			if block_start is None:
				block_start = i+1
				new_s += s[normal_start:i]
			else:
				raise Exception(f'"{s}": Cascaded block at {i+1}')
		elif c == '>':
			if block_start is not None:
				block = s[block_start:i]
				new_s += fn(block)
				normal_start = i+1
				block_start = None
			else:
				raise Exception(f'"{s}": No block to close here {i+1}')
	
	if block_start is not None:
		raise Exception(f'"{s}" has unclosed an bracket')
	
	new_s += s[normal_start:]
	
	return new_s


def _find_outputs(args):
	inputs = set()
	outputs = set()
	
	def fn(s):
		if s.startswith('IN:'):
			inputs.add(s[3:])
		elif s.startswith('OUT:'):
			file_name = s[4:]
			if file_name != op.basename(file_name):
				raise Exception(f'"{file_name}": Invalid file name (no directories allowed)')
			
			outputs.add(file_name)
		else:
			raise Exception(f'"{s}": Invalid prefix (missing "OUT:"?)')
		
		return ''
	
	for arg in args:
		_block_parse(arg, fn)
	
	return (inputs, outputs)
