#!/usr/bin/env python3
## This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0

import shell_worker
import util
import sys
import fitter_cache
import matplotlib
import numpy as np
import collections
import os
import os.path as op
import math
import web
import shutil
import PIL
import matplotlib.pyplot as plt
import scipy.stats
import pprint
import itertools
import random
import multiprocessing
import test_evo
import common
import subprocess



selectors = list(common.descriptions.selectors.keys())
rep_strats = list(common.descriptions.rep_strats.keys())

## Functions to convert properties to colors. Function arguments:
## mean of configuration, lower bound of all means, upper bound of all means
#default_color_fn    = lambda m, l, u: matplotlib.colors.to_hex(plt.cm.pink( (m-l)/(u-l) ))
#rate_color_fn       = lambda m, l, u: matplotlib.colors.to_hex(plt.cm.viridis((max(0, m)/u) ** 0.35)) ## A gamma correction, which just looks good.
#skew_color_fn       = lambda m, l, u: matplotlib.colors.to_hex(plt.cm.PiYG( 0.5 + m/max(abs(l),abs(u))/2 ))
#uniqueness_color_fn = lambda m, l, u: matplotlib.colors.to_hex(plt.cm.RdYlBu( m ))
#quality_color_fn    = lambda m, l, u: matplotlib.colors.to_hex(plt.cm.viridis( m/u ))
#score_fn            = lambda m, l, u: matplotlib.colors.to_hex(plt.cm.viridis((1-(m-l)/(u-l)) ** 2)) ## A gamma correction, which just looks good.
rmsd_fittnes_test   = lambda x: 'rmsd' in x

web_dir = op.join(util.script_dir(), '../public/selector-matrix')
databook_dir = op.join(util.script_dir(), '../databook/selector-matrix')
util.mkdirs(web_dir)
util.mkdirs(databook_dir)

def outlier_test(unfiltered_props):
	_, bad_rmsd = iq_outlier(unfiltered_props['rmsd']['values'], 0)
	hl_to_low, hl_to_high = iq_outlier(unfiltered_props['hl']['values'], 1.5)
	
	a_to_low, a_to_high = iq_outlier(unfiltered_props['a']['values'], 2)
	b_to_low, b_to_high = iq_outlier(unfiltered_props['b']['values'], 2)
	
	outlier_ids = bad_rmsd & (hl_to_low | hl_to_high) | a_to_low | a_to_high | b_to_low | b_to_high
	return list(map(lambda x: x in outlier_ids, range(len(unfiltered_props['rmsd']['values']))))


def copy_diagram(test, fig_name, model_name, key):
	src            = f'{test.base_path}/evo_stats/figure-{fig_name}.png'
	databook_src   = f'{test.base_path}/evo_stats/figure-{fig_name}.pdf'
	path           = f'{web_dir}/{model_name}-{key[0]}-{key[1]}-fig-{fig_name}.png'
	thumbnail_path = f'{web_dir}/{model_name}-{key[0]}-{key[1]}-fig-{fig_name}-small.png'
	databook_path  = f'{databook_dir}/{model_name}-{key[0]}-{key[1]}-fig-{fig_name}.pdf'
	
	if util.outdated(path, src):
		shutil.copy2(src, path)
	
	if util.outdated(databook_path, databook_src):
		shutil.copy2(databook_src, databook_path)
	
	if util.outdated(thumbnail_path, path):
		make_thumbnail(path, thumbnail_path)
		
	return util.Dobject(image=os.path.basename(path), thumbnail=os.path.basename(thumbnail_path))


def plot_rmds(model, key, out_file):
	param = model.tbl[key].unfiltered_props['rmsd']
	
	fig = matplotlib.pyplot.figure(constrained_layout=True, figsize=(5, 4))
	gs = fig.add_gridspec(2, 1, height_ratios=[9,1])
	
	#### Upper part (historgram + normal distribution)
	ax = fig.add_subplot(gs[0,0])
	
	ax.set_xlim(0, model.filtered_param_maxima['rmsd']*1.05)
	
	ax.set_ylabel('Probability')
	
	ax.hist(param['values'], 50, ax.get_xlim(), density=True)
	
	x = np.linspace(*ax.get_xlim(), 100)
	plt.plot(x, scipy.stats.norm.pdf(x, param['values'].mean(), param['values'].std()))
	
	ax.grid(True)
	
	#### Lower part (boxplot)
	ax = fig.add_subplot(gs[1,0], sharex=ax)
	
	ax.set_xlabel('RMSD')
	ax.set_yticks([])
	
	ax.boxplot(
		param['values'],
		sym='+',
		manage_ticks=False,
		patch_artist=True,
		boxprops={'facecolor': 'grey'},
		vert=False,
	)
	ax.grid(True, axis='x')
	
	#### Write out
	common.save_fig(fig, out_file, bbox_inches='tight', pad_inches=0.03)


def plot_fit(model, key, out_file):
	cell = model.tbl[key]
	props = cell.unfiltered_props
	fig = matplotlib.pyplot.figure(constrained_layout=True, figsize=(5, 5))
	ax = fig.add_subplot()
	
	ax.set_xlabel('a')
	ax.set_ylabel('b')
	util.set_limits(
		ax,
		(model.unfiltered_param_minima['a'], model.unfiltered_param_maxima['a']),
		(model.unfiltered_param_minima['b'], model.unfiltered_param_maxima['b']),
	)
	
	syms=list(map(lambda x: 'x' if x else 'o', cell.is_outlier))
	if 'offset' in props:
		l = model.unfiltered_param_minima['offset']
		u = model.unfiltered_param_maxima['offset']
		colors=list(map(lambda v: plt.cm.RdYlBu((v-l)/(u-l)), props['offset']['values']))
	else:
		colors=list(map(lambda x: 'red' if x else 'blue', cell.is_outlier))
	
	for x, y, c, s in zip(props['a']['values'], props['b']['values'], colors, syms):
		ax.scatter(x, y, color=c, marker=s, alpha=0.5)
	ax.grid(True)
	
	common.save_fig(fig, out_file, bbox_inches='tight', pad_inches=0.03)


def plot_params(model, key, out_file):
	cell = model.tbl[key]
	props = cell.unfiltered_props
	fig = matplotlib.pyplot.figure(constrained_layout=True, figsize=(5, 0.85*len(props)))
	gs = fig.add_gridspec(len(props), 1)
	
	syms=list(map(lambda x: 'o' if x else 'o', cell.is_outlier))
	colors=list(map(lambda x: 'red' if x else 'blue', cell.is_outlier))
	
	random.seed(42)
	
	i = 0
	for param_name in sorted(props):
		param = props[param_name]
		ax = fig.add_subplot(gs[i,0])
		ax.set_facecolor((0.95, 0.95, 0.95))
		for spine in ax.spines.values():
			spine.set_edgecolor('white')
		ax.set_xlabel(param_name)
		ax.set_yticks([])
		ax.set_ylim(0, 2)
		ax.boxplot(
			param['values'],
			sym='',
			manage_ticks=False,
			patch_artist=True,
			boxprops={'facecolor': (0.8, 0.8, 0.8)},
			vert=False,
			widths=1.85,
			zorder=1,
		)
		i += 1
		
		for x, c, s in zip(props[param_name]['values'], colors, syms):
			ax.scatter(x, random.uniform(0.4, 1.6), color=c, marker=s, alpha=0.5, edgecolors='none', s=30, zorder=2)
	
	common.save_fig(fig, out_file, bbox_inches='tight')


def example_b(filtered_props, prop_name):
	values = filtered_props[prop_name]['values']
	l = sorted(enumerate(values), key=lambda x: x[1])
	return l[int(len(values) / 2)][0]


fit_model_parameter_diagrams = [
	{'name': 'rmds', 'fn': plot_rmds, 'latex_label': 'Histogramm und Box-Plot sowie geschätzte Normalverteilung (orange) der Wurzeln aus den mittleren quadratischen Abweichungen der Einpassungen.'},
	{'name': 'fit', 'fn': plot_fit, 'latex_label': 'Streudiagramm der Kennzahlen $a$ und $b$ (rot: Ausreißer).'},
	{'name': 'params', 'fn': plot_params, 'latex_label': 'Kombinierter Scatter- und Box-Plot für jede Kennzahl einer jeden Kombination (rot: Ausreißer).'},
]

scatter_model_parameter_diagrams = [
	{'name': 'params', 'fn': plot_params, 'latex_label': 'Kombinierter Scatter- und Box-Plot für jede Kennzahl einer jeden Kombination (rot: Ausreißer).'},
]

config = util.dobjectify({
	'models': {
		'fit_ab_all_med': {
			'test_fittnes_fn': rmsd_fittnes_test,
			'parameter_diagrams': fit_model_parameter_diagrams,
			'get_example_fn': lambda x: example_b(x, 'b'),
			'outliers_fn': outlier_test,
			'examples': [
				{'name': 'diagram', 'fig_name': 'fit_ab_all_med'},
			],
		},
		'fit_ab_all_avg': {
			'test_fittnes_fn': rmsd_fittnes_test,
			'parameter_diagrams': fit_model_parameter_diagrams,
			'get_example_fn': lambda x: example_b(x, 'b'),
			'outliers_fn': outlier_test,
			'examples': [
				{'name': 'diagram', 'fig_name': 'fit_ab_all_avg'},
			],
		},
		'fit_ab_all_max': {
			'test_fittnes_fn': rmsd_fittnes_test,
			'parameter_diagrams': fit_model_parameter_diagrams,
			'get_example_fn': lambda x: example_b(x, 'b'),
			'outliers_fn': outlier_test,
			'examples': [
				{'name': 'diagram', 'fig_name': 'fit_ab_all_max'},
			],
		},
		'fit_ab_parents_med': {
			'test_fittnes_fn': rmsd_fittnes_test,
			'parameter_diagrams': fit_model_parameter_diagrams,
			'get_example_fn': lambda x: example_b(x, 'b'),
			'outliers_fn': outlier_test,
			'examples': [
				{'name': 'diagram', 'fig_name': 'fit_ab_parents_med'},
			],
		},
		'fit_abc_parents_med': {
			'test_fittnes_fn': rmsd_fittnes_test,
			'parameter_diagrams': fit_model_parameter_diagrams,
			'get_example_fn': lambda x: example_b(x, 'b'),
			'outliers_fn': outlier_test,
			'examples': [
				{'name': 'diagram', 'fig_name': 'fit_abc_parents_med'},
			],
		},
		'fit_ab_parants_avg': {
			'test_fittnes_fn': rmsd_fittnes_test,
			'parameter_diagrams': fit_model_parameter_diagrams,
			'get_example_fn': lambda x: example_b(x, 'b'),
			'outliers_fn': outlier_test,
			'examples': [
				{'name': 'diagram', 'fig_name': 'fit_ab_parants_avg'},
			],
		},
		'skew': {
			'parameter_diagrams': scatter_model_parameter_diagrams,
			'get_example_fn': lambda x: example_b(x, 'mean'),
			'examples': [
				{'name': 'diagram', 'fig_name': 'skew'},
			],
		},
		'uniqueness': {
			'parameter_diagrams': scatter_model_parameter_diagrams,
			'get_example_fn': lambda x: example_b(x, 'mean'),
			'examples': [
				{'name': 'diagram', 'fig_name': 'uniqueness'},
			],
		},
		'best_score': {
			'parameter_diagrams': scatter_model_parameter_diagrams,
			'get_example_fn': lambda x: example_b(x, '149'),
			'examples': [],
		},
	},
	'key_examples': [
		{
			'title': 'Detailed Distribution',
			'fig': 'raw_points',
			'model': 'fit_ab_all_med',
			'tbl_dsc': None,
		},
		{
			'title': 'Distribution overview',
			'fig': 'raw_overview',
			'model': 'fit_ab_all_med', ## Just use 'rate' prop, as it's the same for all
			'tbl_dsc': None,
		},
		{
			'title': 'Score skew',
			'fig': 'skew',
			'model': 'fit_ab_all_med',
			'tbl_dsc': lambda m, k: f'''
				<div style="background: {m["props"]["rate"][k]["color"]}70">
					Rate: <span title="Rate mean">{m["props"]["rate"][k]["mean"]:.2f}</span>
					<small title="Rate standard deviation" class="text-muted">±{m["props"]["rate"][k]["std"]:.2f}</small>
				</div>
			''',
		},
		{
			'title': 'Score development',
			'fig': 'ranks',
			'model': 'fit_ab_all_med',
			'tbl_dsc': lambda m, k: f'''
				<div style="background: {m["props"]["rate"][k]["color"]}70">
					Rate: <span title="Rate mean">{m["props"]["rate"][k]["mean"]:.2f}</span>
					<small title="Rate standard deviation" class="text-muted">±{m["props"]["rate"][k]["std"]:.2f}</small>
				</div>
			''',
		},
		{
			'title': 'Uniqueness',
			'fig': 'uniqueness',
			'model': 'fit_ab_all_med',
			'tbl_dsc': lambda m, k: f'''
				<div style="background: {m["props"]["rate"][k]["color"]}70">
					Rate: <span title="Rate mean">{m["props"]["rate"][k]["mean"]:.2f}</span>
					<small title="Rate standard deviation" class="text-muted">±{m["props"]["rate"][k]["std"]:.2f}</small>
				</div>
			''',
		},
		{
			'title': 'Fit model 2: <math xmlns="http://www.w3.org/1998/Math/MathML"><mi>a</mi><mo>&#xB7;</mo><msup><mi>e</mi><mrow><mi>b</mi><mo>&#xB7;</mo><mi>x</mi></mrow></msup></math>',
			'fig': 'fit_ab_parents_med',
			'model': 'fit_abc_parents_med',
			'tbl_dsc': lambda m, k: f'''
				<div style="background: {m["props"]["rate"][k]["color"]}70">
					<span title="Rate mean">{m["props"]["rate"][k]["mean"]:.2f}</span>
					<small title="Rate standard deviation" class="text-muted">±{m["props"]["rate"][k]["std"]:.2f}</small>
				</div>
			''',
		},
		{
			'title': 'Fit model 3: <math xmlns="http://www.w3.org/1998/Math/MathML"><mi>a</mi><mo>&#xB7;</mo><msup><mi>e</mi><mrow><mi>b</mi><mo>&#xB7;</mo><mi>x</mi></mrow></msup></math>, based on best score',
			'fig': 'fit_ab_all_max',
			'model': 'fit_ab_all_max',
			'tbl_dsc': lambda m, k: f'''
				<div style="background: {m["props"]["rate"][k]["color"]}70">
					<span title="Rate mean">{m["props"]["rate"][k]["mean"]:.2f}</span>
					<small title="Rate standard deviation" class="text-muted">±{m["props"]["rate"][k]["std"]:.2f}</small>
				</div>
			''',
		},
		{
			'title': 'Fit old model 1: <math xmlns="http://www.w3.org/1998/Math/MathML"><mi>a</mi><mo>&#xB7;</mo><msup><mi>e</mi><mrow><mi>b</mi><mo>&#xB7;</mo><mi>x</mi></mrow></msup><mo>+</mo><mi>c</mi></math>',
			'fig': 'fit_abc_parents_med',
			'model': 'fit_ab_parents_med',
			'tbl_dsc': lambda m, k: f'''
				<div style="background: {m["props"]["rate"][k]["color"]}70">
					<span title="Rate mean">{m["props"]["rate"][k]["mean"]:.2f}</span>
					<small title="Rate standard deviation" class="text-muted">±{m["props"]["rate"][k]["std"]:.2f}</small>
				</div>
				<div style="background: {m["props"]["offset"][k]["color"]}70">
					<span title="Offset mean">{m["props"]["offset"][k]["mean"]:.2f}</span>
					<small title="Offset standard deviation" class="text-muted">±{m["props"]["offset"][k]["std"]:.2f}</small>
				</div>
			''',
		},
	],
})






def main():
	util.mkdirs(web_dir)
	
	## Get raw data of all combinations and their models
	test_run_combinations = run_tests(selectors, rep_strats, 50)
	run_stats_lists = load_stats(test_run_combinations)
	## run_stats_lists = {
	##   (x1, y1): [ { m: { p: 0.1, q: 0.2, rmsd: 0.1, base_path: "" }, n: { p: 0.1, q: 0.2, base_path: "" } }, ... ],
	##   (x1, y2): [ { m: { p: 0.1, q: 0.2,            base_path: "" }, n: { p: 0.1, q: 0.2, base_path: "" } }, ... ],
	##   (x2, y1): [ { m: { p: 0.1, q: 0.2, rmsd: 0.1, base_path: "" }, n: { p: 0.1, q: 0.2, base_path: "" } }, ... ],
	##   (x2, y2): [ { m: { p: 0.1, q: 0.2, rmsd: 0.1, base_path: "" }, n: { p: 0.1, q: 0.2, base_path: "" } }, ... ],
	## }
	
	models = {}
	for model_name, model_conf in config.models.items():
		models[model_name] = model = util.Dobject(tbl={}, n_min={})
		for prefix, postfix in itertools.product(
			['unfiltered_param', 'filtered_param', 'filtered_param_mean'],
			['minima', 'maxima']
		):
			model[f'{prefix}_{postfix}'] = {}
		
		## Fill table of configuration combinations
		for key in run_stats_lists.keys():
			model.tbl[key] = cell = util.Dobject(
				parameter_diagrams = {},
				
				## * run_stats_lists
				## -------------------------- Fit filter (RMSD)
				## |\
				## | * unfitted_tests
				unfitted_tests = [],
				count_unfitted = None,
				## |
				## * fitted_tests
				fitted_tests = [],
				unfiltered_props = {},
				is_outlier = [],
				## -------------------------- Outlier filter
				## |\
				## | * outlier_tests
				outlier_tests = [],
				count_outlier = None,
				## |
				## * valid_tests
				valid_tests = [],
				filtered_props = {},
				count_valid = None,
				## -------------------------- Representant selector (median)
				## |
				## * example_test
				example_test = None,
				examples = {},
			)
			
			## Rough seperation of test results
			for test in run_stats_lists[key]:
				if model_conf.get('test_fittnes_fn', lambda x: True)(test[model_name]):
					cell.fitted_tests.append(test)
				else:
					cell.unfitted_tests.append(test)
			
			## Collect all values of all props
			for param_name in common.descriptions.models[model_name].props.keys():
				values = np.fromiter(map(lambda x: x[model_name][param_name], cell.fitted_tests), float)
				cell.unfiltered_props[param_name] = param = util.Dobject(
					min = values.min(),
					max = values.max(),
					values = values,
				)
			
			## Find outlier
			outliers_fn = model_conf.get('outliers_fn', lambda x: [False]*len(cell.fitted_tests))
			cell.is_outlier = outliers_fn(cell.unfiltered_props)
			for is_outlier, test in zip(cell.is_outlier, cell.fitted_tests):
				if not is_outlier:
					cell.valid_tests.append(test)
				else:
					cell.outlier_tests.append(test)
			
			## Create stats of good test (non outliers)
			for param_name in common.descriptions.models[model_name].props.keys():
				values = np.fromiter(map(lambda x: x[model_name][param_name], cell.valid_tests), float)
				
				## https://www.graphpad.com/support/faqid/959/
				## https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.kstest.html
				## https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.shapiro.html
				## https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.normaltest.html
				## Shapiro good for small n: https://de.wikipedia.org/wiki/Shapiro-Wilk-Test
				## KS Test not suitable for small N & complicated to implement
				#ks_d, ks_p = scipy.stats.kstest(values, 'norm', args=(values.mean(), values.std()))
				#ks_crit = scipy.stats.ksone.ppf(1-0.05/2, len(values))
				#norm_s, norm_p = scipy.stats.normaltest(values)
				shapiro_w, shapiro_p = scipy.stats.shapiro(values)
				
				cell.filtered_props[param_name] = param = util.Dobject(
					mean = values.mean(),
					std = values.std(),
					min = values.min(),
					max = values.max(),
					values = values,
					#ks_d = ks_d,
					#ks_p = ks_p,
					#ks_crit = ks_crit,
					shapiro_w = shapiro_w,
					shapiro_p = shapiro_p,
					#norm_s = norm_s,
					#norm_p = norm_p,
					#color = <set in next loop>
					#n_min = <set in next loop>
				)
			
			## Find good representative
			get_example = model_conf.get('get_example_fn')
			if get_example is not None:
				cell.example_test = cell.valid_tests[get_example(cell.filtered_props)]
				for example_conf in model_conf.get('examples', []):
					cell.examples[example_conf.name] = copy_diagram(
						cell.example_test, example_conf.fig_name, model_name, key
					)
			
			## Count
			cell.count_unfitted = len(cell.unfitted_tests)
			cell.count_outlier = len(cell.outlier_tests)
			cell.count_valid = len(cell.valid_tests)
			assert(cell.count_unfitted + cell.count_outlier + cell.count_valid == len(run_stats_lists[key]))
		
		
		## Collect minima and maxima
		for param_name in common.descriptions.models[model_name].props.keys():
			for dst, aggregate, container, item in [
				('unfiltered_param_minima',    min, 'unfiltered_props', 'min'),
				('unfiltered_param_maxima',    max, 'unfiltered_props', 'max'),
				('filtered_param_mean_minima', min, 'filtered_props',   'mean'),
				('filtered_param_mean_maxima', max, 'filtered_props',   'mean'),
				('filtered_param_minima',      min, 'filtered_props',   'min'),
				('filtered_param_maxima',      max, 'filtered_props',   'max'),
			]:
				values = list(map(lambda cell: cell[container][param_name][item], model.tbl.values()))
				model[dst][param_name] = aggregate(values)
		
		## Per model parameter range
		confidence = .95
		z = scipy.stats.norm.ppf((1+confidence)/2)
		for param_name, param_conf in common.descriptions.models[model_name].props.items():
			model.n_min[param_name] = meta = util.Dobject()
			#minimum = model.filtered_param_mean_minima[param_name]
			#maximum = model.filtered_param_mean_maxima[param_name]
			means = list(map(lambda x: x.filtered_props[param_name].mean, model.tbl.values()))
			meta.min = minimum = np.percentile(means, 25)
			meta.max = maximum = np.percentile(means, 75)
			meta.span = span = maximum - minimum
			meta.e = e = span / 50 / 2
			
			for cell in model.tbl.values():
				filtered_param = cell.filtered_props[param_name]
				
				## Cell color
				if filtered_param.mean is None:
					filtered_param.color = '#ffffff'
				else:
					norm = param_conf.colorize.norm(minimum, maximum)
					#norm(filtered_param.mean)
					foo = matplotlib.cm.ScalarMappable(norm, param_conf.colorize.cmap)
					filtered_param.color = matplotlib.colors.to_hex(foo.to_rgba(filtered_param.mean))
				
				## Min n
				filtered_param.n_min_float = z**2 * filtered_param.std**2 / e**2
				filtered_param.n_min = math.ceil(filtered_param.n_min_float)
		
		## Parameter diagrams
		for key in run_stats_lists.keys():
			cell = model.tbl[key]
			for diagram_conf in model_conf.get('parameter_diagrams', []):
				for ext in ['pdf', 'png']: ## 'png' has to last, so cell.parameter_diagrams is correct
					file_name           = f'{model_name}-{key[0]}-{key[1]}-{diagram_conf.name}.{ext}'
					thumbnail_file_name = f'{model_name}-{key[0]}-{key[1]}-{diagram_conf.name}-small.{ext}'
					
					path = common.fig_path(f'selector-matrix/{file_name}')
					thumbnail_path = common.fig_path(f'selector-matrix/{thumbnail_file_name}')
					
					if not op.isfile(path):
						diagram_conf.fn(model, key, f'selector-matrix/{file_name}')
					
					if ext == 'png' and util.outdated(thumbnail_path, path):
						make_thumbnail(path, thumbnail_path)
				
				cell.parameter_diagrams[diagram_conf.name] = util.Dobject(
					image=file_name,
					thumbnail=thumbnail_file_name,
				)
		
		matplotlib.pyplot.close('all')
	
	key_examples = {key: {} for key in run_stats_lists}
	for key_example_config in config.key_examples:
		for key in run_stats_lists.keys():
			key_examples[key][key_example_config.fig] = copy_diagram(
				models['fit_ab_all_med'].tbl[key].example_test, key_example_config.fig, 'fit_ab_all_med', key
			)
	
	### Output
	output_html(models, key_examples)
	
	common.save_pipeline(test_run_combinations, 'selector-matrix/pipeline.pdf')
	
	## Correlation
	foo = []
	for model_name, model_config in config.models.items():
		for prop_name in common.descriptions.models[model_name].props.keys():
			foo.append((len(foo), model_name, prop_name))
	n = len(foo)
	
	bar = np.array([[None]*len(foo)]*len(foo), float)
	for (idx_a, model_a_name, prop_a_name), (idx_b, model_b_name, prop_b_name) in itertools.product(foo, foo):
		if idx_a > idx_b:
			continue
		list_a = best_combinations(models, model_a_name, prop_a_name)
		list_b = best_combinations(models, model_b_name, prop_b_name)
		tau, tau_p = scipy.stats.kendalltau(list_a, list_b)
		#rho = scipy.stats.spearmanr(list_a, list_b)[0]
		
		if tau_p <= 0.05:
			bar[idx_a][idx_b] = tau
			bar[idx_b][idx_a] = tau
	
	baz = list(map(lambda x: f'{x[1]} - {x[2]}', foo))
	
	n_split = int(n/2)
	plot_kendall(bar, baz, 'correlation', 'png', 0, n, 0, n, 23)
	plot_kendall(bar, baz, 'correlation', 'pdf', 0, n, 0, n, 23)
	plot_kendall(bar, baz, 'correlation-1', 'pdf', 0, n_split, 0, n_split, 11)
	plot_kendall(bar, baz, 'correlation-2', 'pdf', 0, n_split, n_split, n, 11)
	plot_kendall(bar, baz, 'correlation-3', 'pdf', n_split, n, n_split, n, 11)


def plot_kendall(bar, baz, name, ext, xa, xb, ya, yb, size):
	n = xb - xa
	assert(n == yb - ya)
	
	fig = matplotlib.pyplot.figure(constrained_layout=True, figsize=(size, size))
	ax = fig.add_subplot()
	ax.imshow(bar[xa:xb,ya:yb]**3, cmap='RdBu')
	
	## Labels
	ax.set_xticks(np.arange(n))
	ax.set_yticks(np.arange(n))
	ax.set_xticklabels(baz[ya:yb])
	ax.set_yticklabels(baz[xa:xb])
	ax.tick_params(
		top=True, bottom=True, labeltop=True, labelbottom=False,
		right=True, left=True, labelright=True, labelleft=False
	)
	plt.setp(ax.get_xticklabels(), rotation=40, ha='left', rotation_mode='anchor')
	
	## Grid
	ax.set_xticks(np.arange(n)+0.5, minor=True)
	ax.set_yticks(np.arange(n)+0.5, minor=True)
	ax.grid(which='minor', color=(.9, .9, .9), linestyle='-', linewidth=2)
	
	## Cell Text
	for i, j in itertools.product(range(xa, xb), range(ya, yb)):
		if bar[i,j] is not None:
			val = bar[i,j]
			ax.text(j-ya, i-xa, f'{val:.2f}', ha='center', va='center', color='black' if abs(val) < 0.6 else 'white')
	
	## Write images
	fig_name = f'selector-matrix/{name}.{ext}'
	fig_path = common.fig_path(fig_name)
	tmb_name = f'selector-matrix/{name}-small.{ext}'
	tmb_path = common.fig_path(tmb_name)
	
	if not op.isfile(fig_path):
		common.save_fig(fig, fig_name, bbox_inches='tight', pad_inches=0.03)
	
	if ext == 'png' and util.outdated(tmb_path, fig_path):
		make_thumbnail(fig_path, tmb_path, 600, 600)


def best_combinations(models, model_name, prop_name):
	tbl = models[model_name].tbl
	items = []
	for k in sorted(tbl.keys()):
		items.append(tbl[k].filtered_props[prop_name].mean)
	return items


## Output type: Dict[ Tuple[ep_strat_name, size], List[run] ]
def run_tests(selectors, rep_strats, n):
	producer = fitter_cache.JobFileProduction()
	#producer = fitter_cache.SlurmProduction()
	#producer = fitter_cache.LocalProduction()
	fc = fitter_cache.FitterCache(producer)
	
	## Get all combinations of replacement strategies and population sizes
	test_run_combinations = {}
	for selector in selectors:
		for rep_strat in rep_strats:
			if rep_strat == 'none':
				test_run_combinations[(selector, rep_strat)] = fc.request(csgfitter_evo_cmd([
					f'population_size=500',
					f'selector={selector}',
					f'mating_rate=1',
				]), n)
			elif rep_strat == 'cor_25':
				test_run_combinations[(selector, rep_strat)] = fc.request(csgfitter_evo_cmd([
					f'population_size=500',
					f'selector={selector}',
					f'mating_rate=1',
					f'crossover_rate=0.25',
				]), n)
			else:
				test_run_combinations[(selector, rep_strat)] = fc.request(csgfitter_evo_cmd([
					f'population_size=500',
					f'replacement_strategy={rep_strat}',
					f'selector={selector}',
				]), n)
	
	## Either terminate program, if manual user intervention is required, or return
	if fc.fetch():
		print('It\'s your turn!')
		sys.exit()
	
	return test_run_combinations


def load_stats_task(job):
	print(f'{job["i"]/job["n"]*100:.2f}% ({job["i"]}/{job["n"]}): {job["args"]}')
	test_evo.analyze(*job["args"])
	matplotlib.pyplot.close('all')


## Input type: Dict[ Tuple[rep_strat_name, size], List[run] ]
## Output type: Dict[ Tuple[rep_strat_name, size], List[stats] ] ]
def load_stats(test_run_combinations):
	## Evaluate stats of each run, if not already done
	pool = multiprocessing.Pool(3)
	
	jobs = []
	for runs in test_run_combinations.values():
		for run in runs:
			evo_base_path = op.join(run.base_path, 'evo_stats')
			extracted_stats_path = op.join(evo_base_path, 'stats.json')
			if not op.isfile(extracted_stats_path):
			#if True:
				util.mkdirs(evo_base_path)
				jobs.append({
					'args': [run.stats_path, evo_base_path, ['png', 'pdf'], set([]), set([])],
				})
	
	for i, job in enumerate(jobs):
		job['i'] = i+1
		job['n'] = len(jobs)
	
	pool.map(load_stats_task, jobs)
	
	## Load stats form json file
	run_stats_lists = {}
	for key, runs in test_run_combinations.items():
		run_stats_lists[key] = []
		for run in runs:
			run_stats = util.dobjectify(util.read_json(op.join(run.base_path, 'evo_stats', 'stats.json')))
			run_stats.base_path = run.base_path
			run_stats_lists[key].append(run_stats)
	
	return run_stats_lists



def csgfitter_evo_cmd(evo_args=None, eval_args=None, sample_rate=5, input='tests/renderings/gecco19.json'):
	if evo_args is None:
		evo_args = []
	if eval_args is None:
		eval_args = ['evaluator_12']
	
	return [
		'json_reader', f'path=<IN:{input}>', 'name=csg',
		'csg.', '!', 'surface_sampler', f'rate={sample_rate}', 'name=points', '!', 'fitter.reference',
		'csg.', '!', 'extract_primitives', '!', 'fitter.primitives',
		*eval_args, '!', 'fitter.evaluator',
		'evo', *evo_args, 'max_rounds=150', 'name=fitter',
		'fitter.', '!', 'json_writer', 'path=<OUT:result.json>',
		'fitter.', '!', 'scad_writer', 'path=<OUT:result.scad>',
		'fitter.', '!', 'gv_writer', 'path=<OUT:result.pdf>',
		'fitter.', '!', 'gv_writer', 'path=<OUT:result.gv>',
		'points.', '!', '.npc', 'pcd_writer', 'path=<OUT:points.pcd>',
	]


def fmt(x, i):
	if abs(x) < 100:
		return '{:.2f}'.format(x)
	else:
		return '{:.0f}'.format(x)


def output_html(models, key_examples):
	subthings = []
	for model_name, model_config in config.models.items():
		subthings.append({
			'title': common.descriptions.models[model_name].html_label,
			'url': f'selector-matrix/{ model_name }',
			'key': model_name,
		})
	
	for ext in ['html', 'tex']:
		web.render(f'selector-matrix.{ext}', f'selector-matrix/index.{ext}', {
			'title': 'Selector matrix',
			'selected_subthing_key': '',
			'subthings': subthings,
			'selectors': selectors,
			'rep_strats': rep_strats,
			'models': models,
			'key_examples': key_examples,
			'config': config,
			'descriptions': common.descriptions,
			'escapeLatex': util.escapeLatex,
		})
	
	for model_name, model_config in config.models.items():
		for ext in ['html', 'tex']:
			web.render(f'selector-model.{ext}', f'selector-matrix/{model_name}.{ext}', {
				'title': f'{ model_name } | Selector matrix',
				'selected_subthing_key': model_name,
				'subthings': subthings,
				'selectors': selectors,
				'rep_strats': rep_strats,
				'model': models[model_name],
				'model_name': model_name,
				'model_config': model_config,
				'key_examples': key_examples,
				'config': config,
				'descriptions': common.descriptions,
				'escapeLatex': util.escapeLatex,
				'fmt': fmt,
			})


def make_thumbnail(src, dst=None, max_width=250, max_height=250):
	root, ext = op.splitext(src)
	if dst is None:
		dst = f'{root}-small{ext}'
	
	img = PIL.Image.open(src)
	img.thumbnail((max_width, max_height), PIL.Image.ANTIALIAS)
	img.save(dst, ext[1:].upper())
	
	return dst



def iq_outlier(values, coutlier_constant=1.5, only_higher=False):
	upper_quartile = np.percentile(values, 75)
	lower_quartile = np.percentile(values, 25)
	iqr = (upper_quartile - lower_quartile) * coutlier_constant
	borders = (lower_quartile - iqr, upper_quartile + iqr)
	
	lower_outliers = set()
	higher_outliers = set()
	for idx, value in enumerate(values):
		if value <= borders[0]: lower_outliers.add(idx)
		if value >= borders[1]: higher_outliers.add(idx)
	
	return lower_outliers, higher_outliers


if __name__ == '__main__':
	main()
