## This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0

from PIL import Image, ImageDraw
import os
import math



def _color(intensity):
	return (0, int(intensity*255), int((1-intensity)*255))


class Diagram:
	img_size = (500, 400) ## x, y
	cloud_dim = (0, 1, 2) ## x, y, depth
	img_dir = (1, 1, 1)   ## x, y, depth (+1 means:: x: to the right / y: to the bottom / depth: towards back of screen)
	name = 'test'
	side = -1
	
	scale = 30            ## x & y
	
	offset = [0, 0]       ## x, y
	img_margin = [[20,20], [20,70]]
	
	
	def __init__(self, size=(500, 400), name='test', x_dimension=0, y_dimension=1, depth_dimension=2, x_direction=1, y_direction=1, depth_direction=1, side=-1):
		self.img_size = size
		self.cloud_dim = (x_dimension, y_dimension, depth_dimension)
		self.img_dir = (x_direction, y_direction, depth_direction)
		self.name = name
		self.side = side
	
	
	def calc_scale(self, data):
		min_xy_scale = min(map(
			lambda i: (self.img_size[i] - sum(self.img_margin[i])) / data['size'][self.cloud_dim[i]] if data['size'][self.cloud_dim[i]] > 0 else 0.0,
			[0, 1]
		))
		
		if min_xy_scale == 0:
			min_xy_scale = 30.0
		
		return min_xy_scale
	
	
	def draw(self, dst_dir, points, data, force=False):
		path = os.path.join(dst_dir,'%s-%s.png' % (data['name'], self.name))
		if os.path.exists(path) and not force:
			return path
		
		self.data = data
		
		for i in [0, 1]:
			self.offset[i] = (
				0.5 * (
					  self.img_size[i] - sum(self.img_margin[i])     ## Space available for data
					- data['size'][self.cloud_dim[i]] * self.scale   ## Space actually needed for data
				)                                                    ## ... = Half of the unused space
				+ self.img_margin[i][0]                              ##     + Margin (of one side)
				                                                     ##   v-- Correction for origin of data
				- data['min' if self.img_dir[i] > 0 else 'max'][self.cloud_dim[i]] * self.scale * self.img_dir[i]
			)
		
		im = Image.new('RGB', self.img_size, (255,255,255))
		self.draw = ImageDraw.Draw(im)
		
		grid_width = self.grid_width(40)
		
		self.grid_bg(grid_width)
		self.draw_data(points)
		self.grid_fg(grid_width)
		self.draw_legend()
		
		del self.draw
		
		im.save(path)
		return path
	
	
	##
	## TRANSLATIONS
	##
	def dia_to_cloud(self, coord, depth=0):
		ret = [depth, depth, depth]
		ret[self.cloud_dim[0]] = ( coord[0] - self.offset[0] ) / self.scale / self.img_dir[0]
		ret[self.cloud_dim[1]] = ( coord[1] - self.offset[1] ) / self.scale / self.img_dir[1]
		return tuple(ret)
	
	
	def cloud_to_dia(self, coord):
		return (
			coord[self.cloud_dim[0]] * self.scale * self.img_dir[0] + self.offset[0],
			coord[self.cloud_dim[1]] * self.scale * self.img_dir[1] + self.offset[1]
		)
	
	
	## 1: near / 0: far
	def cloud_to_depth(self, coord):
		cloud_dim = self.cloud_dim[2]
		dim_size = self.data['size'][cloud_dim]
		if dim_size > 0:
			cloud_end_type = 'min' if self.img_dir[2] > 0 else 'max'
			return 1 - (coord[cloud_dim] - self.data[cloud_end_type][cloud_dim]) * self.img_dir[2] / dim_size
		else:
			return 0.5
	
	
	##
	## GRID
	##
	def grid_width(self, min_grid_distance):
		s = self.scale / min_grid_distance
		if s >= 1:
			return 1 / (2 ** math.floor(math.log(s, 2)))
		else:
			return 2 ** math.ceil(math.log(1/s, 2))
	
	
	def grid_bg(self, width):
		self.grid_lines((200, 200, 200), width / 4)
		self.grid_lines((100, 100, 100), width)
	
	
	def grid_fg(self, width):
		self.grid_numbers(0, width, lambda pos: (pos+5, self.img_size[1] - 35))
		self.grid_numbers(1, width, lambda pos: (5, pos+5))
	
	
	def grid_lines(self, color, width):
		self.grid_line(0, color, width)
		self.grid_line(1, color, width)
	
	
	def grid_line(self, img_dim, color, width):
		cloud_dim = self.cloud_dim[img_dim]
		cloud_coord = [0, 0, 0]
		line_coord = [0, 0, self.img_size[0], self.img_size[1]]
		for line in  self.grid_iterate(cloud_dim, width):
			cloud_coord[cloud_dim] = line
			
			img_pos = self.cloud_to_dia(cloud_coord)[img_dim]
			line_coord[img_dim]   = img_pos
			line_coord[img_dim+2] = img_pos
			
			self.draw.line(tuple(line_coord), fill=color)
	
	
	def grid_numbers(self, img_dim, width, pos_fn):
		cloud_dim = self.cloud_dim[img_dim]
		cloud_coord = [0, 0, 0]
		for line in self.grid_iterate(cloud_dim, width):
			cloud_coord[cloud_dim] = line
			img_pos = self.cloud_to_dia(cloud_coord)[img_dim]
			self.draw.text(pos_fn(img_pos), str(line), fill=(0,0,0))
	
	
	def grid_iterate(self, cloud_dim, width):
		start = int(self.dia_to_cloud((0, 0))[cloud_dim] / width)
		end   = int(self.dia_to_cloud(self.img_size)[cloud_dim] / width)
		
		if end < start:
			tmp = start
			start = end
			end = tmp
		
		for line in range(start, end + 1):
			yield line * width
	
	
	##
	## DATA
	##
	def draw_data(self, points):
		points.sort(key=lambda x: x[self.cloud_dim[2]] * -self.img_dir[2])
		normal_end = [0]*3
		for i, point in enumerate(points):
			intensity = self.cloud_to_depth(point)
			coord     = self.cloud_to_dia(point)
			
			## Draw normal if needed ...
			normal_end = None
			if len(point) > 3 and i % 20 == 0:
				normal_end = [point[j] + 0.2*point[j+3] for j in range(3)]
				normal_intensity = self.cloud_to_depth(normal_end)
				
				## ... [normals] in the background
				if normal_intensity < intensity:
					self.data_normals(coord, normal_end)
					normal_end = None
			
			## Draw data point
			self.draw.rectangle([coord, (coord[0]+1, coord[1]+1)], fill=_color(intensity))
			
			## ... [normals] in the foreground
			if normal_end is not None:
				self.data_normals(coord, normal_end)
	
	
	def data_normals(self, start, end):
		self.draw.line(start + self.cloud_to_dia(end), fill=(200,0,0))
	
	
	##
	## LEGEND
	##
	def draw_legend(self):
		## Background
		bg_color=(230, 230, 230)
		self.draw.rectangle([self.img_size[0]-60, self.img_size[1]-60, self.img_size[0], self.img_size[1]], fill=bg_color)
		self.draw.rectangle([0, self.img_size[1]-60, 60, self.img_size[1]], fill=bg_color)
		self.draw.rectangle([0, self.img_size[1]-20, self.img_size[0], self.img_size[1]], fill=bg_color)
		
		## View
		ftr = (self.img_size[0]-20, self.img_size[1]-40)
		ftl = (self.img_size[0]-50, self.img_size[1]-40)
		fbr = (self.img_size[0]-20, self.img_size[1]-10)
		fbl = (self.img_size[0]-50, self.img_size[1]-10)
		btr = (self.img_size[0]-10, self.img_size[1]-50)
		btl = (self.img_size[0]-40, self.img_size[1]-50)
		bbr = (self.img_size[0]-10, self.img_size[1]-20)
		bbl = (self.img_size[0]-40, self.img_size[1]-20)
		sides = [
			[ftl, fbl, bbl, btl], ## 0: left
			[btl, bbl, bbr, btr], ## 1: back
			[fbr, bbr, bbl, fbl], ## 2: bottom
			[ftr, fbr, fbl, ftl], ## 3: front
			[btr, bbr, fbr, ftr], ## 4: right
			[btr, ftr, ftl, btl], ## 5: top
		]
		
		for i, side in enumerate(sides):
			if i == self.side:
				self.draw.polygon(side, outline=(0, 0, 0), fill=(255, 200, 0))
			else:
				self.draw.polygon(side, outline=(0, 0, 0))
		
		## Distance
		offset = 75
		length = 100
		line_coord = [0, self.img_size[1]-15, 0, self.img_size[1]-5]
		
		for i in range(length):
			line_coord[0] = line_coord[2] = offset + i
			self.draw.line(tuple(line_coord), fill=_color(i/length))
		
		self.draw.text((offset+length+5, line_coord[1]), 'near', fill=(0,0,0))
		
		## Axis
		dimensions = [
			{'name': 'X', 'color': (255,0,0)},
			{'name': 'Y', 'color': (0,220,0)},
			{'name': 'Z', 'color': (0,0,255)},
		]
		origin = (30, self.img_size[1]-30)
		
		to = (origin[0]+20*self.img_dir[0], origin[1])
		self.draw.line(origin + to, fill=dimensions[self.cloud_dim[0]]['color'], width=2)
		self.draw.text(to, dimensions[self.cloud_dim[0]]['name'], fill=(0,0,0))
		
		to = (origin[0], origin[1]+20*self.img_dir[1])
		self.draw.line(origin + to, fill=dimensions[self.cloud_dim[1]]['color'], width=2)
		self.draw.text(to, dimensions[self.cloud_dim[1]]['name'], fill=(0,0,0))
		
		if self.img_dir[2] > 0:
			to = (origin[0]+7, origin[1]-10)
		else:
			to = (origin[0]-7, origin[1]+10)
		self.draw.line(origin + to, fill=dimensions[self.cloud_dim[2]]['color'], width=2)
		self.draw.text(to, dimensions[self.cloud_dim[2]]['name'], fill=(0,0,0))
