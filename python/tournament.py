#!/usr/bin/env python3

import random
import argparse
import numpy as np
import math



## See https://pdfs.semanticscholar.org/e1cc/08dd2f2d622dd7c54e76aacd853101fe3451.pdf

## N = population size
## t = tournament size / # of rounds / # of candidates

## Return index of individual based on tournament selection
def draw_idx(N, t, unique):
	if unique:
		candidates = random.sample(range(N), k=t)
	else:
		candidates = random.choices(range(N), k=t)
	
	return min(candidates)


def sample_hist(N, t, unique, sample_size):
	hist = [0] * N
	for i in range(sample_size):
		hist[draw_idx(N, t, unique)] += 1
	
	return hist


def estimate_hist(N, t, sample_size):
	hist = []
	for i in range(N):
		p = ((N-i)/N)**t - ((N-i-1)/N)**t
		hist.append(round(sample_size * p))
	
	return hist


def median_from_hist(hist, sample_size):
	target = sample_size/2
	accumulated = 0
	for idx, cnt in enumerate(hist):
		accumulated += cnt
		if accumulated >= target:
			return idx


def estimate_median(N, t):
	return int(N - N * 2**(-1/t))


def main():
	## Self test (some simple "unit tests")
	assert(median_from_hist([2, 1], 3) == 0)
	assert(median_from_hist([1, 2], 3) == 1)
	assert(median_from_hist([2, 1, 2], 5) == 1)
	assert(median_from_hist([1, 1, 3], 5) == 2)
	
	assert(median_from_hist([2, 2], 4) == 0)
	
	assert(median_from_hist([0, 0, 2], 2) == 2)
	assert(median_from_hist([0, 0, 1], 1) == 2)
	
	for N in range(40, 100, 3):
		for t in range(1, 6):
			hist = estimate_hist(N, t, 100_000)
			assert(100_000-N/2 <= sum(hist) <= 100_000+N/2)
	
	## Get configuration from command line arguments
	parser = argparse.ArgumentParser()
	parser.add_argument('--sample_factor', '-f', default=10_000, type=int)
	parser.add_argument('--unique', '-u', default=False, type=bool)
	conf = parser.parse_args()
	
	## Do simulations
	for N in range(40, 100):
		sample_size = N*conf.sample_factor
		print(f'N: {N}')
		
		for t in [2,3,4,5]:
			sampled_hist = sample_hist(N, t, conf.unique, sample_size)
			sampled_hist_median = median_from_hist(sampled_hist, sample_size)
			
			estimated_hist = estimate_hist(N, t, sample_size)
			estimated_hist_median = median_from_hist(estimated_hist, sample_size)
			
			estimated_median = estimate_median(N, t)
			
			bugs = []
			
			if sampled_hist_median > estimated_hist_median:
				bugs.append(f'Sampled median {sampled_hist_median} > estimated median {estimated_hist_median}')
			
			elif sampled_hist_median < estimated_hist_median:
				bugs.append(f'Sampled median {sampled_hist_median} < estimated median {estimated_hist_median}')
			
			if estimated_median > estimated_hist_median:
				bugs.append(f'Estimated median {estimated_median} > expected median {estimated_hist_median}')
			
			elif estimated_median < estimated_hist_median:
				bugs.append(f'Estimated median {estimated_median} < expected median {estimated_hist_median}')
			
			if bugs:
				print(f'  N={N}, t={t}: {bugs}')



if __name__ == '__main__':
	main()
