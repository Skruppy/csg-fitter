#!/usr/bin/env python3

import sys



with open(sys.argv[1]) as f:
	print(sys.argv[1])
	for line in f:
		if line == 'DATA ascii\n':
			break
	
	for line in f:
		x, y, z, nx, ny, nz = map(float, line.strip().split(' '))
		print(f'translate([{x}, {y}, {z}]) color([1, 0.2, 0.2]) sphere(0.2);')
