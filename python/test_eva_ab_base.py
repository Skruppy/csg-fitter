## This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0



class ModelCreator:
	def pluginPath():
		return 'test_eva_ab'
	
	def build(self, b):
		raise NotImplemented()
	
	@property
	def description(self):
		raise NotImplemented()
