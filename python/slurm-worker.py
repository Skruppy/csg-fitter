#!/usr/bin/env python3

## Use only Python standard library and put everythin in a single script
import socket
import json
import threading
import time
import signal
import subprocess
import pathlib
import datetime
import uuid
import tarfile
import shutil
import os
import argparse
import inspect
import logging



def fmt_msg(msg):
	args = msg['args']
	fmt_args = ', '.join([f'{k}={repr(args[k])}' for k in sorted(args.keys())])
	return f'{msg["signal"]}({fmt_args})'


class Connection(threading.Thread):
	def __init__(self, msg_handler, host='localhost', port=7492):
		super().__init__(daemon=True)
		self.host = host
		self.port = port
		self.msg_handler = msg_handler
		self.running = True
		self.start()
	
	
	def run(self):
		backoff_time = start_backoff_time = 2
		max_backoff_time = 120
		while self.running:
			with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as self.s:
				try:
					self.s.connect((self.host, self.port))
				except (ConnectionRefusedError, socket.gaierror) as ex:
					logging.warning(f'Failed to connect. Retying in {backoff_time}s: {ex}')
					time.sleep(backoff_time)
					backoff_time = int(backoff_time * 1.5)
					if backoff_time > max_backoff_time:
						backoff_time = max_backoff_time
					continue
				
				self.s.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
				self.s.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPIDLE, 120) ## After 2 minutes of inactiviety
				self.s.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPINTVL, 30) ## send keep alives every 30 seconds
				self.s.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPCNT, 10) ## and close the connection after 7 minutes
				
				backoff_time = start_backoff_time
				with self.s.makefile() as f:
					logging.info('Connected')
					self.msg_handler.on_connect()
					
					while True:
						line = f.readline()
						if line == '':
							break
						msg = json.loads(line)
						logging.debug(f'S: {fmt_msg(msg)}')
						getattr(self.msg_handler, f'on_sig_{msg["signal"]}')(**msg['args'])
				
				logging.info('Disconnected')
				self.msg_handler.on_disconnect()
				
				if self.running:
					time.sleep(2)
	
	
	def __getitem__(self, key):
		return lambda **args: self.signal(key, **args)
	
	def signal(self, signal, **args):
		msg = {'signal': signal, 'args': args}
		logging.debug(f'C: {fmt_msg(msg)}')
		try:
			self.s.sendall(json.dumps(msg).encode('utf-8')+b'\n')
		except BrokenPipeError:
			pass
	
	def stop(self):
		self.running = False
		self.s.shutdown(socket.SHUT_RDWR)



class JobInfo(threading.Thread):
	def __init__(self, msg_handler, id, hash, nr, args):
		super().__init__()
		self.msg_handler = msg_handler
		self.id = id
		self.hash = hash
		self.nr = nr
		self.args = args
		self.exit_code = None
	
	
	def run(self):
		## The work path is already fixed in args
		work_dir = pathlib.Path('results', self.hash, str(self.nr))
		tar_file_name = f'{self.hash}-{str(self.nr)}.tar.gz'
		tar_file = pathlib.Path('results', tar_file_name)
		
		try:
			self.exit_code = -100
			(work_dir / 'output').mkdir(parents=True) ## fails if exists
			
			with subprocess.Popen(
				self.args,
				stdout=subprocess.PIPE,
				stderr=subprocess.STDOUT
			) as self.proc:
				stdout = self.proc.stdout
				with (work_dir / 'csgfitter.log').open('wb') as log:
					while True:
						msg = stdout.readline()
						if msg == b'': break
						log.write(msg)
						self.msg_handler.prog_log(self, msg.rstrip().decode(errors='backslashreplace'))
			
			self.exit_code = -101
			
			## Bundle and compress results into archive file (within working dir)
			with tarfile.open(tar_file, 'w:gz') as tar: ## Overwrites existing
				tar.add(work_dir)
			
			## Delete uncompressed results
			shutil.rmtree(work_dir)
			
			## Atomically move compressed results into pickup location
			tar_file.rename(self.msg_handler.pickup_dir / tar_file_name)
			
			self.exit_code = self.proc.returncode
		
		finally:
			self.msg_handler.proc_exited(self)
	
	
	def cancel(self):
		self.proc.terminate()
	
	
	def __str__(self):
		return self.id


class Main:
	def __init__(self):
		self.id = str(uuid.uuid4())
		slurm_job_id = os.environ.get('SLURM_JOB_ID', '???')
		slurm_proc_id = os.environ.get('SLURM_PROCID', '???')
		self.name = f'{slurm_job_id}-{slurm_proc_id}'
		self.start_time = datetime.datetime.now()
		self.last_log_msg = 'Started'
		self.job = None
		
		self.pickup_dir = pathlib.Path('results_done')
		self.pickup_dir.mkdir(parents=True, exist_ok=True)
		
		parser = argparse.ArgumentParser()
		con_defaults = inspect.signature(Connection.__init__).parameters
		parser.add_argument('--host', '-H', default=con_defaults['host'].default)
		parser.add_argument('--port', '-P', default=con_defaults['port'].default, type=int)
		args = parser.parse_args()
		self.s = Connection(self, args.host, args.port)
	
	def on_connect(self):
		self.s['register_worker'](
			id = self.id,
			name = self.name,
			start_time = str(self.start_time),
			last_log_msg = self.last_log_msg,
			job_id = self.job_id,
		)
	
	def on_disconnect(self):
		pass
	
	def on_sig_job(self, id, hash, nr, args):
		if self.job is not None:
			self.log(f'Won\'t accept job {id}. Already running {self.job}.')
			self.send_worker_status()
			return
		
		self.job = JobInfo(self, id, hash, nr, args)
		self.send_worker_status()
		self.job.start()
	
	def on_sig_cancel(self):
		if self.job is not None:
			self.job.cancel()
	
	def on_sig_stop(self):
		self.s.stop()
	
	@property
	def job_id(self):
		return self.job.id if self.job else None
	
	def send_worker_status(self):
		self.s['update_assignment'](job_id=self.job_id)
	
	
	def proc_exited(self, job):
		self.s['update_job_status'](job_id=job.id, exit_code=job.exit_code)
		if job == self.job:
			self.job = None
			self.send_worker_status()
		else:
			self.log(f'Job {job} terminated. Expeced {self.job}')
	
	def log(self, msg):
		logging.info(msg)
		self.last_log_msg = msg
		self.s['update_worker_log'](msg=msg)
	
	def prog_log(self, job, msg):
		self.s['update_job_log'](job_id=job.id, msg=msg)


if __name__ == '__main__':
	m = Main()
	m.s.join()
