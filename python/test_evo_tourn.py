#!/usr/bin/env python3
## This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0

import common
import shutil
import shell_worker
import util
import sys
import fitter_cache
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import collections
import os
import os.path as op
import math
import web
import itertools
import multiprocessing
import test_evo
import scipy
import scipy.stats
import subprocess



mating_rates = list(range(2,102,2))
crossover_rates = list(range(0,102,2))
samples_per_config = 1
model_name = 'fit_ab_all_avg'

def csgfitter_evo_cmd(evo_args=None, eval_args=None, sample_rate=5, input='tests/renderings/gecco19.json'):
	if evo_args is None:
		evo_args = []
	if eval_args is None:
		eval_args = ['evaluator_12']
	
	return [
		### @todo replace with "<IN: ...>"
		'json_reader', f'path=<IN:{input}>', 'name=csg',
		'csg.', '!', 'surface_sampler', f'rate={sample_rate}', 'name=points', '!', 'fitter.reference',
		'csg.', '!', 'extract_primitives', '!', 'fitter.primitives',
		*eval_args, '!', 'fitter.evaluator',
		'evo', *evo_args, 'max_rounds=150', 'name=fitter',
		'fitter.', '!', 'json_writer', 'path=<OUT:result.json>',
		'fitter.', '!', 'scad_writer', 'path=<OUT:result.scad>',
		'fitter.', '!', 'gv_writer', 'path=<OUT:result.pdf>',
		'fitter.', '!', 'gv_writer', 'path=<OUT:result.gv>',
		'points.', '!', '.npc', 'pcd_writer', 'path=<OUT:points.pcd>',
	]


## Output type: Dict[ Tuple[ep_strat_name, size], List[run] ]
def run_tests(mating_rates, crossover_rates, n):
	producer = fitter_cache.JobFileProduction()
	#producer = fitter_cache.SlurmProduction()
	#producer = fitter_cache.LocalProduction()
	fc = fitter_cache.FitterCache(producer)
	
	## Get all combinations of replacement strategies and population sizes
	test_run_combinations = {}
	for mating_rate in mating_rates:
		for crossover_rate in crossover_rates:
			test_run_combinations[(mating_rate, crossover_rate)] = fc.request(csgfitter_evo_cmd([
				f'population_size=500',
				f'mating_rate={mating_rate/100}',
				f'crossover_rate={crossover_rate/100}',
				f'replacement_strategy=worst',
				f'selector=tournament',
			]), n)
	
	## Either terminate program, if manual user intervention is required, or return
	if fc.fetch():
		print('It\'s your turn!')
		sys.exit()
	
	return test_run_combinations


def load_stats_task(job):
	print(f'{job["i"]/job["n"]*100:.2f}% ({job["i"]}/{job["n"]}): {job["args"]}')
	test_evo.analyze(*job["args"])
	matplotlib.pyplot.close('all')


## Input type: Dict[ Tuple[rep_strat_name, size], List[run] ]
## Output type: Dict[ Tuple[rep_strat_name, size], List[stats] ] ]
def load_stats(test_run_combinations):
	## Evaluate stats of each run, if not already done
	pool = multiprocessing.Pool(3)
	
	jobs = []
	for runs in test_run_combinations.values():
		for run in runs:
			evo_base_path = op.join(run.base_path, 'evo_stats')
			extracted_stats_path = op.join(evo_base_path, 'stats.json')
			if not op.isfile(extracted_stats_path):
			#if True:
				util.mkdirs(evo_base_path)
				jobs.append({
					'args': [run.stats_path, evo_base_path, ['png', 'pdf'], set([]), set([])],
				})
	
	for i, job in enumerate(jobs):
		job['i'] = i+1
		job['n'] = len(jobs)
	
	pool.map(load_stats_task, jobs)
	
	## Load stats form json file
	run_stats_lists = {}
	for key, runs in test_run_combinations.items():
		run_stats_lists[key] = []
		for run in runs:
			run_stats = util.dobjectify(util.read_json(op.join(run.base_path, 'evo_stats', 'stats.json')))
			run_stats.base_path = run.base_path
			run_stats_lists[key].append(run_stats)
	
	return run_stats_lists




def plot(config, cmap, norm, base_path, is_smoothed):
	for ext in ['pdf', 'png']:
		path = f'{base_path}-{"smoothed" if is_smoothed else "raw"}.{ext}'
		if not op.exists(common.fig_path(path)):
			fig = plt.figure(constrained_layout=True, figsize=(7, 7))
			ax = fig.add_subplot()
			
			## Image and contour
			extent = (1, 101, -1, 101) ## (left, right, bottom, top) of pixel _edges_
			img = ax.imshow(
				config.smoothed if is_smoothed else config.raw,
				origin='lower',
				extent=extent,
				interpolation='bilinear' if is_smoothed else None,
				cmap = cmap,
				norm = norm,
			)
			CS = ax.contour(
				config.smoothed,
				origin='lower',
				extent=extent,
				levels=8,
				colors=['black'],
				alpha=0.4
			)
			ax.clabel(CS, inline=1, fontsize=10)
			
			fig.colorbar(img, ax=ax, shrink=.8, pad=.0,location='top')
			
			## Maximum marker
			for color, extreme in [('blue', config.smoothed_min), ('red', config.smoothed_max)]:
				[a_x, a_y] = np.where(config.smoothed == extreme.value)
				extreme.mating_rate = a_y[0]*2+2
				extreme.crossover_rate = a_x[0]*2
				ax.plot(extreme.mating_rate, extreme.crossover_rate, '+', markersize=12, mew=4,   color='white')
				ax.plot(extreme.mating_rate, extreme.crossover_rate, '+', markersize=12, mew=1.5, color=color)
			
			## Ticks/Grid/Labels
			ticks = np.arange(0, 101, 10)
			ax.set_xticks(ticks)
			ax.set_yticks(ticks)
			
			ax.grid(alpha=0.5)
			
			labels = list(map(lambda x: f'{x}%', ticks))
			ax.set_xticklabels(labels)
			ax.set_yticklabels(labels)
			
			ax.set_xlabel('Mating rate (MR)')
			ax.set_ylabel('Crossover rate (CoR)')
			
			## Write images
			common.save_fig(fig, path, bbox_inches='tight', pad_inches=0.03)
	
	return os.path.basename(path)[:-4]


def smooth(a, sigma, **kwargs):
	nan_mask = np.isnan(a)
	
	loss = np.ones(a.shape)
	loss[nan_mask] = 0
	loss = scipy.ndimage.gaussian_filter(loss, sigma=sigma, **kwargs)
	
	smoothed = a.copy()
	smoothed[nan_mask] = 0
	smoothed = scipy.ndimage.gaussian_filter(smoothed, sigma=sigma, **kwargs)
	
	return smoothed / loss


def extrema(smoothed, fn):
	value = fn(smoothed)
	[a_x, a_y] = np.where(smoothed == value)
	
	return util.Dobject(
		value = value,
		mating_rate = a_y[0]*2+2,
		crossover_rate = a_x[0]*2,
	)


def main():
	global mating_rates
	global crossover_rates
	test_run_combinations = run_tests(mating_rates, crossover_rates, samples_per_config)
	run_stats_lists = load_stats(test_run_combinations)
	
	configs = [
		util.Dobject(model_name='fit_ab_all_med', prop_name='rate', figures={}),
		util.Dobject(model_name='fit_ab_all_med', prop_name='rmsd', figures={}),
		util.Dobject(model_name='fit_ab_all_avg', prop_name='rate', figures={}),
		util.Dobject(model_name='fit_ab_all_avg', prop_name='rmsd', figures={}),
		util.Dobject(model_name='fit_ab_all_max', prop_name='rate', figures={}),
		util.Dobject(model_name='fit_ab_all_max', prop_name='rmsd', figures={}),
		util.Dobject(model_name='fit_abc_parents_med', prop_name='rate', figures={}),
		util.Dobject(model_name='fit_abc_parents_med', prop_name='offset', figures={}),
		util.Dobject(model_name='fit_abc_parents_med', prop_name='rmsd', figures={}),
		util.Dobject(model_name='fit_ab_parents_med', prop_name='rate', figures={}),
		util.Dobject(model_name='fit_ab_parents_med', prop_name='rmsd', figures={}),
		util.Dobject(model_name='fit_ab_parants_avg', prop_name='rate', figures={}),
		util.Dobject(model_name='fit_ab_parants_avg', prop_name='rmsd', figures={}),
		util.Dobject(model_name='skew', prop_name='mean', figures={}),
		util.Dobject(model_name='skew', prop_name='std', figures={}),
		util.Dobject(model_name='uniqueness', prop_name='mean', figures={}),
		util.Dobject(model_name='best_score', prop_name='149', figures={}),
		util.Dobject(model_name='best_score', prop_name='99', figures={}),
		util.Dobject(model_name='best_score', prop_name='49', figures={}),
	]
	
	web_dir = os.path.join(util.script_dir(), '../public/tournament')
	databook_dir = os.path.join(util.script_dir(), '../databook/tournament')
	util.mkdirs(web_dir)
	util.mkdirs(databook_dir)
	
	for config in configs:
		config.raw = np.empty([len(crossover_rates), len(mating_rates)], dtype=float)
		for (x, mating_rate), (y, crossover_rate) in itertools.product(
			enumerate(mating_rates), enumerate(crossover_rates)
		):
			key = (mating_rate, crossover_rate)
			
			values = []
			for run_stats in run_stats_lists[key]:
				value = run_stats[config.model_name].get(config.prop_name)
				if value is None: continue
				values.append(run_stats[config.model_name][config.prop_name])
			
			config.raw[y,x] = np.mean(values) if values else np.nan
		
		config.raw_min = np.nanmin(config.raw)
		config.raw_max = np.nanmax(config.raw)
		
		config.smoothed = smooth(config.raw, 2.5, mode='nearest')
		config.smoothed_min = extrema(config.smoothed, np.nanmin)
		config.smoothed_max = extrema(config.smoothed, np.nanmax)
		
		desc = common.descriptions.models[config.model_name].props[config.prop_name]
		cmap = desc.colorize.cmap
		norm = desc.colorize.norm(config.raw_min, config.raw_max)
		path = f'tournament/{config.model_name}-{config.prop_name}'
		config.figures['smoothed'] = plot(config, cmap, norm, path, True)
		config.figures['raw']      = plot(config, cmap, norm, path, False)
	
	good = configs[0]
	bad = configs[2]
	diff = util.Dobject(figures={})
	diff.raw = bad.raw / good.raw
	
	diff.smoothed = smooth(diff.raw, 2.5, mode='nearest')
	diff.smoothed_min = extrema(diff.smoothed, np.nanmin)
	diff.smoothed_max = extrema(diff.smoothed, np.nanmax)
	cmap = 'binary'
	norm = matplotlib.colors.Normalize()
	path = 'tournament/diff'
	diff.figures['smoothed'] = plot(diff, cmap, norm, path, True)
	diff.figures['raw']      = plot(diff, cmap, norm, path, False)
	
	diff_values = diff.raw.reshape(-1)
	x_max = np.nanmax(diff.raw)
	kde = scipy.stats.gaussian_kde(diff_values)
	kde_x = np.linspace(0, x_max, 1000)
	for ext in ['pdf', 'png']:
		fig = plt.figure(constrained_layout=True, figsize=(5, 2.5))
		ax = fig.add_subplot()
		
		ax.hist(diff_values, density=True, range=(0, x_max), bins=20)
		ax.plot(kde_x, kde(kde_x))
		
		ax.set_xlabel('Verhältnis' if ext == 'pdf' else 'ratio')
		ax.set_ylabel('Wahrscheinlichkeitsdichte' if ext == 'pdf' else 'probability density')
		
		ax.set_xlim(0, x_max)
		
		common.save_fig(fig, f'tournament/diff-hist.{ext}', bbox_inches='tight', pad_inches=0.03)
	
	
	## MR, CoR
	diff_examples = []
	for mating_rate, crossover_rate in [(80, 56), (80, 60), (98, 38), (72, 74), (90, 32), (20, 10)]:
		key = (mating_rate, crossover_rate)
		run_stats = run_stats_lists[key][0]
		example = util.Dobject(
			mating_rate = mating_rate,
			crossover_rate = crossover_rate,
			base_path = run_stats['base_path'],
			models = {},
		)
		diff_examples.append(example)
		
		for model_name in ['fit_ab_all_med', 'fit_ab_all_avg']:
			dst_file = f'example-{mating_rate}-{crossover_rate}-{model_name}'
			example.models[model_name] = {
				'rate': run_stats[model_name].rate,
				'file': dst_file,
			}
			
			for ext in ['png', 'pdf']:
				src = f'{run_stats.base_path}/evo_stats/figure-{model_name}.{ext}'
				dst = common.fig_path(f'tournament/{dst_file}.{ext}')
				if util.outdated(dst, src):
					shutil.copy2(src, dst)
		
	## MR, CoR
	examples = []
	for mating_rate, crossover_rate in [
		(100, 100), (100, 98), (98, 100), (98, 98),
		(90,  100), (90,  98), (88, 100), (88, 98),
		(100,  50), (100, 48), (98,  50), (98, 48),
		(90,   50), (90,  48), (88,  50), (88, 48),
	]:
		key = (mating_rate, crossover_rate)
		run_stats = run_stats_lists[key][0]
		example = util.Dobject(
			mating_rate = mating_rate,
			crossover_rate = crossover_rate,
			base_path = run_stats['base_path'],
			figures = {},
		)
		examples.append(example)
		
		for model_name in ['fit_ab_all_med', 'uniqueness', 'raw_points', 'ranks']:
			example.figures[model_name] = dst_file = \
				f'example-{mating_rate}-{crossover_rate}-{model_name}'
			
			for ext in ['png', 'pdf']:
				src = f'{run_stats.base_path}/evo_stats/figure-{model_name}.{ext}'
				dst = common.fig_path(f'tournament/{dst_file}.{ext}')
				if util.outdated(dst, src):
					shutil.copy2(src, dst)
	
	common.save_pipeline(test_run_combinations, 'tournament/pipeline.pdf')
	
	for ext in ['html', 'tex']:
		web.render(f'tournament.{ext}', f'tournament/index.{ext}', {
			'title': 'Tournament configuration',
			'selected_subthing_key': '',
			'subthings': [],
			'descriptions': common.descriptions,
			'configs': configs,
			'diff': diff,
			'diff_examples': diff_examples,
			'examples': examples,
			'mating_rates': mating_rates,
			'crossover_rates': crossover_rates,
			'samples_per_config': samples_per_config,
			'escapeLatex': util.escapeLatex,
		})



if __name__ == '__main__':
	main()
