## This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0

from typing import List, Any
import atexit
import dataclasses
import enum
import os
import queue
import shlex
import subprocess
import threading



@dataclasses.dataclass
class _Job:
	args: List[str]
	popen_args: Any
	id: int
	enqueue: bool = False
	handler: Any = None
	exit_code: int = None


class StopMethod(enum.Enum):
	SOFTLY = enum.auto()
	NORMAL = enum.auto()
	BRUTAL = enum.auto()



class _Worker(threading.Thread):
	def __init__(self, manager):
		super().__init__(daemon=True)
		self._manager = manager
		self._proc = None
		self.start()
	
	
	def run(self):
		while True:
			## Wait for next job or stop worker thread
			job = self._manager._jobs.get()
			if job is None:
				self._manager._jobs.task_done()
				break
			
			## Start job
			self._manager._log_start(job)
			self._proc = subprocess.Popen(job.args, **job.popen_args, start_new_session=True)
			
			## Wait for and handle end of job
			ret = os.waitpid(self._proc.pid, 0) ## No busy waiting, compared to Popen.wait()
			job.exit_code = self._proc.wait()
			if job.handler is not None:
				job.handler(job.exit_code)
			self._manager._log_done(job)
			self._manager._jobs.task_done()
	
	
	def stop(self, method=StopMethod.NORMAL):
		if self._proc is None:
			return
		
		if method == StopMethod.NORMAL:
			print(f'Sending SIGTERM to {self._proc.pid}')
			self._proc.terminate()
		elif method == StopMethod.BRUTAL:
			print(f'Sending SIGKILL to {self._proc.pid}')
			self._proc.kill()



class ShellWorker():
	def __init__(self, workers=len(os.sched_getaffinity(0))):
		self._job_cnt = 0
		self._jobs = queue.Queue()
		self._workers = [_Worker(self) for _ in range(workers)]
		self._running = True
		atexit.register(self.escalating_stop)
	
	
	def run(self, *args, enqueue=True, **popen_args):
		if not self._running:
			raise Exception('Not running')
		
		self._job_cnt += 1
		job = _Job(args, popen_args, self._job_cnt)
		
		if callable(enqueue):
			job.handler = enqueue
			job.enqueue = True
		else:
			job.handler = None
			job.enqueue = enqueue
		
		if enqueue:
			self._jobs.put(job)
		else:
			self.wait_done()
			
			## Start job
			self._log_start(job)
			proc = subprocess.Popen(job.args, **job.popen_args)
			
			## Wait for and handle end of job
			os.waitpid(proc.pid, 0)
			job.exit_code = proc.wait()
			self._log_done(job)
	
	
	def reset(self):
		self.wait_done()
		self._job_cnt = 0
	
	
	def escalating_stop(self, first_method=StopMethod.NORMAL):
		for method in filter(lambda x: x.value >= first_method.value, StopMethod):
			try:
				self.stop(method, True)
				return
			except KeyboardInterrupt:
				pass
	
	
	def stop(self, method=StopMethod.NORMAL, clean=False):
		if not clean:
			self.wait_done()
		
		if self._running:
			self._running = False ## Prevent adding new jobs ...
			self.clear()          ## ... before clearing the job queue
			for _ in self._workers:
				self._jobs.put(None)
		
		for worker in self._workers:
			worker.stop(method)
		
		self.wait_done()
	
	
	def clear(self):
		try:
			while True:
				self._jobs.get_nowait()
				self._jobs.task_done()
		except queue.Empty:
			pass
	
	
	def wait_done(self):
		self._jobs.join()
	
	
	def _log_start(self, job):
		print(f'\033[1m> Run {"async" if job.enqueue else "sync"} job {job.id}/{self._job_cnt} ({100*job.id/self._job_cnt:.1f}%): {" ".join(map(shlex.quote, job.args))}\033[0m')
	
	
	def _log_done(self, job):
		if job.exit_code != 0:
			print(f'\033[31;1mJob #{job.id} failed with exit code {ret.exit_code}\033[0m')

	
	@property
	def is_running(self):
		return self._running


def tests_main():
	q = ShellWorker()
	q.run('sleep', '3')
	q.run('sleep', '3')
	q.run('sleep', '3')
	q.run('sleep', '3')
	q.run('sleep', '3')
	q.run('sleep', '3')
	q.run('date', enqueue=False)
	q.wait_done()
	print('done')


if __name__ == '__main__':
	tests_main()
