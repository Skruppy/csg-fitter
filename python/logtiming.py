#!/usr/bin/env python3
## This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0

import dateutil.parser
import sys
import json



def deltas(pathes):
	for path in pathes:
		last_log = None
		
		with open(path) as f:
			for line_nr, line in enumerate(f):
				log = {}
				
				log['date'] = dateutil.parser.isoparse(line[:23])
				
				msg = line[24:-1]
				if msg.startswith('[info] core.d:200:evo Round'):
					msg = '[info] core.d:200:evo End of round'
				log['msg'] = msg
				
				if last_log is not None:
					yield {
						'time': (log['date'] - last_log['date']).total_seconds(),
						'file': path,
						'line': line_nr,
						'msg_start': last_log['msg'],
						'msg_end': log['msg'],
					}
				
				last_log = log


if __name__ == '__main__':
	msgs = list()
	critical_parts = dict()
	
	for delta in deltas(sys.argv[1:]):
		if delta['time'] > 30:
			print(delta)
			key = f'"{delta["msg_start"]}" ==> "{delta["msg_end"]}"'
			if key in critical_parts:
				critical_parts[key] += 1
			else:
				critical_parts[key] = 1
			#critical_parts.add(tuple([delta['msg_start'], delta['msg_end']]))
		
		#if delta['msg_start'] != '[info] core.d:272:create Population.create >':
			#continue
		#del delta['msg_start']
		#del delta['msg_end']
		#msgs.append(delta)
	
	for key, cnt in critical_parts.items():
		print(f'{key}:\t{cnt}')
	
	#with open('pop-create.json', 'w') as f:
		#json.dump(msgs, f)
	
	#with open('pop-create.json') as f:
		#msgs = json.load(f)
	
	#for 
	


