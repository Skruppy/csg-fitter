#!/usr/bin/env python3
## This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0

import shell_worker
import plugin
import test_final_base
import util
import fmtjson
import fitter_cache
import os.path as op
import sys
import shutil
import numpy as np
import web
import math
import collections
import re
import itertools
from test_final_stats import *
import matplotlib
import matplotlib.pyplot as plt
import os
import common
import subprocess



web_dir = op.join(util.script_dir(), '../public/final')
def load_models():
	models = {}
	base_path = op.join(util.script_dir(), '../tests/final')
	csgfitter = op.join(util.script_dir(), '../csgfitter-static')
	util.mkdirs(base_path)
	util.mkdirs(web_dir)
	util.mkdirs(f'{common.databook_dir}/final')
	builder_clses = plugin.loadAll(test_final_base.FinalCreator)
	
	## Create all basic files (json, scad, png) for all tests and both models.
	sh = shell_worker.ShellWorker()
	scad_jobs = []
	copy_jobs = []
	gv_jobs = []
	for builder_cls in builder_clses:
		configurations = getattr(builder_cls, 'configurations', {'': {}})
		
		for config_name, config in configurations.items():
			creator = builder_cls(**config)
			
			full_name = builder_cls.pluginName
			if config_name:
				full_name += f'-{config_name}'
			
			json_path = op.join(base_path, f'{full_name}.json')
			csg_tree = creator.build()
			json_str = fmtjson.format_csg(csg_tree)
			
			models[full_name] = util.Dobject(
				name = full_name,
				json_path = f'tests/final/{full_name}.json',
				sample_rate = creator.sample_rate,
				stats = sum_csg_tree_stats([parse_csg_tree(csg_tree)]),
			)
			
			## Write JSON (but only if needed, to prevent timestamp updates)
			if not op.isfile(json_path) or util.read_file(json_path) != json_str:
				util.write_file(json_str, json_path)
			
			public_json_path = op.join(web_dir, f'{full_name}.json')
			if util.outdated(public_json_path, json_path):
				shutil.copy2(json_path, public_json_path)
			
			## Now: Convert JSON to SCAD (async)
			scad_path = op.join(web_dir, f'{full_name}.scad')
			if util.outdated(scad_path, json_path):
				sh.run(csgfitter, 'convert', f'from={json_path}', f'to={scad_path}')
			
			## Now: Convert JSON to DOT file (async)
			gv_path = op.join(web_dir, f'{full_name}.gv')
			if util.outdated(gv_path, json_path):
				sh.run(
					csgfitter,
					'json_reader', f'path={json_path}', '!',
					'gv_writer', f'path={gv_path}', 'geo_details=true', 'ltr=true',
				)
			
			## Later: Convert SCAD to PNG
			png_path = op.join(web_dir, f'{full_name}.png')
			if util.outdated(png_path, json_path):
				scad_jobs.append([
					'openscad',
					scad_path,
					'-o', png_path,
					'--viewall', '--autocenter',
					'--imgsize=600,400',
					'--view=axes,scales',
					'--render',
					'--camera=0,0,0,340,20,0,1',
				])
			
			databook_png = f'{common.databook_dir}/final/{full_name}.png'
			copy_jobs.append((png_path, databook_png))
			
			## Later: Convert DOT file to PNG and PDF
			for ext in ['pdf', 'png']:
				gv_out = f'final/{full_name}-tree.{ext}'
				if util.outdated(common.fig_path(gv_out), json_path):
					gv_jobs.append((gv_path, gv_out))
	
	## Wait til all json files have been converted to scad files (using
	## `csgfitter-static`) and then start all scad to png renderings (using
	## OpenSCAD).
	sh.wait_done()
	
	for job in scad_jobs:
		sh.run(*job)
	
	for gv_path, gv_out in gv_jobs:
		common.save_gv(util.read_file(gv_path), gv_out)
	
	sh.stop()
	
	for png_path, databook_png in copy_jobs:
		if util.outdated(databook_png, png_path):
			shutil.copy2(png_path, databook_png)
	
	return models


def csgfitter_evo_cmd(evo_args=None, sample_rate=5, input='tests/renderings/gecco19.json', evo_type='evo'):
	if evo_args is None:
		evo_args = []
	
	args = [
		## Create point cloud and primitives from CSG tree
		'json_reader', f'path=<IN:{input}>', 'name=csg',
		'csg.', '!', 'extract_primitives', 'name=primitives',
		'csg.', '!', 'surface_sampler', f'rate={sample_rate}', 'name=sampler',
		
		## Compute reduced point cloud, sufficient for the evolutionary algorithm
		'primitives.primitives', '!', 'reducer.primitives',
		'sampler.npc',           '!', 'reducer.complete_npc',
		'reduce', 'name=reducer',
		
		## Set evaluator
		'evaluator_12', 'name=evaluator',
		
		## Find a solution using specified evolutionary algorithm and simplify it
		'evaluator.',  '!', 'fitter.evaluator',
		'primitives.', '!', 'fitter.primitives',
		'reducer.',    '!', 'fitter.reference',
		evo_type, 'name=fitter', *evo_args, 'max_rounds=150', '!', 'simplify', 'name=simplifier',
		
		## Save raw fitter result as JSON/SCAD/GraphViz
		'fitter.', '!', 'json_writer', 'path=<OUT:raw_result.json>',
		'fitter.', '!', 'scad_writer', 'path=<OUT:raw_result.scad>',
		'fitter.', '!', 'gv_writer',   'path=<OUT:raw_result.gv>',
		
		## Save simplified fitter result as JSON/SCAD/GraphViz
		'simplifier.', '!', 'json_writer', 'path=<OUT:simple_result.json>',
		'simplifier.', '!', 'scad_writer', 'path=<OUT:simple_result.scad>',
		'simplifier.', '!', 'gv_writer',   'path=<OUT:simple_result.gv>',
		
		## Score the fitter result against the complete and the reduced point
		## cloud (since only the parts/cliques are evaluated so far).
		'simplifier.', '!', 'final_score_complete.model',
		'sampler.',    '!', 'final_score_complete.reference',
		'evaluator.',  '!', 'final_score_complete.evaluator',
		'score', 'name=final_score_complete',
		
		'simplifier.', '!', 'final_score_reduced.model',
		'reducer.',    '!', 'final_score_reduced.reference',
		'evaluator.',  '!', 'final_score_reduced.evaluator',
		'score', 'name=final_score_reduced',
	]
	
	return args


def format_diff(base, val, good, c_good, c_bad, c_neutral, fmt_great, fmt_small):
	if base !=  0 and val != 0:
		rel = val / base
	elif base ==  0 and val == 0:
		rel = 1
	else:
		return ''
	
	if good > 0:
		color = c_good if rel > 1 else c_bad
	elif good < 0:
		color = c_good if rel < 1 else c_bad
	else:
		color = ''
	
	if rel > 1:
		n = rel
	else:
		n = 1/rel
	
	accuracy = 2 if n >= 2 else 3
	digits = int(math.log10(n)) - accuracy + 1
	decimals = max(0, -digits)
	n = round(n / 10**digits) * 10**digits
	
	if n < 1.1:
		color = c_neutral
	
	s = f'{n:,.{decimals}f}'
	return (fmt_great if rel >= 1 else fmt_small).format(color=color, s=s)


def run_tests(models, n):
	producer = fitter_cache.JobFileProduction()
	#producer = fitter_cache.SlurmProduction()
	#producer = fitter_cache.LocalProduction(3)
	fc = fitter_cache.FitterCache(producer)
	
	## Get all combinations of replacement strategies and population sizes
	test_run_combinations = {}
	for model in models.values():
		#if not model.name == 'chain':
		#if not model.name.startswith('chain'):
			#continue
		
		for evo_type in ['evo', 'clique_evo', 'part_evo']:
		#for evo_type in ['clique_evo']:
			test_run_combinations[(model.name, evo_type)] = fc.request(
				csgfitter_evo_cmd(
					[
						'population_size=500',
						'mating_rate=0.9',
						'crossover_rate=0.5',
						'replacement_strategy=worst',
						'selector=tournament',
					],
					sample_rate = model.sample_rate,
					input = model.json_path,
					evo_type = evo_type,
				),
				n, rebuild=False
			)
	
	## Either terminate program, if manual user intervention is required, or return
	if fc.fetch():
		print('It\'s your turn!')
		sys.exit()
	
	return test_run_combinations




def main():
	models = load_models()
	model_names = list(models.keys())
	evo_types = ['evo', 'clique_evo', 'part_evo']
	test_run_combinations = run_tests(models, 30)
	
	results = {}
	clique_parser = CliqueParser()
	clique_stats = []
	part_stats = []
	cpus = {}
	evo_times = {k: [] for k in evo_types}
	
	## MODELS
	for model_name in model_names:
		bucket_fills = []
		buckets = {}
		valid_runs = 0
		results[model_name] = result = util.Dobject(
			evos = {},
			failed_buckets = 0,
			valid_buckets = 0,
			stats = models[model_name].stats,
			render_file = f'{model_name}.png',
			json_file = f'{ model_name }.json',
			json = util.read_file(f'public/final/{ model_name }.json'),
			scad_file = f'{ model_name }.scad',
			scad = util.read_file(f'public/final/{ model_name }.scad'),
		)
		
		## EVO
		for evo_type in evo_types:
			test_runs = test_run_combinations[(model_name, evo_type)]
			
			run_stats_list = []
			
			## RUN
			for test_run in test_runs:
				#print(test_run.base_path, model_name, evo_type)
				reduced_run_stats = reduce_run_stats(test_run, result.stats.geo_cnt_mean)
				
				#### Global stats
				## CPU & hostname
				cpu_stats = cpus.get(reduced_run_stats.cpu)
				if cpu_stats is None:
					cpus[reduced_run_stats.cpu] = cpu_stats = util.Dobject(
						name = reduced_run_stats.cpu,
						count = 0,
						hosts = set()
					)
				cpu_stats.count += 1
				cpu_stats.hosts.add(reduced_run_stats.host)
				
				#### Per model stats
				## Handle each bucket per evo and model
				valid_runs += 1
				for bucket in reduced_run_stats.buckets:
					p = bucket.available_points / bucket.requested_points
					bucket_fills.append(p)
					
					if bucket.bucket_key not in buckets:
						buckets[bucket.bucket_key] = util.Dobject(
							geo = bucket.geo,
							points = [],
						)
					buckets[bucket.bucket_key].points.append((
						p, test_run.base_path
					))
				
				#### Per model+evo stats
				foo = util.Dobject()
				run_stats_list.append(foo)
				
				foo.csg_tree_stats = reduced_run_stats.simple_csg_tree
				foo.raw_csg_tree_stats = reduced_run_stats.raw_csg_tree
				
				## Total serial runtime
				foo.runtime = reduced_run_stats.overall_runtime
				
				## Final scores
				foo.complete_score = reduced_run_stats.complete_score
				foo.reduced_score  = reduced_run_stats.reduced_score
				
				## Fitter stats
				foo.fitter = reduced_run_stats.fitter
				evo_times[evo_type].extend(reduced_run_stats.evo_times)
			## end RUN
			
			result.evos[evo_type] = evo_stats = sum_stats(run_stats_list)
			evo_stats.csg_tree = sum_csg_tree_stats(list(map(lambda x: x.csg_tree_stats, run_stats_list)))
			
			fitter_stats = list(map(lambda x: x.fitter, run_stats_list))
			if evo_type == 'evo':
				result.evos[evo_type].fitter = []
			elif evo_type == 'clique_evo':
				result.evos[evo_type].fitter = sum_clique_stats(fitter_stats)
			elif evo_type == 'part_evo':
				result.evos[evo_type].fitter = sum_part_stats(fitter_stats)
		## end EVO
		
		for bucket_key, bucket in buckets.items():
			bucket.key = bucket_key
			bucket.cnt = len(bucket.points)
			bucket.cnt_required = valid_runs
			
			if bucket.cnt == bucket.cnt_required:
				result.valid_buckets += 1
			else:
				result.failed_buckets += 1
		
		result.buckets = sorted(buckets.values(), key=lambda x: (x.geo, x.key))
		result.unique_buckets = result.valid_buckets + result.failed_buckets
		result.worst_bucket_fills = sorted(bucket_fills)[:5]
	## end MODELS
	
	for ext in ['png', 'pdf']:
		name = f'final/scatter.{ext}'
		if not op.exists(common.fig_path(name)):
			np.random.seed(42)
			fig = plt.figure(figsize=(10, 6))
			ax = fig.add_subplot()
			ax.grid(True, which='both')
			
			xs = None
			ys = None
			colors = None
			legend_colors = []
			legend_labels = []
			for c, (evo_name, stats) in zip(['#1f77b4', '#ff7f0e', '#2ca02c'], evo_times.items()):
				legend_colors.append(matplotlib.lines.Line2D([0], [0], color=c, lw=4))
				legend_labels.append({'evo': 'Ohne Zerlegung', 'clique_evo': 'Cliquen-basierte Zerlegung', 'part_evo': 'Pruning-basierte Zerlegung'}[evo_name])
				x = np.fromiter(map(lambda x: x[0], stats), float)
				y = np.fromiter(map(lambda x: x[1], stats), float)
				
				x *= 1 + 0.1 * (np.random.rand(len(x))-0.5)
				color = np.repeat(c, len(stats))
				
				xs = x if xs is None else np.append(xs, x)
				ys = y if ys is None else np.append(ys, y)
				colors = color if colors is None else np.append(colors, color)
			
			plot_idx = np.random.permutation(xs.shape[0])
			ax.scatter(
				xs[plot_idx],
				ys[plot_idx],
				alpha=0.4,
				s = 8,
				edgecolors = 'none',
				c = colors[plot_idx],
				zorder=2,
			)
			ax.set_xscale('log')
			ax.set_yscale('log')
			ax.set_xlabel('Anzahl Geometrien')
			ax.set_ylabel('Zeit [s]\n◀ besser')
			ax.set_xlim(1, xs.max()*1.5)
			ax.set_ylim(1e-2, ys.max()*1.5)
			ax.legend(legend_colors, legend_labels, loc='lower right')
			
			#matplotlib.pyplot.show(block=fig)
			common.save_fig(fig, name, bbox_inches='tight', pad_inches=0.03)
		
		for model_name in model_names:
			model = test_run_combinations[(model_name, 'clique_evo')][0]
			file_name = f'final/{model_name}-graph-clique.{ext}'
			if util.outdated(common.fig_path(file_name), model.stats_path):
				common.save_gv(model.stats['processors']['fitter']['stats']['clique_gv'], file_name, ['fdp', '-Gstart=42'])
			
			model = test_run_combinations[(model_name, 'part_evo')][0]
			file_name = f'final/{model_name}-graph-part.{ext}'
			if util.outdated(common.fig_path(file_name), model.stats_path):
				common.save_gv(model.stats['processors']['fitter']['stats']['graph_gv'], file_name, ['fdp', '-Gstart=42'])
	
	common.save_pipeline(test_run_combinations[(model_names[0], 'part_evo')], 'final/pipeline.pdf')
	
	subthings = []
	for model_name, model_config in models.items():
		subthings.append({
			'title': model_name,
			'url': f'final/{ model_name }',
			'key': model_name,
		})
	
	for ext in ['html', 'tex']:
		if ext == 'html':
			fmt_args = [
				'text-success',
				'text-danger',
				'text-info',
				'<span class="{color}">{s} &times;</span>',
				'<math xmlns="http://www.w3.org/1998/Math/MathML" class="{color}"><mfrac bevelled="true"><mn>1</mn><mn>{s}</mn></mfrac> <mo>&times;</mo></math>'
			]
		else:
			fmt_args = [
				'tab_green',
				'tab_red',
				'tab_blue',
				'\\textcolor{{{color}}}{{${s} \\times$}}',
				'\\textcolor{{{color}}}{{$\\nicefrac{{1}}{{{s}}}$}}',
			]
		
		documents = ['index']
		if ext == 'tex':
			documents += [
				'timing_overview', 'timing_clique', 'timing_part', 'scores', 'tree_ops',
				'tree_tree_height', 'tree_geo_cnt', 'tree_geo_levels', 'tree_op_widths', 'tree_node_cnt',
				'cluster_clique', 'cluster_part',
			]
		
		for document in documents:
			web.render(f'final_{document}.{ext}', f'final/{document}.{ext}', {
				'title': 'Final tests',
				'selected_subthing_key': '',
				'subthings': subthings,
				'models': results,
				'format_diff': lambda a, b, c: format_diff(
					a, b, c, *fmt_args
				),
				'cpus': cpus,
				'escapeLatex': util.escapeLatex,
			})
	
	web.render('final_debug.html', 'final/debug.html', {
		'title': 'Debug infos | Final tests',
		'selected_subthing_key': '',
		'subthings': subthings,
		'models': results,
		'format_diff': format_diff,
		'cpus': cpus,
	})
	
	for model_name, model in models.items():
		web.render('final_details.html', f'final/{ model_name }.html', {
			'title': f'{ model_name } | Final tests',
			'selected_subthing_key': model_name,
			'subthings': subthings,
			'model': results[model_name],
			'model_name': model_name,
			'format_diff': format_diff,
			'cpus': cpus,
		})

if __name__ == '__main__':
	main()
