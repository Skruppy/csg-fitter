## This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0

import shell_worker
import plugin
import test_final_base
import util
import fmtjson
import fitter_cache
import os.path as op
import sys
import shutil
import numpy as np
import web
import math
import collections
import re
import itertools



def reduce_run_stats(test_run, evo_primitives_cnt):
	complete_stats_path = op.join(test_run.base_path, 'stats.json')
	reduced_stats_path = op.join(test_run.base_path, 'reduced-final-stats.json')
	
	if not util.outdated(reduced_stats_path, complete_stats_path):
		return util.dobjectify(util.read_json(reduced_stats_path))
	
	run_stats = util.dobjectify(util.read_json(op.join(test_run.base_path, 'stats.json')))
	
	short_stats = util.Dobject(
		cpu             = run_stats.cpu,
		host            = run_stats.host,
		buckets         = run_stats.processors.reducer.stats,
		overall_runtime = run_stats.processors.fitter.time / 1_000_000,
		complete_score  = run_stats.processors.final_score_complete.stats,
		reduced_score   = run_stats.processors.final_score_reduced.stats,
		simple_csg_tree = parse_csg_tree(util.read_json(f'{test_run.base_path}/output/simple_result.json')),
		raw_csg_tree    = parse_csg_tree(util.read_json(f'{test_run.base_path}/output/raw_result.json')),
	)
	
	fitter_type = run_stats.processors.fitter.type
	fitter_stats = run_stats.processors.fitter.stats
	
	if fitter_type == 'evo':
		short_stats.fitter = util.Dobject()
		timing_stats(run_stats.processors.fitter.time, [], short_stats.fitter)
		short_stats.evo_times = [(evo_primitives_cnt, run_stats.processors.fitter.time / 1_000_000)]
	
	elif fitter_type == 'clique_evo':
		clique_parser = CliqueParser()
		short_stats.fitter = clique_parser.parse(fitter_stats)
		short_stats.evo_times = [(x.primiteve_cnt, x.evo_durartion / 1_000_000) for x in fitter_stats.cliques]
	
	elif fitter_type == 'part_evo':
		short_stats.fitter = parse_part_stats(fitter_stats)
		short_stats.evo_times = [(x.primiteve_cnt, x.evo_duration / 1_000_000) for x in fitter_stats.clusters]
	
	util.write_json(short_stats, reduced_stats_path)
	
	return short_stats



## BEGIN CSG Tree stats ########################################################

def parse_csg_tree(root):
	## Init stats
	stats = util.Dobject(
		tree_height = 0,
		geo_levels = [],
		op_type_cnt = {'union': 0, 'intersect': 0, 'subtract': 0},
		op_widths = [],
	)
	
	## Traverse the tree
	_walk_csg_tree(stats, root, 0)
	
	## Postprocess the stats (compute derived properties)
	stats.geo_cnt = len(stats.geo_levels)
	stats.op_cnt = sum(stats.op_type_cnt.values())
	stats.node_cnt = stats.geo_cnt + stats.op_cnt
	
	return stats


def _walk_csg_tree(stats, node, level):
	stats.tree_height = max(stats.tree_height, level)
	
	if 'op' in node:
		stats.op_type_cnt[node['op']] += 1
		stats.op_widths.append(len(node['childs']))
		
		## Go deeper
		for child in node['childs']:
			_walk_csg_tree(stats, child, level+1)
	
	elif 'geo' in node:
		stats.geo_levels.append(level)
	
	else:
		assert()


def sum_csg_tree_stats(stats):
	results = sum_stats(stats)
	
	results.op_type_ratio = {}
	for op_type in ['union', 'intersect', 'subtract']:
		vals = list(map(lambda x: float(x.op_type_cnt[op_type]), stats))
		results.op_type_ratio[op_type] = np.mean(vals) / results.op_cnt_mean
	
	return results

## END CSG Tree stats ##########################################################

## BEGIN Timing stats ##########################################################

def timing_stats(ts, para_times, stats):
	para_times = list(map(lambda x: x/1_000_000, para_times))
	
	stats.para_nodes = len(para_times)
	
	stats.runtime_ts = ts/1_000_000
	stats.runtime_tp = sum(para_times)
	stats.runtime_T = stats.runtime_tp + stats.runtime_ts
	
	stats.para_factor = stats.runtime_tp / stats.runtime_T
	
	stats.para_worst_time = max(para_times) if para_times else None
	stats.runtime_actual_Tp = stats.para_worst_time + stats.runtime_ts if para_times else None
	stats.runtime_actual_eta_s = stats.runtime_T / stats.runtime_actual_Tp if para_times else None
	
	stats.runtime_theoretical_eta_s = stats.runtime_T / stats.runtime_ts
	stats.runtime_theoretical_Tp = stats.runtime_T / stats.runtime_theoretical_eta_s

## END Timing stats ############################################################

## BEGIN Partition stats #######################################################
## matrjoschka-1 part_evo results/1305b4ba2fd93958499836663c71dc0eaf46b1fd/0
## "clusters": [
##     {
##         "collect_points_durartion": 2,
##         "evo": {
##             "rounds": [...],
##             "type": "evo"
##         },
##         "evo_duration": 196151,
##         "point_cnt": 15,
##         "primiteve_cnt": 5
##     }
## ],
## "disconnect_duration": 13,
## "graph_gv": "graph G {\n\t\n\tedge [style=\"dotted\"]\n\t\n\tedge [style=\"bold\"]\n\top_7f7700669660 -- op_7f7700669990\n\top_7f7700669660 -- op_7f7700669cc0\n\top_7f7700669660 -- op_7f770066b000\n\top_7f7700669660 -- op_7f7700657e00\n\top_7f770066b000 -- op_7f7700669990\n\top_7f770066b000 -- op_7f7700669cc0\n\top_7f770066b000 -- op_7f7700657e00\n\top_7f7700669990 -- op_7f7700669cc0\n\top_7f7700669990 -- op_7f7700657e00\n\top_7f7700669cc0 -- op_7f7700657e00\n\t\n\tedge [style=\"\"]\n\top_7f7700669660 [label = <<TABLE BORDER=\"0\" CELLSPACING=\"1\" CELLPADDING=\"0\">\n\t\t<TR><TD COLSPAN=\"2\"><FONT POINT-SIZE=\"14\">cube<\/FONT><\/TD><\/TR>\n\t\t<TR><TD ALIGN=\"RIGHT\">position: <\/TD><TD ALIGN=\"LEFT\">0, 0, 0<\/TD><\/TR>\n\t\t<TR><TD ALIGN=\"RIGHT\">rotation: <\/TD><TD ALIGN=\"LEFT\">0°, 0°, 0°<\/TD><\/TR>\n\t\t<TR><TD ALIGN=\"RIGHT\">dimension: <\/TD><TD ALIGN=\"LEFT\">2, 2, 2<\/TD><\/TR>\n\t<\/TABLE>>, shape=box, fontsize=10, fontname=\"sans\", fillcolor=\"#ffcbbd\", style=\"filled,bold\"];\n\top_7f770066b000 [label = <<TABLE BORDER=\"0\" CELLSPACING=\"1\" CELLPADDING=\"0\">\n\t\t<TR><TD COLSPAN=\"2\"><FONT POINT-SIZE=\"14\">cylinder<\/FONT><\/TD><\/TR>\n\t\t<TR><TD ALIGN=\"RIGHT\">position: <\/TD><TD ALIGN=\"LEFT\">0, 0, 0<\/TD><\/TR>\n\t\t<TR><TD ALIGN=\"RIGHT\">rotation: <\/TD><TD ALIGN=\"LEFT\">0°, 0°, 90°<\/TD><\/TR>\n\t\t<TR><TD ALIGN=\"RIGHT\">height: <\/TD><TD ALIGN=\"LEFT\">3<\/TD><\/TR>\n\t\t<TR><TD ALIGN=\"RIGHT\">radius: <\/TD><TD ALIGN=\"LEFT\">0.7<\/TD><\/TR>\n\t<\/TABLE>>, shape=box, fontsize=10, fontname=\"sans\", fillcolor=\"#ffcbbd\", style=\"filled,bold\"];\n\top_7f7700669990 [label = <<TABLE BORDER=\"0\" CELLSPACING=\"1\" CELLPADDING=\"0\">\n\t\t<TR><TD COLSPAN=\"2\"><FONT POINT-SIZE=\"14\">cylinder<\/FONT><\/TD><\/TR>\n\t\t<TR><TD ALIGN=\"RIGHT\">position: <\/TD><TD ALIGN=\"LEFT\">0, 0, 0<\/TD><\/TR>\n\t\t<TR><TD ALIGN=\"RIGHT\">rotation: <\/TD><TD ALIGN=\"LEFT\">0°, 0°, 0°<\/TD><\/TR>\n\t\t<TR><TD ALIGN=\"RIGHT\">height: <\/TD><TD ALIGN=\"LEFT\">3<\/TD><\/TR>\n\t\t<TR><TD ALIGN=\"RIGHT\">radius: <\/TD><TD ALIGN=\"LEFT\">0.7<\/TD><\/TR>\n\t<\/TABLE>>, shape=box, fontsize=10, fontname=\"sans\", fillcolor=\"#ffcbbd\", style=\"filled,bold\"];\n\top_7f7700669cc0 [label = <<TABLE BORDER=\"0\" CELLSPACING=\"1\" CELLPADDING=\"0\">\n\t\t<TR><TD COLSPAN=\"2\"><FONT POINT-SIZE=\"14\">cylinder<\/FONT><\/TD><\/TR>\n\t\t<TR><TD ALIGN=\"RIGHT\">position: <\/TD><TD ALIGN=\"LEFT\">0, 0, 0<\/TD><\/TR>\n\t\t<TR><TD ALIGN=\"RIGHT\">rotation: <\/TD><TD ALIGN=\"LEFT\">90°, 0°, 0°<\/TD><\/TR>\n\t\t<TR><TD ALIGN=\"RIGHT\">height: <\/TD><TD ALIGN=\"LEFT\">3<\/TD><\/TR>\n\t\t<TR><TD ALIGN=\"RIGHT\">radius: <\/TD><TD ALIGN=\"LEFT\">0.7<\/TD><\/TR>\n\t<\/TABLE>>, shape=box, fontsize=10, fontname=\"sans\", fillcolor=\"#ffcbbd\", style=\"filled,bold\"];\n\top_7f7700657e00 [label = <<TABLE BORDER=\"0\" CELLSPACING=\"1\" CELLPADDING=\"0\">\n\t\t<TR><TD COLSPAN=\"2\"><FONT POINT-SIZE=\"14\">sphere<\/FONT><\/TD><\/TR>\n\t\t<TR><TD ALIGN=\"RIGHT\">center: <\/TD><TD ALIGN=\"LEFT\">0, 0, 0<\/TD><\/TR>\n\t\t<TR><TD ALIGN=\"RIGHT\">radius: <\/TD><TD ALIGN=\"LEFT\">1.3<\/TD><\/TR>\n\t<\/TABLE>>, shape=box, fontsize=10, fontname=\"sans\", fillcolor=\"#ffcbbd\", style=\"filled,bold\"];\n}\n",
## "identify_duration": 5,
## "merge_duration": 196154,
## "prune_duration": 1,
## "reattach_duration": 0,
## "type": "part_evo"

def parse_part_stats(s):
	scores = list(map(evo_score, s.clusters))
	stats = util.Dobject(
		scores = scores,
		job_cnt = len(s.clusters),
		worst_score = max(scores),
		primitive_cnt = list(map(lambda x: x.primiteve_cnt, s.clusters)),
	)
	
	timing_stats(
		s.prune_duration + s.disconnect_duration + s.identify_duration + s.reattach_duration,
		map(lambda x: x.collect_points_durartion + x.evo_duration, s.clusters),
		stats,
	)
	
	return stats


def sum_part_stats(s):
	return sum_stats(s)

## END Partition stats #########################################################

## BEGIN Clique stats ##########################################################
## matrjoschka-1 clique_evo results/8b3151f73f3e31f0a6c3dfcebb1f4c579e348475/0
## "cliques": [
##     {
##         "collect_points_durartion": 5,
##         "evo": {
##             "rounds": [...],
##             "type": "evo"
##         },
##         "evo_durartion": 123504,
##         "point_cnt": 15,
##         "primiteve_cnt": 5
##     }
## ],
## "detect_po_durartion": 138,
## "failed_merges": 0,
## "find_cliques_durartion": 44,
## "merge_durartion": 0,
## "merge_gv": "digraph G {\n\top_2251865110 [label = \"2251865110\n(Clique 0)\"];\n}\n",
## "point_cnt": 15,
## "primiteve_cnt": 5,
## "successful_merges": 0,
## "type": "clique_evo"

class CliqueParser:
	def __init__(self):
		self._link_re = re.compile(r'^(op_cl_\d+) -> (op_cl_\d+);$')
		self._merge_node_re = re.compile(r'^(op_cl_\d+) \[label = "\d+"\];$')
		self._clique_node_1_re = re.compile(r'^(op_cl_\d+) \[fillcolor.*?label="Clique (\d+)"\];$')
		
		## Those are just here for reference, they will be reinited by parse()
		self._cliques = []
		self._nodes = {}
		self._merge_nodes = []
	
	
	def parse(self, s):
		self._init_cliques(s.cliques)
		self._init_nodes(s.merge_gv)
		self._check()
		
		stats = self._stats
		timing_stats(
			s.detect_po_durartion + s.find_cliques_durartion + s.merge_durartion,
			map(lambda x: x.collect_points_durartion + x.evo_durartion, s.cliques),
			stats,
		)
		return stats
	
	
	def _init_cliques(self, cliques):
		self._cliques = []
		for i, c in enumerate(cliques):
			info = util.Dobject(
				idx = i,
				node = None,
				point_cnt = c.point_cnt,
				primitive_cnt = c.primiteve_cnt,
			)
			info.best_score = sorted(c.evo.rounds[-1].scores)[0]
			self._cliques.append(info)
	
	
	def _init_nodes(self, gv):
		self._nodes = {}
		self._merge_nodes = []
		expected_clique_node = None
		
		gv = gv.strip().split('\n')
		assert(gv[0] == 'digraph G {')
		assert(gv[-1] == '}')
		
		for line in gv[1:-1]:
			## Skip empty lines
			line = line.strip()
			if not line: continue
			
			## Line is node link `op_cl_123 -> op_cl_987;`
			m = self._link_re.match(line)
			if m:
				parent = self._get_node(m.group(2))
				child = self._get_node(m.group(1))
				assert(child.parent is None)
				child.parent = parent
				parent.children.append(child)
				continue
			
			## Line is merge node definition `op_cl_123 [label = "123"];`
			m = self._merge_node_re.match(line)
			if m:
				n = self._get_node(m.group(1))
				assert(n.type == None)
				n.type = 'merge'
				self._merge_nodes.append(n)
				continue
			
			## Line is clique node `op_cl_123 [fillcolor=... label = "Clique 42"];`
			m = self._clique_node_1_re.match(line)
			if m:
				n = self._get_node(m.group(1))
				assert(n.type == None)
				n.type = 'clique'
				n.clique = int(m.group(2))
				
				c = self._cliques[n.clique]
				assert(c.node is None)
				c.node = n
				
				continue
			
			assert()
	
	
	def _get_node(self, name):
		assert(name.startswith('op_cl_'))
		
		if name not in self._nodes:
			self._nodes[name] = util.Dobject(
				name = name,
				children = [],
				parent = None,
				type = None,
				clique = None,
			)
		
		return self._nodes[name]
	
	
	def _check(self):
		## Check numers are correct
		assert(len(self._merge_nodes) == len(self._cliques) - 1)
		assert(len(self._nodes) == len(self._merge_nodes) + len(self._cliques))
		
		## Check clique nodes
		assert(all(map(lambda x: x.node is not None, self._cliques)))
		assert(all(map(lambda x: len(x.node.children) == 0, self._cliques)))
		
		## Check merge nodes
		assert(all(map(lambda x: len(x.children) == 2, self._merge_nodes)))
		
		## Check that there is exactly one root
		roots = list(filter(lambda x: x.parent is None, self._nodes.values()))
		assert(len(roots) == 1)
	
	
	@property
	def _stats(self):
		## * Clique count (same)
		## * Merge node count (same, derived)
		## * Node count (same, derived)
		## * Avg. Merge node children (same, fixed)
		## * Tree height
		## * Avg. clique score
		## * Worst clique score
		if len(self._merge_nodes) <= 2:
			unbalancedness = 0
		else:
			unbalancedness = sum(map(self.is_unbalanced, self._merge_nodes)) / (len(self._merge_nodes) - 2)
		
		scores = list(map(lambda x: x.best_score, self._cliques))
		primitive_cnt = list(map(lambda x: x.primitive_cnt, self._cliques))
		
		return util.Dobject(
			job_cnt = len(self._cliques),
			scores = scores,
			worst_score = max(scores),
			primitive_cnt = primitive_cnt,
			unbalancedness = unbalancedness,
		)
	
	
	@staticmethod
	def is_unbalanced(node):
		if len(node.children) == 0:
			return False
		else:
			[l, r] = node.children
			return \
				CliqueParser.is_unbalanced(l) or \
				CliqueParser.is_unbalanced(r) or \
				abs(CliqueParser.tree_height(l) - CliqueParser.tree_height(r)) > 1
	
	
	@staticmethod
	def tree_height(node):
		if len(node.children) == 0:
			return 0
		else:
			return max(map(CliqueParser.tree_height, node.children)) + 1



def sum_clique_stats(s):
	stats = sum_stats(s)
	
	stats.job_cnt = collections.Counter(map(lambda x: x.job_cnt, s)).most_common(1)[0][0]
	stats.failed_cliques = list(filter(lambda x: x.job_cnt != stats.job_cnt, s))
	
	return stats

## END Clique stats ############################################################

## BEGIN Common utils ##########################################################

def sum_stats(stats, skip=[]):
	results = util.Dobject(
		n = len(stats),
	)
	
	for key, example_value in stats[0].items():
		if key in skip:
			continue
		
		t = type(example_value)
		if t == list:
			vals = list(itertools.chain(*map(lambda x: x[key], stats)))
		elif t == int or t == float:
			vals = list(map(lambda x: x[key], stats))
		else:
			continue
		
		results[f'{key}_mean'] = np.mean(vals)
		results[f'{key}_std'] = np.std(vals)
	
	return results


def evo_score(evo_stats):
	if evo_stats.evo.rounds:
		return sorted(evo_stats.evo.rounds[-1].scores)[0]
	else:
		return 0

## END Common utils ############################################################
