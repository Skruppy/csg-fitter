## This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0

import hashlib
import inspect
import io
import json
import os
import os.path as op
import re
import shutil
import subprocess
import textwrap
import time



def script_dir(resolve_symlink=True):
	## see: https://stackoverflow.com/a/6098238
	path = op.abspath(inspect.getfile(inspect.currentframe().f_back))
	if resolve_symlink: path = op.realpath(path)
	return op.dirname(path)


def mkdirs(path):
	if not op.isdir(path):
		os.makedirs(path)


def sh(*args):
	print('\033[1m> '+(' '.join(args))+'\033[0m')
	subprocess.run(args, check=True)


def read_file(path):
	with open(path) as f:
		return f.read()


def write_file(data, path):
	with open(path, 'w') as f:
		f.write(data)


def read_json(path):
	with open(path) as f:
		return json.load(f)


def write_json(data, path):
	with open(path, 'w') as f:
		json.dump(data, f, indent='\t', sort_keys=True)


def path_length(path):
	path = op.normpath(path)
	i = 0
	while path != '' and path != '/':
		path = op.dirname(path)
		i += 1
	return i


def return_path(path, offset=0):
	length = path_length(path) - offset - 1
	if length <= 0: return '.'
	return op.join(*(['..'] * length))


def split(l, seperator, limit=0):
	start = 0
	ret = []
	for end, item in enumerate(l):
		if item == seperator:
			ret.append(l[start:end])
			start = end+1
			if limit > 0 and len(ret) == limit:
				break
	
	ret.append(l[start:])
	return ret


def sed(path, old, new):
	with open(path, 'r') as f:
		lines = f.readlines()
	
	with open(path, 'w') as f:
		for line in lines:
			f.write(line.replace(old, new))


def parts(stop, n, symetric=False):
	assert(stop > 1)
	assert(2 <= n <= stop)
	
	if symetric and size%2 == 0 and n%2 == 1:
		n -= 1
	
	step = (stop-1) / (n-1)
	for i in range(n):
		yield round(i * step)


def realign(text):
	## Delete leading space
	text = textwrap.dedent(text)
	
	## Delete the first or last line, if empty
	return text[(1 if text[0]=='\n' else 0):]


def listify(x):
	if x is None:
		return list()
	elif isinstance(x, list):
		return x
	elif isinstance(x, set) or isinstance(x, tuple):
		return list(x)
	return [x]


def outdated(targets, dependencies):
	newest_mod = max(map(op.getmtime, listify(dependencies)))
	for target in listify(targets):
		if not op.exists(target) or op.getmtime(target) < newest_mod:
			return True
	return False


def ldict(symbols, ldict_base=None, **nargs):
	env = inspect.stack()[1][0].f_locals
	
	d = nargs
	if ldict_base is not None:
		d.update(ldict_base)
	
	for symbol in symbols:
		key = symbol
		if key.endswith('_'):
			key = key[:-1]
		d[key] = env[symbol]
	
	return d


class Benchmark:
	def __init__(self, name):
		self.__name = name
	
	def __enter__(self):
		self.__startTs = time.monotonic()
	
	def __exit__(self, type, value, traceback):
		timeElapsed = time.monotonic() - self.__startTs
		print(f'\033[1;36m{self.__name} took {timeElapsed}\033[0m')


class Dobject(dict):
	def __init__(self, *pargs, **kargs):
		d = {}
		for patch in pargs:
			d.update(patch)
		d.update(kargs)
		super().__init__(d)
	
	def copy(self):
		return Dobject(super().copy())
	
	__getattr__ = dict.__getitem__
	__setattr__ = dict.__setitem__


def dobjectify(thing):
	if issubclass(type(thing), dict):
		return Dobject((k, dobjectify(v)) for k, v in thing.items())
	
	elif issubclass(type(thing), list):
		return list(map(dobjectify, thing))
	
	else:
		return thing


class Di(object):
	def __init__(self, factories=None):
		self._factories = factories if factories else {}
		self._instances = {}
	
	
	def __getitem__(self, key):
		if key in self._instances:
			return self._instances[key]
		
		elif key in self._factories:
			instance = self._factories[key](self)
			self._instances[key] = instance
			return instance
		
		else:
			raise AttributeError(f'Dependency injector object has factory \'{key}\'')
	
	def __getattr__(self, key):
		return self[key]
	
	def is_instanciated(self, key):
		return key in self._instances
	
	def add_instance(self, name, value):
		if name in self._factories:
			raise ValueError(f'Factory \'{name}\' already exists')
		self._factories[name] = None
		self._instances[name] = value
	
	def add_factory(self, name, factory):
		if name in self._factories:
			raise ValueError(f'Factory \'{name}\' already exists')
		self._factories[name] = factory
	
	@property
	def instances(self):
		return self._instances.copy()
	
	@property
	def factories(self):
		return self._factories.copy()


def sha1sum(s):
	m = hashlib.sha1()
	
	if isinstance(s, str):
		m.update(s.encode('utf-8'))
	elif isinstance(s, bytes):
		m.update(s)
	elif issubclass(type(s), io.IOBase):
		m.update(s.read())
	
	return m.hexdigest()


def set_limits(ax, x=None, y=None, padding=0.05):
	if x is not None:
		x_pad = (x[1] - x[0]) * padding
		ax.set_xlim(x[0]-x_pad, x[1]+x_pad)
	
	if y is not None:
		y_pad = (y[1] - y[0]) * padding
		ax.set_ylim(y[0]-y_pad, y[1]+y_pad)


class Benchmark:
	template = '\033[1;36m{name} took {time}\033[0m'
	
	def __init__(self, name):
		self.__name = name
	
	def __enter__(self):
		self.__startTs = time.monotonic()
	
	def __exit__(self, type, value, traceback):
		timeElapsed = time.monotonic() - self.__startTs
		print(self.template.format(name=self.__name, time=timeElapsed))


def copytree(src, dst):
	## Cleanup destination
	if op.islink(dst):
		os.remove(dst)
	
	elif isdir(dst):
		if not isdir(src):
			shutil.rmtree(dst)
	
	elif isfile(dst):
		if not isfile(src):
			os.remove(dst)
	
	## Copy source to destination (and recurse)
	if isdir(src):
		os.makedirs(dst, exist_ok=True)
		for item in os.listdir(src):
			s = op.join(src, item)
			d = op.join(dst, item)
			copytree(s, d)
	
	else:
		shutil.copy2(src, dst, follow_symlinks=False)


def isdir(path, allow_link=False):
	return op.isdir(path) and (allow_link or not op.islink(path))


def isfile(path, allow_link=False):
	return op.isfile(path) and (allow_link or not op.islink(path))


def what_is(thingy):
	print(f'Type: {type(thingy)}')
	print(f'String: {str(thingy)}')
	print(f'Hash: {id(thingy)}')
	print(f'Dir:')
	for member in dir(thingy):
		print(f'  {member}')
	print()


def replace(text, replacements):
	replacements = dict((re.escape(k), v) for k, v in replacements.items())
	pattern = re.compile("|".join(replacements.keys()))
	return pattern.sub(lambda m: replacements[re.escape(m.group(0))], text)


def escapeLatex(text):
	return replace(text, {
		'\u202F': '\\,',
		'&': '\\&',
		'%': '\\%',
		'$': '\\$',
		'#': '\\#',
		'_': '\\_',
		'{': '\\{',
		'}': '\\}',
		'~': '\\textasciitilde{}',
		'^': '\\textasciicircum{}',
		'\\': '\\textbackslash{}',
	})


def str_range(s, a, b=None):
	if b is None:
		min_idx = 0
		max_idx = a
	else:
		min_idx = a
		max_idx = b
	assert(max_idx >= min_idx)
	
	exps = list(filter(bool, map(lambda x: x.strip(), s.split(','))))
	if '*' in exps:
		for i in range(min_idx, max_idx+1):
			yield i
		return
	
	ranges = []
	for exp in exps:
		parts = exp.split('-', 1)
		if len(parts) == 1:
			start = end = int(parts[0])
		else:
			start = int(parts[0]) if parts[0] else min_idx
			end   = int(parts[1]) if parts[1] else max_idx
		
		if end < start:
			raise Exception(f'{exp}: end < start!')
		if start < min_idx or end > max_idx:
			raise Exception(f'{exp}: Range not within {min_idx} and {max_idx}')
		
		ranges.append((start, end))
	
	last_idx = min_idx
	for start, end in sorted(ranges):
		for i in range(max(start, last_idx), end+1): yield i
		last_idx = end+1

#assert(list(str_range('4', 50)) == [4])
#assert(list(str_range('4,6', 50)) == [4,6])
#assert(list(str_range('4, ,', 50)) == [4])
#assert(list(str_range('  ', 50)) == [])
#assert(list(str_range('4,5', 50)) == [4,5])
#assert(list(str_range(' 4 , 6 - 8 ', 50)) == [4,6,7,8])
#assert(list(str_range('-3', 1, 50)) == [1,2,3])
#assert(list(str_range('48-', 50)) == [48,49,50])
#assert(list(str_range('1-4,3-6', 50)) == [1,2,3,4,5,6])
#assert(list(str_range('1-6,2-5', 50)) == [1,2,3,4,5,6])
#assert(list(str_range('1-3,4-6', 50)) == [1,2,3,4,5,6])
