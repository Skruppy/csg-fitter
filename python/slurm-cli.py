#!/usr/bin/env python3
import socket
import json
import threading
import time
import sys
import signal
import util
import shlex
import curses
import datetime
import sys
import inspect
import argparse
import logging



def fmt_msg(msg):
	args = msg['args']
	fmt_args = ', '.join([f'{k}={repr(args[k])}' for k in sorted(args.keys())])
	return f'{msg["signal"]}({fmt_args})'


class Connection(threading.Thread):
	def __init__(self, msg_handler, host='localhost', port=7492):
		super().__init__(daemon=True)
		self.host = host
		self.port = port
		self.msg_handler = msg_handler
		self.running = True
		self.connected = False
		self.start()
	
	
	def run(self):
		backoff_time = start_backoff_time = 2
		max_backoff_time = 120
		while self.running:
			with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as self.s:
				try:
					self.s.connect((self.host, self.port))
				except (ConnectionRefusedError, socket.gaierror) as ex:
					logging.warning(f'Failed to connect. Retying in {backoff_time}s: {ex}')
					time.sleep(backoff_time)
					backoff_time = int(backoff_time * 1.5)
					if backoff_time > max_backoff_time:
						backoff_time = max_backoff_time
					continue
				
				self.s.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
				self.s.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPIDLE, 120) ## After 2 minutes of inactiviety
				self.s.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPINTVL, 30) ## send keep alives every 30 seconds
				self.s.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPCNT, 10) ## and close the connection after 7 minutes
				self.connected = True
				
				backoff_time = start_backoff_time
				with self.s.makefile() as f:
					logging.info('Connected')
					self.msg_handler.on_connect()
					
					while True:
						line = f.readline()
						if line == '':
							break
						msg = json.loads(line)
						logging.debug(f'S: {fmt_msg(msg)}')
						getattr(self.msg_handler, f'on_sig_{msg["signal"]}')(**msg['args'])
				
				logging.info('Disconnected')
				self.connected = False
				self.msg_handler.on_disconnect()
				
				if self.running:
					time.sleep(2)
	
	
	def __getitem__(self, key):
		return lambda **args: self.signal(key, **args)
	
	def signal(self, signal, **args):
		msg = {'signal': signal, 'args': args}
		logging.debug(f'C: {fmt_msg(msg)}')
		try:
			self.s.sendall(json.dumps(msg).encode('utf-8')+b'\n')
		except BrokenPipeError:
			pass
	
	def stop(self):
		self.running = False
		self.s.shutdown(socket.SHUT_RDWR)


class Indicator:
	chars = "|\-/"
	def __init__(self):
		self.i = 0
	
	def next(self):
		self.i = (self.i + 1) % len(self.chars)
		return self.chars[self.i]




class Table:
	def __init__(self, window):
		self.window = window
		self.data = []
		self.cursor_line = 0
		self.start_line = 0
		self.cursor_padding = 5
		self.page_start = 0
		self.selected = set()
		(self.lines, self.cols) = window.getmaxyx()
	
	
	def redraw(self):
		self.window.clear()
		
		table_height = self.lines
		page_end = min(len(self.data), self.page_start + table_height)
		for i, item in enumerate(self.data[self.page_start:page_end]):
			mode =  curses.A_BOLD if self.cursor_line == i+self.page_start else curses.A_NORMAL
			if self.item_key(item) in self.selected:
				mode |= curses.A_REVERSE
			
			line = ' | '.join(self.item_rep(item))
			self.window.addstr(i, 0, line[:self.cols-1], mode)
		
		self.window.refresh()
	
	
	def set_data(self, data):
		self.data = data
	
	def item_key(self, item):
		raise NotImplemented()
	
	def item_rep(self, item):
		raise NotImplemented()
	
	def toggle_selection(self):
		item_key = self.item_key(self.data[self.cursor_line])
		if item_key not in self.selected:
			self.selected.add(item_key)
		else:
			self.selected.remove(item_key)
	
	def status_text(self):
		status = f'Cursor: {self.cursor_line + 1} of {len(self.data)}'
		if self.selected:
			status += f' | {len(self.selected)} selected'
		return status
	
	def handle_key(self, key):
		table_height = self.lines
		
		## Line + 1
		if key == 'KEY_DOWN':
			self.cursor_line = min(self.cursor_line + 1, len(self.data) - 1)
			if self.cursor_line > self.page_start + table_height - self.cursor_padding - 1:
				self.page_start = min(self.cursor_line - table_height + self.cursor_padding + 1, len(self.data) - table_height)
			self.foo = self.cursor_line
		
		## Line - 1
		elif key == 'KEY_UP':
			self.cursor_line = max(self.cursor_line - 1, 0)
			if self.cursor_line < self.page_start + self.cursor_padding:
				self.page_start = max(self.cursor_line - self.cursor_padding, 0)
		
		## Line + 1
		elif key == 'KEY_SF':
			self.cursor_line = min(self.cursor_line + 1, len(self.data) - 1)
			if self.cursor_line > self.page_start + table_height - self.cursor_padding - 1:
				self.page_start = min(self.cursor_line - table_height + self.cursor_padding + 1, len(self.data) - table_height)
			self.toggle_selection()
		
		## Line - 1
		elif key == 'KEY_SR':
			self.cursor_line = max(self.cursor_line - 1, 0)
			if self.cursor_line < self.page_start + self.cursor_padding:
				self.page_start = max(self.cursor_line - self.cursor_padding, 0)
			self.toggle_selection()
		
		## Line + page
		elif key == 'KEY_NPAGE':
			d = self.cursor_line - self.page_start
			self.cursor_line += self.lines - 1
			if self.cursor_line > len(self.data) - 1:
				self.cursor_line = len(self.data) - 1
			self.page_start = self.cursor_line - d
		
		## Line - page
		elif key == 'KEY_PPAGE':
			self.cursor_line -= self.lines - 1
			if self.cursor_line < 0:
				self.cursor_line = 0
			self.page_start -= self.lines - 1
			if self.page_start < 0:
				self.page_start = 0
		
		elif key == 'KEY_HOME':
			self.cursor_line = 0
			self.page_start = 0
		
		elif key == 'KEY_END':
			self.cursor_line = len(self.data) - 1
			self.page_start = max(0, len(self.data) - table_height)
		
		elif key == ' ':
			self.toggle_selection()
		
		elif key == '\x1B': ## ESC
			self.selected.clear()
		
		else:
			return False
		
		self.redraw()
		return True


## {'id': '0123456789abcdef/0', 'hash': '0123456789abcdef', 'nr': 0, 'args': ['dummy', '5'],
## 'exit_code': None, 'last_log_msg': '', 'paused': False, 'worker': None}
class JobsTable(Table):
	def __init__(self, window):
		super().__init__(window)
	
	def item_key(self, item):
		return item.id
	
	def item_rep(self, item):
		cells = []
		
		cells.append(f'{item.hash[:6]}/{item.nr: <2}')
		
		## p | e | w |
		## f | n | n | available                 | ▶ waiting
		## f | n | s | running                   | ▶ running @ c0de42
		## f | 0 | n | done                      | ▶ done
		## f | 0 | s | RE-RUNNING done           | ▶ done @ c0de42
		## f | ! | n | FAILED                    | ▶ FAILED (42)
		## f | ! | s | RE-RUNNING FAILED         | ▶ FAILED (42) @ c0de42
		## t | n | n | paused                    | ⏸ waiting
		## t | n | s | RUNNING (paused)          | ⏸ running @ c0de42
		## t | 0 | n | done (paused)             | ⏸ done
		## t | 0 | s | RE-RUNNING (paused)       | ⏸ done @ c0de42
		## t | ! | n | FAILED (paused)           | ⏸ FAILED (42)
		## t | ! | s | RE-RUNNING FAILED (paused | ⏸ FAILED (42) @ c0de42
		
		status = '⏸ ' if item.paused else '▶ '
		if item.exit_code is None:
			status += 'waiting' if item.worker is None else 'running'
		elif item.exit_code == 0:
			status += 'done'
		else:
			status += f'FAILED ({item.exit_code})'
		
		if item.worker is not None:
			status += f' @ {item.worker[:6]}'
		
		cells.append(f'{status:25.25}')
		
		cells.append(item.last_log_msg)
		#cells.append(shlex.join(item.job))
		
		return cells


## {'id': 'd026511c-8a68-4f4b-b4b1-d6c5047312a2', 'name': 'w3',
## 'start_time': '2021-09-24 02:07:22.159179', 'paused': False,
## 'host': '172.17.0.1:42180', 'job_id': '0123456789abcdef/8', 'last_log_msg': 'Started'}
class WorkersTable(Table):
	def __init__(self, window):
		super().__init__(window)
	
	def item_key(self, item):
		return item.id
	
	def item_rep(self, item):
		cells = []
		
		cells.append(f'{item.name:10.10}')
		cells.append(item.id[:6])
		
		status = '⏸ ' if item.paused else '▶ '
		if item.job_id is not None:
			(hash, id) = item.job_id.split('/')
			status += f'{hash[:6]}/{id}'
		cells.append(f'{status:13.13}')
		
		host = item.host if item.host else '--'
		cells.append(f'{host:30.30}')
		
		cells.append(item.last_log_msg)
		
		return cells


class Cli:
	def __init__(self):
		self.running = True
		self.update_indicator = Indicator()
		self.key = ''
		self.foo = ''
		self.exit = 0
		self.page_start = 0
		self.workers_status = ''
		self.jobs_status = ''
		
		parser = argparse.ArgumentParser()
		con_defaults = inspect.signature(Connection.__init__).parameters
		parser.add_argument('--host', '-H', default=con_defaults['host'].default)
		parser.add_argument('--port', '-P', default=con_defaults['port'].default, type=int)
		parser.add_argument('job_file', nargs='?')
		args = parser.parse_args()
		self.job_file = args.job_file
		self.con = Connection(self, args.host, args.port)
	
	
	def main(self):
		if self.job_file is None:
			sys.exit(self.run())
		else:
			with open(self.job_file) as f:
				for i, job in enumerate(json.load(f)):
					self.add_job(job['hash'], job['nr'], job['args'])
	
	def on_connect(self):
		self.con['register_controller']()
	
	def on_disconnect(self):
		pass
	
	def add_job(self, hash, nr, args):
		self.con['add_job'](hash=hash, nr=nr, args=args)
	
	def on_sig_worker_report(self, workers):
		workers = util.dobjectify(workers)
		self.workers_table.set_data(workers)
		if self.table == self.workers_table:
			self.table.redraw()
		
		parts = []
		cnt = len(list(filter(lambda x: x.job_id is not None, workers)))
		parts.append(f'{cnt}/{len(workers)-cnt} working/idle')
		
		cnt = len(list(filter(lambda x: x.host is not None, workers)))
		parts.append(f'{cnt}/{len(workers)-cnt} connected/offline')
		
		cnt = len(list(filter(lambda x: x.paused, workers)))
		parts.append(f'{cnt}/{len(workers)-cnt} paused/unpaused')
		
		parts.append(f'{len(workers)} total')
		self.workers_status = ' ~ '.join(parts)
		
		self.redraw()
	
	def on_sig_job_report(self, jobs):
		jobs = util.dobjectify(jobs)
		self.jobs_table.set_data(jobs)
		if self.table == self.jobs_table:
			self.table.redraw()
		
		parts = []
		if cnt := len(list(filter(lambda x: x.exit_code is None and x.worker is None, jobs))):
			parts.append(f'{cnt} waiting')
		
		if cnt := len(list(filter(lambda x: x.worker is not None, jobs))):
			parts.append(f'{cnt} running')
		
		if cnt := len(list(filter(lambda x: x.exit_code is not None and x.exit_code != 0, jobs))):
			parts.append(f'{cnt} failed')
		
		if cnt := len(list(filter(lambda x: x.exit_code is not None and x.exit_code == 0, jobs))):
			parts.append(f'{cnt} done')
		
		if cnt := len(list(filter(lambda x: x.paused, jobs))):
			parts.append(f'{cnt} paused')
		
		parts.append(f'{len(jobs)} total')
		self.jobs_status = ' / '.join(parts)
		
		self.redraw()
	
	def redraw(self):
		self.status_window.clear()
		(self.cols, self.lines) = self.status_window.getmaxyx()
		self.status_window.addstr(0, 0, f'WORKERS: {self.workers_status}', curses.A_REVERSE)
		self.status_window.addstr(1, 0, f'JOBS: {self.jobs_status}', curses.A_REVERSE)
		self.status_window.addstr(2, 0, self.table.status_text(), curses.A_REVERSE)
		self.status_window.refresh()
	
	def run(self):
		curses.wrapper(self._loop)
		self.con.stop()
		return self.exit
	
	def selected(self):
		t = self.table
		return list(t.selected) if t.selected else [t.item_key(t.data[t.cursor_line])]
	
	def _loop(self, window):
		self.window = window
		self.status_window = window.subwin(3, curses.COLS, 0, 0)
		self.main_window = window.subwin(3, 0)
		
		self.jobs_table = JobsTable(self.main_window)
		self.workers_table = WorkersTable(self.main_window)
		self.table = self.jobs_table
		curses.curs_set(0)
		
		update_requested = True
		while self.running:
			if update_requested:
				if self.con.connected:
					self.con['req_worker_report']()
					self.con['req_job_report']()
					next_update = datetime.datetime.now() + datetime.timedelta(seconds=60)
					update_requested = False
				else:
					next_update = datetime.datetime.now() + datetime.timedelta(seconds=1)
				self.foo=str(datetime.datetime.now())
			self.window.timeout(int(max((next_update - datetime.datetime.now()).total_seconds(), 0)*1000))
			
			self.redraw()
			try:
				self.key = self.window.getkey()
				if self.key == 'q':
					self.running = False
				elif self.key == 'Q':
					self.running = False
					self.exit = 1
				elif self.key == 'r':
					update_requested = True
				elif self.key == 'j':
					self.table = self.jobs_table
					self.table.redraw()
				elif self.key == 'w':
					self.table = self.workers_table
					self.table.redraw()
				elif self.key == 'p':
					if self.table == self.workers_table:
						self.con['workers_set_pause'](worker_ids=self.selected(), paused=True)
					elif self.table == self.jobs_table:
						self.con['jobs_set_pause'](job_ids=self.selected(), paused=True)
				elif self.key == 'P':
					if self.table == self.workers_table:
						self.con['workers_set_pause'](worker_ids=self.selected(), paused=False)
					elif self.table == self.jobs_table:
						self.con['jobs_set_pause'](job_ids=self.selected(), paused=False)
				elif self.key == 'C':
					if self.table == self.workers_table:
						self.con['workers_cancel'](worker_ids=self.selected())
				elif self.key == 'S':
					if self.table == self.workers_table:
						self.con['workers_stop'](worker_ids=self.selected())
				else:
					self.table.handle_key(self.key)
			except:
				update_requested = True
			self.redraw()



cli = Cli()
cli.main()

## https://slurm.schedmd.com/sbatch.html#SECTION_OUTPUT-ENVIRONMENT-VARIABLES
## SLURM_STEP_NUM_NODES=1
## SLURM_STEP_NUM_TASKS=1
## SLURM_STEP_TASKS_PER_NODE=1
## SLURM_STEP_LAUNCHER_PORT=42381
## SLURM_SRUN_COMM_HOST=141.84.220.103
## SLURM_TOPOLOGY_ADDR=haselgraben
## SLURM_TOPOLOGY_ADDR_PATTERN=node
## SLURM_CPUS_ON_NODE=4 (Number of CPUs allocated to the batch step.)
## SLURM_CPU_BIND=quiet,mask_cpu:0xF
## SLURM_CPU_BIND_LIST=0xF
## SLURM_CPU_BIND_TYPE=mask_cpu:
## SLURM_CPU_BIND_VERBOSE=quiet
## SLURM_TASK_PID=842266
## SLURM_NODEID=0
## SLURM_PROCID=0
## SLURM_LOCALID=0
## SLURM_LAUNCH_NODE_IPADDR=141.84.220.103
## SLURM_GTIDS=0
## SLURM_JOB_UID=27349
## SLURM_JOB_USER=buchdrucker
## SLURM_JOB_GID=21000
## SLURMD_NODENAME=haselgraben




## SLURM_PRIO_PROCESS=0
## SRUN_DEBUG=3
## SLURM_UMASK=0077
## SLURM_CLUSTER_NAME=ificip
## SLURM_SUBMIT_DIR=/home/b/buchdrucker/csgfitter
## SLURM_SUBMIT_HOST=lam.cip.ifi.lmu.de
## SLURM_JOB_NAME=env
## SLURM_JOB_CPUS_PER_NODE=4
## SLURM_NTASKS=2
## SLURM_NPROCS=2
## SLURM_JOB_ID=367057
## SLURM_JOBID=367057
## SLURM_STEP_ID=0
## SLURM_STEPID=0
## SLURM_NNODES=1
## SLURM_JOB_NUM_NODES=1
## SLURM_NODELIST=kirnach
## SLURM_JOB_PARTITION=All
## SLURM_TASKS_PER_NODE=2
## SLURM_SRUN_COMM_PORT=43959
## SLURM_JOB_ACCOUNT=stud_ifi
## SLURM_JOB_QOS=normal
## SLURM_WORKING_CLUSTER=ificip:slurmmaster2:6817:8704:102
## SLURM_JOB_NODELIST=kirnach
## SLURM_STEP_NODELIST=kirnach
## SLURM_STEP_NUM_NODES=1
## SLURM_STEP_NUM_TASKS=2
## SLURM_STEP_TASKS_PER_NODE=2
## SLURM_STEP_LAUNCHER_PORT=43959
## SLURM_SRUN_COMM_HOST=141.84.220.103
## SLURM_TOPOLOGY_ADDR=kirnach
## SLURM_TOPOLOGY_ADDR_PATTERN=node
## SLURM_CPUS_ON_NODE=4
## SLURM_CPU_BIND=quiet,mask_cpu:0xF
## SLURM_CPU_BIND_LIST=0xF
## SLURM_CPU_BIND_TYPE=mask_cpu:
## SLURM_CPU_BIND_VERBOSE=quiet
## SLURM_TASK_PID=809332
## SLURM_NODEID=0
## SLURM_PROCID=1
## SLURM_LOCALID=1
## SLURM_LAUNCH_NODE_IPADDR=141.84.220.103
## SLURM_GTIDS=0,1
## SLURM_JOB_UID=27349
## SLURM_JOB_USER=buchdrucker
## SLURM_JOB_GID=21000
## SLURMD_NODENAME=kirnach

