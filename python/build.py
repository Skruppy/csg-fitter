#!/usr/bin/env python3
## This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0

import argparse
import subprocess
import sys
import shutil
import util
import os
import os.path as op
import memtime
import dataclasses
import enum
import typing
import itertools
import glob
import datetime



no_action = False
keep_going = False
verbosity = None

################################################################################
#region Executors
class Executor():
	def __init__(self, bc):
		self.bc = bc
	
	@property
	def args(self):
		return self.runner_args + self.target_args + self.compiler_args + \
			self.mode_args + self.config_args + self.type_args
	
	@property
	def target_args(self):
		return [] if self.bc.target.is_exe else [f':{self.bc.target.value}']
	
	@property
	def runner_args(self):
		raise NotImplemented()
	
	@property
	def compiler_args(self):
		return [f'--compiler={self.bc.compiler.value}']
	
	@property
	def mode_args(self):
		return ['--build-mode=singleFile'] if self.bc.mode == Mode.SEPARATED else []
	
	@property
	def config_args(self):
		return [f'--config={self.bc.config.value}']
	
	@property
	def type_args(self):
		if self.bc.type.is_coverage: return [f'--build={self.bc.type.value}', '--coverage']
		else: return [f'--build={self.bc.type.value}']
	
	def prepare(self): pass
	def finalize(self): pass
	def check(self): raise NotImplemented()


class ExecExecutor(Executor):
	@property
	def runner_args(self): return ['dub'] if self.bc.exe_run else ['dub', 'build']
	
	def check(self):
		file = 'csgfitter' if self.bc.config == Config.EXECUTABLE else 'csgfitter-static'
		return util.isfile(file)


class TrialExecutor(Executor):
	@property
	def runner_args(self): return ['trial', 'run']
	
	
	def prepare(self):
		## Delete old coverage artefacts
		if not self.bc.type.is_coverage: return
		for path in ['coverage', '.trial/coverage/html']:
			if op.exists(path):
				if not no_action:
					shutil.rmtree(path)
		
		for file in os.listdir():
			if file.endswith('.lst'):
				if not no_action:
					os.unlink(file)
	
	def finalize(self):
		if no_action: return
		
		## == HTML unittest report
		## Save/Move report
		os.makedirs('public/unittests', exist_ok=True)
		shutil.move('.trial/result.html', f'public/unittests/{self.bc.target.value}.html')
		if not op.isdir('public/unittests/assets'):
			shutil.move('.trial/assets', f'public/unittests/assets')
		
		## == HTML coverage report
		if self.bc.type.is_coverage:
			dst, raw_dst = purge_public_coverage(self.bc.target.value)
			
			if op.isdir('.trial/coverage/html'):
				## Publish report
				flat_copy('.trial/coverage/html', dst)
				
				## Create badge with name
				shutil.copy(f'{dst}/coverage-shield.svg', f'{dst}/named-badge.svg')
				util.sed(f'{dst}/named-badge.svg', 'line coverage', f'{self.bc.target.value} cov')
				
			## Publish raw data
			if op.isdir('coverage/raw'):
				flat_copy('coverage/raw', raw_dst)
				shutil.rmtree('coverage/')
			else:
				for file in os.listdir():
					if file.endswith('.lst'):
						shutil.move(file, raw_dst)
	
	
	def check(self):
		what = self.bc.target.value
		
		## Check that the BINARY exists
		if not util.isfile(f'pkg-{what}/trial-root'): return False
		
		## Check that the UNITTEST REPORT exists
		if not util.isfile(f'public/unittests/{what}.html'): return False
		
		## Check that there is a COVERAGE REPORTS
		return \
			self.bc.type.is_coverage == False or \
			coverage(f'public/coverage/{what}/raw/trial_root.lst') > 0


class DubExecutor(Executor):
	@property
	def runner_args(self):
		args = ['dub', 'test']
		if verbosity <= Verbosity.QUIET:
			args.append('--quiet')
		return args
	
	
	def prepare(self):
		## Delete old coverage artefacts
		if not self.bc.type.is_coverage: return
		for file in os.listdir():
			if file.endswith('.lst'):
				if not no_action:
					os.unlink(file)
	
	
	def finalize(self):
		if no_action: return
		if self.bc.type.is_coverage:
			## Move all text coverage reports into a fresh subproject directory.
			_, raw_dst = purge_public_coverage(self.bc.target.value)
			for file in os.listdir():
				if file.endswith('.lst'):
					shutil.move(file, raw_dst)
	
	
	def check(self):
		what = self.bc.target.value
		
		## Check that the BINARY exists
		if not util.isfile(f'pkg-{what}/csgfitter-{what}-test-unittest'): return False
		
		## Check that there are any COVERAGE REPORTS 
		return \
			self.bc.type.is_coverage == False or \
			any_coverage(f'public/coverage/{what}/raw')
#endregion


################################################################################
#region Build configuration
class Target(enum.Enum):
	EXE =               ('exe',          True)
	TEST_CLI =          ('cli',          False)
	TEST_GENERIC =      ('generic',      False)
	TEST_LIBSCGFITTER = ('libcsgfitter', False)
	
	def __init__(self, value, is_exe):
		self._value_ = value
		self.is_exe = is_exe
	
	@property
	def is_test(self): return not self.is_exe


class Runner(enum.Enum):
	BUILD = ('build', True,  ExecExecutor)
	DUB =   ('dub',   False, DubExecutor)
	TRIAL = ('trial', False, TrialExecutor)
	
	def __init__(self, value, is_exe, executer):
		self._value_ = value
		self.is_exe = is_exe
		self.executer = executer
	
	@property
	def is_test(self): return not self.is_exe


class Compiler(enum.Enum):
	DMD = 'dmd'
	LDC2 = 'ldc2'


class Mode(enum.Enum):
	COMBINED = 'combined'
	SEPARATED = 'separated'


class Config(enum.Enum):
	EXECUTABLE = ('executable', True)
	STATIC =     ('static',     True)
	UNITTEST =   ('unittest',   False)
	TRIAL =      ('trial',      False)
	
	def __init__(self, value, is_exe):
		self._value_ = value
		self.is_exe = is_exe
	
	@property
	def is_test(self): return not self.is_exe


## Used build types and their configuration (Own: defined/overridden in sub.sdl):
##              |     | optimize |           |        |               |           |
##              | Own | + inline | debugInfo | ..Mode | noBoundsCheck | unittests | coverage
## -------------+-----+----------+-----------+--------+---------------+-----------+----------
## unittest-cov |     |          | V         | D      |               | V         | V
## unittest     |     |          | V         | D      |               | V         | 
## unittest-opt |  V  | V        | V         | D      |               | V         | 
## release      |  V! | V        | V         |  R     | V             |           | 
## debug        |     |          | V         | D      |               |           | 
class Type(enum.Enum):
	DEBUG =        ('debug',        True,  False, False)
	RELEASE =      ('release',      True,  False, True)
	UNITTEST =     ('unittest',     False, False, False)
	UNITTEST_OPT = ('unittest-opt', False, False, True)
	UNITTEST_COV = ('unittest-cov', False, True,  False)
	
	def __init__(self, value, is_exe, is_coverage, is_optimized):
		self._value_ = value
		self.is_exe = is_exe
		self.is_coverage = is_coverage
		self.is_optimized = is_optimized
	
	@property
	def is_test(self): return not self.is_exe


@dataclasses.dataclass
class BuildConfiguration:
	cid: int
	target: Target = Target.EXE
	runner: Runner = Runner.BUILD
	compiler: Compiler = Compiler.DMD
	mode: Mode = Mode.COMBINED
	config: Config = Config.EXECUTABLE
	type: Type = Type.DEBUG
	exe_run: bool = False
	extra_args: typing.List[str] = dataclasses.field(default_factory=list)
	
	def __post_init__(self):
		self.executer = self.runner.executer(self)
	
	@property
	def error(self):
		## Everything is testet relative to the target. Hence `target` itself is
		## already tested. `mode` and `compiler` are valid in every combination.
		
		## Runner
		if self.runner.is_exe != self.target.is_exe:
			return 'Exe targets need an exe runner and test targets and a test runner'
		
		if self.runner == Runner.TRIAL and self.compiler == Compiler.LDC2 and self.mode != Mode.COMBINED:
			return 'Trial runner with ldc2 compiler can only work in combined mode'
		
		## Config
		if self.config.is_exe != self.target.is_exe:
			return 'Exe targets need an exe config and test targets and a text config'
		
		if self.config == Config.STATIC and self.compiler != Compiler.LDC2:
			return 'Static builds need ldc2 compiler'
		
		if self.config == Config.UNITTEST and self.runner != Runner.DUB:
			return 'Unittest config need dub runner'
		
		if self.config == Config.TRIAL and self.runner != Runner.TRIAL:
			return 'Trial config need trial runner'
		
		## Type
		if self.type.is_exe != self.target.is_exe:
			return 'Exe targets need an exe type and test targets and a test type'
		
		if self.compiler == Compiler.DMD and self.type.is_optimized:
			## The "unittests" build option makes some unit tests in :generic
			## and :libcsgfitter fail using dmd.
			return 'dmd compiler builds broken optimized executables'
	
	@property
	def args(self):
		return self.executer.args
	
	@property
	def cmd(self):
		return ' '.join(self.args)
	
	@property
	def performance_key(self):
		return (self.target.value, self.runner.value, self.compiler.value, self.mode.value, self.config.value, self.type.value)
	
	@property
	def performance_data(self):
		data = self.performance_db.get(self.performance_key)
		if data is None:
			print(f'Warning: Missing performance data for {self.performance_key}.')
			return {}
		else:
			return data
	
	@property
	def time(self):
		return self.performance_data.get('time_s', sys.maxsize)
	
	@property
	def mem(self):
		return self.performance_data.get('mem_kib', sys.maxsize)
	
	performance_db = {
		('exe', 'build', 'dmd', 'combined', 'executable', 'debug'): {
			'time_s':  15, 'mem_kib': 1570960,  ## 2020-05-26 01:53
		},
		('exe', 'build', 'dmd', 'separated', 'executable', 'debug'): {
			'time_s':  79, 'mem_kib':  807752,  ## 2020-05-26 01:53
		},
		('exe', 'build', 'ldc2', 'combined', 'executable', 'debug'): {
			'time_s':  26, 'mem_kib': 1361960,  ## 2020-05-26 01:53
		},
		('exe', 'build', 'ldc2', 'combined', 'executable', 'release'): {
			'time_s':  61, 'mem_kib': 1045616,  ## 2020-05-26 01:53
		},
		('exe', 'build', 'ldc2', 'combined', 'static', 'debug'): {
			'time_s':  26, 'mem_kib': 1379752,  ## 2020-05-26 01:53
		},
		('exe', 'build', 'ldc2', 'combined', 'static', 'release'): {
			'time_s':  61, 'mem_kib': 1057588,  ## 2020-05-26 01:53
		},
		('exe', 'build', 'ldc2', 'separated', 'executable', 'debug'): {
			'time_s': 103, 'mem_kib': 1013120,  ## 2020-05-26 01:53
		},
		('exe', 'build', 'ldc2', 'separated', 'executable', 'release'): {
			'time_s': 192, 'mem_kib':  613484,  ## 2020-05-26 01:53
		},
		('exe', 'build', 'ldc2', 'separated', 'static', 'debug'): {
			'time_s': 103, 'mem_kib': 1002824,  ## 2020-05-26 01:53
		},
		('exe', 'build', 'ldc2', 'separated', 'static', 'release'): {
			'time_s': 192, 'mem_kib':  616780,  ## 2020-05-26 01:53
		},
		('cli', 'dub', 'dmd', 'combined', 'unittest', 'unittest'): {
			'time_s':  15, 'mem_kib': 1702900,  ## 2020-05-26 01:53
		},
		('cli', 'dub', 'dmd', 'combined', 'unittest', 'unittest-cov'): {
			'time_s':  14, 'mem_kib': 1728540,  ## 2020-05-26 01:53
		},
		('cli', 'dub', 'dmd', 'separated', 'unittest', 'unittest'): {
			'time_s':  97, 'mem_kib': 1161228,  ## 2020-05-26 01:53
		},
		('cli', 'dub', 'dmd', 'separated', 'unittest', 'unittest-cov'): {
			'time_s':  97, 'mem_kib': 1150560,  ## 2020-05-26 01:53
		},
		('cli', 'dub', 'ldc2', 'combined', 'unittest', 'unittest'): {
			'time_s':  28, 'mem_kib': 2038992,  ## 2020-05-26 01:53
		},
		('cli', 'dub', 'ldc2', 'combined', 'unittest', 'unittest-opt'): {
			'time_s':  91, 'mem_kib': 2123056,  ## 2020-05-26 01:53
		},
		('cli', 'dub', 'ldc2', 'combined', 'unittest', 'unittest-cov'): {
			'time_s':  29, 'mem_kib': 2065832,  ## 2020-05-26 01:53
		},
		('cli', 'dub', 'ldc2', 'separated', 'unittest', 'unittest'): {
			'time_s': 120, 'mem_kib': 1317336,  ## 2020-05-26 01:53
		},
		('cli', 'dub', 'ldc2', 'separated', 'unittest', 'unittest-opt'): {
			'time_s': 290, 'mem_kib': 1348232,  ## 2020-05-26 01:53
		},
		('cli', 'dub', 'ldc2', 'separated', 'unittest', 'unittest-cov'): {
			'time_s': 120, 'mem_kib': 1287740,  ## 2020-05-26 01:53
		},
		('cli', 'trial', 'dmd', 'combined', 'trial', 'unittest'): {
			'time_s':  21, 'mem_kib': 2027372,  ## 2020-05-26 01:53
		},
		('cli', 'trial', 'dmd', 'combined', 'trial', 'unittest-cov'): {
			'time_s':  21, 'mem_kib': 2000104,  ## 2020-05-26 01:53
		},
		('cli', 'trial', 'dmd', 'separated', 'trial', 'unittest'): {
			'time_s': 104, 'mem_kib': 1489976,  ## 2020-05-26 01:53
		},
		('cli', 'trial', 'dmd', 'separated', 'trial', 'unittest-cov'): {
			'time_s': 105, 'mem_kib': 1465928,  ## 2020-05-26 01:53
		},
		('cli', 'trial', 'ldc2', 'combined', 'trial', 'unittest'): {
			'time_s':  35, 'mem_kib': 2398400,  ## 2020-05-26 01:53
		},
		('cli', 'trial', 'ldc2', 'combined', 'trial', 'unittest-opt'): {
			'time_s': 100, 'mem_kib': 2461608,  ## 2020-05-26 01:53
		},
		('cli', 'trial', 'ldc2', 'combined', 'trial', 'unittest-cov'): {
			'time_s':  37, 'mem_kib': 2380112,  ## 2020-05-26 01:53
		},
		('generic', 'dub', 'dmd', 'combined', 'unittest', 'unittest'): {
			'time_s':   8, 'mem_kib': 1958296,  ## 2020-05-26 01:53
		},
		('generic', 'dub', 'dmd', 'combined', 'unittest', 'unittest-cov'): {
			'time_s':   8, 'mem_kib': 1951116,  ## 2020-05-26 01:53
		},
		('generic', 'dub', 'dmd', 'separated', 'unittest', 'unittest'): {
			'time_s':  47, 'mem_kib':  805320,  ## 2020-05-26 01:53
		},
		('generic', 'dub', 'dmd', 'separated', 'unittest', 'unittest-cov'): {
			'time_s':  47, 'mem_kib':  766520,  ## 2020-05-26 01:53
		},
		('generic', 'dub', 'ldc2', 'combined', 'unittest', 'unittest'): {
			'time_s':  17, 'mem_kib': 2383956,  ## 2020-05-26 01:53
		},
		('generic', 'dub', 'ldc2', 'combined', 'unittest', 'unittest-opt'): {
			'time_s':  67, 'mem_kib': 2526240,  ## 2020-05-26 01:53
		},
		('generic', 'dub', 'ldc2', 'combined', 'unittest', 'unittest-cov'): {
			'time_s':  17, 'mem_kib': 2385912,  ## 2020-05-26 01:53
		},
		('generic', 'dub', 'ldc2', 'separated', 'unittest', 'unittest'): {
			'time_s':  67, 'mem_kib':  913276,  ## 2020-05-26 01:53
		},
		('generic', 'dub', 'ldc2', 'separated', 'unittest', 'unittest-opt'): {
			'time_s': 192, 'mem_kib':  936940,  ## 2020-05-26 01:53
		},
		('generic', 'dub', 'ldc2', 'separated', 'unittest', 'unittest-cov'): {
			'time_s':  67, 'mem_kib':  894920,  ## 2020-05-26 01:53
		},
		('generic', 'trial', 'dmd', 'combined', 'trial', 'unittest'): {
			'time_s':  14, 'mem_kib': 2176104,  ## 2020-05-26 01:53
		},
		('generic', 'trial', 'dmd', 'combined', 'trial', 'unittest-cov'): {
			'time_s':  14, 'mem_kib': 2200636,  ## 2020-05-26 01:53
		},
		('generic', 'trial', 'dmd', 'separated', 'trial', 'unittest'): {
			'time_s':  53, 'mem_kib': 1142640,  ## 2020-05-26 01:53
		},
		('generic', 'trial', 'dmd', 'separated', 'trial', 'unittest-cov'): {
			'time_s':  54, 'mem_kib': 1052092,  ## 2020-05-26 01:53
		},
		('generic', 'trial', 'ldc2', 'combined', 'trial', 'unittest'): {
			'time_s':  24, 'mem_kib': 2648668,  ## 2020-05-26 01:53
		},
		('generic', 'trial', 'ldc2', 'combined', 'trial', 'unittest-opt'): {
			'time_s':  79, 'mem_kib': 2813096,  ## 2020-05-26 01:53
		},
		('generic', 'trial', 'ldc2', 'combined', 'trial', 'unittest-cov'): {
			'time_s':  26, 'mem_kib': 2649828,  ## 2020-05-26 01:53
		},
		('libcsgfitter', 'dub', 'dmd', 'combined', 'unittest', 'unittest'): {
			'time_s':  77, 'mem_kib': 1999788,  ## 2020-05-26 01:53
		},
		('libcsgfitter', 'dub', 'dmd', 'combined', 'unittest', 'unittest-cov'): {
			'time_s':  84, 'mem_kib': 1998964,  ## 2020-05-26 01:53
		},
		('libcsgfitter', 'dub', 'dmd', 'separated', 'unittest', 'unittest'): {
			'time_s': 155, 'mem_kib': 1136268,  ## 2020-05-26 01:53
		},
		('libcsgfitter', 'dub', 'dmd', 'separated', 'unittest', 'unittest-cov'): {
			'time_s': 173, 'mem_kib': 1133604,  ## 2020-05-26 01:53
		},
		('libcsgfitter', 'dub', 'ldc2', 'combined', 'unittest', 'unittest'): {
			'time_s':  76, 'mem_kib': 2514024,  ## 2020-05-26 01:53
		},
		('libcsgfitter', 'dub', 'ldc2', 'combined', 'unittest', 'unittest-opt'): {
			'time_s': 115, 'mem_kib': 2656956,  ## 2020-05-26 01:53
		},
		('libcsgfitter', 'dub', 'ldc2', 'combined', 'unittest', 'unittest-cov'): {
			'time_s': 168, 'mem_kib': 2517236,  ## 2020-05-26 01:53
		},
		('libcsgfitter', 'dub', 'ldc2', 'separated', 'unittest', 'unittest'): {
			'time_s': 186, 'mem_kib': 1316224,  ## 2020-05-26 01:53
		},
		('libcsgfitter', 'dub', 'ldc2', 'separated', 'unittest', 'unittest-opt'): {
			'time_s': 343, 'mem_kib': 1311212,  ## 2020-05-26 01:53
		},
		('libcsgfitter', 'dub', 'ldc2', 'separated', 'unittest', 'unittest-cov'): {
			'time_s': 209, 'mem_kib': 1304532,  ## 2020-05-26 01:53
		},
		('libcsgfitter', 'trial', 'dmd', 'combined', 'trial', 'unittest'): {
			'time_s':  79, 'mem_kib': 2414436,  ## 2020-05-26 01:53
		},
		('libcsgfitter', 'trial', 'dmd', 'combined', 'trial', 'unittest-cov'): {
			'time_s':  85, 'mem_kib': 2401576,  ## 2020-05-26 01:53
		},
		('libcsgfitter', 'trial', 'dmd', 'separated', 'trial', 'unittest'): {
			'time_s': 166, 'mem_kib': 1641300,  ## 2020-05-26 01:53
		},
		('libcsgfitter', 'trial', 'dmd', 'separated', 'trial', 'unittest-cov'): {
			'time_s': 176, 'mem_kib': 1625016,  ## 2020-05-26 01:53
		},
		('libcsgfitter', 'trial', 'ldc2', 'combined', 'trial', 'unittest'): {
			'time_s':  76, 'mem_kib': 2971404,  ## 2020-05-26 01:53
		},
		('libcsgfitter', 'trial', 'ldc2', 'combined', 'trial', 'unittest-opt'): {
			'time_s': 130, 'mem_kib': 3119908,  ## 2020-05-26 01:53
		},
		('libcsgfitter', 'trial', 'ldc2', 'combined', 'trial', 'unittest-cov'): {
			'time_s': 142, 'mem_kib': 2974624,  ## 2020-05-26 01:53
		},
	}
#endregion


################################################################################
def main():
	global no_action, keep_going, verbosity
	os.chdir(op.normpath(op.join(util.script_dir(), '..')))
	
	possible_targets = ['bin', 'static', 'test', 'cli', 'generic', 'libcsgfitter']
	how_attributes = util.dobjectify([
		{'short': 'R', 'long': 'runner'},
		{'short': 'D', 'long': 'compiler'},
		{'short': 'M', 'long': 'mode'},
		{'short': 'C', 'long': 'config'},
		{'short': 'T', 'long': 'type'},
	])
	
	## == Parse command line arguments
	parser = argparse.ArgumentParser()
	
	## Ungrouped arguments
	parser.add_argument('-d', '--clean', action='count', default=0)
	parser.add_argument('-r', '--run', nargs='?', const='y', choices=['n', 'y'], metavar='_')
	parser.add_argument('-k', '--keep-going', action='store_true', help='Keep on building, event if a build or post check failed.')
	parser.add_argument('-n', '--no-action', action='store_true', help='Don\'t actually write or delete any files.')
	parser.add_argument('-q', '--quiet', action='store_true')
	parser.add_argument('-v', '--verbose', action='store_true')
	parser.add_argument('-f', '--force', action='store_true', help='Build even is there is not enough free system RAM.')
	
	## Actions
	group = parser.add_argument_group(
		title='Actions',
		description='Exactly one action has to be used.'
	)
	group.add_argument(
		'-t', '--target', nargs='*', choices=possible_targets, metavar='_',
		help='Build/Run/Test targets to build. One of %(choices)s ' \
		'("test" is a shortcut for "cli", "generic" and "libcsgfitter". Default: "bin")'
	)
	group.add_argument(
		'-l', '--list', action='store_true',
		help='List all valid build configurations and their IDs ("How"-filters still apply).'
	)
	group.add_argument(
		'-s', '--selftest', const='*', nargs='?', metavar='RANGE',
		help='Run valid build configurations in RANGE of IDs (all, "How"-filtered, by default). ' \
		'Warning: implies -dddd'
	)
	
	## "What"-filters
	group = parser.add_argument_group(
		title='"What"-filters',
	)
	group.add_argument(
		'-c', '--coverage', nargs='?', metavar='_', const='t+h', choices=['n', 't', 't+h', 't,h'],
		help='Restrict whether unit tests should create [t]ext and[+] or[,] [h]tml reports. ' \
		'One of %(choices)s (without _: %(const)s / default: unrestricted)'
	)
	group.add_argument(
		'-p', '--report', nargs='?', metavar='_', const='y', choices=['n', 'y'],
		help='Restrict whether unit test reports should be created. ' \
		'One of %(choices)s (without _: %(const)s / default: unrestricted)'
	)
	group.add_argument(
		'-o', '--optimize', nargs='?', metavar='_', const='y', choices=['n', 'y'],
		help='Restrict whether the target should be optimized. ' \
		'One of %(choices)s (without _: %(const)s / default: unrestricted)'
	)
	
	## "How"-filters
	group = parser.add_argument_group(
		title='"How"-filters',
		description='Can be used by all actions to limit the possible build ' \
		'configurations. If a filter is specified, only build configurations ' \
		'having one of the spezified options is used. Otherwise, all options are possible.'
	)
	fields = dict((field.name, field) for field in dataclasses.fields(BuildConfiguration))
	for a in how_attributes:
		enum = fields[a.long].type
		values = list(map(lambda x: x.value, enum.__members__.values()))
		group.add_argument(
			f'-{a.short}', f'--{a.long}', action='append', choices=values, default=[], metavar='_',
			help=f'Allow only build configurations where {a.long} is one of %(choices)s (default: allow all)'
		)
	
	## Actually parse
	arg_parts = util.split(sys.argv[1:], '--', 1)
	args = parser.parse_args(arg_parts[0])
	remaining_args = arg_parts[1] if len(arg_parts) >= 2 else []
	
	## == Handle user input
	no_action = args.no_action
	keep_going = args.keep_going
	if args.verbose and args.quiet:
		sys.exit('Failed: --quiet or --verbose? Make up your mind!')
	elif args.verbose:
		verbosity = Verbosity.VERBOSE
	elif args.quiet:
		verbosity = Verbosity.QUIET
	else:
		verbosity = Verbosity.NORMAL
	
	## Check action
	user_actions = set()
	if args.list: user_actions.add('list')
	if args.selftest is not None: user_actions.add('selftest')
	if args.target is not None: user_actions.add('target')
	
	if len(user_actions) < 1:
		sys.exit('Failed: No action defined')
	if len(user_actions) > 1:
		sys.exit(f'Failed: Multiple actions defined (--{", --".join(user_actions)})')
	action = user_actions.pop()
	
	## Read "How"-filters
	whitelists = {a.long: getattr(args, a.long) for a in how_attributes}
	
	## == Action LIST
	if action == 'list':
		list_valid_confis(whitelists)
	
	## == Action SELFTEST
	elif action == 'selftest':
		selftest(args.selftest)
	
	## == Action TARGET
	elif action == 'target':
		## Build final list of targets
		targets = set(args.target if args.target else [possible_targets[0]])
		if 'test' in targets:
			targets.remove('test')
			targets.update(['cli', 'generic', 'libcsgfitter'])
		
		## Find build configurations for _all_ targets
		bcs = []
		for target in possible_targets:
			if target in targets:
				write_normal(f'==== build configuration for "{target}"')
				
				bc = find_bc(
					target, whitelists,
					optimized=args.optimize,
					test_report=args.report,
					coverage_report=args.coverage,
					force=args.force,
				)
				if bc is None: continue
				bc.extra_args = remaining_args
				bc.exe_run = args.run == 'y'
				bcs.append((target, bc))
				
				if verbosity >= Verbosity.NORMAL:
					print_bc(bc)
					print()
		
		if len(bcs) != len(targets):
			sys.exit('Failed: Couldn\'t find build configurations for all targets.')
		
		## Cleanup once before building all targets
		if args.clean > 0:
			clean(args.clean)
		
		## Build all targets
		for target, bc in bcs:
			write_normal(f'==== build configuration for "{target}"')
			
			bc.executer.prepare()
			
			mt = sh(bc.args)
			if mt.exit_code != 0:
				exit_maybe('Build.')
			
			bc.executer.finalize()
			if not no_action and not bc.executer.check():
				exit_maybe('Build success conditions not met.')
			
			write_normal('')
	
	else:
		assert()


def find_bc(target, force_how={}, optimized=None, test_report=None, coverage_report=None, force=False):
	## Which build type can be used for optimized and non optimized unittest runs
	## with or without coverage report:
	##         | No Cov       | Cov                
	## --------+--------------+--------------------
	## Non Opt | unittest     | unittest-cov 
	##     Opt | unittest-opt | --                 
	
	## Coverage text+html reports only work with trial (not dub)
	## dmd kann nicht optimieren
	
	filtered_bcs = []
	for bc in valid_configs(force_how):
		## Filter target
		if target == 'static':
			if bc.target != Target.EXE or bc.config != Config.STATIC: continue
		
		elif target == 'bin':
			if bc.target != Target.EXE or bc.config != Config.EXECUTABLE: continue
		
		elif target in ['cli', 'generic', 'libcsgfitter']:
			if bc.target.value != target: continue
		
		else:
			assert()
		
		## Filter test report
		if test_report == 'y':
			if bc.runner != Runner.TRIAL: continue
		elif test_report == 'n':
			if bc.runner == Runner.TRIAL: continue
		
		## Filter coverage report
		if coverage_report == 'n':
			if bc.type.is_coverage: continue
		
		elif coverage_report == 't' or coverage_report == 't,h':
			if not bc.type.is_coverage: continue
		
		elif coverage_report == 't+h' or coverage_report == 't,h':
			if not bc.type.is_coverage or bc.runner != Runner.TRIAL: continue
		
		elif coverage_report is not None:
			assert()
		
		## Filter optimized
		if optimized == 'y':
			if not bc.type.is_optimized: continue
		
		elif optimized == 'n':
			if bc.type.is_optimized: continue
		
		elif optimized is not None:
			assert()
		
		## We got a match, save it.
		filtered_bcs.append(bc)
		write_verbose(f'Candidate: {bc.cmd} (expected time: {bc.time} / mem: {bc.mem})')
	
	write_normal(f'Filters match {len(filtered_bcs)} build configurations')
	
	if not filtered_bcs:
		return None
	
	filtered_bcs.sort(key=lambda x: (x.time, x.cid))
	free_mem = memtime.free()
	for bc in filtered_bcs:
		if bc.mem < free_mem:
			return bc
	
	if force:
		return filtered_bcs[0]
	
	print(
		f'But you don\'t have enogh free RAM to run them without swap ' \
		f'(free: {memtime.format_mem(free_mem)} / ' \
		f'required: {memtime.format_mem(min(map(lambda x: x.mem, filtered_bcs)))})'
	)
	return None


def any_coverage(path):
	for name in os.listdir(path):
		if coverage(op.join(path, name)) > 0:
			return True
	
	return False


def coverage(path):
	with open(path) as f:
		for last_line in f: pass
		if not last_line.endswith('% covered\n'): return 0
		return int(last_line.split(' ')[-2][:-1])


def flat_copy(src, dst):
	for file in os.listdir(src):
		shutil.move(op.join(src, file), dst)


def purge_public_coverage(what):
	dst = f'public/coverage/{what}'
	raw_dst = f'{dst}/raw'
	os.makedirs(raw_dst, exist_ok=True)
	
	for file in os.listdir(dst):
		if file == 'raw': continue
		if not no_action:
			os.unlink(op.join(dst, file))
	
	for file in os.listdir(raw_dst):
		if not no_action:
			os.unlink(op.join(raw_dst, file))
	
	return dst, raw_dst


def valid_configs(whitelists={}):
	## Collect all values of all usefull properties
	prop_vals = []
	for field in dataclasses.fields(BuildConfiguration):
		## Filter keys (field names)
		if field.name in ['exe_run', 'extra_args', 'cid']:
			continue
		
		values = field.type.__members__.values()

		## Filter values
		whitelist = whitelists.get(field.name)
		if whitelist:
			values = filter(lambda x: x.value in whitelist, values)
		
		prop_vals.append([(field.name, v) for v in values])
	
	## "Return list" of all valid build configurations
	for cid, args in enumerate(itertools.product(*prop_vals)):
		bc = BuildConfiguration(**dict(args), cid=cid)
		if not bc.error: yield bc


def sh(args):
	print('\033[1m> '+(' '.join(args))+'\033[0m')
	if not no_action:
		mt = memtime.run(args)
		print(mt)
	else:
		mt = memtime.Memtime(0, 0, 0)
		print('Skipped because of -n/--no-action')
	return mt


def clean(thoroughness):
	patterns = []
	
	## Leftovers [-d]
	## Can always be deleted, never produces rebuilds because of deletion
	if thoroughness >= 1:
		patterns += [
			'trial_*.d',
			'*.lst',
			'coverage/',
		]
	
	## Final targets, 1st party [-dd]
	## without any dependants
	if thoroughness >= 2:
		patterns += [
			'csgfitter',
			'csgfitter-static',
			'pkg-*/csgfitter-*-test-unittest',
			'pkg-*/trial-*',
			
			'public/coverage/',
			'public/unittests/generic.html',
			'public/unittests/libcsgfitter.html',
			'public/unittests/cli.html',
			'.trial/',
		]
	
	## Intermediates, 1st party [-ddd]
	## Deletion causes rebuild of 1st party targets
	if thoroughness >= 3:
		patterns += [
			'pkg-*/libcsgfitter_*.a',
			'pkg-cli/src/version.txt',
		]
	
	## Intermediates, 3rd party [-dddd]
	## Deletion causes rebuild of 1st and 3rd party targets
	if thoroughness >= 4:
		patterns += [
			'.dub/',
			'pkg-*/.dub',
		]
	
	for pattern in patterns:
		for path in glob.glob(pattern):
			if util.isfile(path):
				print(f'Remove [f] {path}')
				if not no_action:
					os.remove(path)
			elif util.isdir(path):
				print(f'Remove [d] {path}')
				if not no_action:
					shutil.rmtree(path)
			elif op.exists(path):
				raise Exception(f'Unknown file system entry type at {path}')


def print_bc(bc):
	props = [
		('Target', bc.target),
		('Runner', bc.runner),
		('Compiler', bc.compiler),
		('Compile mode', bc.mode),
		('Build configuration', bc.config),
		('Build type', bc.type),
	]
	
	for label, value in props:
		print(f'{label+":":24}', end='')
		for option in type(value).__members__.values():
			if value == option:
				print(f' \033[1m{option.value}\033[0m*', end='')
			else:
				print(f' {option.value} ', end='')
		print('')
	print(f'{"Run exe:":23}  {bc.exe_run}')
	print(f'{"Extra args:":23}  {bc.extra_args}')


def list_valid_confis(whitelists):
	for i, bc in enumerate(valid_configs(whitelists)):
		print(f'{i:3}: {bc.cmd}')


def selftest(cids=None, whitelists={}):
	configs = list(valid_configs(whitelists))
	
	if cids is None:
		cids = list(range(len(configs)))
	elif type(cids) == str:
		cids = list(util.str_range(cids, len(configs)-1))
	
	mts = []
	for i, cid in enumerate(cids):
		bc = configs[cid]
		print('\n\n=======================================================')
		print(f'=== Running ID: {cid:3} (selftest {i+1}/{len(cids)})\n')
		print_bc(bc)
		
		clean(4)
		if not no_action and bc.executer.check(): exit_maybe('Build success conditions already met before build.')
		bc.executer.prepare()
		mt = sh(bc.args)
		mts.append((bc, mt))
		if mt.exit_code != 0: exit_maybe(f'Build failed with exit code {mt.exit_code}')
		bc.executer.finalize()
		if not no_action and not bc.executer.check(): exit_maybe('Build success conditions not met.')
	
	print('\tperformance_db = {')
	date = datetime.datetime.now().isoformat(' ')[:16]
	for bc, mt in mts:
		print(
			f"\t\t{bc.performance_key}: {{\n" \
			f"\t\t\t'time_s':{int(mt.time_delta): 4}, " \
			f"'mem_kib':{mt.mem_free_delta:8},  ## {date}\n\t\t}},"\
		)
	print('\t}')


def exit_maybe(msg):
	if not keep_going:
		sys.exit(f'Failed: {msg}')
	else:
		print(f'Failed: {msg} But continuing because of -k/--keep-going')


def write_quiet(msg):
	if verbosity <= Verbosity.QUIET: print(msg)


def write_normal(msg):
	if verbosity <= Verbosity.NORMAL: print(msg)


def write_verbose(msg):
	if verbosity <= Verbosity.VERBOSE: print(msg)


class Verbosity(enum.IntEnum):
	QUIET = enum.auto()
	NORMAL = enum.auto()
	VERBOSE = enum.auto()


if __name__== '__main__':
	main()
