#!/usr/bin/env python3
## This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0

import util
import os
import os.path as op
import jinja2
import shutil
import argparse
import util
import subprocess



script_dir   = util.script_dir()
project_dir  = op.join(script_dir, '..')
public_dir   = op.join(project_dir, 'public')
skeleton_dir = op.join(project_dir, 'public_skel')
databook_dir = op.join(project_dir, 'databook')
template_dir = op.join(script_dir, 'tpl')

_env = jinja2.Environment(
	loader=jinja2.FileSystemLoader(template_dir),
	autoescape=True,
	undefined=jinja2.StrictUndefined,
	trim_blocks=True,
	lstrip_blocks=True,
	line_statement_prefix='#',
	line_comment_prefix='##',
	keep_trailing_newline=True
)


def render(tpl_name, dst, data):
	dst_base_dir = public_dir if tpl_name.endswith('.html') else databook_dir
	dst_file = op.join(dst_base_dir, dst)
	
	util.mkdirs(os.path.dirname(dst_file))
	
	_env.get_template(tpl_name) \
	.stream(**data, return_path=util.return_path(dst)) \
	.dump(dst_file)


def render_tex(tpl_name, dst, data):
	_env.get_template(tpl_name) \
	.stream(**data, return_path=util.return_path(dst)) \
	.dump(op.join(databook_dir, dst))


def main():
	cmds = {
		'clean': cmd_clean,
		'prepare': cmd_prepare,
		'build': cmd_build,
		'index': cmd_index,
		'finalize': cmd_finalize,
		'all': cmd_all,
		'new': cmd_new,
	}
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('targets', nargs='*', choices=list(cmds.keys()), help='')
	args = parser.parse_args()
	
	for target in args.targets:
		cmds[target]()


def cmd_index():
	src = op.join(project_dir, 'csgfitter-static')
	dst = op.join(public_dir, 'csgfitter-static')
	if util.outdated(dst, src):
		shutil.copy2(src, dst)
	version = subprocess.check_output([dst, '--version']).decode('utf-8').strip()
	
	render('index.html', 'index.html', {
		'title': 'Home',
		'selected_subthing_key': '',
		'subthings': [],
		'version': version,
	})
	
	subthings = []
	for lib in ['cli', 'libcsgfitter', 'generic']:
		subthings.append({
			'title': lib,
			'url': f'unittests/{ lib }',
			'key': lib,
		})
	
	render('generic_index.html', f'unittests/index.html', {
		'title': 'Unit tests',
		'selected_subthing_key': '',
		'subthings': subthings,
	})
	
	subthings = []
	for lib in ['cli', 'libcsgfitter', 'generic']:
		subthings.append({
			'title': lib,
			'url': f'coverage/{ lib }/index',
			'key': lib,
		})
	
	util.mkdirs(op.join(public_dir, 'coverage'))
	render('generic_index.html', f'coverage/index.html', {
		'title': 'Code coverage',
		'selected_subthing_key': '',
		'subthings': subthings,
	})


def cmd_clean():
	for path in [public_dir, databook_dir]:
		if op.exists(path):
			shutil.rmtree(path)


def cmd_prepare():
	util.copytree(skeleton_dir, public_dir)
	util.mkdirs(databook_dir)


def cmd_build():
	util.sh('build', '-t', 'test', '-c', 't+h', '-k')
	util.sh('build', '-t', 'static')
	util.sh('test_eva_ab')
	util.sh('test_evo_props')
	util.sh('test_evo_selector')
	util.sh('test_evo_tourn')
	util.sh('test_final')
	util.sh('test_renderings')


def cmd_finalize():
	for root, dirs, files in os.walk(public_dir):
		for i, item in enumerate(dirs + files):
			path = os.path.join(root, item)
			if op.islink(path):
				if op.isdir(path):
					del dirs[i]
				
				src = op.join(op.dirname(path), os.readlink(path))
				util.copytree(src, path)


def cmd_all():
	cmd_prepare()
	cmd_build()
	cmd_index()
	cmd_finalize()


def cmd_new():
	cmd_clean()
	cmd_all()


if __name__== '__main__':
	main()
