#!/usr/bin/env python3
## This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0

import json
from collections import OrderedDict
import re
import argparse



def fix_files(pathes, to_stdout=False):
	for path in pathes:
		csg_tree = json.load(open(path), object_pairs_hook=OrderedDict)
		if to_stdout:
			print(f'==> {path} <==')
			print(format_csg(path))
		
		else:
			s = format_csg(path)
			with open(path, 'w') as f:
				f.write(s)


def format_csg(csg_tree):
	s = json.dumps(csg_tree, indent=2)
	
	## Make coordinates one liners
	s = re.sub(r'\[\n\s*(.*)\n\s*(.*)\n\s*(.*)\n.*\]', r'[\1 \2 \3]', s)
	
	## Put first attribute on the "{"-line
	space = ''
	for i in range(20):
		s = re.sub(f'^{space}{{\n{space}  ', f'{space}{{ ', s, flags=re.M)
		space += '  '
	
	## Make "params" one liners
	def one_line(match):
		content = match.group(2).strip().split('\n')
		content = list(map(lambda x: x.strip(), content))
		content = ' '.join(content)
		return match.group(1)+' '+content+' '+match.group(3)
	
	s = re.sub(r'("params": \{)(.*?)(\})', one_line, s, flags=re.DOTALL)
	
	return s+'\n'


def main():
	parser = argparse.ArgumentParser(description='Format JSON files containing CSG trees into a compact and human readable form.')
	parser.add_argument('pathes', metavar='FILE', nargs='+', help='json files to fix')
	parser.add_argument('-o', '--output', dest='to_stdout', action='store_true', help='print fixed files instead of replacing them')
	args = parser.parse_args()
	fix_files(args.pathes, args.to_stdout)


if __name__== '__main__':
	main()
