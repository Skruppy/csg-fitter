## This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0



class FinalCreator:
	def pluginPath():
		return 'final'
	
	def build(self):
		raise NotImplemented()
