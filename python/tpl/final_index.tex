%% This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
%% Copyright (c) Skruppy <skruppy@onmars.eu>
%% SPDX-License-Identifier: Apache-2.0


\chapter{Finale Analyse}
\label{sec:db_final}
# set model_cnt = models|length
# set repetitions = 30
# set eas = (
	model_cnt +
	(models.values()|map(attribute='evos.clique_evo.fitter.job_cnt_mean')|sum) +
	(models.values()|map(attribute='evos.part_evo.fitter.job_cnt_mean')|sum)
) | int

Für diese Analyse wurden {{ model_cnt }} Modelle auf 3 verschiedene Arten rekonstruiert (ohne Zerlegung, mit cliquen- und mit pruning-basierter Zerlegung).
Da die Rekonstruktion Zufallsprozesse inkorporiert wurde jede Rekonstruktion {{ repetitions }} Mal wiederholt um die Erwartungswerte in der Analyse genauer schätzen zu können.
Somit wurden insgesamt \textbf{ {{- '{:,}'.format(model_cnt*3*repetitions) }} Jobs} auf dem Rechen-Cluster ausgeführt.
Eine dabei verwendete Verarbeitungs-Pipeline ist exemplarisch in \niceRef{fig:db_final_pipeline} zu sehen.

Über alle Modelle und Rekonstruktionsarten entstanden somit {{ eas }} Cluster, welche jeweils mit einem \gls{EA} rekonstruiert wurden.
Würde bei allen Rekonstruktionen die maximale Anzahl an Generationen erstellt, um das beste CSG-Modell zu finden, wäre die Anzahl erzeugter Individuen:

\begin{center}
	\rowfont{\numbersbig}
	\begin{tabular}{C@{}C@{}C@{}C@{}C@{}C@{}C@{}C@{}C}
		{{ eas }}
		& × & 150
		& × & 500
		& × & {{ repetitions }}
		& = & {{ '{:,d}'.format(eas*repetitions*150*500) }}
		\rowfont{\numberssmall}
		\\
		\acrshort{EA}s
		& & \makecell[tc]{Generationen \\ pro \acrshort{EA}}
		& & \makecell[tc]{Individuen \\ pro Generation}
		& & \makecell[tc]{Wieder-\\holungen}
		& & Erzeugte Individuen (CSG-Modelle)
		\\
	\end{tabular}
\end{center}

\begin{landscape}
	\begin{figure}[H]
		\centering
		\includegraphics[width=1\linewidth]{databook/final/pipeline}
		\caption{Beispielhafte Pipeline des \glspl{csgfitter} für die finale Analyse.}
		\label{fig:db_final_pipeline}
	\end{figure}
\end{landscape}
\clearpage

\section{Testmodelle}
\label{sec:db_final_models}
In den folgenden Unterabschnitten werden die Modelle vorgestellt, mit denen die finale Analyse durchgeführt wurde.
Neben der 3D-Darstellung und dem CSG-Baum des Modells gibt es für jede Zerlegungsmethode auch einen Graphen, welcher die Zerlegung visualisert.

Bei der Zerlegung in maximale Cliquen beinhaltet der Graph die einzelnen Geometrien in rechteckigen Knoten.
Diese sind mit dicken schwarzen Kanten verbunden, wenn sie sich überschneiden.
Beide Elemente bilden zusammen den Intersektionsgraphen.
Cliquen sind als ovale Knoten mit einer eindeutigen Farbe dargestellt.
Gehört eine einfache Geometrie einer Clique an, ist dies mit einer gleichfarbigen Kante markiert.
Eine Geometrie kann mehreren Cliquen angehören.

Der Graph für die pruning-basierte Zerlegung ist ebenfalls ein Intersektionsgraph.
Knoten und Kanten, welche entfernt (gepruned) und später wieder eingefügt wurden, sind mit einer normalen Strichstärke eingezeichnet.
Knoten und Kanten, die nicht entfernt werden konnten, sind mit einer dicken Strichstärke eingezeichnet.
Primimplikanten sind als rot umrandete Knoten dargestellt.
Konnte eine Kante des Primimplikantens nicht gepruned werden, sondern wurde bei der Primimplikanten-Erkennung entfernt, ist diese gestrichelt eingezeichnet.
Nach dem prunen und dem freistellen der Primimplikanten werden Zusammenhangskomponenten auf dem verbleibenden Graphen identifiziert.
Geometrien die der selben Zusammenhangskomponente angehören sind in der selben Farbe markiert.

%In the following sections, the clique and the partition based algorithms for fitting an CSG tree to a point cloud are compared against each other.
%For reference, also the base evolutionary algorithm (normal) was evaluated and, if possible the original model.
%The algorithms havbe been appied 30 times to following {{- models | length -}} models:

# for model_name, model in models.items()
	\subsection{Modell \emph{ {{- escapeLatex(model_name) | safe -}} }}
	\label{sec:db_final_models_{{model_name}}}
	
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.45\linewidth]{databook/final/{{ model_name -}} }
		\caption{3D-Darstellung des Modells \emph{ {{- escapeLatex(model_name) | safe -}} }.}
		\label{fig:db_final_models_{{ model_name }}_rendering}
	\end{figure}
	\begin{figure}[H]
		\centering
		%% max height = 0.8/scale and max width = 1.0/scale
		\includegraphics[max height=1.14\pageheight,max width=1.48\linewidth,scale=0.7]{databook/final/{{ model_name -}}-tree }
		\caption{CSG-Baum des Modells \emph{ {{- escapeLatex(model_name) | safe -}} }.}
		\label{fig:db_final_models_{{ model_name }}_tree}
	\end{figure}
	\begin{figure}[H]
		\centering
		\includegraphics[max height=1.14\pageheight,max width=1.48\linewidth,scale=0.7]{databook/final/{{ model_name -}}-graph-clique }
		\caption{Zerlegung in maximale Cliquen des Modells \emph{ {{- escapeLatex(model_name) | safe -}} }.}
		\label{fig:db_final_models_{{ model_name }}_graph_clique}
	\end{figure}
	\begin{figure}[H]
		\centering
		\includegraphics[max height=1.14\pageheight,max width=1.48\linewidth,scale=0.7]{databook/final/{{ model_name -}}-graph-part }
		\caption{Pruning-basierte Zerlegung des Modells \emph{ {{- escapeLatex(model_name) | safe -}} }.}
		\label{fig:db_final_models_{{ model_name }}_graph_part}
	\end{figure}
	
	\clearpage
# endfor

\section{Ausführungszeit}
\label{sec:db_final_timing}
In diesem Abschnitt werden verschiedene Aspekte der benötigten Rekonstruktionszeit untersucht.
Um die Vergleichbarkeit aller Rekonstruktionen und deren Wiederholungen zu gewährleisten, wurden alle Berechnungen auf gleichen CPUs ausgeführt ({{ (cpus.values()|list)[0].name }}).
Im weiteren werden folgende Formelzeichen benutzt:
\begin{itemize}
	\item[$\m{T}_S$] Menge der Ausführungszeiten einzelner Abschnitte die nicht parallel laufen können.
	\item[$\m{T}_P$] Menge der Ausführungszeiten einzelner Abschnitte die parallel laufen können (Zeiten der einzelnen \glspl{EA}).
	\item[$t_p$] Laufzeit aller parallelisierbaren Abschnitte ($\sum \m{T}_P$).
	\item[$t_s$] Laufzeit aller nicht parallelisierbaren Abschnitte ($\sum \m{T}_S$).
	\item[$T$] Gesamtlaufzeit in komplett serieller Ausführung ($t_s + tp$).
	\item[$T_{Pr}$] Reale Gesamtlaufzeit in paralleler Ausführung ($\sum \m{T}_S + \max(\m{T}_P)$).
	\item[$T_{Pa}$] Theoretische minimale Gesamtlaufzeit in paralleler Ausführung nach dem Amdahl’schen Gesetz ($t_S$).
	\item[$\eta_{Sr}$] Speedup bei realer paralleler Ausführung gegenüber serieller Ausführung ($\nicefrac{T}{T_{Pr}}$).
	\item[$\eta_{Sa}$] Theoretischer maximaler Speedup bei paralleler Ausführung gegenüber serieller Ausführung ($\nicefrac{T}{T_{Pa}}$).
	\item[$n_P$] Anzahl gleichzeitig ausführbarer Abschnitte ($| \m{T}_P |$).
\end{itemize}

\subsection{Übersicht über die realen Ausführungszeiten aller Zerlegungsmodelle}
\label{sec:db_final_timing_overview}
Die folgende Tabelle enthält die Ausführungszeiten, welche für die vollständige Rekonstruktion der Testmodelle benötigt wurde.
Die angegebenen Zeiten entsprechen dem Mittelwert der {{ repetitions }} Wiederholungen.
Auf die Angabe der Strandardabweichung ist aus Gründen der Übersicht verzichtet worden.

Für jedes Testmodell wird die benötigte Ausführungszeit für das komplette Modell ohne Zerlegung, sowie für die Ausführungszeit mit cliquen- und pruning-basierter Zerlegung angegben.
Erstere dient als Referenzzeit, an der sich die Zerlegungsmethoden messen lassen müssen.
Dazu ist unter der Ausführungszeit in schwarzer Schrift das Verhältnis zur Referenz angegeben.
Die Farbe gibt an, ob die Zerlegung zu einer Zeitersparnis (grün) oder einem Mehraufwand (rot) geführt hat.

Mit den Zerlegemethoden ist potenziell eine parallele Ausführung möglich.
Daher wird hier die reale Zeit angegeben, welche die Rekonstruktion im seriellen und parallelen Laufzeitmodell benötigt hat.
Sollte eine Zerlegung bei dem Testmodell nicht möglich gewesen sein ($n_P = 1$), ist das Feld grau hinterlegt.

\begin{table}[H]
	\centering
	\input{databook/final/timing_overview}
	\caption{Übersicht über die realen Rekonstruktionszeiten der Testmodelle mit verschiedenen Laufzeitmodellen und Zerlegungsmethoden.}
	\label{tab:db_final_timing_overview}
\end{table}

\clearpage


\subsection{Auswirkung der Zerlegungsmethoden auf die parallele Ausführung}
\label{sec:db_final_timing_specific}
Die folgenden beiden Tabellen geben einen genaueren Einblick in die Auswirkungen der Zelegungsmethoden auf die parallele Ausführung.

\begin{table}[H]
	\centering
	\input{databook/final/timing_clique}
	\caption{Ausführungszeiten verschiedener Laufzeitmodelle mit einer cliquen-basierten Zerlegung.}
	\label{tab:db_final_timing_clique}
\end{table}

\begin{table}[H]
	\centering
	\input{databook/final/timing_part}
	\caption{Ausführungszeiten verschiedener Laufzeitmodelle mit einer pruning-basierten Zerlegung.}
	\label{tab:db_final_timing_part}
\end{table}

\clearpage

\subsection{Auswirkung der Zerlegungsart auf die Ausführungszeit einzelner EAs}
\label{sec:db_final_timing_scatter}
Ob die Gruppierungen einzelner Geometrien durch die Zerlegungsmethoden charakteristische Eigenschaften haben, welche die Rekonstruktionszeit beeinflussen, wird in diesem Abschnitt untersucht.
Dazu werden in dem folgenden verwackelten Streudiagramm die Rekonstruktionszeiten für jeden einzelnen \gls{EA} eingezeichnet, der im Rahmen der finalen Analyse ausgeführt wurde.
Die Quelle der zu rekombinierenden Geometrien, die Zerlegemethode, ist dabei farblich markiert.
Um eine Vergleichbarkeit herzustellen sind die Ausführungszeiten in Abhängigkeit der Anzahl der zu rekombinierenden Geometrien aufgetragen.
Auf der x-Achse wurde auch hier wieder ein künstliches Rauschen eingefügt, um die Punkte zu entzerren.

\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{databook/final/scatter}
	\caption{Ausführungszeiten einzelner \gls{EA}s nach erzeugender Zerlegungsmethode und Anzahl der Geometrien.}
	\label{fig:db_final_timing_scatter}
\end{figure}
\clearpage

\section{Qualität der Rekonstruktionen}
\label{sec:db_final_scores}
Neben der Ausführungsgeschwindigkeit ist auch die Qualität des Endergebnisses wichtig.
Daher beschäftigt sich dieser Abschnitt mit den Scores, welche die Endergebnisse der Rekonstruktionen erzielen konnten.
Um die Ausführungsgeschwindigkeit zu erhöhen wurde die Referenz-Punktwolke zur Rekonstruktion reduziert.
Die reduzierte Punktwolke enthält nur noch drei Punkte pro Geometrie auf dessen Oberfläche der Punkt liegt und Menge an Geometrien die den Punkt beinhalten.

Die folgende Tabelle listet die erzielten Scores für jedes Testmodell und jede Zerlegungsmethode.
Es werden dabei die Scores des finalen Modells gegen die vollständige $s_\text{fc}$ und die reduzierte Punktwolke $s_\text{fr}$ angegeben.
Findet eine Zerlegung statt, werden die Einzel-Scores einer jeden \gls{EA}-Anwendung $\m{S}$ im Mittel und im schlechtesten Fall angegeben.
Farblich ist markiert, ob ein Score einer Zerlegemethode besser (grün) oder schlechter (rot) als sein Pendant ist, der ohne Zerlegung erzielt wurde.
Ist der mittlere oder schlechteste Score der einzelnen \gls{EA}-Ausführung größer Null, wird dies blau hervorgehoben.

\begin{table}[H]
	\centering
	\input{databook/final/scores}
	\caption{Bewertungen final rekonstruierter CSG-Modelle gegen eine vollständige ($s_\text{fc}$) \bzw reduzierte ($s_\text{fr}$) Punktwolke sowie der Cluster-Teilmodelle $\m{S}$.}
	\label{tab:db_final_scores}
\end{table}

\clearpage


\section{Eigenschaften der rekonstruierten CSG-Bäume}
\label{sec:db_final_tree}
Die folgenden Tabellen beschäftigen sich mit den Eigenschaften der erzeugten CSG-Bäume.
Als erste Referenz dient das vorgegebene Testmodell, so wie es in \niceRef{sec:db_final_models} beschrieben ist, und als zweite Referenz das Modell, welches ohne Zerlegungsmethode rekonstruiert wurde.

Die Tabellen enthalten den Mittelwert der betrachteten Eigenschaft.
Unter dem Mittelwert steht das Verhältnis zur ersten Referenz und darunter das zur zweiten Referenz.
Ist der Mittelwert einer Eigenschaft größer als der Mittelwert der Referenz, ist das Verhältnis rot eingefärbt \bzw grün, wenn er kleiner ist.
Die Farben geben somit nur an, ob der Wert größer oder kleiner als die Referenz ist, nicht ob er besser oder schlechter ist.
Letzteres ist, wenn überhaupt, abhängig von der betrachteten Eigenschaft.

\subsection{Anzahl an CSG-Operationen}
\label{sec:db_final_tree_ops}
Die folgende Tabelle gibt zum einen die durchschnittliche Anzahl an CSG-Operationen $n_\text{op}$ im CSG-Baum an.
Zum anderen das Verhältnis der drei Operationen (Vereinigung \enquote{$\cup$}, Differenz \enquote{$-$} und Schnitt \enquote{$\cap$}) untereinander.

\begin{table}[H]
	\centering
	\input{databook/final/tree_ops}
	\caption{Anzahl aller/einzelner CSG-Operationen in der Vorlage / den Rekonstruktionen, verglichen (farbig) mit der Vorlage und der Rekonstruktion ohne Zerlegung.}
	\label{tab:db_final_tree_ops}
\end{table}

\clearpage


# for title, key in [
	('Anzahl primitiver Geometrien', 'geo_cnt'),
	('Anzahl an Knoten', 'node_cnt'),
	('Baumhöhe', 'tree_height'),
	('Durchschnittliche Ebene primitiver Geometrien', 'geo_levels'),
	('Durchschnittliche Anzahl an Kindern pro CSG-Operation', 'op_widths')
]
	\subsection{ {{- title -}} }
	\label{sec:db_final_tree_{{ key }}}
	
	\begin{table}[H]
		\centering
		\input{databook/final/tree_{{key}}}
		\caption{ {{- title }} der Vorlage / der Rekonstruktionen, verglichen (farbig) mit der Vorlage und der Rekonstruktion ohne Zerlegung.}
		\label{tab:db_final_{{ key }}}
	\end{table}
	
	\clearpage
# endfor

\section{Spezielle Eigenschaften der Zerlegungsmethoden}
\label{sec:db_final_cluster}
Die folgenden Tabellen fassen noch einmal speziell die Werte zusammen, welche die Cluster-Methoden ausmachen.

Der einzig neue Wert findet sich in der Tabelle für die cliquen-basierte Zerlegung.
Er beschreibt wie viel Prozent der Knoten des Merge Trees unbalanciert sind.

\subsection{Eigenschaften der cliquen-basierten Zerlegung}
\label{sec:db_final_cluster_clique}
\begin{table}[H]
	\centering
	\input{databook/final/cluster_clique}
	\caption{Eigenschaften der Cluster, welche mit der cliquen-basierten Zerlegungsmethode entstehen.}
	\label{tab:db_final_cluster_clique}
\end{table}

\subsection{Eigenschaften der pruning-basierten Zerlegung}
\label{sec:db_final_cluster_part}
\begin{table}[H]
	\centering
	\input{databook/final/cluster_part}
	\caption{Eigenschaften der Cluster, welche mit der pruning-basierten Zerlegungsmethode entstehen.}
	\label{tab:db_final_cluster_part}
\end{table}
\clearpage
