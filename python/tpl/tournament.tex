%% This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
%% Copyright (c) Skruppy <skruppy@onmars.eu>
%% SPDX-License-Identifier: Apache-2.0

\chapter{Tournament-Konfiguration}
\label{sec:db_tournament}
% \chapter{Auswahl der \gls{MR} und der \gls{CoR}}
# set mr_cnt = mating_rates|length
# set cor_cnt = crossover_rates|length

In diesem Kapitel wird evaluiert, welche \glsreset{MR}\gls{MR} und welche \glsreset{CoR}\gls{CoR} die Beste für die Tournament-Auswahlstrategie ist.
Dazu wurde das Testmodell mit jeder Kombination aus {{ mr_cnt }} verschiedenen \glspl{MR} und {{ cor_cnt }} verschiedenen \glspl{CoR} rekonstruiert.
Da die Rekonstruktion Zufallsprozesse inkorporiert wurden die Daten mit einem Gauß-Filter geglättet, weshalb es ausreicht, dass jede Rekonstruktion nur {{ samples_per_config }} Mal wiederholt wird um die Erwartungswerte in der Analyse genauer schätzen zu können.
Somit wurden \textbf{ {{- '{:,d}'.format(mr_cnt * cor_cnt * samples_per_config) }} Jobs} auf dem Rechen-Cluster ausgeführt.
Eine dabei verwendete Verarbeitungs-Pipeline ist exemplarisch in \niceRef{fig:db_tournament_pipeline} zu sehen.

Würde bei allen Rekonstruktionen die maximale Anzahl an Generationen erstellt, um das beste CSG-Modell zu finden, wäre die Anzahl erzeugter Individuen:

\begin{center}
	\rowfont{\numbersbig}
	\begin{tabular}{C@{}C@{}C@{}C@{}C@{}C@{}C@{}C@{}C@{}C@{}C}
		{{ mr_cnt }}
		& × & {{ cor_cnt }}
		& × & 150
		& × & 500
		& × & {{ samples_per_config }}
		& = & {{ '{:,d}'.format(mr_cnt*cor_cnt*150*500*samples_per_config) }}
		\rowfont{\numberssmall}
		\\
		\acrshort{MR}s
		& & \acrshort{CoR}s
		& & \makecell[tc]{Generationen \\ pro \acrshort{EA}}
		& & \makecell[tc]{Individuen \\ pro Generation}
		& & \makecell[tc]{Wieder-\\holungen}
		& & Erzeugte Individuen (CSG-Modelle)
		\\
	\end{tabular}
\end{center}

\begin{landscape}
	\begin{figure}[H]
		\centering
		\includegraphics[width=1\linewidth]{databook/tournament/pipeline}
		\caption{Beispielhafte Pipeline des \glspl{csgfitter} für die Analyse der Auswirkung von \gls{MR} und \gls{CoR}.}
		\label{fig:db_tournament_pipeline}
	\end{figure}
\end{landscape}
\clearpage

\section{Einzelergebnisse für ausgewählte Kennzahlen}
\label{sec:db_tournament_details}
Die folgenden Teilabschnitte behandeln jeweils die Auswirkung der \gls{MR} und \gls{CoR} auf eine Kennzahl zur Bewertung der Rekonstruktionsqualität.
In den gezeigten Heatmaps liegen die beiden variablen Parameter \gls{MR} und \gls{CoR} auf den Achsen und der Wert der Kennzahl wird durch die Farbe angezeigt.
Eines der Diagramme zeigt dabei die Rohdaten, das andere die geglätteten und interpolierten Daten.
Auf Basis der geglätteten Daten werden Konturlinien zur besseren Orientierung erstellt.
Diese sind auf beiden Diagrammen identisch eingezeichnet.
Ebenso werden auf der selben Basis das Minimum und Maximum ermittelt und mit einem blauen \bzw roten Fadenkreuz auf beiden Diagrammen identisch eingezeichnet.

\clearpage
# for config in configs
	# set model_desc = descriptions.models[config.model_name]
	# set prop_desc = model_desc.props[config.prop_name]
	\subsection{ {{- model_desc.label_latex | safe }} -- {{ prop_desc.label_latex | safe -}} }
	\label{sec:db_tournament_details_{{ config.model_name }}_{{ config.prop_name }}}
	
	\begin{figure}[H]
		\centering
		\subcaptionbox
		{Geglättete Daten\label{fig:db_tournament_details_{{ config.model_name }}_{{ config.prop_name }}_smoothed}}
		{\includegraphics[width=0.49\linewidth]{ databook/tournament/{{ config.figures.smoothed }} }}
		\hfill
		\subcaptionbox
		{Rohdaten\label{fig:db_tournament_details_{{ config.model_name }}_{{ config.prop_name }}_raw}}
		{\includegraphics[width=0.49\linewidth]{ databook/tournament/{{ config.figures.raw }} }}
		\caption{Heatmap mit Minimum (blau) und Maximum (rot) sowie Konturlinien der Kennzahl \enquote{ {{- prop_desc.label_latex -}} } des Bewertungsmodells \enquote{ {{- model_desc.label_latex | safe -}} }.}
		\label{fig:db_tournament_details_{{ config.model_name }}_{{ config.prop_name }}}
	\end{figure}
	
	\begin{table}[H]
		\centering
		\begin{tabular}{ll}
			\makecell[tl]{
				Maximum in den geglätteten Daten \\
				\quiet{(\textcolor{red}{rotes} Fadenkreuz)}
			} &
			{{ '{:.3f}'.format(config.smoothed_max.value) }}
			(\pcent{ {{- config.smoothed_max.mating_rate -}} } \gls{MR} /
			\pcent{ {{- config.smoothed_max.crossover_rate -}} } \gls{CoR}) \\ \addlinespace
			%
			\makecell[tl]{
				Minimum in den geglätteten Daten \\
				\quiet{(\textcolor{blue}{blaues} Fadenkreuz)}
			} &
			{{'{:.3f}'.format(config.smoothed_min.value)}}
			(\pcent{ {{- config.smoothed_min.mating_rate -}} } \gls{MR} /
			\pcent{ {{- config.smoothed_min.crossover_rate -}} } \gls{CoR}) \\ \addlinespace
			%
			\makecell[tl]{
				Maximum der Rohdaten \\
				\quiet{(\emph{nicht} markiert mit einem Fadenkreuz)}
			} &
			{{'{:.3f}'.format(config.raw_max)}} \\ \addlinespace
			%
			\makecell[tl]{
				Minimum der Rohdaten \\
				\quiet{(\emph{nicht} markiert mit einem Fadenkreuz)}
			} &
			{{'{:.3f}'.format(config.raw_min)}}
		\end{tabular}
		\caption{Minima und Maxima der Kennzahl \enquote{ {{- prop_desc.label_latex -}} } des Bewertungsmodells \enquote{ {{- model_desc.label_latex | safe -}} }.}
		\label{tab:db_tournament_details_{{ config.model_name }}_{{ config.prop_name }}}
	\end{table}
	\clearpage
# endfor


\begin{landscape}
	\section{Zusammenfassung der Minima und Maxima}
	\label{sec:db_tournament_summary}
	
	\begin{table}[H]
		\begin{tabularx}{\linewidth}{Xrrrrrr}
			\toprule
			Modell -- Kennzahl &
			\multicolumn{3}{c}{Maximum \quiet{(\textcolor{red}{rotes} Fadenkreuz)}} &
			\multicolumn{3}{c}{Minimum \quiet{(\textcolor{blue}{blaues} Fadenkreuz)}}
			\\ \cmidrule(r){2-4}\cmidrule(r){5-7}
			& Wert & \acrshort{MR} & \acrshort{CoR}
			& Wert & \acrshort{MR} & \acrshort{CoR}
			\\ \midrule
			# for config in configs
				# set model_desc = descriptions.models[config.model_name]
				# set prop_desc = model_desc.props[config.prop_name]
				{{ model_desc.label_latex | safe }} -- {{ escapeLatex(prop_desc.label_latex) }}
	% 			\newline({{escapeLatex(config.model_name)}} - {{escapeLatex(config.prop_name)}})
				&
				{{ '{:.3f}'.format(config.smoothed_max.value) }} &
				\pcent{ {{- config.smoothed_max.mating_rate -}} } &
				\pcent{ {{- config.smoothed_max.crossover_rate -}} } &
				{{ '{:.3f}'.format(config.smoothed_min.value) }} &
				\pcent{ {{- config.smoothed_min.mating_rate -}} } &
				\pcent{ {{- config.smoothed_min.crossover_rate -}} }
				\\
			# endfor
			\bottomrule
		\end{tabularx}
		\caption{Minima und Maxima aller analysierten Bewertungsmodelle und deren Eigenschaften.}
		\label{tab:db_tournament_summary}
	\end{table}
\end{landscape}


\section{Analyse der Diskrepanz zwischen Median und Mittelwert}
\label{sec:db_tournament_med-avg}
Bei genauerer Betrachtung der Ergebnisse aus \niceRef{sec:db_tournament_details_fit_ab_all_med_rate} und \autoref{sec:db_tournament_details_fit_ab_all_avg_rate}\vpageref*{sec:db_tournament_details_fit_ab_all_avg_rate} fällt eine unintuitive Diskrepanz der Rohdaten auf.
So ist ein Großteil der Daten ähnlich, nur im Bereich hoher \gls{MR} und \gls{CoR} fallen die Kennzahlen über eine verrauschte Kante auf niedrige Werte ab.
In diesem Abschnitt wird diese Beobachtung genauer untersucht.

Die beiden verglichenen Bewertungsmodelle sind \enquote{ {{- descriptions.models['fit_ab_all_med'].label_latex | safe -}} } und \enquote{ {{- descriptions.models['fit_ab_all_avg'].label_latex | safe -}} }.
Sie unterscheiden sich nicht in dem Modell, das eingepasst wird, sondern nur darin auf welche Daten das Modell eingepasst wird.
Ersteres bezieht sich auf den Median und zweiteres auf den Mittelwert der Individuenbewertungen einer Generation.

In \niceRef{fig:db_tournament_med-avg_raw} sind die beiden betroffenen Heatmaps noch einmal gegenübergestellt.

\begin{figure}[H]
	\centering
	\subcaptionbox
	{ {{- descriptions.models['fit_ab_all_med'].label_latex | safe -}} \label{fig:db_tournament_med-avg_raw_med}}
	{\includegraphics[width=0.4\linewidth]{databook/tournament/fit_ab_all_med-rate-raw}}
	\quad
	\subcaptionbox
	{ {{- descriptions.models['fit_ab_all_avg'].label_latex | safe -}} \label{fig:db_tournament_med-avg_raw_avg}}
	{\includegraphics[width=0.4\linewidth]{databook/tournament/fit_ab_all_avg-rate-raw}}
	\caption{Gegenüberstellung der Heatmaps basierend auf dem Median und dem Mittelwert.}
	\label{fig:db_tournament_med-avg_raw}
\end{figure}

Um die Theorie genauer zu betrachten, dass die beiden Modelle überwiegend ähnlich in einem Teil sind, jedoch die Kennzahlen des mittelwertbasierten Modells abfallen, wird die relative Differenz der beiden Modelle in \niceRef{fig:db_tournament_med-avg_diff} dargestellt.
\begin{figure}[H]
	\centering
	\subcaptionbox
	{Geglättete Daten\label{fig:db_tournament_med-avg_diff_smoothed}}
	{\includegraphics[width=0.4\linewidth]{databook/tournament/{{diff.figures.smoothed}}}}
	\quad
	\subcaptionbox
	{Rohdaten\label{fig:db_tournament_med-avg_diff_raw}}
	{\includegraphics[width=0.4\linewidth]{databook/tournament/{{diff.figures.raw}}}}
	\caption{Relative Differenz der Heatmaps aus \niceRef{fig:db_tournament_med-avg_raw}.}
	\label{fig:db_tournament_med-avg_diff}
\end{figure}

Weiter ist auffällig, dass zwischen den zwei Bereichen kein fließender Übergang existiert, sondern ein kontraststarkes Rauschen im Übergangsbereich herrscht.
Die Heatmap mit den relativen Differenzen lässt vermuten, dass die Daten des mittelwertbasierten Modells entweder ähnlich dem des medianbasierten Modells sind oder nahe Null.
Ein Wert dazwischen scheint eher selten.
Diese Beobachtung wird mit dem Histogramm in \niceRef{fig:db_tournament_med-avg_diff_hist} näher untersucht.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{databook/tournament/diff-hist}
	\caption{Histogramm und geschätzte Wahrscheinlichkeitsdichte der Daten aus \niceRef{fig:db_tournament_med-avg_diff_raw}.}
	\label{fig:db_tournament_med-avg_diff_hist}
\end{figure}

Um Ursachen für das Problem der oben beschriebenen Beobachtungen zu finden, wurden händisch Stichproben aus dem betroffenen Bereich ausgewählt.
Die Visualisierungen der beiden Bewertungsmodelle einer jeden Stichprobe sind in den folgenden Unterabschnitten zu sehen.

# for example in diff_examples
\subsection{\gls{MR} \pcent{ {{- example.mating_rate -}} } / \gls{CoR} \pcent{ {{- example.crossover_rate -}} }}
\label{sec:db_tournament_med-avg_example_{{ example.mating_rate }}_{{ example.crossover_rate }}}

Beispiel aus \texttt{ {{- example.base_path -}} } mit einem Unterschied der Verbesserungsrate von \pcent{ {{- '{:.1f}'.format((example.models['fit_ab_all_med'].rate - example.models['fit_ab_all_avg'].rate)*100) -}} }p zwischen den beiden Bewertungsmodellen:

\begin{figure}[H]
	\centering
	\subcaptionbox
	{Verbesserungsrate beim medianbasierten Modell: \pcent{ {{- '{:.1f}'.format(example.models['fit_ab_all_med'].rate*100) -}} }\label{fig:db_tournament_med-avg_example_{{ example.mating_rate }}_{{ example.crossover_rate }}_med}}
	{\includegraphics[width=0.49\linewidth]{databook/tournament/{{example.models['fit_ab_all_med'].file}}}}
	\hfill
	\subcaptionbox
	{Verbesserungsrate beim mittelwertbasierten Modell: \pcent{ {{- '{:.1f}'.format(example.models['fit_ab_all_avg'].rate*100) -}} }\label{fig:db_tournament_med-avg_example_{{ example.mating_rate }}_{{ example.crossover_rate }}_avg}}
	{\includegraphics[width=0.49\linewidth]{databook/tournament/{{example.models['fit_ab_all_avg'].file}}}}
	\caption{Gegenüberstellung exemplarischer Populationsentwicklungen mit \gls{MR} \pcent{ {{- example.mating_rate -}} } / \gls{CoR} \pcent{ {{- example.crossover_rate -}} }.}
	\label{fig:db_tournament_med-avg_example_{{ example.mating_rate }}_{{ example.crossover_rate }}}
\end{figure}
# endfor

\section{Beispiele}
\label{sec:db_tournament_examples}
Um die Festlegung der \gls{MR} und \gls{CoR} für die weiteren Analysen zu unterstützen wurden weitere Stichproben ausgewählt.
Sie stammen aus $2 \times 2$ Datenpunkten großen Bereichen, die in den bisherigen Heatmaps vielversprechend aussehen.
Die abgebildeten Diagramme geben eine detailiertere Einsicht in den tatsächlichen Rekonstruktionsverlauf an diesen Stellen.

# for example in examples
	\subsection{MR \pcent{ {{- example.mating_rate -}} } / CoR \pcent{ {{- example.crossover_rate -}} }}
	\label{sec:db_tournament_examples_{{ example.mating_rate }}_{{ example.crossover_rate }}}
	##Beispiel aus \texttt{ {{- example.base_path -}} }:
	
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.55\linewidth]{databook/tournament/{{ example.figures.fit_ab_all_med }}}
		\caption{Entwicklung des Medians der Individuenbewertungen pro Generation mit einer \gls{MR} von \pcent{ {{- example.mating_rate -}} } und einer \gls{CoR} von \pcent{ {{- example.crossover_rate -}} }.}
		\label{fig:db_tournament_examples_{{ example.mating_rate }}_{{ example.crossover_rate }}_fit_ab_all_med}
	\end{figure}
	
	\begin{figure}[H]
		\centering
		\includegraphics[width=\linewidth]{databook/tournament/{{ example.figures.raw_points }}}
		\caption{Kombiniertes verwackeltes Streu- und Violinendiagramm der Individuenbewertungen ausgewählter Generationen mit einer \gls{MR} von \pcent{ {{- example.mating_rate -}} } und einer \gls{CoR} von \pcent{ {{- example.crossover_rate -}} }, aufgeteilt in vier einzeln skalierte Diagramme.}
		\label{fig:db_tournament_examples_{{ example.mating_rate }}_{{ example.crossover_rate }}_raw_points}
	\end{figure}
	
	\begin{figure}[H]
		\centering
		\includegraphics[width=\linewidth]{databook/tournament/{{ example.figures.ranks }}}
		\caption{Rangveränderungen der Rekonstruktion mit einer \gls{MR} von \pcent{ {{- example.mating_rate -}} } und einer \gls{CoR} von \pcent{ {{- example.crossover_rate -}} }: l.\,o.) Alle Individuenbewertungen ausgewählter Generationen nach Rang. l.\,u.) Relative Veränderung der Individuenbewertungen zwischen den ausgewählten Generationen nach Rang. r.\,o.) Entwicklung ausgewählter Quantile der Individuenbewertungen über die Generationen hinweg.}
		\label{fig:db_tournament_examples_{{ example.mating_rate }}_{{ example.crossover_rate }}_ranks}
	\end{figure}
	
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.7\linewidth]{databook/tournament/{{ example.figures.uniqueness }}}
		\caption{Verlauf der Einzigartigkeit der Individuenbewertungen über die Generationen hinweg bei einer \gls{MR} von \pcent{ {{- example.mating_rate -}} } und einer \gls{CoR} von \pcent{ {{- example.crossover_rate -}} }.}
		\label{fig:db_tournament_examples_{{ example.mating_rate }}_{{ example.crossover_rate }}_uniqueness}
	\end{figure}
	\clearpage
# endfor
