%% This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
%% Copyright (c) Skruppy <skruppy@onmars.eu>
%% SPDX-License-Identifier: Apache-2.0


\chapter{Auswahl- und Einfügestrategien}
\label{sec:db_selector-matrix}
# set selector_cnt = selectors|length
# set rep_strat_cnt = rep_strats|length
# set repetitions = 50

In diesem Kapitel werden die Strategien untersucht, welche Individuen aus einer \gls{EA}-Generation auswählen, um mit ihnen eine neue Generation zu bilden.
Das sind zum einen {{ selector_cnt }} Strategien um Individuen als Elternpaare auszuwählen.
Zum anderen sind es {{ rep_strat_cnt }} Einfüge- \bzw Ersetzungsstrategien, die bestimmen an welcher Stelle die Kinder eingefügt werden \bzw welche Individuen durch sie ersetzt werden.
Für jede Kombination dieser Strategien wurde das Testmodell ohne Zerlegungsmethoden rekombiniert.
Da die Rekonstruktion Zufallsprozesse inkorporiert wurde jede Rekonstruktion {{ repetitions }} Mal wiederholt um die Erwartungswerte in der Analyse genauer schätzen zu können.
Somit wurden insgesamt \textbf{ {{- '{:,}'.format(selector_cnt*rep_strat_cnt*repetitions) }} Jobs} auf dem Rechen-Cluster ausgeführt.
Eine dabei verwendete Verarbeitungs-Pipeline ist exemplarisch in \niceRef{fig:db_selector-matrix_pipeline} zu sehen.

Würde bei allen Rekonstruktionen die maximale Anzahl an Generationen erstellt, um das beste CSG-Modell zu finden, wäre die Anzahl erzeugter Individuen:

\begin{center}
	\rowfont{\numbersbig}
	\begin{tabular}{C@{}C@{}C@{}C@{}C@{}C@{}C@{}C@{}C@{}C@{}C}
		{{ selector_cnt }}
		& × & {{ rep_strat_cnt }}
		& × & 150
		& × & 500
		& × & {{ repetitions }}
		& = & {{ '{:,d}'.format(selector_cnt*rep_strat_cnt*150*500*repetitions) }}
		\rowfont{\numberssmall}
		\\
		\makecell[tc]{Auswahl-\\strategien}
		& & \makecell[tc]{Einfüge-\\strategien}
		& & \makecell[tc]{Generationen \\ pro \acrshort{EA}}
		& & \makecell[tc]{Individuen \\ pro Generation}
		& & \makecell[tc]{Wieder-\\holungen}
		& & Erzeugte Individuen (CSG-Modelle)
		\\
	\end{tabular}
\end{center}

\begin{landscape}
	\begin{figure}[H]
		\centering
		\includegraphics[width=1\linewidth]{databook/selector-matrix/pipeline}
		\caption{Beispielhafte Pipeline des \glspl{csgfitter} für die Analyse der Auswahl- und Einfügestrategien.}
		\label{fig:db_selector-matrix_pipeline}
	\end{figure}
\end{landscape}
\clearpage

\section{Eigenschaften der Kombinationen aus einer Auswahl- und Einfügestrategie}
\label{sec:db_selector-matrix_overview}
Die folgenden Tabellen zeigen jeweils unterschiedliche Aspekte, wie sich die verschiedenen Kombinationen aus Auswahl- und Einfügestrategie auf die Rekombination auswirken.
Dazu wurde für jede Kombination eine der {{ repetitions }} Wiederholungen exemplarisch ausgewählt.
Um einen möglichst \enquote{typischen} Repräsentanten für eine Kombination zu finden wurde die Rekonstruktion ausgewählt, welche nach dem Berwertungsmodell \enquote{ {{- descriptions.models['fit_ab_all_med'].label_latex | safe -}} } den Median des Modellparameters $b$ stellt.
Jede Tabelle zeigt somit das selbe Set an Rekonstruktionen, nur ihre Darstellungsform ändert sich.
Diese ist näher im Tabellentitel beschrieben.
Aufgrund ihrer Größe wurden die Tabellen auf zwei Seiten aufgeteilt.

# for model, width, title in [
	('raw_points', 0.9, 'Kombiniertes verwackeltes Streu- und Violinendiagramm der Individuenbewertungen ausgewählter Generationen, aufgeteilt in vier einzeln skalierte Diagramme'),
	('raw_overview', 1, 'Box-Plot der Individuenbewertungen jeder Generation'),
	('skew', 1, 'Verlauf der Schiefe der Individuenbewertungen über die Generationen hinweg'),
	('ranks', 1, 'l.\,o.) Alle Individuenbewertungen ausgewählter Generationen nach Rang. l.\,u.) Relative Veränderung der Individuenbewertungen zwischen den ausgewählten Generationen nach Rang. r.\,o.) Entwicklung ausgewählter Quantile der Individuenbewertungen über die Generationen hinweg'),
	('uniqueness', 1, 'Verlauf der Einzigartigkeit der Individuenbewertungen über die Generationen hinweg')
]
	# set key_example_config = config.key_examples[loop.index0]
	\begin{table}[H]
		\centering
		\begin{tabularx}{ {{- width }}\linewidth}{l|XXX}
			# for col in selectors[:3]
				& {{ escapeLatex(col) }}
			# endfor
			\\\hline\\[-1em]
			# for rep_strat in rep_strats
				\rotatebox{90}{\scriptsize{ {{ escapeLatex(rep_strat) }} }}
				# for selector in selectors[:3]
					# set cell = key_examples[(selector, rep_strat)][key_example_config.fig]
					& \includegraphics[width=\linewidth]{ databook/selector-matrix/{{ cell.image[:-4] }} }
				# endfor
				\\
			# endfor
		\end{tabularx}
		\caption{ {{- title }}.}
		\label{tbl:db_selector-matrix_overview_{{ model }}_1}
	\end{table}
	
	\begin{table}[H]
		\centering
		\begin{tabularx}{ {{- width }}\linewidth}{l|XXX}
			# for col in selectors[3:]
				& {{ escapeLatex(col) }}
			# endfor
			\\\hline\\[-1em]
			# for rep_strat in rep_strats
				# if loop.index0
					\\
				# endif
				\rotatebox{90}{\scriptsize{ {{ escapeLatex(rep_strat) }} }}
				# for selector in selectors[3:]
					# set cell = key_examples[(selector, rep_strat)][key_example_config.fig]
					& \includegraphics[width=\linewidth]{ databook/selector-matrix/{{ cell.image[:-4] }} }
				# endfor
			# endfor
		\end{tabularx}
		\caption{ {{- title }} (Fortsetzung).}
		\label{tbl:db_selector-matrix_overview_{{ model }}}
	\end{table}
# endfor

\clearpage %% Moves content up on the page

\section{Bewertungsmodelle}
\label{sec:db_selector-matrix_models}
Wie unterschiedliche \gls{EA}-Bewertungsmodelle die Kombinationen aus Auswahl- und Einfügestrategie bewerten, wird im Folgenden gezeigt.
Jeder Unterabschnitt befasst sich dabei mit einem Bewertungsmodell.

Zunächst werden Visualisierungen der Anwendung des Bewertungsmodells auf einer beispielhaften Rekonstruktion pro Kombination gezeigt.
Diese Tabellen sind identisch aufgebaut, wie die aus dem vorherigen Abschnitt.
Das Set an exemplarischen Rekonstruktionen ist jedoch für jeden Unterabschnitt verschieden und entspricht einem, welches für das Bewertungsmodell des jeweiligen Unterabschnitts am repräsentativsten ist.
% Im Gegensatz zum vorherigen Abschnitt sind jedoch die Sets an exemplarischen Rekonstruktionen für jeden Unterabschnitt verschieden.
% Sie entsprechen einem, das für das jeweilige Bewertungsmodell am repräsentativsten ist.

Im Anschluss zeigen Tabellen für jede Kennzahl des Bewertungsmodells den Mittelwert sowie die Standardabweichung der jeweiligen Kennzahl für jede Kombination.
Zur leichteren Übersicht steht ein farbliches Feld vor jedem Mittelwert, das ihn repräsentiert.
Das hierzu verwendete Farbschema ist kennzahlabhängig und kann sich somit von Tabelle zu Tabelle unterscheiden.

In den darauffolgenden Tabellen mit Diagrammen werden anschließend die Kennzahlen über alle {{ repetitions }} Wiederholungen visualisiert.
Welche Darstellungen möglich sind hängt von dem Bewertungsmodell ab.
Allen ist jedoch gemein, dass in Streudiagrammen die Datenpunkte der als Ausreißer klassifizierten Rekonstruktionen rot dargestellt sind.
Die genauere Beschreibung der Diagramme ist wieder dem Tabellentitel zu entnehmen.

Die letzten beiden Tabellentypen eines Unterabschnitts geben Auskunft über die Qualität der Testreihe selbst.
Der erste Tabellentyp schätzt dazu die Anzahl der notwendigen Wiederholungen, um den Erwartungswert der Kennzahl auf einem Konfidenzniveau $\gamma$ von \pcent{95} zu schätzen.
Dabei wird ein Konfidenzintervall von \pcent{1} des Interquartilabstandes der Kennzahl aus allen Testreihen verwendet.
Der zweite Tabellentyp gibt Auskunft darüber, wie viele Rekonstruktionen als Ausreißer klassifiziert wurden und wie viele gar nicht erst eingepasst werden konnten.

\clearpage
# for subthing in subthings
	\input{databook/selector-matrix/{{ subthing.key }}}
# endfor

\section{Korrelation der Kennzahlen}
\label{sec:db_selector-matrix_correlation}
Um die Vergleichbarkeit der Kennzahlen zu beurteilen wurde der Rangkorrelationskoeffizient zwischen allen berechnet.
Dazu wurde das Kendall’sche $\tau$ verwendet.
% Die Korrelationsmatrix ist in \niceRef{fig:db_selector-matrix_correlation} als Ganzes zu sehen und, zur besseren Übersicht, in den drei darauffolgenden Abbildungen in Quadranten aufgeteilt.
Die Korrelationsmatrix ist in \niceRef{fig:db_selector-matrix_correlation} als Ganzes zu sehen.
In den drei darauffolgenden Abbildungen ist sie zur besseren Übersicht in Quadranten aufgeteilt.
Durch die Symmetrie konnte ein Quadrant weggelassen werden.

\begin{figure}[H]
	\centering
	\includegraphics[width=\linewidth]{databook/selector-matrix/correlation}
	\caption{Kendall’sche Rangkorrelationskoeffizienten zwischen allen Kennzahlen (vollständig).}
	\label{fig:db_selector-matrix_correlation}
\end{figure}
\clearpage

\begin{figure}[H]
	\centering
	\includegraphics[width=\linewidth]{databook/selector-matrix/correlation-1}
	\caption{Kendall’sche Rangkorrelationskoeffizienten zwischen allen Kennzahlen (Ausschnitt 1/3).}
	\label{fig:db_selector-matrix_correlation1}
\end{figure}
\clearpage
\begin{figure}[H]
	\centering
	\includegraphics[width=\linewidth]{databook/selector-matrix/correlation-2}
	\caption{Kendall’sche Rangkorrelationskoeffizienten zwischen allen Kennzahlen (Ausschnitt 2/3).}
	\label{fig:db_selector-matrix_correlation2}
\end{figure}
\clearpage
\begin{figure}[H]
	\centering
	\includegraphics[width=\linewidth]{databook/selector-matrix/correlation-3}
	\caption{Kendall’sche Rangkorrelationskoeffizienten zwischen allen Kennzahlen (Ausschnitt 3/3).}
	\label{fig:db_selector-matrix_correlation3}
\end{figure}
