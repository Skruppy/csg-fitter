#!/usr/bin/env python3
## This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0


import shell_worker
import plugin
import test_final_base
import util
import fmtjson
import fitter_cache
import os.path as op
import sys
import shutil
import numpy as np
import web
import math
import collections
import re
import itertools



def load_models():
	models = {}
	base_path = op.join(util.script_dir(), '../tests/final')
	web_dir = op.join(util.script_dir(), '../public/final')
	csgfitter = op.join(util.script_dir(), '../csgfitter-static')
	util.mkdirs(base_path)
	util.mkdirs(web_dir)
	builder_clses = plugin.loadAll(test_final_base.FinalCreator)
	csg_parser = CSGTreeParser()
	
	## Create all basic files (json, scad, png) for all tests and both models.
	sh = shell_worker.ShellWorker()
	scad_jobs = []
	for builder_cls in builder_clses:
		configurations = getattr(builder_cls, 'configurations', {'': {}})
		
		for config_name, config in configurations.items():
			creator = builder_cls(**config)
			
			full_name = builder_cls.pluginName
			if config_name:
				full_name += f'-{config_name}'
			
			#if full_name != 'matrjoschka-1':
				#continue
			
			json_path = op.join(base_path, f'{full_name}.json')
			csg_tree = creator.build()
			json_str = fmtjson.format_csg(csg_tree)
			
			models[full_name] = util.Dobject(
				name = full_name,
				json_path = f'tests/final/{full_name}.json',
				sample_rate = creator.sample_rate,
				stats = sum_csg_tree_stats([csg_parser.parse(csg_tree)]),
			)
			
			## Write JSON (but only if needed, to prevent timestamp updates)
			if not op.isfile(json_path) or util.read_file(json_path) != json_str:
				util.write_file(json_str, json_path)
			
			public_json_path = op.join(web_dir, f'{full_name}.json')
			if util.outdated(public_json_path, json_path):
				shutil.copy2(json_path, public_json_path)
			
			## Now: Convert JSON to SCAD (async)
			scad_path = op.join(web_dir, f'{full_name}.scad')
			if util.outdated(scad_path, json_path):
				sh.run(csgfitter, 'convert', f'from={json_path}', f'to={scad_path}')
			
			## Later: Convert SCAD to PNG
			png_path = op.join(web_dir, f'{full_name}.png')
			if util.outdated(png_path, json_path):
				scad_jobs.append([
					'openscad',
					scad_path,
					'-o', png_path,
					'--viewall', '--autocenter', 
					'--imgsize=600,400',
					'--view=axes,scales',
					'--render',
					'--camera=0,0,0,340,20,0,1',
				])
	
	## Wait til all json files have been converted to scad files (using
	## `csgfitter-static`) and then start all scad to png renderings (using
	## OpenSCAD).
	sh.wait_done()
	for job in scad_jobs:
		sh.run(*job)
	sh.stop()
	
	return models


def csgfitter_evo_cmd(evo_args=None, sample_rate=5, input='tests/renderings/gecco19.json', evo_type='evo'):
	if evo_args is None:
		evo_args = []
	
	args = [
		## Create point cloud and primitives from CSG tree
		'json_reader', f'path=<IN:{input}>', 'name=csg',
		'csg.', '!', 'extract_primitives', 'name=primitives',
		'csg.', '!', 'surface_sampler', f'rate={sample_rate}', 'name=sampler',
		
		## Compute reduced point cloud, sufficient for the evolutionary algorithm
		'primitives.primitives', '!', 'reducer.primitives',
		'sampler.npc',           '!', 'reducer.complete_npc',
		'reduce', 'name=reducer',
		
		## Set evaluator
		'evaluator_12', 'name=evaluator',
		
		## Find a solution using specified evolutionary algorithm and simplify it
		'evaluator.',  '!', 'fitter.evaluator',
		'primitives.', '!', 'fitter.primitives',
		'reducer.',    '!', 'fitter.reference',
		evo_type, 'name=fitter', *evo_args, 'max_rounds=150', '!', 'simplify', 'name=simplifier',
		
		## Save raw fitter result as JSON/SCAD/GraphViz
		'fitter.', '!', 'json_writer', 'path=<OUT:raw_result.json>',
		'fitter.', '!', 'scad_writer', 'path=<OUT:raw_result.scad>',
		'fitter.', '!', 'gv_writer',   'path=<OUT:raw_result.gv>',
		
		## Save simplified fitter result as JSON/SCAD/GraphViz
		'simplifier.', '!', 'json_writer', 'path=<OUT:simple_result.json>',
		'simplifier.', '!', 'scad_writer', 'path=<OUT:simple_result.scad>',
		'simplifier.', '!', 'gv_writer',   'path=<OUT:simple_result.gv>',
		
		## Save raw reference point cloud (for repeatability purposes)
		## TODO remove
		'sampler.', '!', '.npc', 'pcd_writer', 'path=<OUT:points.pcd>',
		
		## Score the fitter result against the complete and the reduced point
		## cloud (since only the parts/cliques are evaluated so far).
		'simplifier.', '!', 'final_score_complete.model',
		'sampler.',    '!', 'final_score_complete.reference',
		'evaluator.',  '!', 'final_score_complete.evaluator',
		'score', 'name=final_score_complete',
		
		'simplifier.', '!', 'final_score_reduced.model',
		'reducer.',    '!', 'final_score_reduced.reference',
		'evaluator.',  '!', 'final_score_reduced.evaluator',
		'score', 'name=final_score_reduced',
	]
	
	return args


def format_diff(base, val, good):
	if base !=  0 and val != 0:
		rel = val / base
	elif base ==  0 and val == 0:
		rel = 1
	else:
		return ''
	
	if good > 0:
		color = 'text-success' if rel > 1 else 'text-danger'
	elif good < 0:
		color = 'text-success' if rel < 1 else 'text-danger'
	else:
		color = ''
	
	if rel > 1:
		n = rel
	else:
		n = 1/rel
	
	accuracy = 2 if n >= 2 else 3
	digits = int(math.log10(n)) - accuracy + 1
	decimals = max(0, -digits)
	n = round(n / 10**digits) * 10**digits
	
	if n < 1.1:
		color = 'text-info'
	
	s = f'{n:,.{decimals}f}'
	
	if rel > 1:
		return f'<span class="{color}">{s} &times;</span>'
	else:
		return f'<math xmlns="http://www.w3.org/1998/Math/MathML" class="{color}"><mfrac bevelled="true"><mn>1</mn><mn>{s}</mn></mfrac> <mo>&times;</mo></math>'


def run_tests(models, n):
	producer = fitter_cache.SlurmProduction()
	#producer = fitter_cache.LocalProduction(3)
	fc = fitter_cache.FitterCache(producer)
	
	## Get all combinations of replacement strategies and population sizes
	test_run_combinations = {}
	for model in models.values():
		#if not model.name == 'chain':
		#if not model.name.startswith('chain'):
			#continue
		
		for evo_type in ['evo', 'clique_evo', 'part_evo']:
		#for evo_type in ['clique_evo']:
			test_run_combinations[(model.name, evo_type)] = fc.request(
				csgfitter_evo_cmd(
					[
						f'population_size=500',
						f'mating_rate=1',
						f'crossover_rate=1',
						f'replacement_strategy=worst',
						f'selector=tournament',
					],
					sample_rate = model.sample_rate,
					input = model.json_path,
					evo_type = evo_type,
				),
				n, rebuild=False
			)
	
	## Either terminate program, if manual user intervention is required, or return
	if fc.fetch():
		print('It\'s your turn!')
		sys.exit()
	
	return test_run_combinations




def main():
	models = load_models()
	model_names = list(models.keys())
	evo_types = ['evo', 'clique_evo', 'part_evo']
	test_run_combinations = run_tests(models, 1)
	
	results = {}
	csg_parser = CSGTreeParser()
	clique_parser = CliqueParser()
	clique_stats = []
	part_stats = []
	cpus = {}
	
	for model_name in model_names:
		bucket_fills = []
		buckets = {}
		valid_runs = 0
		results[model_name] = result = util.Dobject(
			evos = {},
			failed_buckets = 0,
			valid_buckets = 0,
			stats = models[model_name].stats,
			render_file = f'{model_name}.png',
			json_file = f'{ model_name }.json',
			json = util.read_file(f'public/final/{ model_name }.json'),
			scad_file = f'{ model_name }.scad',
			scad = util.read_file(f'public/final/{ model_name }.scad'),
		)
		
		for evo_type in evo_types:
			test_runs = test_run_combinations[(model_name, evo_type)]
			csg_tree_stats = []
			raw_csg_tree_stats = []
			
			runtimes = []
			complete_scores = []
			reduced_scores = []
			for test_run in test_runs:
				print(model_name, evo_type, test_run.base_path)
				run_stats = util.dobjectify(util.read_json(op.join(test_run.base_path, 'stats.json')))
				
				csg_tree_stats.append(csg_parser.parse(util.read_json(
					op.join(test_run.base_path, 'output/simple_result.json')
				)))
				raw_csg_tree_stats.append(csg_parser.parse(util.read_json(
					op.join(test_run.base_path, 'output/raw_result.json')
				)))
				
				cpu_stats = cpus.get(run_stats.cpu)
				if cpu_stats is None:
					cpus[run_stats.cpu] = cpu_stats = util.Dobject(
						name = run_stats.cpu,
						count = 0,
						hosts = set()
					)
				cpu_stats.count += 1
				cpu_stats.hosts.add(run_stats.host)
				
				runtimes.append(run_stats.processors.fitter.time/1_000_000)
				complete_scores.append(run_stats.processors.final_score_complete.stats)
				reduced_scores.append(run_stats.processors.final_score_reduced.stats)
				valid_runs += 1
				
				## Handle each bucket per evo and model
				for reduce_stats in run_stats.processors.reducer.stats:
					p = reduce_stats.available_points / reduce_stats.requested_points
					bucket_fills.append(p)
					
					if reduce_stats.bucket_key not in buckets:
						buckets[reduce_stats.bucket_key] = util.Dobject(
							geo = reduce_stats.geo,
							points = [],
						)
					buckets[reduce_stats.bucket_key].points.append((
						p, test_run.base_path
					))
				
				if evo_type == 'clique_evo':
					clique_stats.append(clique_parser.parse(
						run_stats.processors.fitter.stats,
					))
				elif evo_type == 'part_evo':
					part_stats.append(parse_part_stats(
						run_stats.processors.fitter.stats,
					))
			
			result.evos[evo_type] = util.Dobject(
				runtime_mean = np.mean(runtimes),
				runtime_std = np.std(runtimes),
				
				complete_scores_mean = np.mean(complete_scores),
				complete_scores_std = np.std(complete_scores),
				
				reduced_scores_mean = np.mean(reduced_scores),
				reduced_scores_std = np.std(reduced_scores),
				
				csg_tree = sum_csg_tree_stats(csg_tree_stats),
			)
			
			if evo_type == 'evo':
				result.evos[evo_type].para_times = []
			elif evo_type == 'clique_evo':
				result.evos[evo_type].clique = sum_clique_stats(clique_stats)
			elif evo_type == 'part_evo':
				result.evos[evo_type].part = sum_part_stats(part_stats)
		
		for bucket_key, bucket in buckets.items():
			bucket.key = bucket_key
			bucket.cnt = len(bucket.points)
			bucket.cnt_required = valid_runs
			
			if bucket.cnt == bucket.cnt_required:
				result.valid_buckets += 1
			else:
				result.failed_buckets += 1
		
		result.buckets = sorted(buckets.values(), key=lambda x: (x.geo, x.key))
		result.unique_buckets = result.valid_buckets + result.failed_buckets
		result.worst_bucket_fills = sorted(bucket_fills)[:5]
	
	
	subthings = []
	for model_name, model_config in models.items():
		subthings.append({
			'title': model_name,
			'url': f'selector-matrix/{ model_name }',
			'key': model_name,
		})
	
	web.render('final_index.html', 'final/index.html', {
		'title': 'Final tests',
		'selected_subthing_key': '',
		'subthings': subthings,
		'models': results,
		'format_diff': format_diff,
		'cpus': cpus,
	})
	
	web.render('final_debug.html', 'final/debug.html', {
		'title': 'Debug infos | Final tests',
		'selected_subthing_key': '',
		'subthings': subthings,
		'models': results,
		'format_diff': format_diff,
		'cpus': cpus,
	})
	
	for model_name, model in models.items():
		web.render('final_details.html', f'final/{ model_name }.html', {
			'title': f'{ model_name } | Final tests',
			'selected_subthing_key': model_name,
			'subthings': subthings,
			'model': results[model_name],
			'model_name': model_name,
			'format_diff': format_diff,
			'cpus': cpus,
		})


if __name__ == '__main__':
	main()

