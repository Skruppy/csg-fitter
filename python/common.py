## This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0
import collections
import util
import matplotlib
import matplotlib.pyplot as plt
import os.path as op
import io
import subprocess
import pdfrw
import hashlib



## matplotlib.colors.to_hex()
def _props(prop_names):
	if not hasattr(_props, '_props'):
		default_color_fn    = util.Dobject(cmap='pink',      norm=lambda l, u: matplotlib.colors.Normalize(l, u))
		rate_color_fn       = util.Dobject(cmap='viridis',   norm=lambda l, u: matplotlib.colors.PowerNorm(0.35, 0, u, clip=True))
		quality_color_fn    = util.Dobject(cmap='viridis',   norm=lambda l, u: matplotlib.colors.Normalize(0, u))
		skew_color_fn       = util.Dobject(cmap='PiYG',      norm=lambda l, u: matplotlib.colors.Normalize(-max(abs(l),abs(u)), max(abs(l),abs(u))))
		uniqueness_color_fn = util.Dobject(cmap='RdYlBu',    norm=lambda l, u: matplotlib.colors.Normalize(0, 1))
		score_fn            = util.Dobject(cmap='viridis_r', norm=lambda l, u: matplotlib.colors.PowerNorm(1.5, 0, u))
		
		_props._props = util.dobjectify({
			## For fit_* models
			'rate': {
				'label': 'Improvement rate',
				'label_latex': 'Verbesserungsrate',
				'desc': 'Factor by which the score statistic improves each round.',
				'desc_latex': '$1 - e^{-\\lambda}$ Verbesserungsrate pro Generation',
				'colorize': rate_color_fn,
				'scale': 100,
			},
			'offset': {
				'label': 'Offset',
				'label_latex': 'Versatz',
				'desc': 'Parameter <math xmlns="http://www.w3.org/1998/Math/MathML"><mi>c</mi></math> of the model <math xmlns="http://www.w3.org/1998/Math/MathML"><mi>a</mi><mo>&#xB7;</mo><msup><mi>e</mi><mrow><mi>b</mi><mo>&#xB7;</mo><mi>x</mi></mrow></msup><mo>+</mo><mi>c</mi></math>. How much the score statistic improvement function is offset from 0. This is also the limit of the score statistic. With each round, the scores statistic are converging to this number. Good values are colse to 0.',
				'desc_latex': 'Parameter $c$ des Modells $a \\cdot e^{b \\cdot x} + c$',
			},
			'ab_b': {
				'name': 'b',
				'label': 'Model parameter `b`',
				'label_latex': 'Modellparameter $b$',
				'desc': 'Parameter <math xmlns="http://www.w3.org/1998/Math/MathML"><mi>b</mi></math> of the model <math xmlns="http://www.w3.org/1998/Math/MathML"><mi>a</mi><mo>&#xB7;</mo><msup><mi>e</mi><mrow><mi>b</mi><mo>&#xB7;</mo><mi>x</mi></mrow></msup></math>.',
				'desc_latex': 'Parameter $b$ des Modells $a \\cdot e^{b \\cdot x}$',
			},
			'ab_a': {
				'name': 'a',
				'label': 'Model parameter `a`',
				'label_latex': 'Modellparameter $a$',
				'desc': 'Parameter <math xmlns="http://www.w3.org/1998/Math/MathML"><mi>a</mi></math> of the model <math xmlns="http://www.w3.org/1998/Math/MathML"><mi>a</mi><mo>&#xB7;</mo><msup><mi>e</mi><mrow><mi>b</mi><mo>&#xB7;</mo><mi>x</mi></mrow></msup></math>.',
				'desc_latex': 'Parameter $a$ des Modells $a \\cdot e^{b \\cdot x}$',
			},
			'abc_b': {
				'name': 'b',
				'label': 'Model parameter `b`',
				'label_latex': 'Modellparameter $b$',
				'desc': 'Parameter <math xmlns="http://www.w3.org/1998/Math/MathML"><mi>b</mi></math> of the model <math xmlns="http://www.w3.org/1998/Math/MathML"><mi>a</mi><mo>&#xB7;</mo><msup><mi>e</mi><mrow><mi>b</mi><mo>&#xB7;</mo><mi>x</mi></mrow></msup><mo>+</mo><mi>c</mi></math>.',
				'desc_latex': 'Parameter $b$ des Modells $a \\cdot e^{b \\cdot x} + c$',
			},
			'abc_a': {
				'name': 'a',
				'label': 'Model parameter `a`',
				'label_latex': 'Modellparameter $a$',
				'desc': 'Parameter <math xmlns="http://www.w3.org/1998/Math/MathML"><mi>a</mi></math> of the model <math xmlns="http://www.w3.org/1998/Math/MathML"><mi>a</mi><mo>&#xB7;</mo><msup><mi>e</mi><mrow><mi>b</mi><mo>&#xB7;</mo><mi>x</mi></mrow></msup><mo>+</mo><mi>c</mi></math>.',
				'desc_latex': 'Parameter $a$ des Modells $a \\cdot e^{b \\cdot x} + c$',
			},
			'lambda': {
				'label': 'Lambda',
				'label_latex': 'Lambda',
				'desc': 'How many rounds it takes to cut the score statistic by <math xmlns="http://www.w3.org/1998/Math/MathML"><mfrac bevelled="true"><mn>1</mn><mi>e</mi></mfrac></math> (a more physicists friendly version of the rate property).',
				'desc_latex': '$-b$, in Anlehnung an die phys. Zerfallskonstante',
			},
			'hl': {
				'label': 'Half-life',
				'label_latex': 'Halbwertszeit',
				'desc': 'How many rounds it takes to cut the score statistic in half (a more human friendly version of the rate property, but also more fragile).',
				'desc_latex': '\\enquote{Halbwertszeit} $\\nicefrac{ln(2)}{\\lambda}$',
			},
			'y0': {
				'label': 'y0',
				'label_latex': '$y_0$',
				'desc': 'The score statistic in the first population',
				'desc_latex': '$a + \\mathrm{offset}$',
			},
			'rmsd': {
				'label': 'Root-mean-square deviation',
				'label_latex': 'Wurzel der mittleren quadratischen Abweichung',
				'desc': 'A standard measure of fit quality.',
				'desc_latex': 'Einpassungsqualität als Wurzel der mittleren quadratischen Abweichung',
				'colorize': quality_color_fn,
			},
			
			## For skew model
			'skew_mean': {
				'name': 'mean',
				'label': 'Mean skewness',
				'label_latex': 'Mittlere Schiefe',
				'desc': 'Mean skewness of all rounds.',
				'desc_latex': 'Durchschnittliche empirische Schiefe der Bewertungen jeder Generation',
				'colorize': skew_color_fn,
				'scale': 100,
			},
			'skew_std': {
				'name': 'std',
				'label': 'Standard deviation of skewness',
				'label_latex': 'Standardabweichung der Schiefe',
				'desc': 'Standard deviation of skewness.',
				'desc_latex': 'Standardabweichung der empirischen Schiefe der Bewertungen jeder Generation',
				'colorize': quality_color_fn,
			},
			
			## For uniqueness model
			'uniqueness_mean': {
				'name': 'mean',
				'label': 'Average uniqueness',
				'label_latex': 'Mittlere Einzigartigkeit',
				'desc': 'Average uniqueness.',
				'desc_latex': 'Normiertes Integral der Anzahl einzigartiger Bewertungen pro Generation',
				'colorize': uniqueness_color_fn,
				'scale': 100,
			},
			
			## For best_score model
			'49': {
				'label': '50th max',
				'label_latex': '50. Generation',
				'desc': 'Best score of the 50th population',
				'desc_latex': 'Bewertung des besten Individuums der 50. Generation',
				'colorize': score_fn,
			},
			'99': {
				'label': '100th max',
				'label_latex': '100. Generation',
				'desc': 'Best score of the 100th population',
				'desc_latex': 'Bewertung des besten Individuums der 100. Generation',
				'colorize': score_fn,
			},
			'149': {
				'label': '150th max',
				'label_latex': '150. Generation',
				'desc': 'Best score of the 150th population',
				'desc_latex': 'Bewertung des besten Individuums der 150. Generation',
				'colorize': score_fn,
			},
		})
		
		for prop_name, prop in _props._props.items():
			prop.name = prop.get('name', prop_name)
			prop.colorize = prop.get('colorize', default_color_fn)
			prop.scale = prop.get('scale', 1)
	
	return collections.OrderedDict([
		(p.name, p) for p in map(_props._props.get, prop_names)
	])


def _rep_strats():
	rep_strats = collections.OrderedDict([
		('worst_parents_first', util.Dobject(
			label = 'Replace parents first, worst afterwards',
			short_label = 'parents, worst',
		)),
		('random_parents_first', util.Dobject(
			label = 'Replace parents first, random afterwards',
			short_label = 'parents, random',
		)),
		('cor_25', util.Dobject(
			label = 'Replace whole population (cross over rate 25%)',
			short_label = 'all (CoR 25%)',
		)),
		('none', util.Dobject(
			label = 'Replace whole population (cross over rate 100%)',
			short_label = 'all',
		)),
		('best_overall', util.Dobject(
			label = 'Keep best overall',
			short_label = 'keep best overall',
		)),
		('worst', util.Dobject(
			label = 'Replace worst',
			short_label = 'worst',
		)),
		('random', util.Dobject(
			label = 'Replace random',
			short_label = 'random',
		)),
	])
	
	for i, (name, rep_strat) in enumerate(rep_strats.items()):
		rep_strat.name = name
		rep_strat.color = plt.cm.tab10(i)
	
	return rep_strats


def _selectors():
	selectors = collections.OrderedDict([
		('tournament', util.Dobject(
			name = 'tournament',
			label = 'Random, tournament (5 rounds)',
			label_latex = 'Randomisiert, Turnier (5 Runden)',
			short_label = 'rnd. tourn.',
		)),
		('rnd_prop', util.Dobject(
			name = 'rnd_prop',
			label = 'Random, linear prob.',
			label_latex = 'Randomisiert, lineare Wahrscheinlichkeit',
			short_label = 'rnd. lin.',
		)),
		('rnd_uniform', util.Dobject(
			name = 'rnd_uniform',
			label = 'Random, uniform prob.',
			label_latex = 'Randomisiert, gleiche Wahrscheinlichkeit',
			short_label = 'rnd. uni.',
		)),
		('rnd_swipe', util.Dobject(
			name = 'rnd_swipe',
			label = 'Random, uniform prob. (unique)',
			label_latex = 'Randomisiert, gleiche Wahrscheinlichkeit und einzigartig',
			short_label = 'rnd. uni. (unique)',
		)),
		('best_inverse', util.Dobject(
			name = 'best_inverse',
			label = 'Contrary couples out of the bests',
			label_latex = 'Gegensätzliche Paare der Besten',
			short_label = 'contrary couples',
		)),
		('best_with_best', util.Dobject(
			name = 'best_with_best',
			label = 'Alike couples out of the bests',
			label_latex = 'Ähnliche Paare der Besten',
			short_label = 'alike couples',
		)),
	])
	
	for i, (name, selector) in enumerate(selectors.items()):
		selector.name = name
	
	return selectors


def _models():
	models = collections.OrderedDict([
		('fit_ab_all_med', util.Dobject(
			html_label = 'Fit <math xmlns="http://www.w3.org/1998/Math/MathML"><mi>a</mi><mo>&#xB7;</mo><msup><mi>e</mi><mrow><mi>b</mi><mo>&#xB7;</mo><mi>x</mi></mrow></msup></math>: Med. score of whole pop.',
			label_latex = 'Einpassung des Medians aller Bewertungen der Generationen auf $a \\cdot e^{b \\cdot x}$',
			props = _props(['rate', 'ab_a', 'ab_b', 'lambda', 'hl', 'rmsd']),
		)),
		('fit_ab_all_avg', util.Dobject(
			html_label = 'Fit <math xmlns="http://www.w3.org/1998/Math/MathML"><mi>a</mi><mo>&#xB7;</mo><msup><mi>e</mi><mrow><mi>b</mi><mo>&#xB7;</mo><mi>x</mi></mrow></msup></math>: Avg. score of whole pop.',
			label_latex = 'Einpassung des Mittels aller Bewertungen der Generationen auf $a \\cdot e^{b \\cdot x}$',
			props = _props(['rate', 'ab_a', 'ab_b', 'lambda', 'hl', 'rmsd']),
		)),
		('fit_ab_all_max', util.Dobject(
			html_label = 'Fit <math xmlns="http://www.w3.org/1998/Math/MathML"><mi>a</mi><mo>&#xB7;</mo><msup><mi>e</mi><mrow><mi>b</mi><mo>&#xB7;</mo><mi>x</mi></mrow></msup></math>: Best score of whole pop.',
			label_latex = 'Einpassung der Besten aller Bewertungen der Generationen auf $a \\cdot e^{b \\cdot x}$',
			props = _props(['rate', 'ab_a', 'ab_b', 'lambda', 'hl', 'rmsd']),
		)),
		('fit_ab_parents_med', util.Dobject(
			html_label = 'Fit <math xmlns="http://www.w3.org/1998/Math/MathML"><mi>a</mi><mo>&#xB7;</mo><msup><mi>e</mi><mrow><mi>b</mi><mo>&#xB7;</mo><mi>x</mi></mrow></msup></math>: Med. of sampled parent\'s scores',
			label_latex = 'Einpassung des Medians der Elternbewertungen der Generationen auf $a \\cdot e^{b \\cdot x}$',
			props = _props(['rate', 'ab_a', 'ab_b', 'lambda', 'hl', 'rmsd']),
		)),
		('fit_abc_parents_med', util.Dobject(
			html_label = 'Fit <math xmlns="http://www.w3.org/1998/Math/MathML"><mi>a</mi><mo>&#xB7;</mo><msup><mi>e</mi><mrow><mi>b</mi><mo>&#xB7;</mo><mi>x</mi></mrow></msup><mo>+</mo><mi>c</mi></math>: Med. of sampled parent\'s scores',
			label_latex = 'Einpassung des Medians der Elternbewertungen der Generationen auf $a \\cdot e^{b \\cdot x} + c$',
			props = _props(['rate', 'offset', 'abc_a', 'abc_b', 'lambda', 'hl', 'y0', 'rmsd']),
		)),
		('fit_ab_parants_avg', util.Dobject(
			html_label = 'Fit <math xmlns="http://www.w3.org/1998/Math/MathML"><mi>a</mi><mo>&#xB7;</mo><msup><mi>e</mi><mrow><mi>b</mi><mo>&#xB7;</mo><mi>x</mi></mrow></msup></math>: Avg. of sampled parent\'s scores',
			label_latex = 'Einpassung des Mittels der Elternbewertungen der Generationen auf $a \\cdot e^{b \\cdot x}$',
			props = _props(['rate', 'ab_a', 'ab_b', 'lambda', 'hl', 'rmsd']),
		)),
		('skew', util.Dobject(
			html_label = 'Score skew',
			label_latex = 'Schiefe',
			props = _props(['skew_mean', 'skew_std']),
		)),
		('uniqueness', util.Dobject(
			html_label = 'Uniqueness',
			label_latex = 'Einzigartigkeit',
			props = _props(['uniqueness_mean']),
		)),
		('best_score', util.Dobject(
			html_label = 'Best score',
			label_latex = 'Beste Bewertung definierter Generationen',
			props = _props(['49', '99', '149']),
		)),
	])
	
	for i, (name, model) in enumerate(models.items()):
		model.name = name
	
	return models


descriptions = util.Dobject(
	rep_strats = _rep_strats(),
	selectors = _selectors(),
	models = _models(),
)


script_dir   = util.script_dir()
project_dir  = op.join(script_dir, '..')
public_dir   = op.join(project_dir, 'public')
skeleton_dir = op.join(project_dir, 'public_skel')
databook_dir = op.join(project_dir, 'databook')
template_dir = op.join(script_dir, 'tpl')


def fig_path(file_name):
	dst_base_dir = public_dir if file_name.endswith('.png') else databook_dir
	return op.join(dst_base_dir, file_name)


def save_pipeline(runs, file_name):
	if isinstance(runs, dict):
		runs = next(iter(runs.values()))
	if isinstance(runs, list):
		runs = runs[0]
	gv = runs.stats['pipeline_gv'].replace(runs.asset_path(''), '')
	if util.outdated(fig_path(file_name), runs.stats_path):
		save_gv(gv, file_name)


def save_fig(fig, file_name, **kwargs):
	dst_file = fig_path(file_name)
	util.mkdirs(op.dirname(dst_file))
	if dst_file.endswith('.pdf'):
		with io.BytesIO() as f:
			fig.savefig(f, format='pdf', **kwargs)
			pdf = pdfrw.PdfReader(fdata=f.getvalue())
		fix_pdf(pdf)
		pdfrw.PdfWriter().write(dst_file, pdf)
	else:
		fig.savefig(dst_file, **kwargs)
	matplotlib.pyplot.close('all')
	
	return op.basename(file_name)[:-4]


def save_gv(gv, file_name, args=['dot']):
	dst_file = fig_path(file_name)
	util.mkdirs(op.dirname(dst_file))
	if dst_file.endswith('.pdf'):
		proc = subprocess.run(
			[*args, '-Tpdf'],
			check=True,
			input=gv.encode('utf-8'),
			stdout=subprocess.PIPE
		)
		pdf = pdfrw.PdfReader(fdata=proc.stdout)
		fix_pdf(pdf)
		pdfrw.PdfWriter().write(dst_file, pdf)
	else:
		subprocess.run(
			[*args, '-Tpng', f'-o{dst_file}'],
			check=True,
			input=gv,
			text=True,
		)


def fix_pdf(pdf, fix_id=True, generator='unknown'):
	## Set some static dummy values in the PDF
	date = "D:19700101000000+00'00'"
	pdf.Info.CreationDate = date
	pdf.Info.ModDate      = date
	pdf.Info.Creator      = generator
	pdf.Info.Producer     = generator
	
	## See http://stackoverflow.com/questions/20085899/what-is-the-id-field-in-a-pdf-file
	if fix_id:
		if (ID := pdf.get('ID')) is None:
			pdf.ID = ID = pdfrw.PdfArray([None, None])
		ID[0] = ID[1] = pdfrw.PdfString('<00000000000000000000000000000000>')
		
		## Write PDF into a memory buffer
		with io.BytesIO() as f:
			pdfrw.PdfWriter().write(f, pdf)
			f.seek(0)
			
			## Calculate hash of PDF
			h = hashlib.md5()
			for chunk in iter(lambda: f.read(128), b''):
				h.update(chunk)
		
		## Set new IDs
		ID[0] = ID[1] = pdfrw.PdfString(f'<{h.hexdigest()}>')
