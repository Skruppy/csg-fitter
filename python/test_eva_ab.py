#!/usr/bin/env python3
## This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0

import os
import plugin
import test_eva_ab_base
import fmtjson
import util
import shell_worker
import fitter_cache
import numpy
import sys
import web
import os.path as op
import matplotlib
import matplotlib.pyplot as plt
import shutil
import common
import subprocess



evas = [
	['evaluator_10', 'sample_density=1', 'N_rel=0.05'],
	['evaluator_10', 'sample_density=10', 'N_rel=0.05'],
	['evaluator_10', 'sample_density=100', 'N_rel=0.05'],
	['evaluator_10', 'sample_density=10', 'N_rel=0.5'],
	['evaluator_10', 'sample_density=10', 'N_rel=1'],
	
	['evaluator_11', 'sample_density=1', 'max_dist=0.1'],
	['evaluator_11', 'sample_density=1', 'max_dist=0.5'],
	['evaluator_11', 'sample_density=1', 'max_dist=1.0'],
	['evaluator_11', 'sample_density=1', 'max_dist=5.0'],
	['evaluator_11', 'sample_density=10', 'max_dist=0.1'],
	['evaluator_11', 'sample_density=10', 'max_dist=0.5'],
	['evaluator_11', 'sample_density=10', 'max_dist=1.0'],
	['evaluator_11', 'sample_density=10', 'max_dist=5.0'],
	
	['evaluator_12', 'max_dist=0.001', 'max_n_div_deg=0'],
	['evaluator_12', 'max_dist=0.001', 'max_n_div_deg=0.9'],
	['evaluator_12', 'max_dist=0.001', 'max_n_div_deg=9'],
	['evaluator_12', 'max_dist=0.001', 'max_n_div_deg=90'],
	['evaluator_12', 'max_dist=0.1', 'max_n_div_deg=0'],
]

test_types = ['ab', 'ba', 'aa', 'bb']


def main():
	base_path = op.join(util.script_dir(), '../tests/eva_ab')
	csgfitter = op.join(util.script_dir(), '../csgfitter-static')
	creators = {c.pluginName: c() for c in plugin.loadAll(test_eva_ab_base.ModelCreator)}
	new_files = set()
	
	## Create all basic files (json, scad, png) for all tests and both models.
	sh = shell_worker.ShellWorker()
	scad_jobs = []
	for test_name, creator in creators.items():
		test_path = op.join(base_path, test_name)
		util.mkdirs(test_path)
		for type_name in 'ab':
			json_path = op.join(test_path, f'{type_name}.json')
			json_str = fmtjson.format_csg(creator.build(type_name == 'b'))
			force_update = False
			
			## Write JSON (but only if needed, to prevent timestamp updates)
			if not op.isfile(json_path) or util.read_file(json_path) != json_str:
				util.write_file(json_str, json_path)
				new_files.add(json_path)
				force_update = True
			
			## Now: Convert JSON to SCAD (async)
			scad_path = f'{test_path}/{type_name}.scad'
			if force_update or util.outdated(scad_path, json_path):
				sh.run(csgfitter, 'convert', f'from={json_path}', f'to={scad_path}')
				force_update = True
			
			## Later: Convert SCAD to PNG
			png_path = f'{test_path}/{type_name}.png'
			if force_update or util.outdated(png_path, json_path):
				scad_jobs.append([
					'openscad',
					scad_path,
					'-o', png_path,
					'--viewall', '--autocenter', 
					'--imgsize=600,400',
					'--view=axes,scales',
					'--render',
				])
	
	## Wait til all json files have been converted to scad files (using
	## `csgfitter-static`) and then start all scad to png renderings (using
	## OpenSCAD).
	sh.wait_done()
	for job in scad_jobs:
		sh.run(*job)
	sh.stop()
	
	## Collect some evaluation samples based on the JSON files.
	producer = fitter_cache.JobFileProduction()
	#producer = fitter_cache.SlurmProduction()
	#producer = fitter_cache.LocalProduction()
	fc = fitter_cache.FitterCache(producer)
	
	results = {}
	for test_name, creator in creators.items():
		test_path = op.join(base_path, test_name)
		for eva_nr, eva in enumerate(evas):
			for model, reference in test_types:
				results[(test_name, eva_nr, model, reference)] = fc.request(
					[
						'json_reader', f'path=<IN:tests/eva_ab/{test_name}/{model}.json>', '!', 's.model',
						'json_reader', f'path=<IN:tests/eva_ab/{test_name}/{reference}.json>', '!', 'surface_sampler', 'rate=10', '!', 's.reference',
						*eva, '!', 's.evaluator',
						'score', 'name=s', 'path=<OUT:score.json>'
					],
					n = 50,
					rebuild = \
						f'{test_path}/{model}.json' in new_files or \
						f'{test_path}/{reference}.json' in new_files,
				)
	
	if fc.fetch():
		print('It\'s your turn!')
		sys.exit()
	
	## Create stats based on the samples
	tables = {}
	for test_name, creator in creators.items():
		for eva_nr, eva in enumerate(evas):
			for model, reference in test_types:
				key = (test_name, eva_nr, model, reference)
				scores = list(map(
					lambda x: util.read_json(x.asset_path('score.json'))['score'],
					results[key],
				))
				
				tables[key] = cell = util.Dobject()
				cell.mean = numpy.mean(scores)
				cell.std = numpy.std(scores)
				color_fn = (lambda x: plt.cm.RdYlGn(1-x)) if model == reference else plt.cm.PuOr
				cell.color = matplotlib.colors.to_hex(color_fn(cell.mean))
	
	## Prepare web report
	web_dir = op.join(util.script_dir(), '../public/ab')
	databook_dir = op.join(util.script_dir(), '../databook/ab')
	
	## Build index page
	subthings = []
	for name in creators.keys():
		subthings.append({
			'title': name,
			'url': f'ab/{ name }',
			'key': name,
		})
	
	web.render('generic_index.html', 'ab/index.html', {
		'title': 'A/B evaluations',
		'selected_subthing_key': '',
		'subthings': subthings,
	})
	web.render('ab_index.tex', 'ab/index.tex', {
		'subthings': subthings,
		'evas': evas,
	})
	
	common.save_pipeline(results, 'ab/pipeline.pdf')
	
	## Build detailed page and copy their assets
	for test_name in creators.keys():
		tpl_args = {
			'title': f'{ test_name } | A/B evaluations',
			'selected_subthing_key': test_name,
			'subthings': subthings,
			'test_name': test_name,
			'creator': creators[test_name],
			'evas': evas,
			'test_types': test_types,
			'tables': tables,
			'escapeLatex': util.escapeLatex,
			'geos': [
				{
					'name': 'A',
					'scad': util.read_file(f'tests/eva_ab/{ test_name }/a.scad'),
					'json': util.read_file(f'tests/eva_ab/{ test_name }/a.json'),
				},
				{
					'name': 'B',
					'scad': util.read_file(f'tests/eva_ab/{ test_name }/b.scad'),
					'json': util.read_file(f'tests/eva_ab/{ test_name }/b.json'),
				},
			],
		}
		
		for ext in ['html', 'tex']:
			web.render(f'ab_details.{ext}', f'ab/{ test_name }.{ext}', tpl_args)
		
		for geo in 'ab':
			for file_type in ['png', 'json', 'scad']:
				src = f'{base_path}/{test_name}/{geo}.{file_type}'
				dst = f'{web_dir}/{test_name}-{geo}.{file_type}'
				tex_dst = f'{databook_dir}/{test_name}-{geo}.{file_type}'
				if util.outdated(dst, src):
					shutil.copy2(src, dst)
				
				if file_type == 'png' and util.outdated(tex_dst, src):
					shutil.copy2(src, tex_dst)


if __name__ == '__main__':
	main()
