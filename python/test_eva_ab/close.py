## This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0

from test_eva_ab_base import ModelCreator
import math



class CloseModel(ModelCreator):
	def __init__(self):
		self.d = 0.4
		self.h = 0.4
		self.r = 2
	
	def build(self, b):
		csg = {
			"geo": "cylinder",
			"params": {
				"rotation": [0, 0, math.pi/2],
				"center": [(self.h+self.d)/(2 if b else -2), 0, 0],
				"radius": self.r,
				"height": self.h,
			}
		}
		
		return csg
	
	
	@property
	def description(self):
		return f'''
			<h5>Research question</h5>
			<p>
				How does an evaluator deals with close surfaces, but no volume overlap? (expected a/b-score: 1)
			</p>
			
			<h5>Methodology</h5>
			<p>
				There are
				<ul>
					<li>two surfaces facing each other: {self.d:.1f} units apart</li>
					<li>two surfaces facing in the same direction: {self.d + self.h:.1f} units apart</li>
					<li>two surfaces facing away from each other: {self.d + 2*self.h:.1f} units apart</li>
				</ul>
			</p>
			<p>
				By choosing thin round disks as the base geometries. The ratio of facing to other surfaces is kept small.
			</p>
		'''
