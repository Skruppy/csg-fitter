## This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0

from test_eva_ab_base import ModelCreator



class FlipModel(ModelCreator):
	def __init__(self):
		self.n = 20
		self.height = 1
		self.block_size = 1
		self.border_size = 0.2
		assert(self.n % 2 == 0)
	
	
	def build(self, b):
		blocks = []
		for x in range(self.n):
			for y in range(self.n):
				if (x+y) % 2 == b:
					continue
				
				blocks.append({
					"geo": "cube",
					"params": {
						"rotation": [0, 0, 0],
						"center": [self.block_size*x, self.block_size*y, self.height/2],
						"radius": [self.block_size/2, self.block_size/2, 1.1*self.height/2],
					}
				})
		
		csg = {
			"op": "subtract",
			"childs": [
				{
					"geo": "cube",
					"params": {
						"rotation": [0, 0, 0],
						"center": [(self.n-1)*self.block_size/2, (self.n-1)*self.block_size/2, self.height/2],
						"radius": [self.n*self.block_size/2+self.border_size, self.n*self.block_size/2+self.border_size, self.height/2],
					}
				},
				*blocks,
			]
		}
		
		return csg
	
	
	@property
	def description(self):
		## A: distance = 0 | cos(φ) =  1 | n = 4m + 8d + (m+2*d)^2 - m^2
		## B: distance > 0 |             | n = 2m
		## C: distance = 0 | cos(φ) = -1 | n = (m-1)^2
		## 
		##  +AAAAAAAAAAAAAAAAA+
		##  A-----------------A
		##  A--+BBB+---+BBB+--A
		##  A--B   C---C   C--A
		##  A--B   C---C   C--A
		##  A--+CCC+CCC+CCC+--A
		##  A------C   C------A
		##  A------C   C------A
		##  A--+CCC+CCC+CCC+--A
		##  A--B   C---C   C--A
		##  A--B   C---C   C--A
		##  A--+BBB+---+BBB+--A
		##  A-----------------A
		##  +AAAAAAAAAAAAAAAAA+
		## 
		## ???:
		## \lim_{m \rightarrow \infty}(B/A) \rightarrow 0
		## \lim_{m \rightarrow \infty}(B'/A') \rightarrow \infty
		
		inner_size = self.n*self.block_size
		outer_size = inner_size + 2*self.border_size
		
		inner_volume = inner_size**2 * self.height / 2
		total_volume = outer_size**2 * self.height - inner_volume
		border_volume = total_volume - inner_volume
		
		a_surface = 4 * (
			outer_size * self.height +
			2 * ((inner_size + self.border_size) * self.border_size)
		)
		b_surface = 4 * (inner_size/2) * self.height
		c_surface = (self.n-1)*2 * (inner_size * self.height)
		total_surface = a_surface + b_surface + c_surface
		
		return f'''
			<h5>Research question</h5>
			<p>
				How does surface orientation influence the evaluation?
			</p>
			
			<h5>Methodology</h5>
			<p>
				A checkboard pattern is compared to an inverse checkboard, creating mostly surfaces in the same spots, but with inverted orientation (see the following listing).
				<ul>
					<li>{border_volume / total_volume * 100:.1f}% Volume overlap</li>
					<li>Only {b_surface / total_surface * 100:.1f}% of the surfaces don't overlap.</li>
					<li>{c_surface / total_surface * 100:.1f}% of the surfaces overlap, but facing in opposite directions.</li>
					<li>{a_surface / total_surface * 100:.1f}% of the surfaces overlap and are facing in the same direction.</li>
				</ul>
			</p>
		'''
