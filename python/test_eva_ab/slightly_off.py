## This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0

import math
from test_eva_ab_base import ModelCreator



class SlightlyOffModel(ModelCreator):
	def build(self, b):
		## Volume of sphere cap:
		##   V_c = \frac{\pi h^2}{3}(3 r - h)
		## Volume of sphere:
		##   V_s = \frac{4}{3} \pi r^3
		## Volume of lense (intersection of two identical spheres)
		##   V_l = 2 V_c
		## Given the percentage p of how much speheres' A volume is occupied by,
		## the slightly off, sphere B.
		##   V_l = p V_s
		## Solving the distance d between the two spehres centers with q as h
		## relative to the speres radius r:
		##   d = 2r - 2h = 2r(1-q)
		## to get q, and hence d, solve 2 V_c = p V_s for q:
		##   3 q^2 - q^3 = 2p
		point_per_square = 10
		points = 1000
		r = math.sqrt(
			(points/point_per_square) /
			(4*math.pi)
		)
		
		if b:
			q = 0.99333 ## for p = 99%
			#q = 0.96665 ## for p = 95%
			#q = 0.93323 ## for p = 90%
			#q = 0.65270 ## for p = 50%
			
			x = 2*r*(1-q)
		else:
			x = 0
		
		
		csg = { "geo": "sphere",
			"params": { "rotation": [0, 0, 0], "center": [x, 0, 0], "radius": r }
		}
		
		return csg
	
	
	@property
	def description(self):
		return '''
			<h5>Research question</h5>
			<p>
				How does an evaluator deals with close surfaces, and huge volume overlap?
			</p>
			
			<h5>Methodology</h5>
			<p>
				Two volumes overlaping each other 99% volume-wise, but 0% of their surfaces overlap (even though, they are close to each other).
				This is acomplished by two spheres, slightly offset.
			</p>
		'''
