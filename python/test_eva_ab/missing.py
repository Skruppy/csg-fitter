## This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0

import math
from test_eva_ab_base import ModelCreator



class MissingModel(ModelCreator):
	def build(self, b):
		## Sphere surface:
		##   A = 4 \pi r^2
		point_per_square = 10
		points = 100
		r = math.sqrt(
			(points/point_per_square) /
			(4*math.pi)
		)
		
		xs = [-1.5*r]
		if not b:
			xs.append(1.5*r)
		
		csg = {
			"op": "union",
			"childs": [
				{ "geo": "sphere",
					"params": { "rotation": [0, 0, 0], "center": [x, 0, 0], "radius": r }
				} for x in xs
			]
		}
		
		return csg
	
	
	@property
	def description(self):
		return '''
			<h5>Research question</h5>
			<p>
				How does an evaluator deals with missing or extra objects?
			</p>
			
			<h5>Methodology</h5>
			<p><span class="sk-geo sk-geo-a">A</span> → <span class="sk-geo sk-geo-b">B</span>: Half the volume and surface goes missing (decreases by 50%).</p>
			<p><span class="sk-geo sk-geo-b">B</span> → <span class="sk-geo sk-geo-a">A</span>: Volume and surface doubles (increases by 100%).</p>
		'''
