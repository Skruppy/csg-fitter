#!/usr/bin/env python3
## This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0

import sys
import util



def main():
	## Load in raw stats
	total_ns = 0
	for i, path in enumerate(sys.argv[1:]):
		if i % 100 == 0:
			print(f'\r{i+0}/{len(sys.argv[1:])} read')
		data = util.read_json(path)
		for processor in data['processors'].values():
			total_ns += processor['time']
	
	print(int(total_ns/1e6), 'seconds')


if __name__ == '__main__':
	main()
