#!/usr/bin/env python3
## This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0

import subprocess
import sys
import time
import datetime
import math
import util
import dataclasses
import os.path as op



@dataclasses.dataclass
class Memtime:
	mem_free_delta: float
	time_delta: float
	exit_code: int
	
	def __str__(self):
		return \
		f'{format_mem(self.mem_free_delta)} of memory used in ' \
		f'{datetime.timedelta(seconds=self.time_delta)}\n' \
		f'({self.mem_free_delta:,} KiB / {self.time_delta:,.6f} s)'


def format_mem(byte):
	unit_id = min(int(math.log(abs(byte), 1024)-0.2), 2) if byte != 0 else 0
	return f'{byte / 1024**unit_id:.3f} {["K","M","G"][unit_id]}iB'


## Report free RAM in KiB
## @see https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/fs/proc/meminfo.c#n30
def free():
	with open('/proc/meminfo') as f:
		for line in f:
			if line.startswith('MemAvailable:'):
				return int(line[13:-3])


def run(args, interval=0.5):
	mem_free_start = mem_free_min = free()
	time_start = time.monotonic()
	
	proc = subprocess.Popen(args)
	
	while True:
		try:
			proc.wait(interval)
			break
		except subprocess.TimeoutExpired as ex:
			pass
		mem_free_min = min(mem_free_min, free())
	
	time_end = time.monotonic()
	
	return Memtime(
		mem_free_delta = mem_free_start - mem_free_min,
		time_delta = time_end - time_start,
		exit_code = proc.returncode,
	)
	

def main():
	if len(sys.argv) <= 1:
		print(free()*1024)
		sys.exit(0)
	
	if sys.argv[1] == '-h' or sys.argv[1] == '--help':
		print(
			f'Usage: {op.basename(sys.argv[0])} [-h|--help] [-i SECS] [ARGS...]\n' \
			'Executes ARGS and measures used overall system memory in SECS intervals.\n' \
			'Prints maximal memory usage since start and elapsed time at the end.\n' \
			'Without any arguments, prints the free system memmory in byes.',
			file=sys.stderr
		)
		sys.exit(0)
	
	if sys.argv[1] == '-i':
		res = run(sys.argv[3:], float(sys.argv[2]))
	else:
		res = run(sys.argv[1:])
	
	print(res, file=sys.stderr)
	sys.exit(res.exit_code)


if __name__== '__main__':
	main()
