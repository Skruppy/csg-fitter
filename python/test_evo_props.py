#!/usr/bin/env python3
## This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0

import common
import shell_worker
import util
import sys
import fitter_cache
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import collections
import os
import os.path as op
import math
import web
import subprocess


model_name = 'fit_ab_all_med'
samples_per_config = 10

def csgfitter_evo_cmd(evo_args=None, eval_args=None, sample_rate=5, input='tests/renderings/gecco19.json'):
	if evo_args is None:
		evo_args = []
	if eval_args is None:
		eval_args = ['evaluator_12']
	
	return [
		### @todo replace with "<IN: ...>"
		'json_reader', f'path=<IN:{input}>', 'name=csg',
		'csg.', '!', 'surface_sampler', f'rate={sample_rate}', 'name=points', '!', 'fitter.reference',
		'csg.', '!', 'extract_primitives', '!', 'fitter.primitives',
		*eval_args, '!', 'fitter.evaluator',
		'evo', *evo_args, 'max_rounds=150', 'name=fitter',
		'fitter.', '!', 'json_writer', 'path=<OUT:result.json>',
		'fitter.', '!', 'scad_writer', 'path=<OUT:result.scad>',
		'fitter.', '!', 'gv_writer', 'path=<OUT:result.pdf>',
		'fitter.', '!', 'gv_writer', 'path=<OUT:result.gv>',
		'points.', '!', '.npc', 'pcd_writer', 'path=<OUT:points.pcd>',
	]


## Output type: Dict[ Tuple[ep_strat_name, size], List[run] ]
def run_tests(rep_strat_names, sizes, n):
	producer = fitter_cache.JobFileProduction()
	#producer = fitter_cache.SlurmProduction()
	#producer = fitter_cache.LocalProduction()
	fc = fitter_cache.FitterCache(producer)
	
	## Get all combinations of replacement strategies and population sizes
	test_run_combinations = {}
	for rep_strat_name in rep_strat_names:
		for size in sizes:
			if rep_strat_name == 'none':
				test_run_combinations[(rep_strat_name, size)] = fc.request(csgfitter_evo_cmd([
					f'population_size={size}',
					f'mating_rate=1',
				]), n)
			elif rep_strat_name == 'cor_25':
				test_run_combinations[(rep_strat_name, size)] = fc.request(csgfitter_evo_cmd([
					f'population_size={size}',
					f'mating_rate=1',
					f'crossover_rate=0.25',
				]), n)
			else:
				test_run_combinations[(rep_strat_name, size)] = fc.request(csgfitter_evo_cmd([
					f'population_size={size}',
					f'replacement_strategy={rep_strat_name}',
				]), n)
	
	## Either terminate program, if manual user intervention is required, or return
	if fc.fetch():
		print('It\'s your turn!')
		sys.exit()
	
	return test_run_combinations


## Input type: Dict[ Tuple[rep_strat_name, size], List[run] ]
## Output type: Dict[ rep_strat_name, Dict[ size, List[stats] ] ]
def load_stats(test_run_combinations):
	## Evaluate stats of each run, if not already done
	sh = shell_worker.ShellWorker()
	for runs in test_run_combinations.values():
		for run in runs:
			evo_base_path = op.join(run.base_path, 'evo_stats')
			extracted_stats_path = op.join(evo_base_path, 'stats.json')
			if not op.isfile(extracted_stats_path):
			#if True:
				util.mkdirs(evo_base_path)
				sh.run('test_evo', '-l', run.stats_path, '--path', evo_base_path, '/dev/null', '0')
	
	sh.stop()
	
	## Load stats form json file
	stats = {}
	for (rep_strat_name, size), runs in test_run_combinations.items():
		stats[(rep_strat_name, size)] = list(map(
			lambda run: util.read_json(op.join(run.base_path, 'evo_stats', 'stats.json')),
			runs
		))
	
	return stats


def plot(ax, prop_stats, y_min, y_max, y_label):
	ax.set_ylabel(y_label)
	ax.set_xlabel('Population size\nbigger ▶')
	ax.grid(True)
	
	all_values = []
	for rep_strat_stats in prop_stats['strats'].values():
		ax.scatter(
			rep_strat_stats['scatter_x'], rep_strat_stats['scatter_y'],
			alpha=0.5,
			edgecolors = 'none',
			c = rep_strat_stats['rep_strat']['color'],
			s = 15
		)
		ax.plot(
			rep_strat_stats['avg_x'], rep_strat_stats['avg_y'],
			c = rep_strat_stats['rep_strat']['color'],
		)
		all_values.extend(rep_strat_stats['scatter_y'])
	
	set_percentile_limits(ax, all_values, 8)

def plot_std(ax, prop_stats, y_label):
	ax.set_ylabel(y_label)
	ax.set_xlabel('Population size\nbigger ▶')
	ax.grid(True)
	
	all_values = []
	for rep_strat_stats in prop_stats['strats'].values():
		ax.plot(
			rep_strat_stats['std_x'], rep_strat_stats['std_y'],
			c = rep_strat_stats['rep_strat']['color'],
		)
		all_values.extend(rep_strat_stats['std_y'])
	
	set_percentile_limits(ax, all_values, 4)


def main():
	sizes = sorted(map(lambda x: int(x/2)*2,
		set(range(10, 50, 10)) |
		set(range(50, 200, 25)) |
		set(range(200, 1001, 50))
	))
	
	prop_names = ['a', 'b', 'lambda', 'rate', 'hl', 'rmsd']
	
	test_run_combinations = run_tests(common.descriptions.rep_strats.keys(), sizes, samples_per_config)
	run_stats_lists = load_stats(test_run_combinations)
	
	## Type: Dict[ prop_name, Dict[ rep_strat_name, Dict[ ... ] ] ]
	prop_stats_by_name = {}
	for prop_name in prop_names:
		prop_stats_by_name[prop_name] = {
			'strats': {},
			'limits_y': None,
		}
		
		for rep_strat_name, rep_strat in common.descriptions.rep_strats.items():
			prop_stats_by_name[prop_name]['strats'][rep_strat_name] = {
				'rep_strat': rep_strat,
				'scatter_x': [],
				'scatter_y': [],
				'avg_x': [],
				'avg_y': [],
				'std_x': [],
				'std_y': [],
			}
	
	rmsds = []
	for prop_name, prop_stats in prop_stats_by_name.items(): ## Output
		for (rep_strat, size), run_stats_list in run_stats_lists.items(): ## Input
			np.random.seed(42)
			
			## Collect regression property values of all runs
			values = []
			for run_stats in run_stats_list:
				## Skip values where fitter did not converge
				if 'rmsd' not in run_stats[model_name]:
					continue
				
				## Skip values where the root-mean-square deviation (as a
				## measure of fit quality) is too bad.
				rmsd = run_stats[model_name]['rmsd']
				if prop_name == 'rate':
					rmsds.append(rmsd)
				if rmsd > 0.1:
					continue
				
				## Collect values of all runs
				value = run_stats[model_name][prop_name]
				if prop_name == 'rate':
					value *= 100
				values.append(value)
			
			## Prepare and save values for scatter plot
			if size <= 50:
				scatter = 10
			elif size <= 200:
				scatter = 25
			else:
				scatter = 50
			
			prop_stats['strats'][rep_strat]['scatter_x'].extend(size + (scatter/2)*(np.random.rand(len(values))-0.5))
			prop_stats['strats'][rep_strat]['scatter_y'].extend(values)
			
			## Prepare and save average of values for line plot
			if len(values) > 5:
				prop_stats['strats'][rep_strat]['avg_x'].append(size)
				prop_stats['strats'][rep_strat]['avg_y'].append(np.mean(values))
			if len(values) > 5:
				prop_stats['strats'][rep_strat]['std_x'].append(size)
				prop_stats['strats'][rep_strat]['std_y'].append(np.std(values))
	
	rmsds.sort()
	
	
	base_path = os.path.join(util.script_dir(), '../public/size')
	util.mkdirs(base_path)
	

	
	#plot(fig.add_subplot(2,2,1), prop_stats_by_name['rate'], -1, 10, 'Decay rate [% / round]')
	#plot_std(fig.add_subplot(2,2,3), prop_stats_by_name['rate'], 'Decay rate [% / round]')
	#plot(fig.add_subplot(2,2,2), prop_stats_by_name['rmsd'], 0, 0.1, 'RMSD [score points]')
	##plot(fig.add_subplot(2,2,3), prop_stats_by_name['hl'], -400, 500, 'Half-Life time [rounds]')
	#plot(fig.add_subplot(2,2,4), prop_stats_by_name['lambda'], -0.01, 0.125, 'Lambda')
	
	for prop_name in prop_names:
		for ext in ['png', 'pdf']:
			fig = plt.figure(figsize=(14, 6))
			
			fig.legend(
				handles = [matplotlib.lines.Line2D([0], [0], color=rs['color'], lw=4) for rs in common.descriptions.rep_strats.values()],
				labels = [rs['label'] for rs in common.descriptions.rep_strats.values()],
				loc='upper center', ncol=4
			)
			
			plot(fig.add_subplot(), prop_stats_by_name[prop_name], -1, 10, common.descriptions.models[model_name].props[prop_name].label)
			common.save_fig(fig, f'size/{prop_name}-avg.{ext}', bbox_inches='tight', pad_inches=0.03)
			
			fig = plt.figure(figsize=(14, 6))
			
			fig.legend(
				handles = [matplotlib.lines.Line2D([0], [0], color=rs['color'], lw=4) for rs in common.descriptions.rep_strats.values()],
				labels = [rs['label'] for rs in common.descriptions.rep_strats.values()],
				loc='upper center', ncol=4
			)
			
			plot_std(fig.add_subplot(), prop_stats_by_name[prop_name], common.descriptions.models[model_name].props[prop_name].label)
			common.save_fig(fig, f'size/{prop_name}-std.{ext}', bbox_inches='tight', pad_inches=0.03)
	
	for ext in ['html', 'tex']:
		web.render(f'sizes.{ext}', f'size/index.{ext}', {
			'title': 'Population size',
			'selected_subthing_key': '',
			'subthings': [],
			'descriptions': common.descriptions,
			'sizes': sorted(list(sizes)),
			'prop_names': prop_names,
			'model_name': model_name,
			'samples_per_config': samples_per_config,
		})
	
	common.save_pipeline(test_run_combinations, 'size/pipeline.pdf')


def set_percentile_limits(ax, values, q=5, p=0.2):
	upper_quartile = np.percentile(values, 100-q)
	lower_quartile = np.percentile(values, q)
	iqr = (upper_quartile - lower_quartile) * p
	util.set_limits(ax, y=(lower_quartile - iqr, upper_quartile + iqr))


if __name__ == '__main__':
	main()
