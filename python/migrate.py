#!/usr/bin/env python3

from typing import List
import dataclasses
import hashlib
import json
import os
import os.path as op
import pathlib
import shell_worker
import shlex
import subprocess
import sys
import tempfile
import util
import shutil


for entry in os.scandir('results'):
	key_path = f'{entry.path}/key.json'
	if not op.isfile(key_path):
		continue
	
	key_data = util.read_json(key_path)
	
	if list(key_data['inputs'].keys())[0].startswith('tests/eva_ab/'): continue
	
	for proc_name, proc in key_data['processors'].items():
		if 'max_n_div_deg' not in proc['properties']: continue
		if proc['properties']['max_n_div_deg'] != '0': continue
		proc['properties']['max_n_div_deg'] = '60'
		break
	else:
		continue
	
	
	key_content = json.dumps(key_data, indent='\t', sort_keys=True) + '\n'
	key_hash = util.sha1sum(key_content)
	util.write_file(key_content, f'{key_path}')
	
	new_path = f'results/{key_hash}'
	if op.exists(new_path):
		shutil.rmtree(new_path)
	
	os.rename(entry.path, new_path)
