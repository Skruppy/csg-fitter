#!/usr/bin/env python3
## This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0

import web
import csv
import os
import subprocess
import shutil
from diagram import Diagram
import util
import glob
import distutils.dir_util
import sys
import shell_worker
import yaml



def read_csv(path):
	points = []
	data = {
		'min': [float('Inf'),  float('Inf'),  float('Inf')],
		'max': [float('-Inf'), float('-Inf'), float('-Inf')],
		'size': [0]*3,
		'name': os.path.basename(path)[:-4],
	}
	
	with open(path) as csvfile:
		for point in csv.reader(csvfile, delimiter=' '):
			point = list(map(float, point))
			
			for i in range(3):
				if data['min'][i] > point[i]: data['min'][i] = point[i]
				if data['max'][i] < point[i]: data['max'][i] = point[i]
			
			points.append(point)
	
	data['n_points'] = len(points)
	
	if data['n_points'] == 0:
		for i in range(3):
			data['min'][i] = 0.0
			data['max'][i] = 0.0
	
	for i in range(3):
		data['size'][i] = data['max'][i] - data['min'][i]
	
	return (points, data)


def plot(dst_dir, points, data, diagram_groups):
	for diagrams in diagram_groups:
		## Find a common zoom level, which fits the object in all diagrams in current group
		scale = float('Inf')
		for diagram in diagrams:
			scale = min(diagram.calc_scale(data), scale)
		
		## Plot same point cloud on all diagrams in current group
		for diagram in diagrams:
			diagram.scale = scale
			diagram.draw(dst_dir, points, data)


def render(sh, base_dst_path, name, sizes, *exta_args):
	src = base_dst_path+'.scad'
	for size in sizes:
		dst = f'{base_dst_path}-scad-{size[1]}-{name}.png'
		if util.outdated(dst, src):
			sh.run(
				'openscad', src,
				'-o', dst,
				'--viewall', '--autocenter',
				'--imgsize=%s,%s'%size[0],
				'--view=axes,scales',
				'--render',
				*exta_args,
				stdout=subprocess.DEVNULL
			)


def main():
	## == Configuration
	base_dir = os.path.normpath(os.path.join(util.script_dir(), '..'))
	src_dir = os.path.join(base_dir, 'tests', 'renderings')
	public_dir = os.path.join(base_dir, 'public')
	dst_dir = os.path.join(public_dir, 'renderings')
	csgfitter = os.path.join(base_dir, 'csgfitter-static')
	
	sizes = [
		((400, 400), 'small'),
		((1600, 1600), 'large'),
	]
	
	diagram_groups = [[
			Diagram(size, name+'-rhs-front',  0, 1, 2,   1, -1, -1, 3),
			Diagram(size, name+'-rhs-right',  2, 1, 0,  -1, -1, -1, 4),
			Diagram(size, name+'-rhs-top',    0, 2, 1,   1,  1, -1, 5),
			Diagram(size, name+'-rhs-left',   2, 1, 0,   1, -1,  1, 0),
			Diagram(size, name+'-rhs-back',   0, 1, 2,  -1, -1,  1, 1),
			Diagram(size, name+'-rhs-bottom', 0, 2, 1,   1, -1,  1, 2),
		] for size, name in sizes]
	
	## Set cwd to project directory
	os.chdir(base_dir)
	
	## Get csgfitter version
	version = subprocess.check_output([csgfitter, '--version']).decode('utf-8').strip()
	
	## Assure output directory exists
	util.mkdirs(public_dir)
	util.mkdirs(dst_dir)
	
	## Find input files
	base_src_pathes = list(map(lambda x: x[:-5], glob.glob(os.path.join(src_dir, '*.json'))))
	subthings = []
	for base_src_path in base_src_pathes:
		name = os.path.basename(base_src_path)
		subthings.append({
			'title': name,
			'url': f'renderings/{ name }',
			'key': name,
		})
	
	sh = shell_worker.ShellWorker()
	for base_src_path in base_src_pathes:
		name = os.path.basename(base_src_path)
		base_dst_path = os.path.join(dst_dir, name)
		src_path = f'{base_src_path}.json'
		
		if len(sys.argv) > 1 and name not in sys.argv: continue
		print(name)
		
		config = {
			'surface_density': 500,
			'volume_density': 500,
		}
		config_path = base_src_path+'.yaml'
		with open(config_path) as f:
			config.update(yaml.safe_load(f))
		
		## Raw JSON
		shutil.copyfile(src_path, base_dst_path+'.json')
		
		## Draw CSG tree
		dst = f'{base_dst_path}-tree.png'
		if util.outdated(dst, src_path):
			sh.run(
				csgfitter,
				'json_reader', f'path={src_path}', '!',
				'gv_writer', 'format=png', f'path={dst}', 'geo_details=true', 'ltr=true',
				stdout=subprocess.DEVNULL
			)
		
		## Clique/Partion graph
		#dst_part = f'{base_dst_path}-part.png'
		#dst_clique = f'{base_dst_path}-clique.png'
		graph_json_path = f'{base_dst_path}-graph-stats.json'
		if util.outdated(graph_json_path, src_path):
			sh.run(
				csgfitter, '-s',  graph_json_path,
				'json_reader', f'path={src_path}', 'name=csg',
				'csg.', '!', 'extract_primitives', 'name=primitives',
				'csg.', '!', 'surface_sampler', f'rate={config["surface_density"]}', 'name=sampler',
				'reduce', 'name=reducer',
				'sampler.', '!', 'reducer.complete_npc',
				'primitives.', '!', 'reducer.primitives',
				'evaluator_12', 'name=evaluator',
				'primitives.', '!', 'clique_fitter.primitives',
				'reducer.', '!', 'clique_fitter.reference',
				'evaluator.', '!', 'clique_fitter.evaluator',
				'primitives.', '!', 'part_fitter.primitives',
				'reducer.', '!', 'part_fitter.reference',
				'evaluator.', '!', 'part_fitter.evaluator',
				'clique_evo', 'name=clique_fitter', 'population_size=2', 'mating_rate=0', 'crossover_rate=1', 'replacement_strategy=worst', 'selector=tournament', 'max_rounds=1',
				'part_evo', 'name=part_fitter', 'population_size=2', 'mating_rate=0', 'crossover_rate=1', 'replacement_strategy=worst', 'selector=tournament', 'max_rounds=1',
				stdout=subprocess.DEVNULL
			)
		
		## Create point clouds
		dst = f'{base_dst_path}-surface.csv'
		if util.outdated(dst, [src_path, config_path]):
			sh.run(
				csgfitter,
				'json_reader', f'path={src_path}', '!',
				'surface_sampler', f'rate={str(config["surface_density"])}', '!', '.npc',
				'xyz_writer', f'path={dst}',
				stdout=subprocess.DEVNULL
			)
		
		dst = f'{base_dst_path}-volume.csv'
		if util.outdated(dst, [src_path, config_path]):
			sh.run(
				csgfitter,
				'json_reader', f'path={src_path}', '!',
				'volume_sampler', 'sample_rate='+str(config['volume_density']),
				'min_x='+str(config['min_x']), 'min_y='+str(config['min_y']), 'min_z='+str(config['min_z']),
				'max_x='+str(config['max_x']), 'max_y='+str(config['max_y']), 'max_z='+str(config['max_z']), '!', '.pc',
				'xyz_writer', f'path={dst}',
				stdout=subprocess.DEVNULL
			)
		
		## OpenSCAD
		dst = f'{base_dst_path}.scad'
		if util.outdated(dst, src_path):
			sh.run(
				csgfitter,
				'json_reader', f'path={src_path}', '!', 'scad_writer', f'path={dst}',
				stdout=subprocess.DEVNULL
			)
		
		sh.wait_done()
		render(sh, base_dst_path, '3d', sizes, '--camera=0,0,0,340,20,0,1')
		for side, cam in {'front': '0,0,0', 'right': '0,90,0', 'top': '270,0,0', 'left': '0,270,0', 'back': '0,180,0', 'bottom': '90,0,0'}.items():
			render(sh, base_dst_path, side, sizes, '--projection=ortho', '--camera=0,0,0,'+cam+',1')
		
		graph_json = util.dobjectify(util.read_json(graph_json_path))
		
		dst_gv = f'{base_dst_path}-graph-part.gv'
		if util.outdated(dst_gv, graph_json_path):
			with open(dst_gv, 'w') as f:
				f.write(graph_json.processors.part_fitter.stats.graph_gv)
		
		dst_png = f'{base_dst_path}-graph-part.png'
		if util.outdated(dst_png, graph_json_path):
			subprocess.run(['fdp', '-Tpng', f'-o{dst_png}', dst_gv], check=True)
		
		dst_gv = f'{base_dst_path}-graph-clique.gv'
		if util.outdated(dst_gv, graph_json_path):
			with open(dst_gv, 'w') as f:
				f.write(graph_json.processors.clique_fitter.stats.clique_gv)
		
		dst_png = f'{base_dst_path}-graph-clique.png'
		if util.outdated(dst_png, graph_json_path):
			subprocess.run(['fdp', '-Tpng', f'-o{dst_png}', dst_gv], check=True)
		
		dst_gv = f'{base_dst_path}-graph-clique-merge.gv'
		if util.outdated(dst_gv, graph_json_path):
			with open(dst_gv, 'w') as f:
				f.write(graph_json.processors.clique_fitter.stats.merge_gv)
		
		dst_png = f'{base_dst_path}-graph-clique-merge.png'
		if util.outdated(dst_png, graph_json_path):
			subprocess.run(['dot', '-Tpng', f'-o{dst_png}', dst_gv], check=True)
		
		## Plot point clouds
		meta_data = {}
		for type_ in ['surface', 'volume']:
			points, data = read_csv(base_dst_path+'-'+type_+'.csv')
			plot(dst_dir, points, data, diagram_groups)
			meta_data[type_] = data
			
		
		## HTML report
		#web.rendering(name, meta_data, names, version)
		web.render('rendering.html', f'renderings/{ name }.html', {
			'title': name+' | Test renderings',
			'selected_subthing_key': name,
			'subthings': subthings,
			'name': name,
			'meta_data': meta_data,
			'scad': util.read_file(f'public/renderings/{ name }.scad'),
			'json': util.read_file(f'public/renderings/{ name }.json'),
			'version': version,
		})
	
	web.render('generic_index.html', f'renderings/index.html', {
		'title': 'Test renderings',
		'selected_subthing_key': '',
		'subthings': subthings,
	})
	sh.stop()


if __name__== '__main__':
	main()
