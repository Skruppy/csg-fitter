#!/usr/bin/env python3
## This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0

import json
import sys
import subprocess
import util



def walk_values(data, callback, parent=None, path=None):
	if path is None:
		path = list()
	
	if isinstance(data, dict):
		for k, v in data.items():
			walk_values(v, callback, data, path+[k])
	
	elif isinstance(data, list):
		for i, v in enumerate(data):
			walk_values(v, callback, data, path+[str(i)])
	
	else:
		callback(data, path, parent)


def handle_graphs(data, path, parent):
	if path[-1].endswith('_gv'):
		gv_path = '-'.join(path) + '.gv'
		png_path = '-'.join(path) + '.png'
		
		with open(gv_path, 'w') as f:
			f.write(data)
		
		with open(png_path, 'w') as f:
			subprocess.check_call(['dot', '-Tpng', gv_path], stdout=f)


def handle_durations(data, path, context):
	if path[-1].endswith('_duration'):
		path[-1] = path[-1][:-len('_duration')]
	
	elif path[-1] == 'duration':
		path = path[:-1]
	
	else:
		return
	
	key = '-'.join(path)
	if key in context:
		raise Exception('')
	
	context[key] = {
		'time': data,
		'path': path,
	}


def group_records(path, records):
	group = {'children': [], 'name': path[-1]}
	
	buckets = {}
	for v in records:
		## If the time record is exactly matching the current path, save the
		## record's time as the current group's one
		if v['path'] == path[1:]:
			group['time'] = v['time']
		
		## If the record path is longer than the current path, put the record in
		## a bucket based on the element following the current path for further
		## processing
		else:
			key = v['path'][len(path)-1]
			if key not in buckets:
				buckets[key] = list()
			
			buckets[key].append(v)
	
	for k, v in buckets.items():
		group['children'].append(group_records(path+[k], v))
	
	## Condense groups containing only one child group, if not a timed group
	if len(group['children']) == 1 and 'time' not in group:
		group['children'][0]['name'] = path[-1]+'-'+group['children'][0]['name']
		group = group['children'][0]
	
	return group


def main():
	## Load in raw stats
	data = util.read_json(sys.argv[1])
	
	## Extract graphs
	walk_values(data, handle_graphs)
	
	## Build tree map
	records = {}
	walk_values(data, lambda a, b, c: handle_durations(a, b, records))
	root = group_records(['csgfitter'], records.values())
	util.write_json(root, 'timings.json')


if __name__ == '__main__':
	main()
