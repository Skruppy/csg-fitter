#!/usr/bin/env python3

import os
import os.path as op
import util
import shutil
import json
import subprocess
import fitter_cache
import dataclasses
import re
import sys
from typing import Dict



class NotARun: pass
class InvalidRun: pass

@dataclasses.dataclass
class ValidRun:
	key: Dict[str, util.Dobject]


class KeyCache:
	def __init__(self):
		self._keys = {}
		self._base_path = os.path.dirname(util.script_dir())
	
	
	def key_from_args(self, args):
		## Create simple, short and uniqe hash of args
		args_hash = hash(json.dumps(args))
		
		## Try to return existing key
		key = self._keys.get(args_hash, None)
		if key is not None:
			return key
		
		## Create new key
		self._keys[args_hash] = key = util.Dobject(
			args=args, args_hash=args_hash, content=None, hash=None
		)
		
		## Convert args to processor configurations
		cache_stats_path = '/tmp/csgfitter_fixer.json'
		csgfitter_args = ['./csgfitter', '-p', '-s', cache_stats_path, *args]
		ret = subprocess.run(csgfitter_args, cwd=self._base_path)
		if ret.returncode != 0:
			print(f'Failed with {ret.returncode}: {csgfitter_args}')
			return key
		
		stats = util.read_json(cache_stats_path)
		os.remove(cache_stats_path)
		
		processors = stats['processors']
		
		key_data = {'processors': {}, 'inputs': {}}
		
		## Add processor configurations to cache key
		for processor in processors.values():
			key_data['processors'][processor['name']] = {k: processor[k] for k in ['inputs', 'name', 'outputs', 'properties', 'type']}
		
		## Add input files to cache key
		inputs, _ = fitter_cache._find_outputs(args)
		
		missing_inputs = list(filter(lambda x: not op.isfile(x), inputs))
		if missing_inputs:
			print(f'Missing input files: {csgfitter_args}')
			for missing_input in missing_inputs:
				print(f'  {missing_input}')
			return key
		
		for input in inputs:
			with open(input, 'rb') as f:
				key_data['inputs'][input] = util.sha1sum(f)
		
		key.content = json.dumps(key_data, indent='\t', sort_keys=True) + '\n'
		key.hash = util.sha1sum(key.content)
		
		return key
	
	
	def key_from_run(self, run_dir, cur_cache_key):
		## Skip non runs (files)
		if run_dir.is_file():
			if run_dir.name in ['key.json']:
				return NotARun()
			
			## Warn about unknown files
			print(f'\'{run_dir.path}\' is not a valid run')
			return InvalidRun()
		
		## Read run arguments from stats file
		## Warn/Skip missing stats file
		stats_path = op.join(run_dir.path, 'stats.json')
		if not op.isfile(stats_path):
			print(f'Missing stats file \'{stats_path}\'')
			
			## Warn about unfinised runs (having some, but no stats file)
			for root, dirs, files in os.walk(run_dir.path):
				if len(files) > 0:
					print(f'=> Non empty run \'{run_dir.path}\' is missing stats files')
					return InvalidRun()
			
			## DELETE run diretory skeletons without any files
			else:
				print(f'=> Empty run \'{run_dir.path}\' can be deleted')
				shutil.rmtree(run_dir.path)
				return NotARun()
		
		raw_args = util.read_json(stats_path)['args']
		
		## Prepare arguments
		args = raw_args.copy()
		for i, arg in enumerate(raw_args):
			## Fix output pathes
			if cur_cache_key in arg or 'results/' in arg:
				output_prefix = f'path={run_dir.path}/output/'
				#print(run_dir.name)
				if re.match(f'^path=results/[0-9a-f]{{40}}/{run_dir.name}/output/.', arg):
					arg = f'path=<OUT:{arg[len(output_prefix):]}>'
				else:
					print(f'{run_dir.path}: \'{arg}\' argument looks linke an unknown output path (expected it to start with \'{output_prefix}\')')
					return InvalidRun()
			
			## Fix input pathes
			elif 'tests/' in arg:
				input_prefix='path='
				if arg.startswith(input_prefix):
					path = os.path.relpath(arg[len(input_prefix):])
					arg = f'path=<IN:{path}>'
				else:
					print(f'{run_dir.path}: \'{arg}\' argument looks linke an unknown input path')
					return InvalidRun()
			
			elif '/' in arg:
				print(f'{run_dir.path}: \'{arg}\' is this maybe a path?')
				return InvalidRun()
			
			args[i] = arg
		
		## Collect arguments (map runs to unique arguments)
		return ValidRun(self.key_from_args(args))
	
	
	def key_from_cache_entry(self, entry):
		## Warn/Skip non cache entries (MD5 based directories)
		if len(entry.name) != 40:
			print(f'\'{entry.path}\' is not a valid cache entry')
			return NotARun()
		
		if not entry.is_dir():
			print(f'\'{entry.path}\' is not a directory (but could clash)')
			return InvalidRun()
		
		## Collect arguments of each run
		run_keys = {}
		found_anything = False
		found_bugs = False
		for run_dir in os.scandir(entry.path):
			result = self.key_from_run(run_dir, entry.name)
			
			## Skip non run entries
			if isinstance(result, NotARun):
				continue
			
			## Note that this cache entry has at least something to work with/fix
			found_anything = True
			
			## Note/Skip buggy runs
			if isinstance(result, InvalidRun):
				found_bugs = True
				continue
			
			## Save valid runs
			key = result.key
			if key.hash not in run_keys:
				run_keys[key.hash] = util.Dobject(key=key, runs=[])
			
			run_keys[key.hash].runs.append(run_dir.path)
		
		## Warn/Skip entries containing at least one bug
		if found_bugs:
			print(f'Skipping \'{entry.path}\' because of bugs')
			return InvalidRun()
			
		## DELETE cache entries without any files
		if not found_anything:
			assert(len(run_keys) == 0)
			print(f'Deleting \'{entry.path}\' without any files')
			shutil.rmtree(entry.path)
			return NotARun()
		
		assert(found_bugs == False)
		assert(found_anything == True)
		assert(len(run_keys) != 0)
		
		## Warn/Skip cache entr with runs with different cache keys
		if len(run_keys) > 1:
			print(f'\'{entry.path}\' runs with different cache keys:')
			for foo in run_keys.values():
				print(f'  {foo.key.hash} from {foo.key.args} in:')
				for run in foo.runs:
					print(f'    {run}')
				print()
			return InvalidRun()
		
		## Save and return key
		key = run_keys.popitem()[1].key
		key_path = op.join(entry.path, 'key.json')
		if op.exists(key_path):
			if util.read_file(key_path) != key.content:
				print(f'Cache key "{key_path}" already exists, but is different. Saved as "{key_path}.new".')
				util.write_file(key.content, f'{key_path}.new')
			return NotARun()
		
		util.write_file(key.content, key_path)
		
		return ValidRun(key)


def main():
	key_cache = KeyCache()
	run_keys = {}
	found_bugs = False
	for entry in os.scandir('results'):
		result = key_cache.key_from_cache_entry(entry)
		
		## Skip non run entries
		if isinstance(result, NotARun):
			continue
		
		## Note/Skip buggy entries
		if isinstance(result, InvalidRun):
			found_bugs = True
			continue
		
		## Save valid entries
		key = result.key
		
		new = op.join('results', key.hash)
		if new != entry.path and op.exists(new):
			print(f'Can\'t move {entry.path} to {new}, as the later already exists')
			found_bugs = True
			continue
		
		other = run_keys.get(new)
		if other is not None:
			print(f'{entry.path} has the same cache key as {other}')
			found_bugs = True
			continue
		
		run_keys[new] = entry.path
	
	if found_bugs:
		sys.exit('Stopping because of bugs')
	
	for new, old in run_keys.items():
		if new == old:
			continue
		
		print(f'Move {old} -> {new}')
		os.rename(old, new)


if __name__ == '__main__':
	main()
