CSG Fitter
==========

[This project](https://gitlab.com/Skrupellos/csg-fitter) is a playground with the aim to experiment with methods to reconstruct [CSG](https://en.wikipedia.org/wiki/Constructive_solid_geometry) trees from point clouds.


Installation
------------

There are two main ways to get the application:


### Download pre-built executable

Just Download the [latest](https://skrupellos.gitlab.io/csg-fitter/csgfitter-static) or [any other](https://gitlab.com/Skrupellos/csg-fitter/pipelines) pre-built x86_64 executable.
As it is a static one, no D standard library is required.


### From source

As this is an application written in [D](https://dlang.org/) you the DMD and LDC [D compilers](https://wiki.dlang.org/Compilers) and [DUB](https://dub.pm/) in order to build the application.
Afterwards you can get the [source](https://gitlab.com/Skrupellos/csg-fitter) from GitLab.
```
git clone https://gitlab.com/Skrupellos/csg-fitter.git
```

Since the build process is complicated, the `bin/build` helper script can be used to build, run an test the application:

* Build and **run** optimized binary with some arguments
```
bin/build -r -- some arguments
```

* Build **optimized binary**
```
bin/build
```

* Build optimized **statically linked** binary
```
bin/build static
```

* Build **non-optimized** binary (quicker)
```
bin/build -q
```

* Print **help** to see all options
```
bin/build -h
```

---

It can be called from any directory (no need to be at the project root).
To make it even more simple, a `.envrc` exists, which can be used by [direnv](https://direnv.net/) to add all helper scripts to `$PATH`.
Now you can simply call `build` from anywhere within the project.

More (or up to date) info on how to build can be found in [.gitlab-ci.yml](.gitlab-ci.yml) and [dub.sdl](dub.sdl) along with the [DUB documentation](https://dub.pm/commandline.html).
A way to setup the tool chain can be found in the [Dockerfile](Dockerfile).


### Unit tests

There are two types of unit tests runner available for this project.

1) The [*Trial*](https://gitlab.com/szabobogdan3/trial) unit test runner can create coverage and other reports.
They can be run with:
```
bin/build tests
```
The generated test reports are stored locally in `public/` or [online](https://skrupellos.gitlab.io/csg-fitter/index.html).

2) The [*standard D*](https://dlang.org/spec/unittest.html) unit tests are the most basic ones but fast and with low memory usage.
They can be run with:
```
bin/build -q tests
```


Usage
-----

Some [usage examples](https://gitlab.com/Skrupellos/csg-fitter/wikis/pipeline) can be found in the Wiki.

A full list of options can be obtained by reading the source or with
```
./csgfitter --help
```


Docker image
------------

The provided [Dockerfile](Dockerfile) is used to create the automated build and test environment for this project.
Build images can be found in the [GitLab container registry](https://gitlab.com/Skrupellos/csg-fitter/container_registry).

The image provides the *DMD* D compiler (Version 2.086.0) and *LDC* D compiler (Version 1.16.0).
The image is based on Debian Buster (Version 10).
