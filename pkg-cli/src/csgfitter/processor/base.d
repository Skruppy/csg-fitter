// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module csgfitter.processor.base;

import generic.util;
import std.conv : to;
import std.experimental.logger : tracef;
import std.json : JSONValue;
import painlessjson : toJSON;



public struct NoParameters {}

public struct InputRep {}
public struct OutputRep {}



enum ProcessorState {
	UNDEFINED, // Initial state
	LINKED,    // All necessary links are established with the correct contracts.
	READY,     // All required data is available and the job can be run.
	DONE,      // Job has run
}


class Rep {
	abstract void print();
}


abstract class Processor {
	public const string name;
	private ProcessorState _state = ProcessorState.UNDEFINED;
	
	this(string name) {
		this.name = name;
	}
	
	abstract string[] outputs(this T)() {
		return [typeid(T).stringof];
	}
	
	public final void configure() {
		state = _configure() ? ProcessorState.READY : ProcessorState.LINKED;
	}
	
	public final void compute() {
		if(_state != ProcessorState.READY) throw new Exception("Can't compute. "~name~" is not READY but "~_state.to!string);
		_compute();
		state = ProcessorState.DONE;
	}
	
	protected abstract bool _configure();
	protected abstract void _compute();
	
	public final ProcessorState state() { return _state; }
	protected final void state(ProcessorState state) {
		if(_state == state) return;
		tracef("Processor %s transitioned state: %s -> %s", name, _state, state);
		_state = state;
	}
	
	public JSONValue stats() {
		return JSONValue(null);
	}
	
	public abstract JSONValue json_properties();
}



abstract class ProcessorBase(T) if(is(T == struct)) : Processor {
	public const T properties;
	
	this(string name, T properties = T()) {
		super(name);
		this.properties = properties;
	}
	
	public override JSONValue json_properties() {
		return properties.toJSON;
	}
}



AttrInfo!InputRep[string] inputs(T)() {
	return attributes!(T, InputRep);
}



AttrInfo!OutputRep[string] outputs(T)() {
	return attributes!(T, OutputRep);
}


enum bool isProcessor(T) =
	is(T : ProcessorBase!U, U) && 
	is(typeof(T.typeName) == immutable shared string) && 
	is(typeof(T.typeDescription) == immutable shared string);
