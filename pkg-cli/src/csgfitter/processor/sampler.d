// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module csgfitter.processor.sampler;

import csgfitter.processor.base;
import csgfitter.rep.csg;
import csgfitter.rep.cloud;
import std.math : isNaN;
import libcsgfitter.sampler.surface;
import std.math : isFinite;
import libcsgfitter.sampler.volume;
import std.meta : AliasSeq;



alias Samplers = AliasSeq!(SurfaceSampler, VolumeSampler);

/**
* Surface sampler
* @{
*/
class SurfaceSampler : ProcessorBase!SurfaceProperties {
	public static immutable shared string typeName = "surface_sampler";
	public static immutable shared string typeDescription = "Create point cloud w/ normals from surface of a CSG model.";
	
	@InputRep
	public CsgRep csg;
	
	@OutputRep
	public NormalPointCloudRep npc;
	
	
	this(string name, SurfaceProperties parameters=SurfaceProperties()) {
		super(name, parameters);
		npc = new NormalPointCloudRep();
	}
	
	
	override protected bool _configure() {
		// Check all inputs are linked and sane
		if(csg is null) throw new Exception(name~": Required input csg is not set");
		if(isNaN(properties.rate)) throw new Exception(name~": Property sample_rate is not set");
		
		// Check all data is available
		return csg.root !is null;
	}
	
	override protected void _compute() {
		npc = new NormalPointCloudRep(csg.root.surface_sample(properties.rate));
	}
}
/** @} */


/**
* Volume sampler
* @{
*/
class VolumeSampler : ProcessorBase!SampleProperties {
	public static immutable shared string typeName = "volume_sampler";
	public static immutable shared string typeDescription = "Create point cloud w/o normals from volume of a CSG model.";
	
	@InputRep
	public CsgRep csg;
	
	@OutputRep
	public PointCloudRep pc;
	
	
	this(string name, SampleProperties parameters=SampleProperties()) {
		super(name, parameters);
		pc = new PointCloudRep();
	}
	
	
	override protected bool _configure() {
		// Check all inputs are linked and sane
		if(csg is null) throw new Exception(name~": Required input csg is not set");
		if(!properties.min_x.isFinite) throw new Exception(name~": Property min_x is not set");
		if(!properties.min_y.isFinite) throw new Exception(name~": Property min_y is not set");
		if(!properties.min_z.isFinite) throw new Exception(name~": Property min_z is not set");
		if(!properties.max_x.isFinite) throw new Exception(name~": Property max_x is not set");
		if(!properties.max_y.isFinite) throw new Exception(name~": Property max_y is not set");
		if(!properties.max_z.isFinite) throw new Exception(name~": Property max_z is not set");
		if(!properties.sample_rate.isFinite) throw new Exception(name~": Property sample_rate is not set");
		
		// Check all data is available
		return csg.root !is null;
	}
	
	
	override protected void _compute() {
		pc = new PointCloudRep(csg.root.sample_volume(properties));
	}
}
/** @} */
