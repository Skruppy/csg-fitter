// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module csgfitter.processor.fitter;

import csgfitter.processor.base;
import csgfitter.rep.cloud;
import csgfitter.rep.csg;
import csgfitter.rep.eval;
import csgfitter.rep.primitives;
import libcsgfitter.fitter.evo;
import libcsgfitter.fitter.clique_evo;
import libcsgfitter.fitter.part_evo;
import std.meta : AliasSeq;
import painlessjson : toJSON;
import std.json : JSONValue;



alias Fitters = AliasSeq!(EvoFitter, CliqueEvoFitter, PartEvoFitter);

/**
* Simple evolutionary algorithm
* @{
*/
class EvoFitter : ProcessorBase!EvoParameters {
	public static immutable shared string typeName = "evo";
	public static immutable shared string typeDescription = "Reconstruct CSG using evolutionary algorithm.";
	
	@InputRep
	public NormalPointCloudRep reference;
	
	@InputRep
	public PrimitivesRep primitives;
	
	@InputRep
	public EvaluatorRep evaluator;
	
	@OutputRep
	public CsgRep csg;
	
	private EvoStats _stats;
	
	this(string name, EvoParameters parameters=EvoParameters()) {
		super(name, parameters);
		csg = new CsgRep();
	}
	
	override protected bool _configure() {
		// Check all inputs are linked and sane
		if(reference is null) throw new Exception(name~": Required reference point cloud is not set");
		if(primitives is null) throw new Exception(name~": Required input primitives is not set");
		if(evaluator is null) throw new Exception(name~": Required input evaluator is not set");
		
		// Check all data is available
		return reference.set && primitives.set && evaluator.e !is null;
	}
	
	override protected void _compute() {
		csg = new CsgRep(evo(reference.points, primitives.primitives, evaluator.e, properties, _stats));
	}
	
	override public JSONValue stats() {
		return _stats.toJSON;
	}
}
/** @} */


/**
* Clustered evolutionary algorithm
* @{
*/
class CliqueEvoFitter : ProcessorBase!EvoParameters {
	public static immutable shared string typeName = "clique_evo";
	public static immutable shared string typeDescription = "Reconstruct CSG using evolutionary algorithm and cliques based clustering.";
	
	@InputRep
	public NormalPointCloudRep reference;
	
	@InputRep
	public PrimitivesRep primitives;
	
	@InputRep
	public EvaluatorRep evaluator;
	
	@OutputRep
	public CsgRep csg;
	
	private CliqueStats _stats;
	
	this(string name, EvoParameters parameters=EvoParameters()) {
		super(name, parameters);
		csg = new CsgRep();
	}
	
	override protected bool _configure() {
		// Check all inputs are linked and sane
		if(reference is null) throw new Exception(name~": Required reference point cloud is not set");
		if(primitives is null) throw new Exception(name~": Required input primitives is not set");
		if(evaluator is null) throw new Exception(name~": Required input evaluator is not set");
		
		// Check all data is available
		return reference.set && primitives.set && evaluator.e !is null;
	}
	
	override protected void _compute() {
		csg = new CsgRep(clique_evo(reference.points, primitives.primitives, evaluator.e, properties, _stats));
	}
	
	override public JSONValue stats() {
		return _stats.toJSON;
	}
}
/** @} */


/**
* Clustered evolutionary algorithm
* @{
*/
class PartEvoFitter : ProcessorBase!EvoParameters {
	public static immutable shared string typeName = "part_evo";
	public static immutable shared string typeDescription = "Reconstruct CSG using evolutionary algorithm and pruning based clustering.";
	
	@InputRep
	public NormalPointCloudRep reference;
	
	@InputRep
	public PrimitivesRep primitives;
	
	@InputRep
	public EvaluatorRep evaluator;
	
	@OutputRep
	public CsgRep csg;
	
	private PartStats _stats;
	
	this(string name, EvoParameters parameters=EvoParameters()) {
		super(name, parameters);
		csg = new CsgRep();
	}
	
	override protected bool _configure() {
		// Check all inputs are linked and sane
		if(reference is null) throw new Exception(name~": Required reference point cloud is not set");
		if(primitives is null) throw new Exception(name~": Required input primitives is not set");
		if(evaluator is null) throw new Exception(name~": Required input evaluator is not set");
		
		// Check all data is available
		return reference.set && primitives.set && evaluator.e !is null;
	}
	
	override protected void _compute() {
		csg = new CsgRep(part_evo(reference.points, primitives.primitives, evaluator.e, properties, _stats));
	}
	
	override public JSONValue stats() {
		return _stats.toJSON;
	}
}
/** @} */
