// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module csgfitter.processor.tools;

import libcsgfitter.tools.bincsg : binarize;
import csgfitter.processor.base;
import csgfitter.rep.csg;
import libcsgfitter.tools.convert;
import csgfitter.rep.primitives;
import csgfitter.rep.cloud;
import csgfitter.rep.eval;
import libcsgfitter.tools.primitives;
import libcsgfitter.tools.reduce;
import libcsgfitter.tools.simplify;
import libcsgfitter.tools.score;
import std.meta : AliasSeq;
import painlessjson : toJSON;
import std.json : JSONValue;



alias Tools = AliasSeq!(BinCsgProcessor, ConvertProcessor, PrimitivesExtractorProcessor, ReduceProcessor, ScoreProcessor, SimplifyProcessor);

/**
* Binarize CSG tree
* @{
*/
class BinCsgProcessor : ProcessorBase!NoParameters {
	public static immutable shared string typeName = "bincsg";
	public static immutable shared string typeDescription = "Convert any CSG tree into binary CSG tree.";
	
	@InputRep
	public CsgRep csg;
	
	@OutputRep
	public CsgRep bincsg;
	
	this(string name, NoParameters parameters=NoParameters()) {
		super(name, parameters);
		bincsg = new CsgRep(null);
		bincsg.isBinary = true;
	}
	
	override protected bool _configure() {
		// Check all inputs are linked and sane
		if(csg is null) throw new Exception(name~": Required input csg is not set");
		
		// Check all data is available
		return csg.root !is null;
	}
	
	override protected void _compute() {
		this.bincsg = new CsgRep(binarize(csg.root));
		this.bincsg.isBinary = true;
	}
}
/** @} */


/**
* Convert CSG tree files
* @{
*/
public class ConvertProcessor : ProcessorBase!ConvertProperties {
	public static immutable shared string typeName = "convert";
	public static immutable shared string typeDescription = "Convert file containing CSG tree from one format into another.";
	
	this(string name, ConvertProperties parameters=ConvertProperties()) {
		super(name, parameters);
	}
	
	override protected bool _configure() {
		return true;
	}
	
	override protected void _compute() {
		convert_csg(properties);
	}
}
/** @} */


/**
* Extract primitives
* @{
*/
class PrimitivesExtractorProcessor : ProcessorBase!NoParameters {
	public static immutable shared string typeName = "extract_primitives";
	public static immutable shared string typeDescription = "Extract primitives from CSG tree.";
	
	@InputRep
	public CsgRep csg;
	
	@OutputRep
	public PrimitivesRep primitives;
	
	this(string name, NoParameters parameters=NoParameters()) {
		super(name, parameters);
		primitives = new PrimitivesRep();
	}
	
	override protected bool _configure() {
		// Check all inputs are linked and sane
		if(csg is null) throw new Exception(name~": Required input csg is not set");
		
		// Check all data is available
		return csg.root !is null;
	}
	
	override protected void _compute() {
		primitives = new PrimitivesRep(csg.root.extract_primitives);
	}
}
/** @} */


/**
* Reduce
* @{
*/
class ReduceProcessor : ProcessorBase!NoParameters {
	public static immutable shared string typeName = "reduce";
	public static immutable shared string typeDescription = "Reduce point cloud.";
	
	@InputRep
	public NormalPointCloudRep complete_npc;
	
	@InputRep
	public PrimitivesRep primitives;
	
	@OutputRep
	public NormalPointCloudRep reduced_npc;
	
	private ReduceStats[] _stats;
	
	this(string name, NoParameters parameters=NoParameters()) {
		super(name, parameters);
		reduced_npc = new NormalPointCloudRep();
	}
	
	override protected bool _configure() {
		// Check all inputs are linked and sane
		if(complete_npc is null) throw new Exception(name~": Required input complete_npc is not set");
		if(primitives is null) throw new Exception(name~": Required input primitives is not set");
		
		// Check all data is available
		return complete_npc.set && primitives.set;
	}
	
	override protected void _compute() {
		this.reduced_npc = new NormalPointCloudRep(reduce(complete_npc.points, primitives.primitives, _stats));
	}
	
	override public JSONValue stats() {
		return _stats.toJSON;
	}
}
/** @} */


/**
* Score
* @{
*/
public struct ScoreProperties {
	public string path;
}


class ScoreProcessor : ProcessorBase!ScoreProperties {
	public static immutable shared string typeName = "score";
	public static immutable shared string typeDescription = "Scores model against reference point cloud and write result into JSON file.";
	
	@InputRep
	public NormalPointCloudRep reference;
	
	@InputRep
	public CsgRep model;
	
	@InputRep
	public EvaluatorRep evaluator;
	
	private float _stats;
	
	this(string name, ScoreProperties parameters=ScoreProperties()) {
		super(name, parameters);
	}
	
	override protected bool _configure() {
		// Check all inputs are linked and sane
		if(reference is null) throw new Exception(name~": Required input reference point cloud is not set");
		if(model is null) throw new Exception(name~": Required input model is not set");
		if(evaluator is null) throw new Exception(name~": Required input evaluator is not set");
		
		// Check all data is available
		return reference.set && model.root !is null && evaluator.e !is null;
	}
	
	override protected void _compute() {
		_stats = score(reference.points, model.root, evaluator.e, properties.path);
	}
	
	override public JSONValue stats() {
		return _stats.toJSON;
	}
}
/** @} */


/**
* Simplify
* @{
*/
class SimplifyProcessor : ProcessorBase!NoParameters {
	public static immutable shared string typeName = "simplify";
	public static immutable shared string typeDescription = "Simplify CSG tree.";
	
	@InputRep
	public CsgRep csg;
	
	@OutputRep
	public CsgRep simple_csg;
	
	this(string name, NoParameters parameters=NoParameters()) {
		super(name, parameters);
		simple_csg = new CsgRep(null);
		simple_csg.isBinary = true;
	}
	
	override protected bool _configure() {
		// Check all inputs are linked and sane
		if(csg is null) throw new Exception(name~": Required input csg is not set");
		
		// Check all data is available
		return csg.root !is null;
	}
	
	override protected void _compute() {
		this.simple_csg = new CsgRep(simplify(csg.root));
	}
}
/** @} */
