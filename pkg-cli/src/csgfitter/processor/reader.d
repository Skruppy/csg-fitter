// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module csgfitter.processor.reader;

import libcsgfitter.reader.hcsg;
import csgfitter.processor.base;
import libcsgfitter.reader.json;
import csgfitter.rep.csg;
import std.meta : AliasSeq;



alias Readers = AliasSeq!(HcsgReader, JsonReader);

/**
* HCSG
* @{
*/
public struct HcsgParameters {
	public string path;
	public string hcsg;
}

public final class HcsgReader : ProcessorBase!HcsgParameters {
	public static immutable shared string typeName = "hcsg_reader";
	public static immutable shared string typeDescription = "Read \"human readable\" CSG tree from file or string.";
	
	@OutputRep
	public CsgRep csg;
	
	
	public this(string name, HcsgParameters parameters=HcsgParameters()) {
		super(name, parameters);
		csg = new CsgRep(null);
		csg.isBinary = true;
	}
	
	
	public override bool _configure() {
		return true;
	}
	
	
	public override void _compute() {
		auto csg = properties.hcsg == ""
			? read_hcsg_file(properties.path)
			: read_hcsg_str(properties.hcsg);
		
		this.csg = new CsgRep(csg);
		this.csg.isBinary = true;
	}
}
/** @} */


/**
* JSON
* @{
*/
public struct JsonParameters {
	public string path;
	public string json;
}

public class JsonReader : ProcessorBase!JsonParameters {
	public static immutable shared string typeName = "json_reader";
	public static immutable shared string typeDescription = "Read CSG tree from JSON file or string.";
	
	@OutputRep
	public CsgRep csg;
	
	this(string name, JsonParameters parameters=JsonParameters()) {
		super(name, parameters);
		csg = new CsgRep(null);
	}
	
	override protected bool _configure() {
		return true;
	}
	
	override protected void _compute() {
		csg = new CsgRep(
			properties.json == ""
			? read_json_file(properties.path)
			: read_json_str(properties.json)
		);
	}
}
/** @} */
