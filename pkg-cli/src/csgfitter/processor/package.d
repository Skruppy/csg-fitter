// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module csgfitter.processor;

public import csgfitter.processor.evaluator;
public import csgfitter.processor.fitter;
public import csgfitter.processor.reader;
public import csgfitter.processor.sampler;
public import csgfitter.processor.tools;
public import csgfitter.processor.writer;

import std.meta : AliasSeq;

alias Processors = AliasSeq!(Evaluators, Fitters, Readers, Samplers, Tools, Writers);
