// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module csgfitter.processor.evaluator;

import libcsgfitter.evaluator.e10;
import libcsgfitter.evaluator.e11;
import libcsgfitter.evaluator.e12;
import csgfitter.processor.base;
import csgfitter.rep.cloud;
import csgfitter.rep.eval;
import std.meta : AliasSeq;



alias Evaluators = AliasSeq!(Evaluator10Processor, Evaluator11Processor, Evaluator12Processor);

class EvaluatorProcess(P, C, alias NAME, alias DESCRIPTION) : ProcessorBase!P {
	public static immutable shared string typeName = NAME;
	public static immutable shared string typeDescription = DESCRIPTION;
	
	@OutputRep
	public EvaluatorRep evaluator;
	
	this(string name, P parameters=P()) {
		super(name, parameters);
		evaluator = new EvaluatorRep();
	}
	
	override bool _configure() {
		return true;
	}
	
	override void _compute() {
		evaluator = new EvaluatorRep(new C(&properties));
	}
}


alias Evaluator10Processor = EvaluatorProcess!(
	Evaluator10Parameters, Evaluator10,
	"evaluator_10", "Generate objective function \"e10\" (min dir diff in kNN)"
);

alias Evaluator11Processor = EvaluatorProcess!(
	Evaluator11Parameters, Evaluator11,
	"evaluator_11", "Generate objective function \"e11\" (min dir diff in NN)."
);

alias Evaluator12Processor = EvaluatorProcess!(
	Evaluator12Parameters, Evaluator12,
	"evaluator_12", "Generate objective function \"e12\" (derived points)"
);
