// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module csgfitter.processor.writer;

import libcsgfitter.writer.gv;
import libcsgfitter.writer.hcsg;
import libcsgfitter.writer.json;
import libcsgfitter.writer.pcd;
import libcsgfitter.writer.scad;
import libcsgfitter.writer.vtk;
import libcsgfitter.writer.xyz;
import csgfitter.processor.base;
import csgfitter.rep.csg;
import csgfitter.rep.cloud;
import std.meta : AliasSeq;



alias Writers = AliasSeq!(GvWriter, HcsgWriter, JsonWriter, PCDWriter, ScadWriter, VTKWriter, XYZWriter);

/**
* Graphviz CSG trees
* @{
*/
class GvWriter : ProcessorBase!GvProperties {
	public static immutable shared string typeName = "gv_writer";
	public static immutable shared string typeDescription = "Write CSG tree using GraphViz. Available formats: gv*, png, pdf, svg, jpeg";
	
	@InputRep
	public CsgRep csg;
	
	this(string name, GvProperties parameters=GvProperties()) {
		super(name, parameters);
	}
	
	override protected bool _configure() {
		// Check all inputs are linked and sane
		if(csg is null) throw new Exception(name~": Required input csg is not set");
		
		// Check all data is available
		return csg.root !is null;
	}
	
	override protected void _compute() {
		csg.root.write_gv(properties);
	}
}
/** @} */


/**
* HCSG trees
* @{
*/
public class HcsgWriter : ProcessorBase!HcsgProperties {
	public static immutable shared string typeName = "hcsg_writer";
	public static immutable shared string typeDescription = "Write CSG tree in \"human readable\" CSG format.";
	
	@InputRep
	public CsgRep csg;
	
	this(string name, HcsgProperties parameters=HcsgProperties()) {
		super(name, parameters);
	}
	
	override protected bool _configure() {
		// Check all inputs are linked and sane
		if(csg is null) throw new Exception(name~": Required input csg is not set");
		if( ! csg.isBinary) throw new Exception(name~": A binary CSG tree is required");
		
		// Check all data is available
		return csg.root !is null;
	}
	
	override protected void _compute() {
		csg.root.write_hcsg(properties);
	}
}
/** @} */


/**
* JSON CSG trees
* @{
*/
public class JsonWriter : ProcessorBase!JsonProperties {
	public static immutable shared string typeName = "json_writer";
	public static immutable shared string typeDescription = "Write CSG tree in JSON format.";
	
	@InputRep
	public CsgRep csg;
	
	this(string name, JsonProperties parameters=JsonProperties()) {
		super(name, parameters);
	}
	
	override protected bool _configure() {
		// Check all inputs are linked and sane
		if(csg is null) throw new Exception(name~": Required input csg is not set");
		
		// Check all data is available
		return csg.root !is null;
	}
	
	override protected void _compute() {
		csg.root.write_json(properties);
	}
}
/** @} */


/**
* PCD point clouds
* @{
*/
public struct PCDProperties {
	public string path;
}


class PCDWriter : ProcessorBase!PCDProperties {
	public static immutable shared string typeName = "pcd_writer";
	public static immutable shared string typeDescription = "Write point cloud in Point Cloud Library format.";
	
	@InputRep
	public PointCloudRep pc;
	
	@InputRep
	public NormalPointCloudRep npc;
	
	public this(string name, PCDProperties parameters=PCDProperties()) {
		super(name, parameters);
	}
	
	override protected bool _configure() {
		// Check all inputs are linked and sane
		if(pc is null && npc is null) throw new Exception(name~": Required input pc or npc is not set");
		if(pc !is null && npc !is null) throw new Exception(name~": Only one input can be set");
		
		// Check all data is available
		return pc !is null && pc.set || npc !is null && npc.set;
	}
	
	override protected void _compute() {
		if(pc !is null) { pc.points.write_pcd(properties.path); }
		else {            npc.points.write_pcd(properties.path); }
	}
}
/** @} */


/**
* OpenSCAD CSG trees
* @{
*/
class ScadWriter : ProcessorBase!ScadProperties {
	public static immutable shared string typeName = "scad_writer";
	public static immutable shared string typeDescription = "Write CSG tree in OpenSCAD format.";
	
	@InputRep
	public CsgRep csg;
	
	this(string name, ScadProperties parameters=ScadProperties()) {
		super(name, parameters);
	}
	
	override protected bool _configure() {
		// Check all inputs are linked and sane
		if(csg is null) throw new Exception(name~": Required input csg is not set");
		
		// Check all data is available
		return csg.root !is null;
	}
	
	override protected void _compute() {
		csg.root.write_scad(properties);
	}
}
/** @} */


/**
* VTK point clouds
* @{
*/
public struct VTKProperties {
	public string path;
}


class VTKWriter : ProcessorBase!VTKProperties {
	public static immutable shared string typeName = "vtk_writer";
	public static immutable shared string typeDescription = "Write point cloud in Visualization Toolkit format.";
	
	@InputRep
	public PointCloudRep pc;
	
	@InputRep
	public NormalPointCloudRep npc;
	
	public this(string name, VTKProperties parameters=VTKProperties()) {
		super(name, parameters);
	}
	
	override protected bool _configure() {
		// Check all inputs are linked and sane
		if(pc is null && npc is null) throw new Exception(name~": Required input pc or npc is not set");
		if(pc !is null && npc !is null) throw new Exception(name~": Only one input can be set");
		
		// Check all data is available
		return pc !is null && pc.set || npc !is null && npc.set;
	}
	
	override protected void _compute() {
		if(pc !is null) { pc.points.write_vtk(properties.path); }
		else {            npc.points.write_vtk(properties.path); }
	}
}
/** @} */


/**
* XYZ point clouds
* @{
*/
public class XYZWriter : ProcessorBase!XYZProperties {
	public static immutable shared string typeName = "xyz_writer";
	public static immutable shared string typeDescription = "Write point cloud in CSV/XYZ format.";
	
	@InputRep
	public PointCloudRep pc;
	
	@InputRep
	public NormalPointCloudRep npc;
	
	public this(string name, XYZProperties parameters=XYZProperties()) {
		super(name, parameters);
	}
	
	override protected bool _configure() {
		// Check all inputs are linked and sane
		if(pc is null && npc is null) throw new Exception(name~": Required input pc or npc is not set");
		if(pc !is null && npc !is null) throw new Exception(name~": Only one input can be set");
		
		// Check all data is available
		return pc !is null && pc.set || npc !is null && npc.set;
	}
	
	override protected void _compute() {
		if(pc !is null) { pc.points.write_xyz(properties); }
		else {            npc.points.write_xyz(properties); }
	}
}
/** @} */
