// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module csgfitter.cli;

import generic.util;
import csgfitter.plr;
import csgfitter.processor : Processors;
import csgfitter.runtime;
import core.cpuid : processor;
import std.algorithm.sorting : sort;
import std.ascii : newline;
import std.experimental.logger : LogLevel;
import std.experimental.logger : globalLogLevel;
import std.experimental.logger : infof;
import std.format : format;
import std.getopt : GetOptException;
import std.getopt : defaultGetoptPrinter;
import std.getopt : getopt;
import std.range : repeat;
import std.stdio : stderr;
import std.stdio : write;
import std.stdio : writeln;
import std.string : strip;
import std.string : wrap;
import painlessjson : toJSON;
import painlessjson : SerializedName;
import std.stdio : File;
import std.datetime : Clock;
import std.datetime : msecs;
import std.algorithm.iteration : map;
import std.array : array;



struct Stats {
	string start_date;
	@SerializedName("version") string _version;
	string cpu;
	string host;
	string[] args;
	string pipeline_gv;
	ProcessorStats[string] processors;
}

shared struct AppMetaData {
	string appName = "csgfitter";
	string commit;
}
immutable shared AppMetaData appMeta;

shared static this() {
	appMeta.commit = importMaybe!("version.txt", "unknown").strip;
}


void print(IFactory factory) {
	writeln(factory.name);
	writeln('-'.repeat(factory.name.length));
	
	write(factory.description.wrap);
	
	foreach(name; factory.properties.keys.sort) {
		writeln(" P: "~name~" ("~factory.properties[name]~")");
	}
	foreach(ref dsc; factory.inputs.values.sort!"a.name < b.name") {
		writeln(" I: "~dsc.name~" ("~dsc.type.className~")");
	}
	foreach(ref dsc; factory.outputs.values.sort!"a.name < b.name") {
		writeln(" O: "~dsc.name~" ("~dsc.type.className~")");
	}
}


int cli(string[] args) {
	Stats stats;
	bool listProcessors = false;
	string statsPath = "";
	int verbosity = 0;
	bool showVersion = false;
	bool pretend = false;
	try {
		auto getoptResult = getopt(
			args,
			"list|l",     "List all processors", &listProcessors,
			"stats|s",    "Write statistics to file", &statsPath,
			"pretend|p",  "Do no calculations, only simulate a pipe run", &pretend,
			"verbose|v+", "Be more verbose (can be accumulated)", &verbosity,
			"version",    "Output version and exit", &showVersion,
		);
		
		// Option: --help
		if(getoptResult.helpWanted) {
			`
			Usage: %s [OPTION]... PIPELINE
			
			The pipeline syntax and some usage examples can be found online:
			<https://gitlab.com/Skruppy/csg-fitter/wikis/pipeline>
			`.mls.format(appMeta.appName).writeln;
			
			defaultGetoptPrinter("Available options:", getoptResult.options);
			
			`
			
			Source at: <https://gitlab.com/Skruppy/csg-fitter>
			Copyright (c) Skruppy <skruppy@onmars.eu>
			Licensed under the Apache License, Version 2.0 (the "License")
			`.mls.write;
			
			return 0;
		}
	}
	// Invalid option
	catch(GetOptException ex) {
		stderr.writeln(appMeta.appName~": "~ex.message);
		return 1;
	}
	
	// Option: --version
	if(showVersion) {
		writeln(appMeta.commit);
		return 0;
	}
	
	// Option: --verbose
	if(verbosity >= 2)      globalLogLevel = LogLevel.trace;
	else if(verbosity >= 1) globalLogLevel = LogLevel.info;
	else                    globalLogLevel = LogLevel.warning;
	
	// Main programm
	infof("Running %s version %s", appMeta.appName, appMeta.commit);
	
	auto rt = new Runtime();
	
	foreach(alias Processor; Processors) {
		rt.addFactory!Processor;
	}
	
	if(listProcessors) {
		int needsSeperator = false;
		foreach(factory; rt.factories.values.sort!"a.name < b.name") {
			if(needsSeperator) write(newline);
			else needsSeperator = true;
			
			factory.print;
		}
		
		return 0;
	}
	
	// Init everything
	rt.configure(args[1..$]);
	stats.collect_stats(args, rt);
	
	// Run everything
	rt.run(stats.processors, pretend);
	
	// Save stats
	if(statsPath != "") {
		scope auto file = File(statsPath, "w");
		file.writeln(stats.toJSON.toPrettyString);
		file.close();
	}
	
	return 0;
}


void collect_stats(ref Stats stats, string[] args, Runtime rt) {
	// Version
	stats._version = appMeta.commit;
	
	// CPU
	stats.cpu = processor;
	
	// FQDN hostname
	stats.host = hostname();
	
	// Command line arguments
	stats.args = args[1..$];
	
	// Start time
	auto now = Clock.currTime();
	now.fracSecs = msecs(0);
	stats.start_date = now.toISOExtString;
	
	// Pipeline graph
	stats.pipeline_gv = `
	digraph pipeline {
		rankdir=LR;
		labelloc=t;
		nodesep=.1;
		ranksep=1;
		node [style="filled,rounded", shape=box, fontsize="9", fontname="sans", margin="0.0,0.0"];
	`.mls;
	foreach(name; rt.processors.keys.sort) {
		auto proc = rt.processors[name];
		stats.pipeline_gv ~= "\n\n\tsubgraph cluster_%s {\n".format(name);
		stats.pipeline_gv ~= `
		fontname="Bitstream Vera Sans";
		fontsize="8";
		style="filled,rounded";
		color=black;
		fillcolor="#aaffaa";
		label = <<TABLE BORDER="0" CELLSPACING="1" CELLPADDING="0">
		`.mls;
		
		// Name
		stats.pipeline_gv ~= "\t\t<TR><TD COLSPAN=\"2\"><FONT POINT-SIZE=\"12\">%s</FONT></TD></TR>\n".format(name);
		
		// Type
		stats.pipeline_gv ~= "\t\t<TR><TD COLSPAN=\"2\"><FONT POINT-SIZE=\"10\">(%s)</FONT></TD></TR>\n".format(proc.factory.name);
		
		// Properties
		foreach(prop_key; proc.properties.keys.sort) {
			stats.pipeline_gv ~= "\t\t<TR><TD ALIGN=\"RIGHT\">%s: </TD><TD ALIGN=\"LEFT\">%s</TD></TR>\n".format(prop_key, proc.properties[prop_key]);
		}
		
		stats.pipeline_gv ~= "\t\t</TABLE>>;\n";
		
		// Input ports
		string in_node_name = "";
		stats.pipeline_gv ~= "\n\t\tsubgraph cluster_%s_in {\n".format(name);
		stats.pipeline_gv ~= `
		label="";
		style="invis";
		`.mls;
		
		foreach(port_name; proc.factory.inputs.keys.sort) {
			in_node_name = "%s_in_%s".format(name, port_name);
			stats.pipeline_gv ~= "\t\t\t%s [color=black, fillcolor=\"#aaaaff\", label=\"%s\", height=\"0.2\", style=\"filled,solid\"];\n".format(in_node_name, port_name);
		}
		if(proc.factory.inputs.length == 0) {
			in_node_name = "%s_in_dummy".format(name);
			stats.pipeline_gv ~= "\t\t\t%s [width=0, shape=\"point\", style=\"invis\"];\n".format(in_node_name);
		}
		
		stats.pipeline_gv ~= "\t\t}\n";
		
		// Output ports
		string out_node_name = "";
		stats.pipeline_gv ~= "\n\t\tsubgraph cluster_%s_out {\n".format(name);
		stats.pipeline_gv ~= `
		label="";
		style="invis";
		`.mls;
		
		foreach(port_name; proc.factory.outputs.keys.sort) {
			out_node_name = "%s_out_%s".format(name, port_name);
			stats.pipeline_gv ~= "\t\t\t%s [color=black, fillcolor=\"#ffaaaa\", label=\"%s\", height=\"0.2\", style=\"filled,solid\"];\n".format(out_node_name, port_name);
		}
		if(proc.factory.outputs.length == 0) {
			out_node_name = "%s_out_dummy".format(name);
			stats.pipeline_gv ~= "\t\t\t%s [width=0,  shape=\"point\", style=\"invis\"];\n".format(out_node_name);
		}
		
		stats.pipeline_gv ~= "\t\t}\n";
		
		// Helper: Order input and output subgraphs
		stats.pipeline_gv ~= "\t\t\t%s -> %s [style=\"invis\"];\n".format(in_node_name, out_node_name);
		
		stats.pipeline_gv ~= "\t}\n";
		
		// Link ports
		foreach(port_name; proc.egressLinks.keys.sort) {
			auto links = proc.egressLinks[port_name].sort!"a.dstPort < b.dstPort";
			foreach(link; links) {
				stats.pipeline_gv ~= "\t%s_out_%s -> %s_in_%s;\n".format(name, port_name, link.dstProcess.name, link.dstPort);
			}
		}
	}
	stats.pipeline_gv ~= "}\n";
	
	// Processor configuration (stats)
	foreach(name, proc; rt.processors) {
		string[string] inputs;
		foreach(port_name, _; proc.factory.inputs) {
			if(auto link = port_name in proc.ingressLinks) {
				inputs[port_name] = "%s.%s".format(link.srcProcess.name, link.srcPort);
			}
			else {
				inputs[port_name] = null;
			}
		}
		
		string[][string] outputs;
		foreach(port_name, _; proc.factory.outputs) {
			if(auto links = port_name in proc.egressLinks) {
				outputs[port_name] = (*links).map!(x => "%s.%s".format(x.dstProcess.name, x.dstPort)).array;
			}
			else {
				outputs[port_name] = null;
			}
		}
		
		stats.processors[name] = ProcessorStats(
			name,
			proc.factory.name,
			-1,
			proc.properties.toJSON,
			inputs,
			outputs,
			null.toJSON,
		);
	}
}



version(unittest) {
private:
	import fluent.asserts;
	import generic.testfiles;
	
	shared static this() {
		globalLogLevel = LogLevel.warning;
	}
}

@("Usage examples")
unittest {
	cli(["csgfitter", "-v"]).stfu.should.equal(0);
	cli(["csgfitter", "-v", "-v"]).stfu.should.equal(0);
	cli(["csgfitter", "--verbose"]).stfu.should.equal(0);
	cli(["csgfitter", "-l"]).stfu.should.equal(0);
	cli(["csgfitter", "--list"]).stfu.should.equal(0);
	cli(["csgfitter", "-V"]).stfu.should.equal(0);
	cli(["csgfitter", "--version"]).stfu.should.equal(0);
	cli(["csgfitter", "json_reader", "path=tests/renderings/coordinate_system.json", "!", "scad_writer", "path="~test_path("csgfitter-1.scad")]).stfu.should.equal(0);
	cli(["csgfitter", "json_reader", "path=tests/renderings/coordinate_system.json", "!", "bincsg", "!", "surface_sampler", "rate=500", "!", ".npc", "vtk_writer", "path="~test_path("csgfitter-2.vtk")]).stfu.should.equal(0);
	cli(["csgfitter", "json_reader", "path=tests/renderings/coordinate_system.json", "!", "scad_writer", "path="~test_path("csgfitter-3.scad")]).stfu.should.equal(0);
	
	// Disable debug output from further testes
	cli(["csgfitter"]);
}
