// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module csgfitter.plr.lexer;

import generic.parser.symbols;
import std.array : join;
import std.array : split;
import std.conv : to;
import std.format : format;
import std.range : isInputRange;
import std.range.primitives : empty;
import std.range.primitives : front;
import std.range.primitives : popFront;
import generic.re;
import std.traits : Unqual;



abstract class Token : Terminal {
	string position;
}



class LinkToken : Token {
	bool verbose;
	this(bool verbose) { this.verbose = verbose; }
	override string toString() const { return "LinkToken( %s )".format(verbose?"!!":"!"); }
	
	
	override bool opEquals(Object o) {
		if(typeid(this) != typeid(o)) return false;
		alias a = this;
		auto b = cast(typeof(this)) o;
		
		return a.verbose == b.verbose;
	}
}



class PropertyToken : Token {
	string key;
	string value;
	this(string key, string value) { this.key = key; this.value = value; }
	override string toString() const { return "PropertyToken(%s=%s)".format(key, value); }
	
	
	override bool opEquals(Object o) {
		if(typeid(this) != typeid(o)) return false;
		alias a = this;
		auto b = cast(typeof(this)) o;
		
		return
			a.key == b.key &&
			a.value == b.value;
	}
}



class ElementToken : Token {
	string elementName;
	this(string elementName) { this.elementName = elementName; }
	override string toString() const { return "ElementToken(%s)".format(elementName); }
	
	
	override bool opEquals(Object o) {
		if(typeid(this) != typeid(o)) return false;
		alias a = this;
		auto b = cast(typeof(this)) o;
		
		return a.elementName == b.elementName;
	}
}



class FullRefToken : Token {
	string elementName;
	string[] ports;
	this(string elementName, string[] ports) { this.elementName = elementName; this.ports = ports; }
	override string toString() const { return "FullRefToken(%s.%s)".format(elementName, ports.join(",")); }
	
	
	override bool opEquals(Object o) {
		if(typeid(this) != typeid(o)) return false;
		alias a = this;
		auto b = cast(typeof(this)) o;
		
		return
			a.elementName == b.elementName &&
			a.ports == b.ports;
	}
}



class PortRefToken : Token {
	string[] ports;
	this(string[] ports) { this.ports = ports; }
	override string toString() const { return "PortRefToken(.%s)".format(ports.join(",")); }
	
	
	override bool opEquals(Object o) {
		if(typeid(this) != typeid(o)) return false;
		alias a = this;
		auto b = cast(typeof(this)) o;
		
		return a.ports == b.ports;
	}
}


class LexerException : Exception {
	private this(string msg) { super(msg); }
}


private struct TokenStream(Range) {
	alias R = Unqual!Range;
	R input;
	size_t pos;
	
	this(R input) {
		this.input = input;
	}
	
	bool empty() {
		return input.empty;
	}
	
	Token front() {
		assert( ! input.empty, "Attempting to fetch the front of an empty token stream.");
		auto token = tokenizeSingle(input.front);
		if(token is null) {
			throw new LexerException("The "~(pos+1).to!string~"th token \""~input.front~"\" is not valid.");
		}
		return token;
	}
	
	void popFront() {
		assert( ! input.empty, "Attempting to popFront an empty token stream.");
		input.popFront();
		pos++;
	}
}

// This is needed to work around extremly high memory usage in compile time
// regex parsing. Even when using regex() instead of ctRegex(), CTFE kicks in
// and eats all the good old memories.
private const Regex propertyRegEx;
private const Regex portRegEx;
private const Regex fullFerRegEx;
private const Regex elementRegEx;
private static this() {
	propertyRegEx = regex(`^(\w+)=(.*)$`);
	portRegEx     = regex(`^\.(\w+(,\w+)*)$`);
	fullFerRegEx  = regex(`^(\w+)\.(\w+(,\w+)*){0,1}$`);
	elementRegEx  = regex(`^\w+$`);
}

private Token tokenizeSingle(string s) {
	Captures c;
	if(s == "!")  return new LinkToken(false);
	if(s == "!!") return new LinkToken(true);
	
	c = s.matchFirst(propertyRegEx);
	if( ! c.empty) return new PropertyToken(c[1], c[2]);
	
	c = s.matchFirst(portRegEx);
	if( ! c.empty) return new PortRefToken(c[1].split(","));
	
	c = s.matchFirst(fullFerRegEx);
	if( ! c.empty) return new FullRefToken(c[1], c[2].split(","));
	
	c = s.matchFirst(elementRegEx);
	if( ! c.empty) return new ElementToken(s);
	
	return null;
}


auto tokenize(Range)(Range input) if(isInputRange!(Unqual!Range)) {
	return TokenStream!(Range)(input);
}



version(unittest) {
private:
	import core.exception : AssertError;
	import fluent.asserts;
	import std.array : array;
}


@("Token equality")
unittest {
	new LinkToken(true).should.equal(new LinkToken(true));
	new LinkToken(false).should.equal(new LinkToken(false));
	new LinkToken(true).should.not.equal(  new LinkToken(false));
	new LinkToken(false).should.not.equal( new LinkToken(true));
	
	new PropertyToken("aaa", "bbb").should.equal(new PropertyToken("aaa", "bbb"));
	new PropertyToken("aaa", "bbb").should.not.equal( new PropertyToken("aaa", "XXX"));
	new PropertyToken("aaa", "bbb").should.not.equal( new PropertyToken("XXX", "bbb"));
	
	new ElementToken("aaa").should.equal(new ElementToken("aaa"));
	new ElementToken("aaa").should.not.equal( new ElementToken("XXX"));
	
	new FullRefToken("aaa", []).should.equal(              new FullRefToken("aaa", []));
	new FullRefToken("aaa", ["x", "y", "z"]).should.equal( new FullRefToken("aaa", ["x", "y", "z"]));
	new FullRefToken("aaa", ["x", "y", "z"]).should.not.equal( new FullRefToken("XXX", ["x", "y", "z"]));
	new FullRefToken("aaa", ["x", "y", "z"]).should.not.equal( new FullRefToken("aaa", ["x", "?", "z"]));
	
	new PortRefToken([]).should.equal(              new PortRefToken([]));
	new PortRefToken(["x", "y", "z"]).should.equal( new PortRefToken(["x", "y", "z"]));
	new PortRefToken(["x", "y", "z"]).should.not.equal( new PortRefToken(["x", "?", "z"]));
}


@("Tokenizer (singel tokens)")
unittest {
	tokenizeSingle("foo= bar = ! !! . baz ").should.equal(new PropertyToken("foo", " bar = ! !! . baz "));
	tokenizeSingle("foo =bar").should.equal(null);
	tokenizeSingle("foo.bar=baz").should.equal(null);
	tokenizeSingle("foo baz").should.equal(null);
	
	tokenizeSingle("foo.").should.equal( new FullRefToken("foo", []));
	tokenizeSingle("foo.bar").should.equal( new FullRefToken("foo", ["bar"]));
	tokenizeSingle("foo.bar,baz").should.equal( new FullRefToken("foo", ["bar", "baz"]));
	tokenizeSingle(".").should.equal(null);
	tokenizeSingle(".,bar").should.equal(null);
	tokenizeSingle("foo.,bar").should.equal(null);
	tokenizeSingle(".bar,,baz").should.equal(null);
	tokenizeSingle("foo.bar,,baz").should.equal(null);
	tokenizeSingle(".bar,").should.equal(null);
	tokenizeSingle("foo.bar,").should.equal(null);
	tokenizeSingle(".bar").should.equal( new PortRefToken(["bar"]));
	tokenizeSingle(".bar,baz").should.equal( new PortRefToken(["bar", "baz"]));
	
	tokenizeSingle("foo_bar").should.equal( new ElementToken("foo_bar"));
	
	tokenizeSingle("!").should.equal( new LinkToken(false));
	tokenizeSingle("!!").should.equal( new LinkToken(true));
}


@("Basic working range")
unittest {
	auto tokenStream = ["block", "name=foo"].tokenize;
	tokenStream.should.equal([new ElementToken("block"), new PropertyToken("name", "foo")]);
}


@("Range functions")
unittest {
	auto tokenStream = ["block", "name=foo"].tokenize;
	tokenStream.front.should.equal(new ElementToken("block"));
	tokenStream.front.should.equal(new ElementToken("block"));
	tokenStream.empty.should.equal(false);
	tokenStream.popFront.should.not.throwAnyException;
	
	tokenStream.front.should.equal(new PropertyToken("name", "foo"));
	tokenStream.front.should.equal(new PropertyToken("name", "foo"));
	tokenStream.empty.should.equal(false);
	tokenStream.popFront.should.not.throwAnyException;
	
	tokenStream.front.should.throwException!AssertError;
	tokenStream.empty.should.equal(true);
	tokenStream.popFront.should.throwException!AssertError;
}


@("Lexer exception / invalid token")
unittest {
	({["block", "."].tokenize.array;}).should.throwAnyException.withMessage.equal("The 2th token \".\" is not valid.");
}
