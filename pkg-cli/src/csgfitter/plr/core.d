// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module csgfitter.plr.core;

import generic.parser.slr1;
import generic.parser.slr1.helper;
import csgfitter.plr.lexer;
import csgfitter.runtime;
import std.algorithm.iteration : each;



/*
https://mdaines.github.io/grammophone/#/slr1-table
GRAPH -> .
GRAPH -> STATEMENT_LIST .

STATEMENT_LIST -> STATEMENT .
STATEMENT_LIST -> STATEMENT_LIST STATEMENT .

STATEMENT -> CLOSED_CHAIN .
STATEMENT -> ELEMENT .

CLOSED_CHAIN -> CHAIN_START link CHAIN_END .
CLOSED_CHAIN -> OPEN_CHAIN link CHAIN_END .
CLOSED_CHAIN -> OPEN_CHAIN port_ref link CHAIN_END .

OPEN_CHAIN -> CHAIN_START link          ELEMENT .
OPEN_CHAIN -> CHAIN_START link port_ref ELEMENT .
OPEN_CHAIN -> OPEN_CHAIN link          ELEMENT .
OPEN_CHAIN -> OPEN_CHAIN link port_ref ELEMENT .

CHAIN_END -> ELEMENT .
CHAIN_END -> full_ref .
CHAIN_END -> port_ref ELEMENT .

CHAIN_START -> ELEMENT .
CHAIN_START -> full_ref .
CHAIN_START -> ELEMENT port_ref .

ELEMENT -> element .
ELEMENT -> ELEMENT propertie .
*/



//#### "TREE"
private class Context {
	ElementDeclaration[] elementDeclarations;
	Link[] links;
	
	void register(ElementDeclaration x) {
		elementDeclarations ~= x;
	}
	
	void register(Link x) {
		links ~= x;
	}
	
	void instantiate(Runtime rt) {
		elementDeclarations.each!(x => x.instantiate(rt));
		links.each!(x => x.instantiate(rt));
	}
}


private abstract class Element {
	string name;
}


private class ElementDeclaration : Element {
	string type;
	string[string] properties;
	
	this(string type, Context ctx) {
		this.type = type;
		ctx.register(this);
	}
	
	void instantiate(Runtime rt) {
		name = rt.create(type, properties);
	}
}


private class ElementProxy : Element {
	this(string name) {
		this.name = name;
	}
}


struct LinkSite {
	Element element;
	string[] ports;
}


private class Link {
	LinkSite lhs;
	LinkSite rhs;
	bool verbose;
	
	this(LinkSite lhs, LinkSite rhs, bool verbose, Context ctx) {
		this.lhs = lhs;
		this.rhs = rhs;
		this.verbose = verbose;
		ctx.register(this);
	}
	
	void instantiate(Runtime rt) {
		rt.link(lhs.element.name, rhs.element.name, lhs.ports, rhs.ports, verbose);
	}
}



//#### SYMBOLS
private class GraphNT : NonTerminal {
	@slr1production("GRAPH → ε")                                         this(Context ctx)                                                            {}
	@slr1production("GRAPH → STATEMENT_LIST")                            this(Context ctx, StatementListNT s)                                         {}
}

private class StatementListNT : NonTerminal {
	@slr1production("STATEMENT_LIST → STATEMENT")                        this(Context ctx,                    StatementNT s)                          {}
	@slr1production("STATEMENT_LIST → STATEMENT_LIST STATEMENT")         this(Context ctx, StatementListNT l, StatementNT s)                          {}
}

private class StatementNT : NonTerminal {
	@slr1production("STATEMENT → CLOSED_CHAIN")                          this(Context ctx, ClosedChainNT x)                                           {}
	@slr1production("STATEMENT → ELEMENT")                               this(Context ctx, ElementNT x)                                               {}
}

private class ClosedChainNT : NonTerminal {
	Link data;
	@slr1production("CLOSED_CHAIN → CHAIN_START link CHAIN_END")         this(Context ctx, ChainStartNT s,                LinkToken l, ChainEndNT e)  { data = new Link(s.data,                                e.data, l.verbose, ctx); }
	@slr1production("CLOSED_CHAIN → OPEN_CHAIN link CHAIN_END")          this(Context ctx, OpenChainNT c,                 LinkToken l, ChainEndNT e)  { data = new Link(c.data.rhs,                            e.data, l.verbose, ctx); }
	@slr1production("CLOSED_CHAIN → OPEN_CHAIN port_ref link CHAIN_END") this(Context ctx, OpenChainNT c, PortRefToken r, LinkToken l, ChainEndNT e)  { data = new Link(LinkSite(c.data.rhs.element, r.ports), e.data, l.verbose, ctx); }
}

private class OpenChainNT : NonTerminal {
	Link data;
	@slr1production("OPEN_CHAIN → CHAIN_START link ELEMENT")             this(Context ctx, ChainStartNT x, LinkToken l,                 ElementNT e)  { data = new Link(x.data,     LinkSite(e.data, []),      l.verbose, ctx); }
	@slr1production("OPEN_CHAIN → CHAIN_START link port_ref ELEMENT")    this(Context ctx, ChainStartNT x, LinkToken l, PortRefToken r, ElementNT e)  { data = new Link(x.data,     LinkSite(e.data, r.ports), l.verbose, ctx); }
	@slr1production("OPEN_CHAIN → OPEN_CHAIN link ELEMENT")              this(Context ctx, OpenChainNT x,  LinkToken l,                 ElementNT e)  { data = new Link(x.data.rhs, LinkSite(e.data, []),      l.verbose, ctx); }
	@slr1production("OPEN_CHAIN → OPEN_CHAIN link port_ref ELEMENT")     this(Context ctx, OpenChainNT x,  LinkToken l, PortRefToken r, ElementNT e)  { data = new Link(x.data.rhs, LinkSite(e.data, r.ports), l.verbose, ctx); }
}

private class ChainEndNT : NonTerminal {
	LinkSite data;
	@slr1production("CHAIN_END → ELEMENT")                               this(Context ctx, ElementNT e)                                               { data = LinkSite(e.data,                          []);      }
	@slr1production("CHAIN_END → full_ref")                              this(Context ctx, FullRefToken r)                                            { data = LinkSite(new ElementProxy(r.elementName), r.ports); }
	@slr1production("CHAIN_END → port_ref ELEMENT")                      this(Context ctx, PortRefToken r, ElementNT e)                               { data = LinkSite(e.data,                          r.ports); }
}

private class ChainStartNT : NonTerminal {
	LinkSite data;
	@slr1production("CHAIN_START → ELEMENT")                             this(Context ctx, ElementNT e)                                               { data = LinkSite(e.data,                          []);      }
	@slr1production("CHAIN_START → full_ref")                            this(Context ctx, FullRefToken r)                                            { data = LinkSite(new ElementProxy(r.elementName), r.ports); }
	@slr1production("CHAIN_START → ELEMENT port_ref")                    this(Context ctx, ElementNT e, PortRefToken r)                               { data = LinkSite(e.data,                          r.ports); }
}

private class ElementNT : NonTerminal {
	ElementDeclaration data;
	@slr1production("ELEMENT → element")                                 this(Context ctx, ElementToken e)                                            { data = new ElementDeclaration(e.elementName, ctx); }
	@slr1production("ELEMENT → ELEMENT propertie")                       this(Context ctx, ElementNT e, PropertyToken p)                              { e.data.properties[p.key] = p.value; data = e.data; }
}


private auto ref constructParser() {
	const int stateCnt = 30; // = maxState + 1
	auto productions = collectSLR1Productions!(Context, GraphNT, StatementListNT, StatementNT, ClosedChainNT, OpenChainNT, ChainEndNT, ChainStartNT, ElementNT);
	
	auto sa = function(SLR1State x)  { return new SLR1ShiftAction!Context(x); };
	auto ra = delegate(string x) { return new SLR1ReduceAction!Context(productions[x]); };
	
	SLR1Action!Context[TypeInfo_Class][stateCnt] actionsTable = [
		  [ /* 0 */
			typeid(FullRefToken):  sa(9),
			typeid(ElementToken):  sa(8),
			typeid(End):           ra("GRAPH → ε"),
		],[ /* 1 */
			typeid(End):           new SLR1AcceptAction!Context(),
		],[ /* 2 */
			typeid(FullRefToken):  sa(9),
			typeid(ElementToken):  sa(8),
			typeid(End):           ra("GRAPH → STATEMENT_LIST"),
		],[ /* 3 */
			typeid(FullRefToken):  ra("STATEMENT_LIST → STATEMENT"),
			typeid(ElementToken):  ra("STATEMENT_LIST → STATEMENT"),
			typeid(End):           ra("STATEMENT_LIST → STATEMENT"),
		],[ /* 4 */
			typeid(FullRefToken):  ra("STATEMENT → CLOSED_CHAIN"),
			typeid(ElementToken):  ra("STATEMENT → CLOSED_CHAIN"),
			typeid(End):           ra("STATEMENT → CLOSED_CHAIN"),
		],[ /* 5 */
			typeid(LinkToken):     ra("CHAIN_START → ELEMENT"),
			typeid(PortRefToken):  sa(12),
			typeid(FullRefToken):  ra("STATEMENT → ELEMENT"),
			typeid(ElementToken):  ra("STATEMENT → ELEMENT"),
			typeid(PropertyToken): sa(11),
			typeid(End):           ra("STATEMENT → ELEMENT"),
		],[ /* 6 */
			typeid(LinkToken):     sa(13),
		],[ /* 7 */
			typeid(LinkToken):     sa(14),
			typeid(PortRefToken):  sa(15),
		],[ /* 8 */
			typeid(LinkToken):     ra("ELEMENT → element"),
			typeid(PortRefToken):  ra("ELEMENT → element"),
			typeid(FullRefToken):  ra("ELEMENT → element"),
			typeid(ElementToken):  ra("ELEMENT → element"),
			typeid(PropertyToken): ra("ELEMENT → element"),
			typeid(End):           ra("ELEMENT → element"),
		],[ /* 9 */
			typeid(LinkToken):     ra("CHAIN_START → full_ref"),
		],[ /* 10 */
			typeid(FullRefToken):  ra("STATEMENT_LIST → STATEMENT_LIST STATEMENT"),
			typeid(ElementToken):  ra("STATEMENT_LIST → STATEMENT_LIST STATEMENT"),
			typeid(End):           ra("STATEMENT_LIST → STATEMENT_LIST STATEMENT"),
		],[ /* 11 */
			typeid(LinkToken):     ra("ELEMENT → ELEMENT propertie"),
			typeid(PortRefToken):  ra("ELEMENT → ELEMENT propertie"),
			typeid(FullRefToken):  ra("ELEMENT → ELEMENT propertie"),
			typeid(ElementToken):  ra("ELEMENT → ELEMENT propertie"),
			typeid(PropertyToken): ra("ELEMENT → ELEMENT propertie"),
			typeid(End):           ra("ELEMENT → ELEMENT propertie"),
		],[ /* 12 */
			typeid(LinkToken):     ra("CHAIN_START → ELEMENT port_ref"),
		],[ /* 13 */
			typeid(PortRefToken):  sa(18),
			typeid(FullRefToken):  sa(19),
			typeid(ElementToken):  sa(8),
		],[ /* 14 */
			typeid(PortRefToken):  sa(22),
			typeid(FullRefToken):  sa(19),
			typeid(ElementToken):  sa(8),
		],[ /* 15 */
			typeid(LinkToken):     sa(23),
		],[ /* 16 */
			typeid(FullRefToken):  ra("CLOSED_CHAIN → CHAIN_START link CHAIN_END"),
			typeid(ElementToken):  ra("CLOSED_CHAIN → CHAIN_START link CHAIN_END"),
			typeid(End):           ra("CLOSED_CHAIN → CHAIN_START link CHAIN_END"),
		],[ /* 17 */
			typeid(LinkToken):     ra("OPEN_CHAIN → CHAIN_START link ELEMENT"),
			typeid(PortRefToken):  ra("OPEN_CHAIN → CHAIN_START link ELEMENT"),
			typeid(FullRefToken):  ra("CHAIN_END → ELEMENT"),
			typeid(ElementToken):  ra("CHAIN_END → ELEMENT"),
			typeid(PropertyToken): sa(11),
			typeid(End):           ra("CHAIN_END → ELEMENT"),
		],[ /* 18 */
			typeid(ElementToken):  sa(8),
		],[ /* 19 */
			typeid(FullRefToken):  ra("CHAIN_END → full_ref"),
			typeid(ElementToken):  ra("CHAIN_END → full_ref"),
			typeid(End):           ra("CHAIN_END → full_ref"),
		],[ /* 20 */
			typeid(FullRefToken):  ra("CLOSED_CHAIN → OPEN_CHAIN link CHAIN_END"),
			typeid(ElementToken):  ra("CLOSED_CHAIN → OPEN_CHAIN link CHAIN_END"),
			typeid(End):           ra("CLOSED_CHAIN → OPEN_CHAIN link CHAIN_END"),
		],[ /* 21 */
			typeid(LinkToken):     ra("OPEN_CHAIN → OPEN_CHAIN link ELEMENT"),
			typeid(PortRefToken):  ra("OPEN_CHAIN → OPEN_CHAIN link ELEMENT"),
			typeid(FullRefToken):  ra("CHAIN_END → ELEMENT"),
			typeid(ElementToken):  ra("CHAIN_END → ELEMENT"),
			typeid(PropertyToken): sa(11),
			typeid(End):           ra("CHAIN_END → ELEMENT"),
		],[ /* 22 */
			typeid(ElementToken):  sa(8),
		],[ /* 23 */
			typeid(PortRefToken):  sa(28),
			typeid(FullRefToken):  sa(19),
			typeid(ElementToken):  sa(8),
		],[ /* 24 */
			typeid(LinkToken):     ra("OPEN_CHAIN → CHAIN_START link port_ref ELEMENT"),
			typeid(PortRefToken):  ra("OPEN_CHAIN → CHAIN_START link port_ref ELEMENT"),
			typeid(FullRefToken):  ra("CHAIN_END → port_ref ELEMENT"),
			typeid(ElementToken):  ra("CHAIN_END → port_ref ELEMENT"),
			typeid(PropertyToken): sa(11),
			typeid(End):           ra("CHAIN_END → port_ref ELEMENT"),
		],[ /* 25 */
			typeid(LinkToken):     ra("OPEN_CHAIN → CHAIN_START link port_ref ELEMENT"),
			typeid(PortRefToken):  ra("OPEN_CHAIN → CHAIN_START link port_ref ELEMENT"),
			typeid(FullRefToken):  ra("CHAIN_END → port_ref ELEMENT"),
			typeid(ElementToken):  ra("CHAIN_END → port_ref ELEMENT"),
			typeid(PropertyToken): sa(11),
			typeid(End):           ra("CHAIN_END → port_ref ELEMENT"),
		],[ /* 26 */
			typeid(FullRefToken):  ra("CLOSED_CHAIN → OPEN_CHAIN port_ref link CHAIN_END"),
			typeid(ElementToken):  ra("CLOSED_CHAIN → OPEN_CHAIN port_ref link CHAIN_END"),
			typeid(End):           ra("CLOSED_CHAIN → OPEN_CHAIN port_ref link CHAIN_END"),
		],[ /* 27 */
			typeid(FullRefToken):  ra("CHAIN_END → ELEMENT"),
			typeid(ElementToken):  ra("CHAIN_END → ELEMENT"),
			typeid(PropertyToken): sa(11),
			typeid(End):           ra("CHAIN_END → ELEMENT"),
		],[ /* 28 */
			typeid(ElementToken):  sa(8),
		],[ /* 29 */
			typeid(FullRefToken):  ra("CHAIN_END → port_ref ELEMENT"),
			typeid(ElementToken):  ra("CHAIN_END → port_ref ELEMENT"),
			typeid(PropertyToken): sa(11),
			typeid(End):           ra("CHAIN_END → port_ref ELEMENT"),
		]
	];
	
	SLR1State[TypeInfo_Class][stateCnt] gotoTable = [
		0: [
			typeid(GraphNT):         1,
			typeid(StatementListNT): 2,
			typeid(StatementNT):     3,
			typeid(ClosedChainNT):   4,
			typeid(ElementNT):       5,
			typeid(ChainStartNT):    6,
			typeid(OpenChainNT):     7,
		],
		2: [
			typeid(StatementNT):    10,
			typeid(ClosedChainNT):   4,
			typeid(ElementNT):       5,
			typeid(ChainStartNT):    6,
			typeid(OpenChainNT):     7,
		],
		13: [
			typeid(ElementNT):      17,
			typeid(ChainEndNT):     16,
		],
		14: [
			typeid(ElementNT):      21,
			typeid(ChainEndNT):     20,
		],
		18: [
			typeid(ElementNT):      24,
		],
		22: [
			typeid(ElementNT):      25,
		],
		23: [
			typeid(ElementNT):      27,
			typeid(ChainEndNT):     26,
		],
		28: [
			typeid(ElementNT):      29,
		],
	];
	
	return SLR1Parser!Context(actionsTable, gotoTable);
}


void configure(Runtime rt, string[] args) {
	auto parser = constructParser();
	auto ctx = new Context();
	
	parser.parse(args.tokenize, ctx);
	
	ctx.instantiate(rt);
}


version(unittest) {
private:
	import fluent.asserts;
}

@("\"Tree\"")
unittest {
	auto parser = constructParser();
	
	auto ctx = new Context();
	parser.parse(["block1", "foo=bar", "name=x", "foo=baz", ".p1,p2", "!", "block2", "!!", "foo.", "block3"].tokenize, ctx);
	
	// Check declarations
	ElementDeclaration[string] decls;
	foreach(decl; ctx.elementDeclarations) {
		decls[decl.type] = decl;
		decl.name = decl.type;
	}
	
	decls.keys.should.containOnly(["block1", "block2", "block3"]);//TODO
	decls["block1"].properties.should.equal(["name": "x", "foo": "baz"]);
	decls["block2"].properties.length.should.equal(0);
	decls["block3"].properties.length.should.equal(0);
	
	// Check links
	ctx.links.length.should.equal(2);
	ctx.links[0].verbose.should.not.equal(ctx.links[1].verbose);
	Link verboseLink, nonVerboseLink;
	if(ctx.links[0].verbose) {
		verboseLink = ctx.links[0]; nonVerboseLink = ctx.links[1];
	}
	else {
		verboseLink = ctx.links[1]; nonVerboseLink = ctx.links[0];
	}
	
	nonVerboseLink.lhs.element.name.should.equal("block1");
	nonVerboseLink.lhs.ports.should.equal(["p1", "p2"]);
	nonVerboseLink.rhs.element.name.should.equal("block2");
	nonVerboseLink.rhs.ports.should.equal([]);
	
	verboseLink.lhs.element.name.should.equal("block2");
	verboseLink.lhs.ports.should.equal([]);
	verboseLink.rhs.element.name.should.equal("foo");
	verboseLink.rhs.ports.should.equal([]);
}

@("Parser")
unittest {
	auto parser = constructParser();
	
	foreach(a; ["blockA", "refA.", "refA.foo,bar", ".foo,bar"])
	foreach(b; ["blockB", "refB.", "refB.foo,bar", ".foo,bar"]) {
		parser.parse(["block1", "!", a, "block2", b, "!", "block3"].tokenize, new Context()).should.not.throwAnyException;
	}
	
	// Missing reference element
	parser.parse(["block1", "!", "foo.bar",    ".bar", "!", "block3"].tokenize, new Context()).should.throwException!ParserException;
	parser.parse(["block1", "!", "foo.",       ".bar", "!", "block3"].tokenize, new Context()).should.throwException!ParserException;
	parser.parse(["block1", "!",               ".bar", "!", "block3"].tokenize, new Context()).should.throwException!ParserException;
	parser.parse(["block1", "!",    ".bar",    ".bar", "!", "block3"].tokenize, new Context()).should.throwException!ParserException;
	parser.parse(["block1", "!",    ".bar", "foo.bar", "!", "block3"].tokenize, new Context()).should.throwException!ParserException;
	parser.parse(["block1", "!",    ".bar", "foo.",    "!", "block3"].tokenize, new Context()).should.throwException!ParserException;
	parser.parse(["block1", "!",    ".bar",            "!", "block3"].tokenize, new Context()).should.throwException!ParserException;
	
	// One site is missing completely
	parser.parse(["block1", "!", "foo.bar",            "!", "block3"].tokenize, new Context()).should.throwException!ParserException;
	parser.parse(["block1", "!", "foo.",               "!", "block3"].tokenize, new Context()).should.throwException!ParserException;
	parser.parse(["block1", "!",            "foo.bar", "!", "block3"].tokenize, new Context()).should.throwException!ParserException;
	parser.parse(["block1", "!",            "foo.",    "!", "block3"].tokenize, new Context()).should.throwException!ParserException;
	
	// Misc
	parser.parse(["ref.", "foo=bar"].tokenize, new Context()).should.throwException!ParserException;
	parser.parse(["block1", "!"].tokenize, new Context()).should.throwException!ParserException;
	parser.parse(tokenize(cast(string[])null), new Context()).should.not.throwAnyException;
}
