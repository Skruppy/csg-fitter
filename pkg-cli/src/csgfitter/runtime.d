// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module csgfitter.runtime;

import generic.util;
import csgfitter.processor.base;
import std.algorithm.searching : canFind;
import std.container.dlist : DList;
import std.conv : to;
import std.datetime.stopwatch : AutoStart;
import std.datetime.stopwatch : StopWatch;
import std.experimental.logger : info;
import std.experimental.logger : trace;
import std.experimental.logger : tracef;
import std.experimental.typecons : Final;
import std.range : join;
import std.range : zip;
import std.traits : FieldNameTuple;
import std.traits : Unqual;
import std.traits : getSymbolsByUDA;
import std.json : JSONValue;
import std.format : format;
import painlessjson : toJSON;



public struct ProcessorStats {
	string name;
	string type;
	long time;
	JSONValue properties;
	string[string] inputs;
	string[][string] outputs;
	JSONValue stats;
}

//#### RUNTIME
public class Runtime {
	private IProcWrapper[string] _processors;
	private IFactory[string] _factories;
	
	
	public void addFactory(T)() if(isProcessor!T) {
		if(T.typeName in _factories) throw new Exception("Factory \""~T.typeName~"\" already registered");
		_factories[T.typeName] = new Factory!T(this);
	}
	
	
	public auto factories() {
		return _factories.dup;
	}
	
	
	public string create(string typeName, string[string] parameters=null) {
		auto factory = typeName in _factories;
		if(factory is null) throw new Exception("No element type \""~typeName~"\"");
		IProcWrapper wrapper = factory.newProcessorWrapper(parameters);
		_processors[wrapper.name] = wrapper;
		return wrapper.name; // Processors stay inside the machine!
	}
	
	
	public bool hasProcessor(string name) {
		return (name in _processors) != null;
	}
	
	
	public IProcWrapper[string] processors() {
		return _processors.dup;
	}
	
	
	public void link(string srcProc, string dstProc, string[] srcPorts=[], string[] dstPorts=[], bool verbose=false) {
		auto src = srcProc in _processors;
		if(src is null) throw new Exception("No element source \""~srcProc~"\" for link");
		auto dst = dstProc in _processors;
		if(dst is null) throw new Exception("No element destination \""~dstProc~"\" for link");
		
		if(srcPorts.length == 0) {
			auto outputs = src.factory.outputs;
			if(outputs.length < 1) throw new Exception("The source element "~srcProc~" ("~src.factory.name~") has no egress ports to link to "~dstProc~" ("~dst.factory.name~"). Can't link from there.");
			if(outputs.length > 1) throw new Exception("The source element "~srcProc~" ("~src.factory.name~") has multiple egress ports to link to "~dstProc~" ("~dst.factory.name~"). Please state ports explicitly.");
			srcPorts = outputs.keys;
		}
		
		if(dstPorts.length == 0) {
			auto inputs = dst.factory.inputs;
			if(inputs.length < 1) throw new Exception("The destination element "~dstProc~" ("~dst.factory.name~") has no ingress ports to link from "~srcProc~" ("~src.factory.name~"). Can't link to there.");
			if(inputs.length > 1) throw new Exception("The destination element "~dstProc~" ("~dst.factory.name~") has multiple ingress ports to link from "~srcProc~" ("~src.factory.name~"). Please state ports explicitly.");
			dstPorts = inputs.keys;
		}
		
		if(srcPorts.length != dstPorts.length) throw new Exception("Can't link from "~srcProc~"."~srcPorts.join(",")~" ("~src.factory.name~") to "~dstProc~"."~dstPorts.join(",")~" ("~dst.factory.name~"). Please provide matching port lists.");
		
		foreach(ref srcPort, ref dstPort; zip(srcPorts, dstPorts)) {
			auto link = new Link(*src, srcPort, *dst, dstPort, verbose);
			src.linkEgress(link);
			dst.linkIngress(link);
		}
	}
	
	
	public void run(ref ProcessorStats[string] stats, bool pretend=false) {
		DList!IProcWrapper readyStack;
		
		// Distribute contracts
		foreach(wrapper; _processors) {
			foreach(port, links; wrapper.egressLinks) {
				auto rep = wrapper.get(port);
				foreach(link; links) {
					tracef("Copy initial value from %s.%s to %s.%s: %s", wrapper.proc.name, port, link.dstProcess.proc.name, link.dstPort, rep);
					link.dstProcess.set(link.dstPort, rep);
				}
			}
		}
		
		// Check contracts
		foreach(wrapper; _processors) {
			wrapper.proc.configure();
			if(wrapper.proc.state == ProcessorState.READY) {
				readyStack.push(wrapper);
			}
			else if(wrapper.proc.state != ProcessorState.LINKED) {
				throw new Exception(wrapper.proc.name~" ("~wrapper.factory.name~") is not configured to run.");
			}
		}
		
		// Run
		while( ! readyStack.empty) {
			IProcWrapper[] modified;
			
			auto wrapper = readyStack.pop;
			trace("Next round with "~wrapper.proc.name);
			
			auto sw = StopWatch(AutoStart.yes);
			if( ! pretend) {
				wrapper.proc.compute();
			}
			sw.stop();
			
			ProcessorStats* p_stats = wrapper.proc.name in stats;
			if( ! p_stats) {
				stats[wrapper.proc.name] = ProcessorStats();
				p_stats = &stats[wrapper.proc.name];
			}
			p_stats.time = sw.peek.total!"usecs";
			p_stats.stats = wrapper.proc.stats;
			info(wrapper.proc.name~" took "~sw.peek.total!"msecs".to!string~" ms");
			
			foreach(port, links; wrapper.egressLinks) {
				auto rep = wrapper.get(port);
				bool canPrint = rep !is null;
				foreach(link; links) {
					tracef("Propagate result from %s.%s to %s.%s: %s", wrapper.proc.name, port, link.dstProcess.proc.name, link.dstPort, rep);
					link.dstProcess.set(link.dstPort, rep);
					
					// Collect affected processors who can transition from LINKED to READY (as a set)
					if(
						link.dstProcess.proc.state == ProcessorState.LINKED &&
						modified.canFind(link.dstProcess) == false
					) {
						modified ~= link.dstProcess;
					}
					
					// Print representation, but only once
					if(canPrint && link.verbose) {
						rep.print();
						canPrint = false;
					}
				}
			}
			
			foreach(affected; modified) {
				affected.proc.configure();
				if(affected.proc.state == ProcessorState.READY) {
					readyStack.push(affected);
				}
			}
		}
	}
}



//#### FACTORY
public interface IFactory {
	public string name();
	public string description();
	public string[string] properties();
	public AttrInfo!InputRep[string] inputs();
	public AttrInfo!OutputRep[string] outputs();
	protected IProcWrapper newProcessorWrapper(string[string] parametersStrings);
}



private final class Factory(T) : IFactory {
	private Runtime rt;
	private int unnamedPostfix = 1;
	
	
	protected this(Runtime rt) {
		this.rt = rt;
	}
	
	
	public string name() {
		return T.typeName;
	}
	
	public string description() {
		return T.typeDescription;
	}
	
	
	public string[string] properties() {
		string[string] ret;
		
		foreach(id; FieldNameTuple!(typeof(T.properties))) {
			ret[id] = typeof(__traits(getMember, T.properties, id)).stringof;
		}
		
		return ret;
	}
	
	
	public AttrInfo!InputRep[string] inputs() {
		return csgfitter.processor.base.inputs!T;
	}
	
	public AttrInfo!OutputRep[string] outputs() {
		return csgfitter.processor.base.outputs!T;
	}
	
	
	protected IProcWrapper newProcessorWrapper(string[string] sProperties) {
		// Find a name
		string name;
		if("name" in sProperties) {
			name = sProperties["name"];
			sProperties.remove("name");
		}
		else {
			do {
				name = T.typeName ~ (unnamedPostfix++).to!string;
			} while(rt.hasProcessor(name));
		}
		
		// Parse properties
		Unqual!(typeof(T.properties)) properties;
		properties.fromStrings(sProperties);
		
		// Instantiate element
		return new ProcWrapper!(T)(new T(name, properties), this);
	}
}



//#### PROCESS
public interface IProcWrapper {
	public string name();
	public IFactory factory();
	public Link[string] ingressLinks();
	public Link[][string] egressLinks();
	public string[string] properties();
	
	protected Processor proc();
	protected Rep get(string port);
	protected void set(string port, Rep rep);
	protected void linkIngress(Link link);
	protected void linkEgress(Link link);
}


private class ProcWrapper(T) : IProcWrapper {
	private Final!T _proc;
	private Final!IFactory _factory;
	private Link[string] _ingressLinks;
	private Link[][string] _egressLinks;
	
	protected string name() { return proc.name; }
	protected IFactory factory() { return _factory; }
	protected Link[string] ingressLinks() { return _ingressLinks.dup; }
	protected Link[][string] egressLinks() { return _egressLinks.dup; }
	
	public string[string] properties() {
		string[string] ret;
		auto copy = _proc.properties;
		
		foreach(id; FieldNameTuple!(typeof(T.properties))) {
			auto value = __traits(getMember, copy, id);
			ret[id] = "%s".format(value);
		}
		
		return ret;
	}
	
	protected this(T proc, IFactory factory) {
		_proc = proc;
		_factory = factory;
	}
	
	protected Processor proc() { return _proc; }
	
	protected Rep get(string port) {
		foreach(alias member; getSymbolsByUDA!(T, OutputRep)) {
			if(member.stringof == port) {
				return __traits(getMember, _proc, member.stringof);
			}
		}
		throw new Exception("There is no port "~name~"."~port~" ("~factory.name~") to read from.");
	}
	
	protected void set(string port, Rep rep) {
		foreach(alias member; getSymbolsByUDA!(T, InputRep)) {
			if(member.stringof == port) {
				if(rep !is null) {
					if(typeid(rep) != typeid(typeof(member))) throw new Exception("Input type "~typeid(rep).to!string~" does not match expected type "~typeid(typeof(member)).to!string~" of "~name~"."~port~" ("~factory.name~")");
					__traits(getMember, _proc, member.stringof) = cast(typeof(member))rep;
				}
				else {
					__traits(getMember, _proc, member.stringof) = null;
				}
				return;
			}
		}
		throw new Exception("There is no port "~name~"."~port~" ("~factory.name~") to set.");
	}
	
	protected void linkIngress(Link link) {
		assert(link.dstProcess is this);
		if(link.dstPort in _ingressLinks) throw new Exception("Can't link from "~link.srcProcess.name~"."~link.srcPort~" ("~link.srcProcess.factory.name~") to "~link.dstProcess.name~"."~link.dstPort~" ("~link.dstProcess.factory.name~"). Destination port has already an ingress link from "~_ingressLinks[link.dstPort].srcProcess.name~"."~_ingressLinks[link.dstPort].srcPort~" ("~_ingressLinks[link.dstPort].srcProcess.factory.name~").");
		_ingressLinks[link.dstPort] = link;
	}
	
	protected void linkEgress(Link link) {
		assert(link.srcProcess is this);
		_egressLinks[link.srcPort] ~= link;
	}
}



//#### LINK
public final class Link {
	private IProcWrapper _srcProcess;
	private IProcWrapper _dstProcess;
	public const string srcPort;
	public const string dstPort;
	public const bool verbose;
	
	protected this(IProcWrapper srcProcess, string srcPort, IProcWrapper dstProcess, string dstPort, bool verbose) {
		_srcProcess = srcProcess;
		_dstProcess = dstProcess;
		this.srcPort = srcPort;
		this.dstPort = dstPort;
		this.verbose = verbose;
	}
	
	public IProcWrapper srcProcess() { return _srcProcess; }
	public IProcWrapper dstProcess() { return _dstProcess; }
}


version(unittest) {
private:
	import fluent.asserts;
	
	class IntRep : Rep {
		int i;
		int printCnt;
		this(int i = 0) { this.i = i; }
		override void print() { printCnt++; }
	}
	
	struct Params {
		int i = -1;
	}
	
	class ProcA : ProcessorBase!Params {
		static immutable shared string typeName = "a";
		static immutable shared string typeDescription = "Proc a";
		@InputRep IntRep inA;
		@OutputRep IntRep outA;
		@OutputRep IntRep outAExtra;
		this(string name, Params parameters) { super(name, parameters); if(parameters.i >= 0) inA = new IntRep(parameters.i); }
		override protected bool _configure() { return inA !is null; }
		override protected void _compute() { outA = new IntRep(inA.i + 1); inA = null; }
	}
	
	class ProcB : ProcessorBase!Params {
		static immutable shared string typeName = "b";
		static immutable shared string typeDescription = "Duplicated description";
		@InputRep IntRep inB;
		@InputRep IntRep inBExtra;
		@OutputRep IntRep outB;
		this(string name, Params parameters) { super(name, parameters); if(parameters.i >= 0) inB = new IntRep(parameters.i); }
		override protected bool _configure() { return inB !is null; }
		override protected void _compute() { outB = new IntRep(inB.i + 1); inB = null; }
	}
	
	class ProcC : ProcessorBase!NoParameters {
		static immutable shared string typeName = "c";
		static immutable shared string typeDescription = "Duplicated description";
		this(string name, NoParameters parameters) { super(name, parameters); }
		override protected bool _configure() { return true; }
		override protected void _compute() { }
	}
	
	class ProcD : ProcessorBase!NoParameters { // Copy cat
		static immutable shared string typeName = "c";
		static immutable shared string typeDescription = "Something something";
		this(string name, NoParameters parameters) { super(name, parameters); }
		override protected bool _configure() { return true; }
		override protected void _compute() { }
	}
	
	Runtime newRt(string[] types) {
		auto rt = new Runtime();
		rt.addFactory!ProcA;
		rt.addFactory!ProcB;
		rt.addFactory!ProcC;
		foreach(type; types) {
			rt.create(type);
		}
		return rt;
	}
}

@("Link")
unittest {
	ProcessorStats[string] stats;
	Runtime rt;
	rt = new Runtime();
	
	// Adding factories
	rt.addFactory!(ProcB); // new one
	rt.addFactory!(ProcC); // another new one (but same description)
	rt.addFactory!(ProcC).should.throwAnyException.withMessage.equal("Factory \"c\" already registered"); // existing one (same class)
	rt.addFactory!(ProcD).should.throwAnyException.withMessage.equal("Factory \"c\" already registered"); // existing one (different class)
	
	rt.factories.keys.should.containOnly(["b", "c"]); // State is not corrupted
	rt.factories["b"].name.should.equal("b");
	rt.factories["c"].name.should.equal("c");
// 	rt.factories["c"].description.should.equal("Duplicated description");
	
	// Test: Adding processors
	rt = newRt([]);
	rt.create("a").should.equal("a1"); // Implicit name
	rt.create("a", ["name": "foo"]).should.equal("foo"); // Explicit name
	rt.create("a").should.equal("a2"); // Correct counting within factory
	rt.create("b").should.equal("b1"); // Correct counting between factory
	rt.create("b").should.equal("b2"); // ... cont.
	rt.create("x").should.throwAnyException.withMessage.equal("No element type \"x\""); // Non existing factory
	
	rt.processors.keys.should.containOnly(["a1", "a2", "foo", "b1", "b2"]); // State is not corrupted
	
	// Test: Adding links (chain) ...
	foreach(p; [["a1", "a2", "a3"], ["a3", "a2", "a1"], ["a2", "a1", "a3"]]) { // ... independen of order of processor creation
		foreach(q; [[p[0], p[1], p[1], p[2]], [p[1], p[2], p[0], p[1]]]) { // ... independen of order of link creation
			rt = new Runtime();
			rt.addFactory!ProcA;
			rt.create("a", p[0] == "a1" ? ["i": "0"] : null);
			rt.create("a", p[0] == "a2" ? ["i": "0"] : null);
			rt.create("a", p[0] == "a3" ? ["i": "0"] : null);
			
			rt.link(q[0], q[1], ["outA"], ["inA"]).should.not.throwAnyException;
			rt.link(q[2], q[3], ["outA"], ["inA"]).should.not.throwAnyException;
			rt.run(stats);
			(cast(ProcWrapper!ProcA)rt._processors[p[0]])._proc.outA.i.should.equal(1);
			(cast(ProcWrapper!ProcA)rt._processors[p[1]])._proc.outA.i.should.equal(2);
			(cast(ProcWrapper!ProcA)rt._processors[p[2]])._proc.outA.i.should.equal(3);
		}
	}
	
	// Test: Adding links (one-to-many) + verbose links (print only once)
	rt = new Runtime();
	rt.addFactory!ProcB;
	rt.create("b", ["i": "0"]);
	rt.create("b");
	rt.create("b");
	rt.link("b1", "b2", ["outB"], ["inB"], true).should.not.throwAnyException; // to "many" ports on ...
	rt.link("b1", "b2", ["outB"], ["inBExtra"], true).should.not.throwAnyException; // ... the same processor
	rt.link("b1", "b3", ["outB"], ["inB"], true).should.not.throwAnyException; // to the same port on different processors
	rt.run(stats);
	(cast(ProcWrapper!ProcB)rt._processors["b1"])._proc.outB.printCnt.should.equal(1);
	(cast(ProcWrapper!ProcB)rt._processors["b1"])._proc.outB.i.should.equal(1);
	(cast(ProcWrapper!ProcB)rt._processors["b2"])._proc.outB.i.should.equal(2);
	(cast(ProcWrapper!ProcB)rt._processors["b2"])._proc.outB.i.should.equal(2);
	(cast(ProcWrapper!ProcB)rt._processors["b3"])._proc.outB.i.should.equal(2);
	
	// many-to-one, method A
	rt = newRt(["a", "a", "b"]);
	rt.link("a1", "b1", ["outA"], ["inB"]);
	rt.link("a2", "b1", ["outAExtra"], ["inB"]).should.throwAnyException.withMessage.equal("Can't link from a2.outAExtra (a) to b1.inB (b). Destination port has already an ingress link from a1.outA (a).");
	rt.link("a2", "b1", ["outAExtra"], ["inBExtra"]).should.not.throwAnyException; // Control
	
	// many-to-one, method B
	newRt(["a", "b"]).link("a1", "b1", ["outA", "outAExtra"], ["inB", "inB"]).should.throwAnyException.withMessage.equal("Can't link from a1.outAExtra (a) to b1.inB (b). Destination port has already an ingress link from a1.outA (a).");
	newRt(["a", "b"]).link("a1", "b1", ["outA", "outAExtra"], ["inB", "inBExtra"]).should.not.throwAnyException; // Control
	
	// Link non existing processors
	newRt(["a"]).link("x1", "a1", ["X"], ["inA"]).should.throwAnyException.withMessage.equal("No element source \"x1\" for link"); // at the source
	newRt(["a"]).link("a1", "x1", ["outA"], ["X"]).should.throwAnyException.withMessage.equal("No element destination \"x1\" for link"); // at the destination
	
	// Default ports: too many
	newRt(["a", "b"]).link("a1", "b1", [], ["inB", "inBExtra"]).should.throwAnyException.withMessage.equal("The source element a1 (a) has multiple egress ports to link to b1 (b). Please state ports explicitly.");
	newRt(["a", "b"]).link("a1", "b1", [], []).should.throwAnyException.withMessage.equal("The source element a1 (a) has multiple egress ports to link to b1 (b). Please state ports explicitly.");
	newRt(["a", "b"]).link("a1", "b1", ["outA", "outAExtra"], []).should.throwAnyException.withMessage.equal("The destination element b1 (b) has multiple ingress ports to link from a1 (a). Please state ports explicitly.");
	
	// Default ports: too few
	newRt(["a", "c"]).link("c1", "a1", [], ["inA"]).should.throwAnyException.withMessage.equal("The source element c1 (c) has no egress ports to link to a1 (a). Can't link from there.");
	newRt(["b", "c"]).link("b1", "c1", ["outB"], []).should.throwAnyException.withMessage.equal("The destination element c1 (c) has no ingress ports to link from b1 (b). Can't link to there.");
	
	// Default ports: different count
	newRt(["a", "a"]).link("a1", "a2", ["outA", "outAExtra"], ["inA"]).should.throwAnyException.withMessage.equal("Can't link from a1.outA,outAExtra (a) to a2.inA (a). Please provide matching port lists.");
	
	// Default ports: control
	newRt(["a", "b"]).link("b1", "a1", [], []).should.not.throwAnyException;
	
	// Invalid ports
	rt = newRt(["a", "b"]); rt.link("a1", "b1", ["outA"], ["X"]);
	rt.run(stats).should.throwAnyException.withMessage.equal("There is no port b1.X (b) to set.");
	
	rt = newRt(["a", "b"]); rt.link("a1", "b1", ["X"], ["inB"]);
	rt.run(stats).should.throwAnyException.withMessage.equal("There is no port a1.X (a) to read from.");
}
