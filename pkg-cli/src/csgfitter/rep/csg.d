// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module csgfitter.rep.csg;

import generic.tree;
import libcsgfitter.csgtree;
import csgfitter.processor.base;



class CsgRep : Rep {
	public CsgNode root;
	public bool isBinary = false;
	
	public this(CsgNode root=null) {
		this.root = root;
	}
	
	override public void print() {
		root.pprinter.print();
	}
}
