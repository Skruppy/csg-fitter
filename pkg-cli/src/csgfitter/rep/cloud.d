// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module csgfitter.rep.cloud;

import libcsgfitter.point;
import csgfitter.processor.base;
import std.format : format;
import std.stdio : writeln;



private class PointCloudBaseRep(T) : Rep {
	public T[] points;
	bool set;
	
	public this() {}
	public this(T[] points) { this.points = points; set = true; }
	
	override public void print() {
		auto size = points.length;
		writeln("Sample size of %s".format(size));
		
		// If the point cloud is bigger than a small test dataset, show only a
		// few points.
		if(size > 150) { size = 10; }
		
		foreach(ref point; points[0..size]) {
			writeln(point);
		}
	}
}


alias PointCloudRep = PointCloudBaseRep!Point;
alias NormalPointCloudRep = PointCloudBaseRep!NormalPoint;
