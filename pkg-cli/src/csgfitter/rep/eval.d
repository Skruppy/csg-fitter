// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module csgfitter.rep.eval;

import libcsgfitter.csgtree;
import libcsgfitter.evaluator.base;
import csgfitter.processor.base;
import std.format : format;
import std.stdio : writeln;



public class EvaluatorRep : Rep {
	public Evaluator e;
	
	public this() {}
	public this(Evaluator e) { this.e = e; }
	
	override public void print() {
		writeln("%s".format(e));
	}
}
