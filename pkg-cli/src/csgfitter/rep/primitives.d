// This file is part of csgfitter <https://gitlab.com/Skrupellos/csg-fitter>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

module csgfitter.rep.primitives;

import libcsgfitter.csgtree;
import csgfitter.processor.base;
import std.stdio : writefln;
import std.stdio : writeln;



public class PrimitivesRep : Rep {
	public Geo[] primitives;
	bool set;
	
	public this() {}
	public this(Geo[] primitives) { this.primitives = primitives; set = true; }
	
	override public void print() {
		writefln("%s primitives:", primitives.length);
		foreach(ref primitive; primitives) {
			writeln("  ", primitive);
		}
	}
}
