* **Project**: [mathml.css](https://github.com/fred-wang/mathml.css)
* **License**: [MPL-2.0](https://www.mozilla.org/en-US/MPL/2.0/)
* **Author**: By fred-wang and others


* **Project**: [Bootstrap](https://getbootstrap.com/docs/4.5/getting-started/download/)
* **License**: [MIT](https://github.com/twbs/bootstrap/blob/main/LICENSE)
* **Author**: [The Bootstrap Authors](https://github.com/twbs/bootstrap/graphs/contributors)


* **Project**: [jQuery](https://jquery.com/download/)
* **License**: [MIT](https://jquery.org/license/)
* **Author**: JS Foundation and other contributors
